
/*  File: arfilterapiskl.c  */

#ifdef _WIN32
#include <windows.h>
#endif /* _WIN32 */

#include <stdlib.h>
#include <string.h>

#include "arfilterapi.h"

/*****************************************************************************/
/*                                                                           */
/*                 Action Request System Filter API Skeleton                 */
/*                                                                           */
/*****************************************************************************/
/* Description: This is a skeleton AR Filter API plug-in.  It is intended to */
/*    be used as a starting point for further plug-in development.           */
/*                                                                           */
/*    All functions, unless otherwise noted, are optional.  If a function is */
/*    not defined, the plug-in service will return an appropriate response.  */
/*                                                                           */
/*    General notes on memory management:                                    */
/*                                                                           */
/*       1) Do not deallocate (free) memory passed into the ARPlugin or      */
/*          AR Filter API functions.  The plug-in service will deallocate    */
/*          memory associated with the input ("IN") arguments.               */
/*                                                                           */
/*       2) Do not deallocate (free) memory returned by the ARPlugin or      */
/*          AR Filter API functions.  The plug-in service will deallocate    */
/*          memory associated with the return ("OUT") arguments using free().*/
/*                                                                           */
/*    There is one exception to the above rules regarding memory management. */
/*    The plug-in has complete control over the allocation and deallocation  */
/*    of the "object" via ARPluginCreateInstance() and                       */
/*    ARPluginDeleteInstance(), respectively.                                */
/*                                                                           */
/*****************************************************************************/

#ifdef _WIN32
                        /* DllMain is only required in Windows environments. */

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  ul_reason_for_call, 
                      LPVOID lpReserved)
{
   return TRUE;
}
#endif /* _WIN32 */


/*****************************************************************************/
/*                                                                           */
/*                             ARPluginIdentify                              */
/*                                                                           */
/*****************************************************************************/
/*   Description:  This call is not optional.  All plug-ins must define this */
/*      function.                                                            */
/*                                                                           */
/*      This function is called by the plug-in service to identify           */
/*      what type of plug-in this implements, what version of the AR Filter  */
/*      API specification it supports, and its name.                         */
/*                                                                           */
/*      Recommended plug-in naming convention:                               */
/*         <Company Name>.<Plug-In Type>.<Plug-In Name>                      */
/*                                                                           */
/*      E.g., If company XYZ has created the "Greatest Plug-In Ever" (GPE)   */
/*      they might name it "XYZ.ARF.GPE".                                    */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginIdentify(
ARPluginIdentification      *id,     /* OUT; information about the plug-in */
ARStatusList                *status  /* OUT; error message(s)*/
)
{
   id->type = AR_PLUGIN_TYPE_FILTER;
   strcpy(id->name, "EXAMPLE.ARF.SKELETON");
   id->version = ARFILTERAPI_PLUGIN_VERSION;
   memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginInitialization                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to initilize  */
/*    the plug-in.  This is where the plug-in should initialize any data     */
/*    that will be global and accessed by all instances of the plug-in that  */
/*    are created via ARPluginCreateInstance().                              */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginInitialization(
int            argc,    /* IN;  number of arguments passed into the service */
char         **argv,    /* IN;  arguments passed into the plug-in service at */
                        /*      the command line. */
ARStatusList  *status   /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginTermination                              */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to terminate  */
/*    the plug-in.  This is where the plug-in should deallocate any          */
/*    resources that have been allocated either globally or by each instance */
/*    created by ARPluginCreateInstance().                                   */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginTermination(
ARStatusList *status        /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));
   
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginCreateInstance                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to create an  */
/*    instance of the plug-in.  Each instance is guaranteed to be involved   */
/*    in one thread of operation at any one time.                            */
/*                                                                           */
/*    The structure returned via the 'object' pointer will be provided in    */
/*    subsequent plug-in calls.  This allows you to attach arbitrary data    */
/*    to an instance that will be inherently thread safe.                    */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginCreateInstance(
void         **object,   /* OUT; plug-in instance */
ARStatusList  *status    /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   if (object != NULL)
   {
      /*
       * Set *object to point to something that you have 
       * allocated and initialized.
       */
      *object = NULL;
   }

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginDeleteInstance                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to delete an  */
/*    instance of the plug-in.  Each instance is guaranteed to be involved   */
/*    in one thread of operation at any one time.                            */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginDeleteInstance(
void         *object,  /* IN;  plug-in instance to delete*/
ARStatusList *status   /* OUT; status of the operation */
)
{
   if (object != NULL)
   {
      /*
       * Anything that you allocated in ARPluginCreateInstance
       * should be deallocated here.
       */
   }

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                               ARFilterApiCall                             */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called whenever a request is made by the AR */
/*    System Server to the filter api.                                       */
/*                                                                           */
/*    The input values are passed as parameters.                             */
/*                                                                           */
/*    A customized filter api plug-in must define this function.  Any        */
/*    global information or resources accessed here must be protected with   */
/*    appropriate mutual exclusion locks to be multi-thread safe.            */
/*                                                                           */
/*    A customized filter api plug-in may return outValues and/or status.    */
/*    An error in the status parameter will be passed to the workflow and    */
/*    stop filter processing.                                                */
/*    A status of type AR_RETURN_ERROR will cause the calling filter to fail.*/
/*    A status of type AR_RETURN_WARNING will be passed to the AR server     */
/*    client.                                                                */
/*                                                                           */
/*    All memory in inValues and all memory returned via outValues and       */
/*    status will be freed by the RPC layer.                                 */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT void ARFilterApiCall(
void         *object,    /* IN;  plug-in instance to delete*/
ARValueList  *inValues,  /* IN;  arguments passed via workflow */
ARValueList  *outValues, /* OUT; return values back to workflow */
ARStatusList *status     /* OUT; status of the operation */
)
{
   return;
}
