/*  File: main.h  */

#define COMMAND_EXIT                       -1
#define COMMAND_LOGIN                       0
#define COMMAND_GET_ENTRY                   1
#define COMMAND_SET_ENTRY                   2
#define COMMAND_CREATE_ENTRY                3
#define COMMAND_DELETE_ENTRY                4
#define COMMAND_GETLIST_ENTRY               5
#define COMMAND_GET_FILTER                  6
#define COMMAND_SET_FILTER                  7
#define COMMAND_CREATE_FILTER               8
#define COMMAND_DELETE_FILTER               9
#define COMMAND_GETLIST_FILTER              10
#define COMMAND_GETLIST_GROUP               11
#define COMMAND_GET_SCHEMA                  12
#define COMMAND_SET_SCHEMA                  13
#define COMMAND_CREATE_SCHEMA               14
#define COMMAND_DELETE_SCHEMA               15
#define COMMAND_GETLIST_SCHEMA              16
#define COMMAND_GETMULT_SCHEMA              17
#define COMMAND_GET_SCH_FIELD               18
#define COMMAND_SET_SCH_FIELD               19
#define COMMAND_CREATE_SCH_FIELD            20
#define COMMAND_DELETE_SCH_FIELD            21
#define COMMAND_GETLIST_SCH_FIELD           22
#define COMMAND_GET_CHAR_MENU               23
#define COMMAND_SET_CHAR_MENU               24
#define COMMAND_CREATE_CHAR_MENU            25
#define COMMAND_DELETE_CHAR_MENU            26
#define COMMAND_GETLIST_CHAR_MENU           27
#define COMMAND_EXPORT                      28
#define COMMAND_IMPORT                      29
#define COMMAND_GET_SERVER_INFO             30
#define COMMAND_VERIFY_USER                 31
#define COMMAND_EXECUTE                     32
#define COMMAND_OPEN_OUT                    33
#define COMMAND_CLOSE_OUT                   34
#define COMMAND_RECORD                      35
#define COMMAND_STOP_RECORD                 36
#define COMMAND_GETLIST_SERVER              37
#define COMMAND_INITIALIZATION              38
#define COMMAND_TERMINATION                 39
#define COMMAND_GET_ACTIVE_LINK             40
#define COMMAND_SET_ACTIVE_LINK             41
#define COMMAND_CREATE_ACTIVE_LINK          42
#define COMMAND_DELETE_ACTIVE_LINK          43
#define COMMAND_GETLIST_ACTIVE_LINK         44
#define COMMAND_GET_MULTIPLE_ACTIVE_LINKS   45
#define COMMAND_MERGE_ENTRY                 46
#define COMMAND_LOAD_AR_QUAL_STRUCT         47
#define COMMAND_EXPAND_CHAR_MENU            48
#define COMMAND_SET_SERVER_INFO             49
#define COMMAND_GETLIST_USER                50
#define COMMAND_GET_ESCALATION              51
#define COMMAND_SET_ESCALATION              52
#define COMMAND_CREATE_ESCALATION           53
#define COMMAND_DELETE_ESCALATION           54
#define COMMAND_GETLIST_ESCALATION          55
#define COMMAND_ENTRY_STATISTICS            56
#define COMMAND_GET_SERVER_STAT             57
#define COMMAND_GETLIST_SQL                 58
#define COMMAND_DELETE_MULTI_FIELD          59
#define COMMAND_EXECUTE_PROCESS             60
#define COMMAND_GET_VUI                     61
#define COMMAND_SET_VUI                     62
#define COMMAND_CREATE_VUI                  63
#define COMMAND_DELETE_VUI                  64
#define COMMAND_GETLIST_VUI                 65
#define COMMAND_SET_SERVER_PORT             66
#define COMMAND_GET_MULTIPLE_ENTRY          67
#define COMMAND_GET_SUPPORT_FILE            68
#define COMMAND_SET_SUPPORT_FILE            69
#define COMMAND_CREATE_SUPPORT_FILE         70
#define COMMAND_DELETE_SUPPORT_FILE         71
#define COMMAND_GETLIST_SUPPORT_FILE        72
#define COMMAND_LAUNCH_THREAD               73
#define COMMAND_LAUNCH_WAITING_THREAD       74
#define COMMAND_RELEASE_WAITING_THREADS     75
#define COMMAND_SLEEP_TIMER                 76
#define COMMAND_RANDOM_SLEEP_TIMER          77
#define COMMAND_MILLISECOND_SLEEP_TIMER     78
#define COMMAND_BEGIN_LOOP                  79
#define COMMAND_END_LOOP                    80
#define COMMAND_GETLIST_ENTRY_WITH_FIELDS   81
#define COMMAND_GETENTRY_BLOB               82
#define COMMAND_GET_CONTAINER               83
#define COMMAND_SET_CONTAINER               84
#define COMMAND_CREATE_CONTAINER            85
#define COMMAND_DELETE_CONTAINER            86
#define COMMAND_GETLIST_CONTAINER           87
#define COMMAND_GETMULT_CONTAINER           88
#define COMMAND_GET_ERROR_MESSAGE           89
#define COMMAND_SET_LOGGING                 90
#define COMMAND_CLOSE_NET_CONNECTIONS       91
#define COMMAND_SIGNAL                      92
#define COMMAND_VALIDATE_FORM_CACHE         93
#define COMMAND_GET_MULTIPLE_FIELDS         94
#define COMMAND_GETLIST_SCHEMA_WITH_ALIAS   95
#define COMMAND_GET_LOCALIZED_VALUE         96
#define COMMAND_CREATE_ALERT_EVENT          97
#define COMMAND_REGISTER_ALERTS             98
#define COMMAND_DEREGISTER_ALERTS           99
#define COMMAND_GETLIST_ALERT_USER          100
#define COMMAND_GET_ALERT_COUNT             101
#define COMMAND_DECODE_ALERT_MESSAGE        102
#define COMMAND_ENCODE_QUALIFIER            103
#define COMMAND_DECODE_QUALIFIER            104
#define COMMAND_ENCODE_ASSIGN               105
#define COMMAND_DECODE_ASSIGN               106
#define COMMAND_ENCODE_HISTORY              107
#define COMMAND_ENCODE_DIARY                108
#define COMMAND_GETLIST_EXT_SCHEMA_CANDS    109
#define COMMAND_GET_MULT_EXT_FIELD_CANDS    110
#define COMMAND_EXPAND_SS_MENU              111
#define COMMAND_VALIDATE_LICENSE            112
#define COMMAND_VALIDATE_MULTIPLE_LICENSES  113
#define COMMAND_GETLIST_LICENSE             114
#define COMMAND_CREATE_LICENSE              115
#define COMMAND_DELETE_LICENSE              116
#define COMMAND_GET_MULT_LOCALIZED_VALUES   117
#define COMMAND_GETLIST_SQL_FOR_AL          118
#define COMMAND_EXECUTE_PROCESS_FOR_AL      119
#define COMMAND_DRIVER_VERSION              120
#define COMMAND_GET_SESSION_CONFIGURATION   121
#define COMMAND_SET_SESSION_CONFIGURATION   122
#define COMMAND_ENCODE_DATE                 123
#define COMMAND_DECODE_DATE                 124
#define COMMAND_XML_CREATE_ENTRY            125
#define COMMAND_XML_GET_ENTRY               126
#define COMMAND_XML_SET_ENTRY               127
#define COMMAND_GET_MULT_CURR_RATIO_SETS    128
#define COMMAND_GET_CURRENCY_RATIO          129
#define COMMAND_UNIMPORT                    130
#define COMMAND_GET_MULTIPLE_ENTRYPOINTS    131
#define COMMAND_GETLIST_ROLE                132
#define COMMAND_IMPORT_LICENSE              133
#define COMMAND_EXPORT_LICENSE              134
#define COMMAND_GETLIST_APPLICATION_STATES  135
#define COMMAND_GET_APPLICATION_STATE       136
#define COMMAND_SET_APPLICATION_STATE       137
#define COMMAND_BEGIN_BULK_ENTRY_TXN        138
#define COMMAND_END_BULK_ENTRY_TXN          139
#define COMMAND_GETLIST_ENTRY_BLOCKS        140
#define COMMAND_GET_MULTIPLE_FILTERS        141
#define COMMAND_GET_MULTIPLE_ESCALATIONS    142
#define COMMAND_GET_MULTIPLE_CHAR_MENUS     143
#define COMMAND_GET_MULTIPLE_VUIS           144
#define COMMAND_SET_IMPERSONATED_USER       145
#define COMMAND_SERVICE_ENTRY               146
#define COMMAND_EXPORT_TO_FILE              147
#define COMMAND_GET_CLIENT_CHARSET          148
#define COMMAND_GET_SERVER_CHARSET          149
#define COMMAND_XML_PARSE_DOCUMENT          150
#define COMMAND_CREATE_MULTIPLE_FIELDS      151
#define COMMAND_SET_MULTIPLE_FIELDS         152
#define COMMAND_XML_SERVICE_ENTRY           153
#define COMMAND_SET_SCH_FIELD_WITH_IMAGE    154
#define COMMAND_CREATE_SCH_FIELD_WITH_IMAGE 155
#define COMMAND_GET_IMAGE                   156
#define COMMAND_SET_IMAGE                   157
#define COMMAND_CREATE_IMAGE                158
#define COMMAND_DELETE_IMAGE                159
#define COMMAND_GETLIST_IMAGE               160
#define COMMAND_WFD_EXECUTE                 161
#define COMMAND_WFD_GET_CURRENT_LOCATION    162
#define COMMAND_WFD_GET_FIELDVALUES         163
#define COMMAND_WFD_SET_FIELDVALUES         164
#define COMMAND_GET_OBJECT_CHANGE_TIMES     165
#define COMMAND_WFD_GET_DEBUG_MODE          166
#define COMMAND_WFD_SET_DEBUG_MODE          167
#define COMMAND_GET_MULTIPLE_IMAGES         168
#define COMMAND_WFD_GET_FILTER_QUAL         169
#define COMMAND_WFD_SET_FILTER_QUAL         170
#define COMMAND_RUN_ESCALATION              171
#define COMMAND_WFD_TERMINATE               172
#define COMMAND_GETLIST_ENTRY_WITH_MULTISCHEMA_FIELDS 173
#define COMMAND_BEGIN_CLIENT_TRANSACTION    174
#define COMMAND_END_CLIENT_TRANSACTION      175
#define COMMAND_SET_CLIENT_TRANSACTION      176
#define COMMAND_REMOVE_CLIENT_TRANSACTION   177
#define COMMAND_WFD_GET_KEYWORD             178
#define COMMAND_WFD_GET_USER_CONTEXT        179
#define COMMAND_WFD_RMT_SET_BKPT            180
#define COMMAND_WFD_RMT_CLR_BKPT            181
#define COMMAND_WFD_RMT_LIST_BKPT           182
#define COMMAND_WFD_RMT_CLR_BP_LIST         183
#define COMMAND_GETONE_ENTRY_WITH_FIELDS    184
#define COMMAND_GET_CACHE_EVENTS            185
#define COMMAND_SET_GET_ENTRY               186
#define COMMAND_CREATE_OVERLAY              187
#define COMMAND_CREATE_OVERLAY_FROM_OBJECT  188
#define COMMAND_VERIFY_USER2                189
#define MAX_COMMAND                         189

typedef char CommandType[6];  /* buffer to hold a command */

#ifdef MAIN
CommandType commands[MAX_COMMAND + 1] =
   {
      /* COMMAND_LOGIN                       */ "log",
      /* COMMAND_GET_ENTRY                   */ "ge",
      /* COMMAND_SET_ENTRY                   */ "se",
      /* COMMAND_CREATE_ENTRY                */ "ce",
      /* COMMAND_DELETE_ENTRY                */ "de",
      /* COMMAND_GETLIST_ENTRY               */ "gle",
      /* COMMAND_GET_FILTER                  */ "gf",
      /* COMMAND_SET_FILTER                  */ "sf",
      /* COMMAND_CREATE_FILTER               */ "cf",
      /* COMMAND_DELETE_FILTER               */ "df",
      /* COMMAND_GETLIST_FILTER              */ "glf",
      /* COMMAND_GETLIST_GROUP               */ "glg",
      /* COMMAND_GET_SCHEMA                  */ "gs",
      /* COMMAND_SET_SCHEMA                  */ "ss",
      /* COMMAND_CREATE_SCHEMA               */ "cs",
      /* COMMAND_DELETE_SCHEMA               */ "ds",
      /* COMMAND_GETLIST_SCHEMA              */ "gls",
      /* COMMAND_GETMULT_SCHEMA              */ "gms",
      /* COMMAND_GET_SCH_FIELD               */ "gsf",
      /* COMMAND_SET_SCH_FIELD               */ "ssf",
      /* COMMAND_CREATE_SCH_FIELD            */ "csf",
      /* COMMAND_DELETE_SCH_FIELD            */ "dsf",
      /* COMMAND_GETLIST_SCH_FIELD           */ "glsf",
      /* COMMAND_GET_CHAR_MENU               */ "gc",
      /* COMMAND_SET_CHAR_MENU               */ "sc",
      /* COMMAND_CREATE_CHAR_MENU            */ "cc",
      /* COMMAND_DELETE_CHAR_MENU            */ "dc",
      /* COMMAND_GETLIST_CHAR_MENU           */ "glc",
      /* COMMAND_EXPORT                      */ "exp",
      /* COMMAND_IMPORT                      */ "imp",
      /* COMMAND_GET_SERVER_INFO             */ "gsi",
      /* COMMAND_VERIFY_USER                 */ "ver",
      /* COMMAND_EXECUTE                     */ "ex",
      /* COMMAND_OPEN_OUT                    */ "oout",
      /* COMMAND_CLOSE_OUT                   */ "cout",
      /* COMMAND_RECORD                      */ "rec",
      /* COMMAND_STOP_RECORD                 */ "srec",
      /* COMMAND_GETLIST_SERVER              */ "svr",
      /* COMMAND_INITIALIZATION              */ "init",
      /* COMMAND_TERMINATION                 */ "term",
      /* COMMAND_GET_ACTIVE_LINK             */ "gal",
      /* COMMAND_SET_ACTIVE_LINK             */ "sal",
      /* COMMAND_CREATE_ACTIVE_LINK          */ "cal",
      /* COMMAND_DELETE_ACTIVE_LINK          */ "dal",
      /* COMMAND_GETLIST_ACTIVE_LINK         */ "glal",
      /* COMMAND_GET_MULTIPLE_ACTIVE_LINKS   */ "gmal",
      /* COMMAND_MERGE_ENTRY                 */ "me",
      /* COMMAND_LOAD_AR_QUAL_STRUCT         */ "lqs",
      /* COMMAND_EXPAND_CHAR_MENU            */ "ec",
      /* COMMAND_SET_SERVER_INFO             */ "ssi",
      /* COMMAND_GETLIST_USER                */ "glu",
      /* COMMAND_GET_ESCALATION              */ "ges",
      /* COMMAND_SET_ESCALATION              */ "ses",
      /* COMMAND_CREATE_ESCALATION           */ "ces",
      /* COMMAND_DELETE_ESCALATION           */ "des",
      /* COMMAND_GETLIST_ESCALATION          */ "gles",
      /* COMMAND_ENTRY_STATISTICS            */ "stat",
      /* COMMAND_GET_SERVER_STAT             */ "gss",
      /* COMMAND_GETLIST_SQL                 */ "glsql",
      /* COMMAND_DELETE_MULTI_FIELD          */ "dmsf",
      /* COMMAND_EXECUTE_PROCESS             */ "proc",
      /* COMMAND_GET_VUI                     */ "gv",
      /* COMMAND_SET_VUI                     */ "sv",
      /* COMMAND_CREATE_VUI                  */ "cv",
      /* COMMAND_DELETE_VUI                  */ "dv",
      /* COMMAND_GETLIST_VUI                 */ "glsv",
      /* COMMAND_SET_SERVER_PORT             */ "ssp",
      /* COMMAND_GET_MULTIPLE_ENTRY          */ "gme",
      /* COMMAND_GET_SUPPORT_FILE            */ "gfl",
      /* COMMAND_SET_SUPPORT_FILE            */ "sfl",
      /* COMMAND_CREATE_SUPPORT_FILE         */ "cfl",
      /* COMMAND_DELETE_SUPPORT_FILE         */ "dfl",
      /* COMMAND_GETLIST_SUPPORT_FILE        */ "glfl",
      /* COMMAND_LAUNCH_THREAD               */ "lt",
      /* COMMAND_LAUNCH_WAITING_THREAD       */ "lwt",
      /* COMMAND_RELEASE_WAITING_THREADS     */ "rwt",
      /* COMMAND_SLEEP_TIMER                 */ "st",
      /* COMMAND_RANDOM_SLEEP_TIMER          */ "rst",
      /* COMMAND_MILLISECOND_SLEEP_TIMER     */ "msst",
      /* COMMAND_BEGIN_LOOP                  */ "bl",
      /* COMMAND_END_LOOP                    */ "el",
      /* COMMAND_GETLIST_ENTRY_WITH_FIELDS   */ "glewf",
      /* COMMAND_GETENTRY_ATTACH             */ "geb",
      /* COMMAND_GET_CONTAINER               */ "gco",
      /* COMMAND_SET_CONTAINER               */ "sco",
      /* COMMAND_CREATE_CONTAINER            */ "cco",
      /* COMMAND_DELETE_CONTAINER            */ "dco",
      /* COMMAND_GETLIST_CONTAINER           */ "glco",
      /* COMMAND_GETMULT_CONTAINER           */ "gmco",
      /* COMMAND_GET_ERROR_MESSAGE           */ "gem",
      /* COMMAND_SET_LOGGING                 */ "slog",
      /* COMMAND_CLOSE_NET_CONNECTIONS       */ "cnc",
      /* COMMAND_SIGNAL                      */ "sig",
      /* COMMAND_VALIDATE_FORM_CACHE         */ "vfc",
      /* COMMAND_GET_MULTIPLE_FIELDS         */ "gmsf",
      /* COMMAND_GETLIST_SCHEMA_WITH_ALIAS   */ "glsa",
      /* COMMAND_GET_LOCALIZED_VALUE         */ "glv",
      /* COMMAND_CREATE_ALERT_EVENT          */ "cae",
      /* COMMAND_REGISTER_ALERTS             */ "rfa",
      /* COMMAND_DEREGISTER_ALERTS           */ "dfa",
      /* COMMAND_GETLIST_ALERT_USER          */ "glau",
      /* COMMAND_GET_ALERT_COUNT             */ "gac",
      /* COMMAND_DECODE_ALERT_MESSAGE        */ "dcam",
      /* COMMAND_ENCODE_QUALIFIER            */ "ecqal",
      /* COMMAND_DECODE_QUALIFIER            */ "dcqal",
      /* COMMAND_ENCODE_ASSIGN               */ "ecasn",
      /* COMMAND_DECODE_ASSIGN               */ "dcasn",
      /* COMMAND_ENCODE_HISTORY              */ "echst", 
      /* COMMAND_ENCODE_DIARY                */ "ecdia",
      /* COMMAND_GETLIST_EXT_SCHEMA_CANDS    */ "glxsc",
      /* COMMAND_GET_MULT_EXT_FIELD_CANDS    */ "gmxfc",
      /* COMMAND_EXPAND_SS_MENU              */ "essm",
      /* COMMAND_VALIDATE_LICENSE            */ "vl",
      /* COMMAND_VALIDATE_MULTIPLE_LICENSES  */ "vml",
      /* COMMAND_GETLIST_LICENSE             */ "gll",
      /* COMMAND_CREATE_LICENSE              */ "cl",
      /* COMMAND_DELETE_LICENSE              */ "dl",
      /* COMMAND_GET_MULT_LOCALIZED_VALUES   */ "gmlv",
      /* COMMAND_GETLIST_SQL_FOR_AL          */ "sqlal",
      /* COMMAND_EXECUTE_PROCESS_FOR_AL      */ "epal",
      /* COMMAND_DRIVER_VERSION              */ "dver",
      /* COMMAND_GET_SESSION_CONFIGURATION   */ "gsc",
      /* COMMAND_SET_SESSION_CONFIGURATION   */ "ssc",
      /* COMMAND_ENCODE_DATE                 */ "ecdat",
      /* COMMAND_DECODE_DATE                 */ "dcdat",
      /* COMMAND_XML_CREATE_ENTRY            */ "xmlce",
      /* COMMAND_XML_GET_ENTRY               */ "xmlge",
      /* COMMAND_XML_SET_ENTRY               */ "xmlse",
      /* COMMAND_GET_MULT_CURR_RATIO_SETS    */ "gmcrs",
      /* COMMAND_GET_CURRENCY_RATIO          */ "gcr",
      /* COMMAND_UNIMPORT                    */ "unimp",
      /* COMMAND_GET_MULTIPLE_ENTRYPOINTS    */ "gmep",
      /* COMMAND_GETLIST_ROLE                */ "glr",
      /* COMMAND_IMPORT_LICENSE              */ "iml",
      /* COMMAND_EXPORT_LICENSE              */ "exl",
      /* COMMAND_GETLIST_APPLICATION_STATES  */ "glas",
      /* COMMAND_GET_APPLICATION_STATE       */ "gas",
      /* COMMAND_SET_APPLICATION_STATE       */ "sas",
      /* COMMAND_BEGIN_BULK_ENTRY_TXN        */ "bbet",
      /* COMMAND_END_BULK_ENTRY_TXN          */ "ebet",
      /* COMMAND_GETLIST_ENTRY_BLOCKS        */ "gleb",
      /* COMMAND_GET_MULTIPLE_FILTERS        */ "gmf",
      /* COMMAND_GET_MULTIPLE_ESCALATIONS    */ "gmes",
      /* COMMAND_GET_MULTIPLE_CHAR_MENUS     */ "gmc",
      /* COMMAND_GET_MULTIPLE_VUIS           */ "gmv",
      /* COMMAND_SET_IMPERSONATED_USER       */ "imusr",
      /* COMMAND_SERVICE_ENTRY               */ "sve",
      /* COMMAND_EXPORT_TO_FILE              */ "expf",
      /* COMMAND_GET_CLIENT_CHARSET          */ "gccs",
      /* COMMAND_GET_SERVER_CHARSET          */ "gscs",
      /* COMMAND_XML_PARSE_DOCUMENT          */ "xmlpd",
      /* COMMAND_CREATE_MULTIPLE_FIELDS      */ "cmsf",
      /* COMMAND_SET_MULTIPLE_FIELDS         */ "smsf",
      /* COMMAND_XML_SERVICE_ENTRY           */ "xmlsv",
      /* COMMAND_SET_SCH_FIELD_WITH_IMAGE    */ "ssfwi",
      /* COMMAND_CREATE_SCH_FIELD_WITH_IMAGE */ "csfwi",
      /* COMMAND_GET_IMAGE                   */ "gi",
      /* COMMAND_SET_IMAGE                   */ "si",
      /* COMMAND_CREATE_IMAGE                */ "ci",
      /* COMMAND_DELETE_IMAGE                */ "di",
      /* COMMAND_GETLIST_IMAGE               */ "gli",
      /* COMMAND_WFD_EXECUTE                 */ "dbex",
      /* COMMAND_WFD_GET_CURRENT_LOCATION    */ "dbcl",
      /* COMMAND_WFD_GET_FIELDVALUES         */ "dbgf",
      /* COMMAND_WFD_SET_FIELDVALUES         */ "dbsf",
      /* COMMAND_GET_OBJECT_CHANGE_TIMES     */ "goct",
      /* COMMAND_WFD_GET_DEBUG_MODE          */ "dbgm",
      /* COMMAND_WFD_SET_DEBUG_MODE          */ "dbsm",
      /* COMMAND_GETMULTIPLE_IMAGEs          */ "gmi",
      /* COMMAND_WFD_GET_FILTER_QUAL         */ "dbgq",
      /* COMMAND_WFD_SET_FILTER_QUAL         */ "dbsq",
      /* COMMAND_RUN_ESCALATION              */ "resc",
      /* COMMAND_WFD_TERMINATE               */ "dbta",
      /* COMMAND_GETLIST_ENTRY_WITH_MULTISCHEMA_FIELDS*/ "glmsf",
      /* COMMAND_BEGIN_CLIENT_TRANSACTION    */ "bcmt",
      /* COMMAND_END_CLIENT_TRANSACTION      */ "ecmt",
      /* COMMAND_SET_CLIENT_TRANSACTION      */ "scmt",
      /* COMMAND_REMOVE_CLIENT_TRANSACTION   */ "rcmt",
      /* COMMAND_WFD_GET_KEYWORD             */ "dbkw",
      /* COMMAND_WFD_GET_USER_CONTEXT        */ "dbuc",
      /* COMMAND_WFD_RMT_SET_BKPT            */ "dbbp",
      /* COMMAND_WFD_RMT_CLR_BKPT            */ "dbbc",
      /* COMMAND_WFD_RMT_LIST_BKPT           */ "dbbl",
      /* COMMAND_WFD_RMT_CLR_BP_LIST         */ "dbbx",
      /* COMMAND_GETONE_ENTRY_WITH_FIELDS    */ "goewf",
      /* COMMAND_GET_CACHE_EVENTS            */ "gce",
      /* COMMAND_SET_GET_ENTRY               */ "sge",
      /* COMMAND_CREATE_OVERLAY              */ "co",
      /* COMMAND_CREATE_OVERLAY_FROM_OBJECT  */ "cofo",
      /* COMMAND_VERIFY_USER2                */ "ver2",
};
#else
extern CommandType commands[MAX_COMMAND + 1];
#endif

void InitCommandProcessing(void);
void TermCommandProcessing(void);
ARBoolean ExtCommandProcessing(char *);

