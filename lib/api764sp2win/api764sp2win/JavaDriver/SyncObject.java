package com.bmc.arsys.demo.javadriver;

public class SyncObject
{
   boolean flag = false;

   public SyncObject( boolean bFlag )
   {
      flag = bFlag;
   }
   public synchronized boolean getFlag( )
   {
      return flag;  
   }

   public synchronized void setFlag( boolean bFlag )
   {
      flag = bFlag;  
   }

}