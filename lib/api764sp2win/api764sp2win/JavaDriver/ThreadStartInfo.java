package com.bmc.arsys.demo.javadriver;

import java.io.*;

public class ThreadStartInfo {

    InputFile inputFile = null;
    String outputFileName = null;
    PrintWriter outputFile = null;
    boolean isStdOut = false;
    String authentication = null;
    String user = null;
    String password = null;
    String locale = null;
    String charset = null;
    String timezone = null;
    String server = null;
    long upperBound = 0;
    boolean waitFlag = false;
    SyncObject waitObject = null;
    SyncObject releaseObject = null;

    public void cleanUp() throws IOException {
        inputFile = null;
        outputFile = null;
        outputFileName = null;
        authentication = null;
        user = null;
        password = null;
        server = null;
        upperBound = 0;
        waitFlag = false;
        waitObject = null;
        locale = null;
        charset = null;
        releaseObject = null;
    }

    public void setInputFile(InputFile file) {
        inputFile = file;
    }

    public InputFile getInputFile() {
        return inputFile;
    }

    public boolean getIsStdOut() {
        return isStdOut;
    }

    public void setOutputToStdOut() {
        if (isStdOut == false) {
            if (outputFile != null) {
                outputFile.close();
                outputFile = null;
            }
            isStdOut = true;
            outputFile = new PrintWriter(System.out);
            outputFileName = null;
        }
    }

    public void setOutputFile(PrintWriter fp) {
        if ((isStdOut != true) && (outputFile != null)) {
            outputFile.close();
            outputFile = null;
        }
        outputFile = fp;
        isStdOut = false;
    }

    public PrintWriter getOutputFile() {
        return outputFile;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setOutputFileName(String name) {
        outputFileName = name;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String name) {
        authentication = name;
    }

    public void setUser(String name) {
        user = name;
    }

    public String getUser() {
        return user;
    }

    public void setPassword(String pwd) {
        password = pwd;
    }

    public String getPassword() {
        return password;
    }

    public void setServer(String svr) {
        server = svr;
    }

    public String getServer() {
        return server;
    }

    public void setLocale(String inLocale) {
        locale = inLocale;
    }

    public String getLocale() {
        return locale;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * @return the timezone
     */
    public String getTimezone() {
        return timezone;
    }

    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setUpperBound(long ub) {
        upperBound = ub;
    }

    public long getUpperBound() {
        return upperBound;
    }

    public void setWaitFlag(boolean flag) {
        waitFlag = flag;
    }

    public boolean getWaitFlag() {
        return waitFlag;
    }

    public void setWaitObject(SyncObject obj) {
        waitObject = obj;
    }

    public SyncObject getWaitObject() {
        return waitObject;
    }

    public void setReleaseObject(SyncObject obj) {
        releaseObject = obj;
    }

    public SyncObject getReleaseObject() {
        return releaseObject;
    }

}