package com.bmc.arsys.demo.javadriver;

import java.io.*;

import com.bmc.arsys.api.ObjectPrinter;

public class WfdOutputWriter extends ObjectPrinter {

    public void printResult(String resultString) {
        if ((WFD.quietMode & WFD.SUPPRESS_RESULTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(resultString);
            outputFile.flush();
        }
    }

    public void driverPrintHeader(String header) {
        if ((WFD.quietMode & WFD.SUPPRESS_HEADERS) == 0) {
            ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
            if (threadControlBlockPtr.getPrimaryThread()) {
                System.out.println(header);
            }
        }
    }

    public void driverPrintPrompt(String prompt) {
        if ((WFD.quietMode & WFD.SUPPRESS_PROMPTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
            if (threadControlBlockPtr.getPrimaryThread()) {
                System.out.print(prompt);
            }
        }
    }

    public void driverPrintNotSupportCommand(int commandCode) {
        ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
        String buffer = threadControlBlockPtr.getBuffer();
        String args = threadControlBlockPtr.getArgs();
        StringBuilder strBuf = new StringBuilder();
        strBuf.append(" **** No support for this command: ").append(commandCode).append(", with args: ").append(args)
                .append(", in line: ").append(buffer);
        if (threadControlBlockPtr.getCurrentInputFileName() != null)
            strBuf.append(", from: ").append(threadControlBlockPtr.getCurrentInputFileName());
        strBuf.append(", in driver \n");
        driverPrintError(strBuf.toString());
    }

    public void driverPrintMenu(String menuLine) {
        if ((WFD.quietMode & WFD.SUPPRESS_MENU) == 0) {
            System.out.print(menuLine);
        }
    }

    public void driverPrintError(String errorMessage) {
        if ((WFD.quietMode & WFD.SUPPRESS_RESULTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(errorMessage);
            outputFile.flush();
        }
    }

    public void driverPrintException(Exception e) {
        if ((WFD.quietMode & WFD.SUPPRESS_RESULTS) == 0) {
            e.printStackTrace();
        }
    }

    public void driverPrintWarning(String warning) {
        if ((WFD.quietMode & WFD.SUPPRESS_WARNINGS) == 0) {
            ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(warning);
            outputFile.flush();
        }
    }

    public void closeOutputFile() {
        ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();

        if (threadControlBlockPtr.getIsStdOut()) {
            driverPrintWarning(" **** Output to stdout; cannot close stdout file\n");
        } else {
            // Set the output to stdout
            threadControlBlockPtr.setOutputToStdOut();
        }
    }

    public void openOutputFile(String fileName) {
        if ((fileName == null) || fileName.length() == 0) {
            driverPrintWarning(" **** No filename specified so no change to output file\n");
        } else { /* open the new file for writing */
            if ((WFD.getQuietMode() & WFD.SUPPRESS_RESULTS) != 0) {
                return;
            }

            String absoluteFileName = null;
            String directory = WFD.getResultDirectory();

            if (directory != null && (directory.length() > 0)) {
                absoluteFileName = directory + "\\" + fileName;
            } else {
                absoluteFileName = fileName;
            }

            try {
                ThreadControlBlock threadControlBlockPtr = WFD.getThreadControlBlockPtr();
                threadControlBlockPtr.setOutputFile(absoluteFileName);
            } catch (Exception e) {
                driverPrintWarning(" **** File error during open; no change to output file" + "\n");
            }
        }
    }

    public void driverPrintHelp() {
        System.out.print("         AR System Workflow Debugger\n");

        System.out.println();

        /* print main command menu */
        /* columns       0xxxxxxxx1xxxxxxxxx2xxxxxxxxx3xxxxxxxxx4xxxxxxxxx5xxxxxxxxx6xxxxxxxxx7xxxxxxxxx8 */
        /*               1        0         0         0         0         0         0         0         0 */
		System.out.print("\n");
		System.out.print(" Execution       Local BkPts     Data            Misc.        \n");
		System.out.print(" -------------   -------------   -------------   -------------\n");
		System.out.print(" step      (s)   brk pt   (bp)   get FVL  (gf)   init   (init)\n");
		System.out.print(" run to bp (g)   clr bk   (bc)   set FVL  (sf)   term   (term)\n");
		System.out.print(" run to rbp(r)   list bkp (bl)   get qual (gq)   log     (log)\n");
		System.out.print("                 clr list (bx)   set qual (sq)   quit  (q|x|e)\n");
		System.out.print("                                 location (cl)   help    (h|?)\n");
		System.out.print(" term API (ta)   Remote BkPts    stack fr (st)   set port(ssp)\n");
		System.out.print(" finish   (fi)   -------------   list stk (li)   get mode (gm)\n");
		System.out.print("                 brk pt  (rbp)   get keywd(kw)   set mode (sm)\n");
		System.out.print("                 clr bk  (rbc)   user ctx (uc)   mode all(sma)\n");
		System.out.print("                 list bkp(rbl)                   mode clr(smc)\n");
		System.out.print("                 clr list(rbx)                   restart (rst)\n");

    }
    
    public void printExtraHelp() {
        System.out.print("         AR System Workflow Debugger Information\n");

        System.out.println();

        /* print main command menu */
        /* columns       0xxxxxxxx1xxxxxxxxx2xxxxxxxxx3xxxxxxxxx4xxxxxxxxx5xxxxxxxxx6xxxxxxxxx7xxxxxxxxx8 */
        /*               1        0         0         0         0         0         0         0         0 */
		System.out.print("WorkflowName indicates either FilterName or EscalationName\n");
		System.out.print("Information in square brackets is optional.\n");
		System.out.print(" Current location format:\n");
		System.out.print(" (Workflow Stage) API Name->Schema Name->WorkflowName[GuideName]->action ## (Action Type)\n");

    }

}