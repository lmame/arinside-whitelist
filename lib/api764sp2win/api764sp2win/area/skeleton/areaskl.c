
/*  File: areaskl.c  */

#ifdef _WIN32
#include <windows.h>
#endif /* _WIN32 */

#include <stdlib.h>
#include <string.h>

#include "area.h"

/*****************************************************************************/
/*                                                                           */
/*       Action Request System External Authentication (AREA) Skeleton       */
/*                                                                           */
/*****************************************************************************/
/* Description:  This is a skeleton AREA plug-in.  It is intended to be used */
/*    as a starting point for further plug-in development.                   */ 
/*                                                                           */
/*    All functions, unless otherwise noted, are optional.  If a function is */
/*    not defined, the plug-in service will return an appropriate response.  */
/*                                                                           */
/*    General notes on memory management:                                    */
/*                                                                           */
/*       1) Do not deallocate (free) memory passed into the ARPlugin or      */
/*          AREA functions.  The plug-in service will deallocate memory      */
/*          associated with the input ("IN") arguments.                      */
/*                                                                           */
/*       2) Do not deallocate (free) memory returned by the ARPlugin or      */
/*          AREA functions.  The plug-in service will deallocate memory      */
/*          associated with the return ("OUT") arguments using free().       */
/*                                                                           */
/*    There are two exceptions to the above rules regarding memory           */
/*    management.  First, the plug-in has complete control over the          */
/*    allocation and deallocation of the "object" via                        */
/*    ARPluginCreateInstance() and ARPluginDeleteInstance(), respectively.   */
/*    Second, the AREAFreeCallback() is used to deallocate response          */
/*    allocated, if any, in the AREAVerifyLoginCallback().                   */
/*                                                                           */
/*****************************************************************************/

#ifdef _WIN32
BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
   return TRUE;
}

#endif /* _WIN32 */

/*****************************************************************************/
/*                                                                           */
/*                             ARPluginIdentify                              */
/*                                                                           */
/*****************************************************************************/
/*   Description:  This call is not optional.  All plug-ins must define this */
/*      function.                                                            */
/*                                                                           */
/*      This call is used by the plug-in service to identify what kind of    */
/*      plug-in this is going to implement, what version of the AREA         */
/*      specification it supports, and its name.                             */
/*                                                                           */
/*      Recommended plug-in naming convention:                               */
/*         <Company Name>.<Plug-In Type>.<Plug-In Name>                      */
/*                                                                           */
/*      E.g., If company XYZ has created the "Greatest Plug-In Ever" (GPE)   */
/*      they might name it "XYZ.AREA.GPE".                                   */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginIdentify(
ARPluginIdentification *id,     /* OUT; information about the plug-in */
ARStatusList           *status  /* OUT; error message(s)*/
)
{
   id->type = AR_PLUGIN_TYPE_AREA;
   strcpy(id->name, "EXAMPLE.AREA.SKELETON");
   id->version = AREA_PLUGIN_VERSION;
   memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginInitialization                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to initilize  */
/*    the plug-in.  This is where the plug-in should initialize any data     */
/*    that will be global and accessed by all instances of the plug-in that  */
/*    are created via ARPluginCreateInstance().                              */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginInitialization(
int            argc,    /* IN;  number of arguments passed into the service */
char         **argv,    /* IN;  arguments passed into the plug-in service at */
                        /*      the command line. */
ARStatusList  *status   /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginTermination                              */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to terminate  */
/*    the plug-in.  This is where the plug-in should deallocate any          */
/*    resources that have been allocated either globally or by each instance */
/*    created by ARPluginCreateInstance().                                   */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginTermination(
ARStatusList *status        /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));
   
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginCreateInstance                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to create an  */
/*    instance of the plug-in.  Each instance is guaranteed to be involved   */
/*    in one thread of operation at any one time.                            */
/*                                                                           */
/*    The structure returned via the 'object' pointer will be provided in    */
/*    subsequent plug-in calls.  This allows you to attach arbitrary data    */
/*    to an instance that will be inherently thread safe.                    */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginCreateInstance(
void         **object,   /* OUT; plug-in instance */
ARStatusList  *status    /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   if (object != NULL)
   {
      /*
       * Set *object to point to something that you have 
       * allocated and initialized.
       */
      *object = NULL;
   }

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          ARPluginDeleteInstance                           */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called by the plug-in service to delete an  */
/*    instance of the plug-in.  Each instance is guaranteed to be involved   */
/*    in one thread of operation at any one time.                            */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginDeleteInstance(
void         *object,  /* IN;  plug-in instance to delete*/
ARStatusList *status   /* OUT; status of the operation */
)
{
   if (object != NULL)
   {
      /*
       * Anything that you allocated in ARPluginCreateInstance
       * should be deallocated here.
       */
   }

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                          AREAVerifyLoginCallback                          */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called whenever a request is made by the AR */
/*    System Server to authenticate a user.                                  */
/*                                                                           */
/*    The user name and password (unencrypted) are passed as parameters.     */
/*                                                                           */
/*    A customized authentication plug-in must define this function.  Any    */
/*    global information or resources accessed here must be protected with   */
/*    appropriate mutual exclusion locks to be multi-thread safe.            */
/*                                                                           */
/*    A customized authentication plug-in must return a response.  A NULL    */
/*    response will be interpreted as a failed login attempt.                */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT void AREAVerifyLoginCallback(
void                *object,         /* IN;  plug-in instance */
ARAccessNameType     user,           /* IN;  user name to authenticate */
ARPasswordType	     password,       /* IN;  user's password */
ARNameType	     networkAddr,    /* IN;  AR API client's IP address */
ARAuthType           authString,     /* IN;  used for misc. purposes */
AREAResponseStruct **response        /* OUT; success/fail login w/ info */
)
{
   return;
}


/*****************************************************************************/
/*                                                                           */
/*                             AREANeedToSyncCallback                        */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called periodically at the request of the   */
/*    AR System server.                                                      */
/*                                                                           */
/*    A customized authentication plug-in must define this function.  Any    */
/*    global information or resources accessed here must be protected with   */
/*    appropriate mutual exclusion locks to be multi-thread safe.            */
/*                                                                           */
/*    A non-zero return value will instruct the AR System server to flush    */
/*    its cache of user information.                                         */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int AREANeedToSyncCallback(
void              *object         /* IN; plug-in instance */
)
{
   return 0;
}


/*****************************************************************************/
/*                                                                           */
/*                               AREAFreeCallback                            */
/*                                                                           */
/*****************************************************************************/
/* Description: This function is called after a response is made to the AR   */
/*    System Server to authenticate a user.                                  */
/*                                                                           */
/*    A pointer to the response structure returned by                        */
/*    AREAVerifyLoginCallback() is passed as a parameter.                    */
/*                                                                           */
/*    A customized authentication plug-in must define this function.  Any    */
/*    global information or resources accessed here must be protected with   */
/*    appropriate mutual exclusion locks to be multi-thread safe.            */
/*                                                                           */
/*    A customized authentication plug-in must return a response.  A NULL    */
/*    response will be interpreted as a failed login attempt.                */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT void AREAFreeCallback(
void                *object,    /* IN; plug-in instance */
AREAResponseStruct  *response   /* IN; if needed, free the response */
)
{
   return;
}

