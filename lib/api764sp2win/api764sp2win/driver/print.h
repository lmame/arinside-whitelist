/*  File:  print.h  */

#define NO_SUPPRESSION       0x0000
#define SUPPRESS_RESULTS     0x0001
#define SUPPRESS_HEADERS     0x0002
#define SUPPRESS_PROMPTS     0x0004
#define SUPPRESS_MENU        0x0008
#define SUPPRESS_ERRORS      0x0010
#define SUPPRESS_WARNINGS    0x0020
#define SUPPRESS_TIME        0x0040
#define SUPPRESS_DATE        0x0080
#define SUPPRESS_FOR_WFD     0x0100

#define NOT_WFD_QUIET (!(gQuietMode & SUPPRESS_FOR_WFD))

void DriverPrintResult(const char *format, ...);
void DriverPrintError(const char *format, ...);
void DriverPrintWarning(const char *format, ...);
void DriverPrintPrompt(const char *format, ...);
void DriverPrintResultHeader(const char *);
void DriverPrintHeader(const char *);
void DriverPrintMenu(const char *);
void DriverPrintMallocCallError(const char *);

void PrintAuxillaryStatus(void);

void PrintARBoolean(char *, ARBoolean);
void PrintInt(char *, int);
void PrintUInt(char *, unsigned int);
void PrintLong(char *, ARLong32);
void PrintULong(char *, ARULong32);
void PrintPtr(const char *, void *);
void PrintReal(char *, double);
void PrintChar(char *, char *);
void PrintARTimestamp(char *, ARTimestamp);
void PrintARTimestampList(char *, char *, ARTimestampList *);
void PrintARDate(char *, int);
void PrintARUnsignedIntList(char *, char *, ARUnsignedIntList *);
void PrintAREntryIdList(char *, char *, AREntryIdList *);
void PrintARInternalId(char *, ARInternalId);
void PrintARInternalIdList(char *, char *, ARInternalIdList *);
void PrintARLocaleList(char *, char *, ARLocaleList *);
void PrintARNameList(char *, char *, ARNameList *);
void PrintARServerNameList(char *, char *, ARServerNameList *);
void PrintARTextStringList(char *, char *, ARTextStringList *);
void PrintARStatusStruct(ARStatusStruct *);
void PrintARStatusList(ARStatusList *);
void PrintARStatusStructIndent(char *, ARStatusStruct *);
void PrintARStatusListIndent(char *, ARStatusList *);
void PrintARStatusListList(ARStatusListList *);
void PrintARByteList(char *, char *, ARByteList *);
void PrintARControlStruct(ARControlStruct *);
void PrintARCoordList(char *, char *, ARCoordList *);
void PrintARValueStruct(char *, ARValueStruct *);
void PrintARValueList(char *, ARValueList *);
void PrintARValueListList(ARValueListList *);
void PrintAREntryListFieldStruct(AREntryListFieldStruct *);
void PrintAREntryListFieldList(AREntryListFieldList *);
void PrintARSortList(ARSortList *);
void PrintAREntryListStruct(AREntryListStruct *);
void PrintAREntryListList(AREntryListList *);
void PrintAREntryBlockList(AREntryBlockList *);
void PrintARFieldValueStruct(ARFieldValueStruct *);
void PrintARFieldValueList(ARFieldValueList *);
void PrintARFieldValueListList(ARFieldValueListList *);
void PrintAREntryListFieldValueList(AREntryListFieldValueList *);
void PrintARFieldValueOrArithStruct(char *, ARFieldValueOrArithStruct *);
void PrintARArithOpStruct(char *, ARArithOpStruct *);
void PrintARRelOpStruct(char *, ARRelOpStruct *);
void PrintARQualifierList(char *, ARQualifierList *);
void PrintARQualifierStruct(char *, ARQualifierStruct *);
void PrintARIndexStruct(ARIndexStruct *);
void PrintARIndexList(ARIndexList *);
void PrintARArchiveInfo(ARArchiveInfoStruct *);
void PrintARAuditInfo(ARAuditInfoStruct *);
void PrintARCompoundSchema(ARCompoundSchema *);
void PrintARCompoundSchemaList(ARCompoundSchemaList *);
void PrintARDisplayPropList(char *, char *, ARPropList *);
void PrintARDisplayPropListList(char *header1, char *header2,
                                ARPropListList *value);
void PrintARObjectPropList(char *, char *, ARPropList *);
void PrintARDisplayInstanceList(ARDisplayInstanceList *);
void PrintARDisplayInstanceListList(char *, ARDisplayInstanceListList *);
void PrintARAssignFieldStruct(char *, ARAssignFieldStruct *);
void PrintARAssignSQLStruct(char *, ARAssignSQLStruct *);
void PrintARAssignStruct(char *, ARAssignStruct *);
void PrintARFieldAssignList(ARFieldAssignList *);
void PrintARArithOpAssignStruct(char *, ARArithOpAssignStruct *);
void PrintARFunctionAssignStruct(char *, ARFunctionAssignStruct *);
void PrintARFieldAssignStruct(ARFieldAssignStruct *);
void PrintARFieldCharacteristics(ARFieldCharacteristics *);
void PrintARDDEStruct(char *, ARDDEStruct *);
void PrintARAutomationStruct(char *, ARAutomationStruct *);
void PrintARCOMValueStruct(char *, ARCOMValueStruct *, char *);
void PrintARCOMMethodList(ARCOMMethodList *);
void PrintARCOMMethodStruct(ARCOMMethodStruct *);
void PrintARCOMMethodParmList(ARCOMMethodParmList *);
void PrintARCOMMethodParmStruct(ARCOMMethodParmStruct *);
void PrintARFilterActionNotify(ARFilterActionNotify *);
void PrintARPushFieldsList(ARPushFieldsList *);
void PrintARSQLStruct(ARSQLStruct *);
void PrintARFilterActionStruct(ARFilterActionStruct *);
void PrintARFilterActionList(ARFilterActionList *, ARBoolean);
void PrintARActiveLinkMacroStruct(ARActiveLinkMacroStruct *);
void PrintARActiveLinkActionStruct(ARActiveLinkActionStruct *);
void PrintARActiveLinkActionList(ARActiveLinkActionList *, ARBoolean);
void PrintARPermissionStruct(ARPermissionStruct *, ARBoolean);
void PrintARPermissionList(ARPermissionList *, ARBoolean);
void PrintARPermissionListList(char *, ARPermissionListList *);
void PrintARGroupInfoStruct(ARGroupInfoStruct *);
void PrintARGroupInfoList(ARGroupInfoList *);
void PrintARUserLicenseStruct(ARUserLicenseStruct *);
void PrintARUserLicenseList(ARUserLicenseList *);
void PrintARUserInfoStruct(ARUserInfoStruct *);
void PrintARUserInfoList(ARUserInfoList *);
void PrintARIntegerLimitsStruct(ARIntegerLimitsStruct *);
void PrintARRealLimitsStruct(ARRealLimitsStruct *);
void PrintARCharLimitsStruct(ARCharLimitsStruct *);
void PrintARFieldLimitList(char *, ARFieldLimitList *);
void PrintARFieldLimitStruct(ARFieldLimitStruct *);
void PrintARCharMenuItemStruct(char *, ARCharMenuItemStruct *);
void PrintARCharMenuQueryStruct(char *, ARCharMenuQueryStruct *);
void PrintARCharMenuStruct(char *, ARCharMenuStruct *);
void PrintARCharMenuList(char *, ARCharMenuList *);
void PrintARStructItemStruct(ARStructItemStruct *);
void PrintARStructItemList(ARStructItemList *);
void PrintARServerInfoStruct(ARServerInfoStruct *);
void PrintARServerInfoStruct2(ARServerInfoStruct *);
void PrintARServerInfoList(ARServerInfoList *);
void PrintARServerInfoList2(ARServerInfoList *);
void PrintARServerInfoRequestList(char *, ARServerInfoRequestList *);
void PrintARStatisticsResultStruct(char *, ARStatisticsResultStruct *);
void PrintARStatisticsResultList(char *, ARStatisticsResultList *);
void PrintARStatusHistoryList(char *, ARStatusHistoryList *);
void PrintARDiaryList(char *, ARDiaryList *);
void PrintARFieldMapping(char *, ARFieldMappingStruct *);
void PrintARFieldMappingList(char *, ARFieldMappingList *);
void PrintARBooleanList(char *, ARBooleanList *);
void PrintARContainerInfo(char *, ARContainerInfo *);
void PrintARContainerInfoList(char *, char *, ARContainerInfoList *);
void PrintARContainerOwnerObj(char *, ARContainerOwnerObj *);
void PrintARContainerOwnerObjList(ARContainerOwnerObjList *);
void PrintARReferenceList(ARReferenceList *);
void PrintARReferenceStruct(char *, ARReferenceStruct *);
void PrintARExtReferenceStruct(char *, ARExtReferenceStruct *);
void PrintARCloseWndStruct(ARCloseWndStruct *);
void PrintARAccessNameList(char *, char *, ARAccessNameList *);
void PrintARRoleInfoList(ARRoleInfoList *);
void PrintARBulkEntryReturnList(ARBulkEntryReturnList *);
void PrintReturnCode(int);
void PrintARObjectChangeTimestampList(ARObjectChangeTimestampList *);
void PrintWFDLocationStruct( char *, ARWfdCurrentLocation * );
char * WfdGetStageText( int );
void PrintARWfdUserContext( ARWfdUserContext * );
void PrintWFDBreakpoint( ARWfdRmtBreakpoint *, int );
void PrintARMultiSchemaFieldValueStructIndent(char *, ARMultiSchemaFieldFuncValueStruct *);
void PrintARMultiSchemaFieldValueListIndent(char *, ARMultiSchemaFieldFuncValueList *);
void PrintARMultiSchemaFieldValueListList(ARMultiSchemaFieldFuncValueListList *);

