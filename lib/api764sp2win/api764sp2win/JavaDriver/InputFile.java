package com.bmc.arsys.demo.javadriver;

import java.io.*;

public class InputFile {
    BufferedReader fp = null;
    String name = null;
    boolean isStdIn = false;
    long position = 0;

    public InputFile(String fileName) throws FileNotFoundException {
        name = fileName;
        fp = new BufferedReader(new FileReader(fileName));
        isStdIn = false;
        position = 0;
    }

    public InputFile(InputStream in, boolean flag) {
        name = "stdin";
        fp = new BufferedReader(new InputStreamReader(in));
        isStdIn = flag;
    }

    public void addToCurrentPosition(long increment) {
        position = position + increment;
    }

    public long getCurrentPosition() {
        return position;
    }

    public void setCurrentPosition(long pos) {
        position = pos;
    }

    public void close() throws IOException {
        name = null;

        if (!isStdIn) {
            if (fp != null) {
                fp.close();
            }
        }

        fp = null;
    }

    public boolean isFileStdIn() {
        return isStdIn;
    }

    public String getFileName() {
        return name;
    }

    public BufferedReader getFileReader() {
        return fp;
    }
}