package com.bmc.arsys.demo.javadriver;

/**
 * @author yuhuang
 *
 */
public class LocaleCharSet
{

	private String m_locale;
	private String m_charSet;
	/**
	 * default constructor
	 *
	 */
	public LocaleCharSet()
	{
	}
	/**
	 * Passing locale[.charset]
	 */
	public LocaleCharSet(String localeStr)
	{
		parseLocaleAndCharSet(localeStr);
	}

	/**
	 * 
	 * @param locale
	 * @param charSet
	 */
	public LocaleCharSet(String locale, String charSet){
		m_locale = locale;
		m_charSet = charSet;
	}
    public String getCharSet()
	{
		return m_charSet;
	}

	public void setCharSet(String charSet)
	{
		m_charSet = charSet;
	}
	public String getLocale()
	{
		return m_locale;
	}

	public void setLocale(String locale)
	{
		m_locale = locale;
	}
	
	/**
	 * Parsing the parameter in format of locale[.charset]
	 * @param localeCharSetStr
	 */
	public void parseLocaleAndCharSet(String localeCharSetStr){
		if (localeCharSetStr == null) {
			m_locale = "";
			m_charSet = "";
		} else {
			int idx = localeCharSetStr.indexOf(".");
			if (idx > -1){                        //has charset
				m_locale = localeCharSetStr.substring(0, idx);
				m_charSet = localeCharSetStr.substring(idx+1);
			} else if (localeCharSetStr.length() > 0){   //only has locale
				m_locale = localeCharSetStr; 
				m_charSet = "";
			} else {
				m_locale = "";
				m_charSet = "";
			}
		}
	}
    
    public String toString(){
        String dispStr = m_locale;
        if (m_charSet != null && m_charSet.length() > 0){
        	dispStr = (m_locale != null) ? m_locale : "";
     	   	dispStr += "."+m_charSet;
        }
        return dispStr;
    }
}