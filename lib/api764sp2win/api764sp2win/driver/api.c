/* File: api.c */

#include <stdlib.h>
#ifdef _WIN32
#include <time.h>
#ifndef snprintf
#define  snprintf _snprintf
#endif
#define  strcasecmp  _stricmp    /* Case-insensitive compare on Windows */
#else /* _WIN32 */
#include <unistd.h>
#endif /* _WIN32 */
#include <stdio.h>
#include <string.h>

#include "ar.h"
#include "arextern.h"
#include "arfree.h"
#include "api.h"
#include "get.h"
#include "globals.h"
#include "print.h"
#include "util.h"
#include "wfd_shared.h"
#include "arerrno.h"

#ifndef OUTPUT_MODE
void BeginAPICall()
{
   /* perform any generic pre-call operations here */
}

void EndAPICall(status)
ARStatusList *status;  /* IN; status list */
{
   /* perform any generic post-call operations here */
}
#else
void BeginAPICall();
void EndAPICall();
#endif

static ARBoolean bSvcEntryCall;


/*****************************************************************************/
/*                                                                           */
/*                           APIPrintStatusHistory                           */
/*                                                                           */
/*****************************************************************************/

int APIPrintStatusHistory(header, value)
char      *header;            /* IN; header string for the value */
char      *value;             /* IN; value to decode and print */

{
   int                   result;
   ARStatusHistoryList   statHistList;
   ARStatusList          status = {0, NULL};

   /* Call routine to decode the value */
   result = ARDecodeStatusHistory(GetControlStructPtr(), value, &statHistList,
                                  &status);

   /* Print results */
   if (result >= AR_RETURN_ERROR)
   {
      /* if its ServiceEntry call then ignore decode error and */
      /* print value as string                                 */
      if (bSvcEntryCall)
      {
         FreeARStatusList(&status, FALSE);
         PrintChar(header, (value == NULL) ? " " : value);         
         return result;
      }
      else
      {
         DriverPrintResult("%s   DECODE STATUS-HISTORY FAILURE\n", header);
         PrintARStatusList(&status);
      }
   }
   else
      PrintARStatusHistoryList(header, &statHistList);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARStatusHistoryList(&statHistList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIPrintDiary                                */
/*                                                                           */
/*****************************************************************************/

int APIPrintDiary(header, value)
char      *header;            /* IN; header string for the value */
char      *value;             /* IN; value to decode and print */

{
   ARDiaryList    diaryList;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Call routine to decode the value */
   result = ARDecodeDiary(GetControlStructPtr(), value, &diaryList, &status);

   /* Print results */
   if (result >= AR_RETURN_ERROR)
   {
      /* if it's a ServiceEntry call or if the format is wrong */
      /* then ignore decode error and print value as string    */
      if( bSvcEntryCall || (status.statusList->messageNum == AR_ERROR_DIARY_FORMAT) )
      {
         FreeARStatusList(&status, FALSE);
         PrintChar(header, (value == NULL) ? " " : value);         
         return result;
      }
      else
      {
         DriverPrintResult("%s   DECODE DIARY FAILURE\n", header);
         PrintARStatusList(&status);
      }
   }
   else
      PrintARDiaryList(header, &diaryList);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARDiaryList(&diaryList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIPrintDiaryList                              */
/*                                                                           */
/*****************************************************************************/

void APIPrintDiaryList(header, header2, value)
char             *header;     /* IN; header string for the value */
char             *header2;    /* IN; header string for 2nd+ item */
ARTextStringList *value;      /* IN; value to print */

{
   unsigned int      i;       /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      APIPrintDiary(header2, value->stringList[i]);
}


/*****************************************************************************/
/**********                                                         **********/
/**********                          ENTRY                          **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                               APIARGetEntry                               */
/*                                                                           */
/*****************************************************************************/

int APIARGetEntry()

{
   AREntryIdList      entryId;
   ARFieldValueList   fieldList;
   ARInternalIdList   idList;
   unsigned int       numItems;
   int                result;
   ARNameType         schema;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else      
      entryId.entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("GetEntry");
      return AR_RETURN_ERROR;
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
      strcpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""));
   DriverPrintPrompt("Ids of fields to retrieve:\n");
   GetARInternalIdList(&idList);

   /* Call routine */
   BeginAPICall();
   result = ARGetEntry(GetControlStructPtr(), schema, &entryId, &idList,
                       &fieldList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetEntry");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARFieldValueList(&fieldList);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARInternalIdList(&idList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARFieldValueList(&fieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeAREntryIdList(&entryId, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetMultipleEntries                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleEntries()

{
   AREntryIdListList      entryId;
   ARBooleanList          existList;
   ARFieldValueListList   fieldList;
   unsigned int           i;
   ARInternalIdList       idList;
   unsigned int           j;
   unsigned int           numEntries;
   unsigned int           numItems;
   int                    result;
   ARNameType             schema;
   ARStatusList           status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE ENTRIES");
   strcpy(schema, GetChar("Schema: ", ""));
   numEntries = GetUnsignedInt("Number of Entries in EntryId List ? (1): ", 1);
   entryId.numItems = numEntries;
   if (numEntries == 0)
      entryId.entryIdList = NULL;
   else
      entryId.entryIdList = (AREntryIdList *) malloc(numEntries *
                                                     sizeof(AREntryIdList));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("GetMultipleEntries");
      return AR_RETURN_ERROR;
   }
   for (i = 0; i < numEntries; i++)
   {
      DriverPrintPrompt("Enter entry id number %u\n", i + 1);
      numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
      entryId.entryIdList[i].numItems = numItems;
      if (numItems == 0)
         entryId.entryIdList[i].entryIdList = NULL;
      else         
         entryId.entryIdList[i].entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
       if (entryId.entryIdList[i].entryIdList == NULL)
       {
          DriverPrintMallocCallError("GetMultipleEntries");
          free(entryId.entryIdList);
          return AR_RETURN_ERROR;
       }
       for (j = 0; j < entryId.entryIdList[i].numItems; j++)
       {
          strcpy(entryId.entryIdList[i].entryIdList[j],
                 GetChar("Entry Id: ", ""));
       }
   }
   DriverPrintPrompt("Ids of fields to retrieve:\n");
   GetARInternalIdList(&idList);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleEntries(GetControlStructPtr(), schema, &entryId,
                                 &idList, &existList, &fieldList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleEntries");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARBooleanList("Entry exists list", &existList);
      PrintARFieldValueListList(&fieldList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARInternalIdList(&idList, FALSE);
   FreeARBooleanList(&existList, FALSE);
   FreeARFieldValueListList(&fieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeAREntryIdListList(&entryId, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                               APIARSetEntry                               */
/*                                                                           */
/*****************************************************************************/

int APIARSetEntry()

{
   AREntryIdList      entryId;
   ARFieldValueList   fieldList;
   ARTimestamp        getTime;
   unsigned int       numItems;
   unsigned int       option;
   int                result;
   ARNameType         schema;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else
      entryId.entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("SetEntry");
      return AR_RETURN_ERROR;
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
      strcpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""));
   DriverPrintPrompt("Field/value pairs to set:\n");
   GetARFieldValueList(&fieldList);
   getTime = GetARTimestamp("Time of Get operation (0): ", (ARTimestamp) 0);
   option = GetUnsignedInt("SetEntry option ? (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARSetEntry(GetControlStructPtr(), schema, &entryId, &fieldList,
                       getTime, option, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetEntry");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARFieldValueList(&fieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeAREntryIdList(&entryId, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                               APIARSetGetEntry                            */
/*                                                                           */
/*****************************************************************************/

int APIARSetGetEntry()

{
   AREntryIdList      entryId;
   ARStatusList       geStatus = {0, NULL};
   ARFieldValueList   getFieldList;
   ARTimestamp        getTime;
   ARInternalIdList   idList;
   unsigned int       numItems;
   unsigned int       option;
   int                result;
   ARNameType         schema;
   ARStatusList       seStatus = {0, NULL};
   ARFieldValueList   setFieldList;

   /* Get parameters */
   DriverPrintHeader("SET GET ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else
      entryId.entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("SetGetEntry");
      return AR_RETURN_ERROR;
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
      strcpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""));
   DriverPrintPrompt("Field/value pairs to set:\n");
   GetARFieldValueList(&setFieldList);
   getTime = GetARTimestamp("Time of Get operation (0): ", (ARTimestamp) 0);
   option = GetUnsignedInt("SetEntry option ? (0): ", 0);

   DriverPrintPrompt("Ids of fields to retrieve:\n");
   GetARInternalIdList(&idList);

   /* Call routine */
   BeginAPICall();   
   result = ARSetGetEntry(GetControlStructPtr(), schema, &entryId, &setFieldList,
                          getTime, option, &idList, &getFieldList, &seStatus,
                          &geStatus);
   if (seStatus.numItems > 0)
      EndAPICall(&seStatus);
   else
      EndAPICall(&geStatus);
   
   /* Print results */
   DriverPrintResultHeader("ARSetGetEntry");
   if (result < AR_RETURN_ERROR)
      PrintARFieldValueList(&getFieldList);

   PrintReturnCode(result);
   DriverPrintHeader("SetEntry status - seStatus");
   PrintARStatusList(&seStatus);
   DriverPrintHeader("GetEntry status - geStatus");
   PrintARStatusList(&geStatus);

   /* Cleanup */
   FreeARFieldValueList(&setFieldList, FALSE);
   FreeARFieldValueList(&getFieldList, FALSE);
   FreeARStatusList(&seStatus, FALSE);
   FreeARStatusList(&geStatus, FALSE);
   FreeAREntryIdList(&entryId, FALSE);
   FreeARInternalIdList(&idList, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARCreateEntry                             */
/*                                                                           */
/*****************************************************************************/

int APIARCreateEntry()

{
   AREntryIdType      entryId;
   ARFieldValueList   fieldList;
   int                result;
   ARNameType         schema;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("CREATE ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   DriverPrintPrompt("Field/value pairs to create:\n");
   GetARFieldValueList(&fieldList);

   /* Call routine */
   BeginAPICall();
   result = ARCreateEntry(GetControlStructPtr(), schema, &fieldList, entryId,
                          &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateEntry");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Entry id: ", entryId);
      SaveCreateEntryId(entryId);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARFieldValueList(&fieldList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARDeleteEntry                             */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteEntry()

{
   AREntryIdList   entryId;
   unsigned int    numItems;
   unsigned int    option;
   int             result;
   ARNameType      schema;
   ARStatusList    status = {0, NULL};
   

   /* Get parameters */
   DriverPrintHeader("DELETE ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else
      entryId.entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("DeleteEntry");
      return AR_RETURN_ERROR;
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
      strcpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""));
   option = GetUnsignedInt("DeleteEntry option ? (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteEntry(GetControlStructPtr(), schema, &entryId, option,
                          &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteEntry");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   FreeAREntryIdList(&entryId, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARMergeEntry                              */
/*                                                                           */
/*****************************************************************************/

int APIARMergeEntry()

{
   AREntryIdType      entryId;
   ARFieldValueList   fieldList;
   unsigned int       mergeType;
   unsigned int       multiMatchOption;
   ARQualifierStruct  qualifier;
   int                result;
   ARNameType         schema;
   ARStatusList       status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("MERGE ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   DriverPrintPrompt("Field/value pairs to merge:\n");
   GetARFieldValueList(&fieldList);
   mergeType = GetUnsignedInt(
       "Operation on duplicate entry (error, new ID, overwrite, merge) (1-4): ", 4);

   GetARQualifierStruct(&qualifier);
   multiMatchOption = GetUnsignedInt("Multimatch option (0): ",0);

   /* Call routine */
   BeginAPICall();
   result = ARMergeEntry(GetControlStructPtr(), schema, &fieldList, mergeType,
                         &qualifier, multiMatchOption, entryId, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARMergeEntry");
   PrintReturnCode(result);
   PrintChar("Entry id: ", entryId);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARFieldValueList(&fieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetListEntry                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListEntry()

{
   AREntryListList        entryList;
   unsigned int           firstRetrieve;
   AREntryListFieldList   getListFields;
   unsigned int           maxRetrieve;
   unsigned int           numMatches;
   unsigned int          *numMatchesPtr;
   ARQualifierStruct      qualifier;
   int                    result;
   ARNameType             schema;
   ARSortList             sortList;
   ARStatusList           status = {0, NULL};
   ARBoolean              useLocale = FALSE;

   /* Get parameters */
   DriverPrintHeader("GETLIST ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   GetARQualifierStruct(&qualifier);
   GetAREntryListFieldList(&getListFields);
   GetARSortList(&sortList);
   firstRetrieve = GetUnsignedInt("First Retrieve (0): ", 0);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ",
                                500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Use Locale for search? (F): ", FALSE))
      useLocale = TRUE;

   /* Call routine */
   BeginAPICall();
   result = ARGetListEntry(GetControlStructPtr(), schema, &qualifier,
                           &getListFields, &sortList, firstRetrieve,
                           maxRetrieve, useLocale, &entryList, numMatchesPtr,
                           &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListEntry");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintAREntryListList(&entryList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
      SaveEntryListEntryIds(&entryList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeAREntryListFieldList(&getListFields, FALSE);
   FreeARSortList(&sortList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeAREntryListList(&entryList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetListEntryWithFields                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetListEntryWithFields()

{
   AREntryListFieldValueList   entryList;
   unsigned int                firstRetrieve;
   AREntryListFieldList        getListFields;
   unsigned int                maxRetrieve;
   unsigned int                numMatches;
   unsigned int               *numMatchesPtr;
   ARQualifierStruct           qualifier;
   int                         result;
   ARSortList                  sortList;
   ARNameType                  schema;
   ARStatusList                status = {0, NULL};
   ARBoolean                   useLocale = FALSE;

   /* Get parameters */
   DriverPrintHeader("GETLIST ENTRY WITH FIELDS");
   strcpy(schema, GetChar("Schema: ", ""));
   GetARQualifierStruct(&qualifier);
   GetAREntryListFieldList(&getListFields);
   GetARSortList(&sortList);
   firstRetrieve = GetUnsignedInt("First Retrieve (0): ", 0);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ",
                                500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Use Locale for search? (F): ", FALSE))
      useLocale = TRUE;

   /* Call routine */
   BeginAPICall();
   result = ARGetListEntryWithFields(GetControlStructPtr(), schema, &qualifier,
                                     &getListFields, &sortList, firstRetrieve,
                                     maxRetrieve, useLocale, &entryList,
                                     numMatchesPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListEntryWithFields");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintAREntryListFieldValueList(&entryList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
      SaveEntryListFieldValueEntryIds(&entryList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeAREntryListFieldList(&getListFields, FALSE);
   FreeARSortList(&sortList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeAREntryListFieldValueList(&entryList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetOneEntryWithFields                         */
/*                                                                           */
/*****************************************************************************/

int APIARGetOneEntryWithFields()

{
   AREntryListFieldValueList   entryList;
   AREntryListFieldList        getListFields;
   unsigned int                numMatches;
   unsigned int               *numMatchesPtr;
   ARQualifierStruct           qualifier;
   int                         result;
   ARSortList                  sortList;
   ARNameType                  schema;
   ARStatusList                status = {0, NULL};
   ARBoolean                   useLocale = FALSE;

   /* Get parameters */
   DriverPrintHeader("GETONE ENTRY WITH FIELDS");
   strcpy(schema, GetChar("Schema: ", ""));
   GetARQualifierStruct(&qualifier);
   GetAREntryListFieldList(&getListFields);
   GetARSortList(&sortList);

   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Use Locale for search? (F): ", FALSE))
      useLocale = TRUE;

   /* Call routine */
   BeginAPICall();
   result = ARGetOneEntryWithFields(GetControlStructPtr(), schema, &qualifier,
                                    &getListFields, &sortList, useLocale, &entryList,
                                    numMatchesPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetOneEntryWithFields");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintAREntryListFieldValueList(&entryList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
      SaveEntryListFieldValueEntryIds(&entryList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeAREntryListFieldList(&getListFields, FALSE);
   FreeARSortList(&sortList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeAREntryListFieldValueList(&entryList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetListEntryBlocks                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetListEntryBlocks()

{
   AREntryBlockList       entryBlockList = {0, NULL};
   unsigned int           firstRetrieve;
   AREntryListFieldList   getListFields;
   unsigned int           maxRetrieve;
   unsigned int           numMatches;
   unsigned int          *numMatchesPtr;
   unsigned int           numRows;
   unsigned int           numRowsPerBlock;
   unsigned int          *numRowsPtr;
   ARQualifierStruct      qualifier;
   int                    result;
   ARNameType             schema;
   ARSortList             sortList;
   ARStatusList           status = {0, NULL};
   ARBoolean              useLocale = FALSE;

   /* Get parameters */
   DriverPrintHeader("GETLIST ENTRY BLOCKS");
   strcpy(schema, GetChar("Schema: ", ""));
   GetARQualifierStruct(&qualifier);
   GetAREntryListFieldList(&getListFields);
   GetARSortList(&sortList);
   numRowsPerBlock = GetUnsignedInt("Number of Rows Per Block (0): ", 0);
   firstRetrieve = GetUnsignedInt("First Retrieve (0): ", 0);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ", 500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Get number of returned rows? (F): ", FALSE))
      numRowsPtr = &numRows;
   else
      numRowsPtr = NULL;

   if (GetARBoolean("Use Locale for search? (F): ", FALSE))
      useLocale = TRUE;

   /* Call routine */
   BeginAPICall();
   result = ARGetListEntryBlocks(GetControlStructPtr(), schema, &qualifier,
                                 &getListFields, &sortList, numRowsPerBlock,
                                 firstRetrieve, maxRetrieve, useLocale,
                                 &entryBlockList, numRowsPtr, numMatchesPtr,
                                 &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListEntryBlocks");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      unsigned int i;
      for (i = 0; i < entryBlockList.numItems; i++)
      {
         AREntryListFieldValueList   entryList;

         ARGetEntryBlock(&entryBlockList, i, &entryList, &status);
         PrintAREntryListFieldValueList(&entryList);
         FreeAREntryListFieldValueList(&entryList, FALSE);
      }
      PrintAREntryBlockList(&entryBlockList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
      if (numRowsPtr != NULL)
         PrintUInt("Number of returned rows: ", numRows);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeAREntryListFieldList(&getListFields, FALSE);
   FreeAREntryBlockList(&entryBlockList, FALSE);
   FreeARSortList(&sortList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetEntryStatistics                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetEntryStatistics()

{
   ARInternalIdList            groupByList;
   ARQualifierStruct           qualifier;
   int                         result;
   ARStatisticsResultList      results;
   ARNameType                  schema;
   unsigned int                statistic;
   ARStatusList                status = {0, NULL};
   ARFieldValueOrArithStruct   target;

   /* Get parameters */
   DriverPrintHeader("GET ENTRY STATISTICS");
   strcpy(schema, GetChar("Schema: ", ""));
   GetARQualifierStruct(&qualifier);
   DriverPrintPrompt("Target Expression:\n");
   GetARFieldValueOrArithStruct(&target);
   statistic = GetUnsignedInt(
        "Statistic operation to perform (Count,Sum,Avg,Min,Max) (1-5)(1): ", 1);
   DriverPrintPrompt("Ids of fields to group statistics by:\n");
   GetARInternalIdList(&groupByList);

   /* Call routine */
   BeginAPICall();
   result = ARGetEntryStatistics(GetControlStructPtr(), schema, &qualifier,
                                 &target, statistic, &groupByList, &results,
                                 &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetEntryStatistics");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARStatisticsResultList("", &results);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeARFieldValueOrArithStruct(&target, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARStatisticsResultList(&results, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetEntryBLOB                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetEntryBLOB()

{
   AREntryIdList   entryId;
   ARInternalId    fieldId;
   ARLocStruct     loc;
   unsigned int    numItems;
   int             result;
   ARNameType      schema;
   ARStatusList    status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GET ENTRY BLOB");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else
      entryId.entryIdList = (AREntryIdType *) 
                              malloc(numItems * sizeof(AREntryIdType));
   if (entryId.entryIdList == NULL)
   {
      DriverPrintMallocCallError("GetEntryBLOB");
      return AR_RETURN_ERROR;
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
      strcpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""));
   fieldId = GetLong("Field Id: ", 1L);
   GetARLocStructForGet(&loc);

   /* Call routine */
   BeginAPICall();
   result = ARGetEntryBLOB(GetControlStructPtr(), schema, &entryId, 
                           fieldId, &loc, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetEntryBLOB");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   FreeARLocStruct(&loc, FALSE);
   FreeAREntryIdList(&entryId, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARServiceEntry                             */
/*                                                                           */
/*****************************************************************************/

int APIARServiceEntry()
{ 
   AREntryIdList      entryId;
   ARInternalIdList   idList;
   ARFieldValueList   inputFieldList;
   unsigned int       numItems;
   ARFieldValueList   outputFieldList;
   ARInternalIdList  *ptrIdList = NULL;
   int                result;
   ARNameType         schema;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SERVICE ENTRY");
   strcpy(schema, GetChar("Schema: ", ""));
   numItems = GetUnsignedInt("Number of Items in EntryId ? (1): ", 1);
   entryId.numItems = numItems;
   if (numItems == 0)
      entryId.entryIdList = NULL;
   else
   {
      entryId.entryIdList = (AREntryIdType *) calloc(numItems, sizeof(AREntryIdType));
      if (entryId.entryIdList == NULL)
      {
         DriverPrintMallocCallError("ServiceEntry");
         return AR_RETURN_ERROR;
      }
   }

   for (numItems = 0; numItems < entryId.numItems; numItems++)
   {
      strncpy(entryId.entryIdList[numItems], GetChar("Entry Id: ", ""),
              AR_MAX_ENTRYID_SIZE);
   }

   DriverPrintPrompt("Field/value pairs to service:\n");
   GetARFieldValueList(&inputFieldList);
   DriverPrintPrompt("Ids of fields to retrieve:\n");
   GetARInternalIdList(&idList);

   if (idList.numItems != 0)
      ptrIdList = &idList;

   /* Call routine */
   BeginAPICall();
   result = ARServiceEntry(GetControlStructPtr(), schema, &entryId, &inputFieldList,
                           ptrIdList, &outputFieldList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARServiceEntry");
   PrintReturnCode(result);
   bSvcEntryCall = TRUE;
   if (result < AR_RETURN_ERROR)
      PrintARFieldValueList(&outputFieldList);
   PrintARStatusList(&status);
   bSvcEntryCall = FALSE;

   /* Cleanup */
   FreeARFieldValueList(&inputFieldList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARFieldValueList(&outputFieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeAREntryIdList(&entryId, FALSE);

   if (ptrIdList)
      FreeARInternalIdList(ptrIdList, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARLoadARQualifierStruct                         */
/*                                                                           */
/*****************************************************************************/

int APIARLoadARQualifierStruct()

{
   ARNameType          displayTag;
   ARQualifierStruct   qualifier;
   char                qualString[1024];
   int                 result;
   ARNameType          schema;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("LOAD ARQualifierStruct");
   strcpy(schema, GetChar("Schema: ", ""));
   strcpy(displayTag, GetChar("Display Tag (): ", ""));
   strcpy(qualString, GetChar("Qualification string: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARLoadARQualifierStruct(GetControlStructPtr(), schema, displayTag,
                                    qualString, &qualifier, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARLoadARQualifierStruct");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARQualifierStruct("", &qualifier);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&qualifier, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/**********                                                         **********/
/**********                         STRUCTURE                       **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                             APIARGetContainer                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetContainer()
{
   ARInternalIdList          admingrpList;
   ARPermissionList          assignedGroupList;
   char                     *changeDiary;
   ARNameType                containerName;
   char                     *description;
   ARPermissionList          groupList;
   char                     *helpText;
   char                     *label;
   ARAccessNameType          lastChanged;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   ARContainerOwnerObjList   ownerObjList;
   ARReferenceList           references;
   ARReferenceTypeList       refTypes;
   int                       result;
   ARStatusList              status = {0, NULL};
   ARTimestamp               timestamp;
   unsigned int              type;
   
   /* Get parameters */
   DriverPrintHeader("GET CONTAINER");
   strcpy(containerName, GetChar("Container name: ", ""));
   GetARReferenceTypeList(&refTypes, "   ");
   
   /* Call routine */
   BeginAPICall();
   result = ARGetContainer(GetControlStructPtr(), containerName, &refTypes,
                           &assignedGroupList, &groupList, &admingrpList,
                           &ownerObjList, &label, &description, &type,
                           &references, &helpText, owner, &timestamp,
                           lastChanged, &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetContainer");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Assigned Group List: ", "");
      PrintARPermissionList(&assignedGroupList, FALSE);
      PrintChar("Complete Group List: ", "");
      PrintARPermissionList(&groupList, FALSE);
      PrintARInternalIdList("Sub Admin Group list: ", "   ",
                            &admingrpList);
      PrintARContainerOwnerObjList(&ownerObjList);
      PrintChar("Label: ", label);
      PrintChar("Description: ", description);
      PrintUInt("Type:", type);
      PrintARReferenceList(&references);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARPermissionList(&groupList, FALSE);
      FreeARInternalIdList(&admingrpList, FALSE);
      FreeARReferenceList(&references, FALSE);
      FreeARPropList(&objPropList, FALSE);
      FreeARContainerOwnerObjList(&ownerObjList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
   }
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARSetContainer                             */
/*                                                                           */
/*****************************************************************************/

int APIARSetContainer()
{
   ARInternalIdList          admingrpList;
   ARInternalIdList         *admingrpListPtr;
   char                      changeDiary[1024];
   char                     *changeDiaryPtr;
   ARNameType                containerName;
   char                      description[ARMAX_CON_DESCRIPTION_LEN + 1];
   char                     *descriptionPtr;
   ARPermissionList          groupList;
   ARPermissionList         *groupListPtr;
   char                      helpText[1024];
   char                     *helpTextPtr;
   char                      label[256];
   char                     *labelPtr;
   ARNameType                newName;
   char                     *newNamePtr;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARPropList               *objPropListPtr;
   ARAccessNameType          owner;
   ARContainerOwnerObjList   ownerObjList;
   ARContainerOwnerObjList  *ownerObjListPtr;
   char                     *ownerPtr;
   ARReferenceList           references;
   ARReferenceList          *referencesPtr;
   ARBoolean                 removeFlag;
   int                       result;
   ARStatusList              status = {0, NULL};
   unsigned int              type;
   unsigned int             *typePtr;

   /* Get parameters */
   DriverPrintHeader("SET CONTAINER");
   strcpy(containerName, GetChar("Container name: ", ""));
   if (GetARBoolean("Change container name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change group list? (F): ", FALSE))
   {
      GetARPermissionList(FALSE, &groupList);
      groupListPtr = &groupList;
   }
   else
      groupListPtr = NULL;
   if (GetARBoolean("Change sub admin group list? (F): ", FALSE))
   {
      DriverPrintPrompt("Ids of sub admin groups allowed to access container:\n");
      GetARInternalIdList(&admingrpList);
      admingrpListPtr = &admingrpList;
   }
   else
      admingrpListPtr = NULL;
   if (GetARBoolean("Change container owner object list? (F): ", FALSE))
   {
      GetARContainerOwnerObjList(&ownerObjList, "   ");
      ownerObjListPtr = &ownerObjList;
   }
   else
      ownerObjListPtr = NULL;
   if (GetARBoolean("Change label? (F): ", FALSE))
   {
      strcpy(label, GetChar("   Label (): ", ""));
      labelPtr = label;
   }
   else
      labelPtr = NULL;
   if (GetARBoolean("Change description? (F): ", FALSE))
   {
      strcpy(description, GetChar("   Description (): ", ""));
      descriptionPtr = description;
   }
   else
      descriptionPtr = NULL;
   if (GetARBoolean("Change container type? (F): ", FALSE))
   {
      type = GetUnsignedInt("Active Link Guide, Application, Packing List, Filter Guide (1-4) (2): ", 2);
      typePtr = &type;
   }
   else
      typePtr = NULL;
   if (GetARBoolean("Change reference list? (F): ", FALSE))
   {
      GetARReferenceList(&references);
      referencesPtr = &references;
      removeFlag = GetARBoolean("Remove invalid references? (F): ", FALSE);
   }
   else
      referencesPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetContainer(GetControlStructPtr(), containerName, newNamePtr,
                           groupListPtr, admingrpListPtr, ownerObjListPtr, labelPtr,
                           descriptionPtr, typePtr, referencesPtr, removeFlag,
                           helpTextPtr, ownerPtr, changeDiaryPtr,
                           objPropListPtr, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetContainer");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (groupListPtr != NULL)
      FreeARPermissionList(&groupList, FALSE);
   if (admingrpListPtr != NULL)
      FreeARInternalIdList(&admingrpList, FALSE);
   if (ownerObjListPtr != NULL)
      FreeARContainerOwnerObjList(&ownerObjList, FALSE);
   if (referencesPtr != NULL)
      FreeARReferenceList(&references, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARCreateContainer                            */
/*                                                                           */
/*****************************************************************************/

int APIARCreateContainer()
{
   ARInternalIdList          admingrpList;
   char                      changeDiary[1024];
   ARNameType                containerName;
   char                      description[ARMAX_CON_DESCRIPTION_LEN + 1];
   ARPermissionList          groupList;
   char                      helpText[1024];
   char                      label[256];
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   ARContainerOwnerObjList   ownerObjList;
   ARReferenceList           references;
   ARBoolean                 removeFlag;
   int                       result;
   ARStatusList              status = {0, NULL};
   unsigned int              type;
      
   /* Get parameters */
   DriverPrintHeader("CREATE CONTAINER");
   strcpy(containerName, GetChar("Container name: ", ""));
   GetARPermissionList(FALSE, &groupList);
   DriverPrintPrompt("Ids of sub admin groups allowed to access container:\n");
   GetARInternalIdList(&admingrpList);
   GetARContainerOwnerObjList(&ownerObjList, "   ");
   strcpy(label, GetChar("   Label (): ", ""));
   strcpy(description, GetChar("   Description (): ", ""));
   type = GetUnsignedInt("Active Link Guide, Application, Packing List, Filter Guide (1-4) (2): ", 2);
   GetARReferenceList(&references);
   removeFlag = GetARBoolean("Remove invalid references? (F): ", FALSE);
   strcpy(helpText, GetChar("Help Text (): ", ""));
   strcpy(owner, GetChar("Owner (): ", ""));
   strcpy(changeDiary, GetChar("Change Diary (): ", ""));
   GetARPropList(&objPropList, FALSE);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateContainer(GetControlStructPtr(), containerName, &groupList,
                              &admingrpList, &ownerObjList, label, description,
                              type, &references, removeFlag, helpText, owner,
                              changeDiary, &objPropList, objectModificationLogLabel,
                              &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateContainer");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARPermissionList(&groupList, FALSE);
   FreeARInternalIdList(&admingrpList, FALSE);
   FreeARContainerOwnerObjList(&ownerObjList, FALSE);
   FreeARReferenceList(&references, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARDeleteContainer                            */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteContainer()
{
   ARNameType     containerName;
   unsigned int   deleteOption;
   char          *objectModificationLogLabel = NULL;
   int            result;
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("DELETE CONTAINER");
   strcpy(containerName, GetChar("Container name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteContainer(GetControlStructPtr(), containerName, deleteOption, 
                              objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteContainer");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetListContainer                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetListContainer()
{
   unsigned int              attributes;
   ARTimestamp               changedSince;
   ARContainerInfoList       containerList;
   ARContainerTypeList       containerTypes;
   ARContainerOwnerObjList   ownerObjList;
   int                       result;
   ARPropList                serverObjPropList = {0, NULL};
   ARStatusList              status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GETLIST CONTAINER");
   changedSince = GetARTimestamp("Get all changed since (0): ",
                                 (ARTimestamp) 0);
   GetARContainerTypeList(&containerTypes, "   ");
   attributes = GetUnsignedInt("Container attributes (none, hidden) (0-1) (0): ", 0);
   GetARContainerOwnerObjList(&ownerObjList, "   ");
   GetARPropList(&serverObjPropList, FALSE);
   
   /* Call routine */
   BeginAPICall();
   result = ARGetListContainer(GetControlStructPtr(), changedSince,
                               &containerTypes, attributes, &ownerObjList, 
                               &serverObjPropList, &containerList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListContainer");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARContainerInfoList("Container list: ", "   ", &containerList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARContainerInfoList(&containerList, FALSE);
   FreeARContainerOwnerObjList(&ownerObjList, FALSE);
   FreeARContainerTypeList(&containerTypes, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetMultipleContainers                         */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleContainers()
{
                                 /* input parameters */
   unsigned int                  attributes;
   ARTimestamp                   changedSince;
   ARContainerTypeList           containerTypes;
   ARNameList                    nameList;
   ARContainerOwnerObjList       ownerObjList;
   ARReferenceTypeList           refTypes;

                                 /* return parameters */
   ARInternalIdListList          admingrpListList;
   ARTextStringList              changeDiaryList;
   ARNameList                    containerNameList;
   ARTextStringList              descriptionList;
   ARBooleanList                 existList;
   ARPermissionListList          groupListList;
   ARTextStringList              helpTextList;
   ARTextStringList              labelList;
   ARAccessNameList              lastChangedList;
   ARPropListList                objPropListList;
   ARAccessNameList              ownerList;
   ARContainerOwnerObjListList   ownerObjListList;
   ARReferenceListList           referencesList;
   ARTimestampList               timestampList;   
   ARUnsignedIntList             typeList;

                                 /* local variables */
   unsigned int                  i;
   int                           result;
   ARStatusList                  status = {0, NULL};
   

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE CONTAINERS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get containers by name:\n");
   GetARNameList(&nameList, "");

   GetARContainerTypeList(&containerTypes, "");
   attributes = GetUnsignedInt("Container attributes (none, hidden) (0-1) (0): ", 0);
   GetARContainerOwnerObjList(&ownerObjList, "");
   GetARReferenceTypeList(&refTypes, "");
   
   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleContainers(GetControlStructPtr(),
                                    changedSince,
                                    &nameList,
                                    &containerTypes,
                                    attributes,
                                    &ownerObjList,
                                    &refTypes,
                                    &existList,
                                    &containerNameList,
                                    (ARPermissionListList *) 0,
                                    &groupListList,
                                    &admingrpListList,
                                    &ownerObjListList,
                                    &labelList,
                                    &descriptionList,
                                    &typeList,
                                    &referencesList,
                                    &helpTextList,
                                    &ownerList,
                                    &timestampList,                                    
                                    &lastChangedList,
                                    &changeDiaryList,
                                    &objPropListList,
                                    &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleContainers");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      for (i = 0; i < existList.numItems; i++)
      {
         PrintChar("Container name: ", containerNameList.nameList[i]);
         PrintARBoolean("Exists: ", existList.booleanList[i]);

         if (existList.booleanList[i])
         {  
            PrintARPermissionList(&groupListList.permissionList[i], FALSE);
            PrintARInternalIdList("Sub Admin group list: ", "   ",
                                  &admingrpListList.internalIdListList[i]);
            PrintARContainerOwnerObjList(&ownerObjListList.ownerObjListList[i]);
            PrintChar("Label: ", labelList.stringList[i]);
            PrintChar("Description: ", descriptionList.stringList[i]);
            PrintUInt("Type:", typeList.intList[i]);
            PrintARReferenceList(&referencesList.referenceListList[i]);
            PrintChar("Help Text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Last update time: ", timestampList.timestampList[i]);
            PrintChar("Owner: ", ownerList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("Object properties: ", "   ", &objPropListList.propsList[i]);
         }
         PrintChar("", "");
      }
   }
   
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
                           /* free output parameters */
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&containerNameList, FALSE);
      FreeARPermissionListList(&groupListList, FALSE);
      FreeARInternalIdListList(&admingrpListList, FALSE);
      FreeARContainerOwnerObjListList(&ownerObjListList, FALSE);
      FreeARTextStringList(&labelList, FALSE);
      FreeARTextStringList(&descriptionList, FALSE);
      FreeARUnsignedIntList(&typeList, FALSE);
      FreeARReferenceListList(&referencesList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownerList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }

                           /* free input parameters */
   FreeARNameList(&nameList, FALSE);
   FreeARContainerTypeList(&containerTypes, FALSE);
   FreeARContainerOwnerObjList(&ownerObjList, FALSE);
   FreeARReferenceTypeList(&refTypes, FALSE);

   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARGetSchema                               */
/*                                                                           */
/*****************************************************************************/

int APIARGetSchema()

{
   ARInternalIdList          admingrpList;
   ARArchiveInfoStruct       archiveInfo;
   ARPermissionList          assignedGroupList;
   ARAuditInfoStruct         auditInfo;
   char                     *changeDiary;
   ARCompoundSchema          comSchema;
   ARNameType                defaultVui;
   AREntryListFieldList      getListFields;
   ARPermissionList          groupList;
   char                     *helpText;
   ARIndexList               indexList;
   ARAccessNameType          lastChanged;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   int                       result;
   ARSchemaInheritanceList   schemaInheritanceList;
   ARNameType                schemaName;
   ARSortList                sortList;
   ARStatusList              status = {0, NULL};
   ARTimestamp               timestamp;
   
   /* Get parameters */
   DriverPrintHeader("GET SCHEMA");
   strcpy(schemaName, GetChar("Schema name: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetSchema(GetControlStructPtr(), schemaName, &comSchema,
                        &schemaInheritanceList, &assignedGroupList, &groupList,
                        &admingrpList, &getListFields, &sortList, &indexList,
                        &archiveInfo, &auditInfo, defaultVui, &helpText, &timestamp,
                        owner, lastChanged, &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetSchema");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARCompoundSchema(&comSchema);
      PrintChar("Assigned Group List: ", "");
      PrintARPermissionList(&assignedGroupList, FALSE);
      PrintChar("Complete Group List: ", "");
      PrintARPermissionList(&groupList, FALSE);
      PrintARInternalIdList("Sub Admin Group list: ", "   ", &admingrpList);
      PrintAREntryListFieldList(&getListFields);
      PrintARSortList(&sortList);
      PrintARIndexList(&indexList);
      PrintARArchiveInfo(&archiveInfo);
      PrintARAuditInfo(&auditInfo);
      PrintChar("Default VUI: ", defaultVui);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARCompoundSchema(&comSchema, FALSE);
      FreeARPermissionList(&groupList, FALSE);
      FreeARInternalIdList(&admingrpList, FALSE);
      FreeAREntryListFieldList(&getListFields, FALSE);
      FreeARSortList(&sortList, FALSE);
      FreeARIndexList(&indexList, FALSE);
      FreeARArchiveInfoStruct(&archiveInfo, FALSE);
      FreeARPropList(&objPropList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
   }
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARSetSchema                               */
/*                                                                           */
/*****************************************************************************/

int APIARSetSchema()

{
   ARInternalIdList          admingrpList;
   ARInternalIdList         *admingrpListPtr;
   ARArchiveInfoStruct       archiveInfo;
   ARArchiveInfoStruct      *archiveInfoPtr;
   ARAuditInfoStruct         auditInfo;
   ARAuditInfoStruct        *auditInfoPtr;
   char                      changeDiary[1024];
   char                     *changeDiaryPtr;
   ARCompoundSchema          comSchema;
   ARCompoundSchema         *comSchemaPtr;
   ARNameType                defaultVui;
   char                     *defaultVuiPtr;
   AREntryListFieldList      getListFields;
   AREntryListFieldList     *getListFieldsPtr;
   ARPermissionList          groupList;
   ARPermissionList         *groupListPtr;
   char                      helpText[1024];
   char                     *helpTextPtr;
   ARIndexList               indexList;
   ARIndexList              *indexListPtr;
   ARNameType                newName;
   char                     *newNamePtr;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARPropList               *objPropListPtr;
   ARAccessNameType          owner;
   char                     *ownerPtr;
   int                       result;
   ARSchemaInheritanceList  *schemaInheritanceListPtr = NULL;
   ARNameType                schemaName;
   unsigned int              setOption = 0;
   ARSortList                sortList;
   ARSortList               *sortListPtr;
   ARStatusList              status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("SET SCHEMA");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   if (GetARBoolean("Change schema name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change compound schema info? (F): ", FALSE))
   {
      GetARCompoundSchema(&comSchema);
      comSchemaPtr = &comSchema;
   }
   else
      comSchemaPtr = NULL;
   if (GetARBoolean("Change group list? (F): ", FALSE))
   {
      GetARPermissionList(FALSE, &groupList);
      groupListPtr = &groupList;
   }
   else
      groupListPtr = NULL;
   if (GetARBoolean("Change sub admin group list? (F): ", FALSE))
   {
      DriverPrintPrompt("Ids of sub admin groups allowed to access schema:\n");
      GetARInternalIdList(&admingrpList);
      admingrpListPtr = &admingrpList;
   }
   else
      admingrpListPtr = NULL;
   if (GetARBoolean("Change query list fields? (F): ", FALSE))
   {
      GetAREntryListFieldList(&getListFields);
      getListFieldsPtr = &getListFields;
   }
   else
      getListFieldsPtr = NULL;
   if (GetARBoolean("Change sort list? (F): ", FALSE))
   {
      GetARSortList(&sortList);
      sortListPtr = &sortList;
   }
   else
      sortListPtr = NULL;
   if (GetARBoolean("Change index list? (F): ", FALSE))
   {
      GetARIndexList(&indexList);
      indexListPtr = &indexList;
   }
   else
      indexListPtr = NULL;
   if (GetARBoolean("Change archive info? (F): ", FALSE))
   {
      memset(&archiveInfo, 0, sizeof(ARArchiveInfoStruct));
      GetARArchiveInfoStruct(&archiveInfo);
      archiveInfoPtr = &archiveInfo;
   }
   else
      archiveInfoPtr = NULL;
   if (GetARBoolean("Change audit info? (F): ", FALSE))
   {
      memset(&auditInfo, 0, sizeof(ARAuditInfoStruct));
      GetARAuditInfoStruct(&auditInfo);
      auditInfoPtr = &auditInfo;
   }
   else
      auditInfoPtr = NULL;   
   if (GetARBoolean("Change default VUI? (F): ", FALSE))
   {
      strcpy(defaultVui, GetChar("   Default VUI (): ", ""));
      defaultVuiPtr = defaultVui;
   }
   else
      defaultVuiPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetSchema(GetControlStructPtr(), schemaName, newNamePtr,
                        comSchemaPtr, schemaInheritanceListPtr,
                        groupListPtr, admingrpListPtr, getListFieldsPtr,
                        sortListPtr, indexListPtr, archiveInfoPtr, auditInfoPtr,
                        defaultVuiPtr, helpTextPtr, ownerPtr,
                        changeDiaryPtr, objPropListPtr, setOption, 
                        objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetSchema");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (comSchemaPtr != NULL)
      FreeARCompoundSchema(&comSchema, FALSE);
   if (groupListPtr != NULL)
      FreeARPermissionList(&groupList, FALSE);
   if (admingrpListPtr != NULL)
      FreeARInternalIdList(&admingrpList, FALSE);
   if (getListFieldsPtr != NULL)
      FreeAREntryListFieldList(&getListFields, FALSE);
   if (sortListPtr != NULL)
      FreeARSortList(&sortList, FALSE);
   if (indexListPtr != NULL)
      FreeARIndexList(&indexList, FALSE);
   if (archiveInfoPtr != NULL)
      FreeARArchiveInfoStruct(&archiveInfo, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARCreateSchema                             */
/*                                                                           */
/*****************************************************************************/

int APIARCreateSchema()

{
   ARInternalIdList          admingrpList;
   ARArchiveInfoStruct       archiveInfo;
   ARAuditInfoStruct         auditInfo;
   char                      changeDiary[1024];
   ARCompoundSchema          comSchema;
   ARNameType                defaultVui;
   AREntryListFieldList      getListFields;
   ARPermissionList          groupList;
   char                      helpText[1024];
   ARIndexList               indexList;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   int                       result;
   ARSchemaInheritanceList   schemaInheritanceList = {0, NULL};
   ARNameType                schemaName;
   ARSortList                sortList;
   ARStatusList              status = {0, NULL};
      
   /* Get parameters */
   DriverPrintHeader("CREATE SCHEMA");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   GetARCompoundSchema(&comSchema);
   GetARPermissionList(FALSE, &groupList);
   DriverPrintPrompt("Ids of sub admin groups allowed to access schema:\n");
   GetARInternalIdList(&admingrpList);
   GetAREntryListFieldList(&getListFields);
   GetARSortList(&sortList);
   GetARIndexList(&indexList);
   memset(&archiveInfo, 0, sizeof(ARArchiveInfoStruct));
   if (GetARBoolean("Create archive info? (F): ", FALSE))
      GetARArchiveInfoStruct(&archiveInfo);
   memset(&auditInfo, 0, sizeof(ARAuditInfoStruct));
   if (GetARBoolean("Create audit info? (F): ", FALSE))
      GetARAuditInfoStruct(&auditInfo);
   strcpy(defaultVui, GetChar("Default Vui (): ", ""));
   strcpy(helpText, GetChar("Help Text (): ", ""));
   strcpy(owner, GetChar("Owner (): ", ""));
   strcpy(changeDiary, GetChar("Change Diary (): ", ""));
   GetARPropList(&objPropList, FALSE);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateSchema(GetControlStructPtr(), schemaName, &comSchema,
                           &schemaInheritanceList, &groupList, &admingrpList,
                           &getListFields, &sortList, &indexList, &archiveInfo,
                           &auditInfo, defaultVui, helpText, owner, changeDiary,
                           &objPropList, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateSchema");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARCompoundSchema(&comSchema, FALSE);
   FreeARPermissionList(&groupList, FALSE);
   FreeARInternalIdList(&admingrpList, FALSE);
   FreeAREntryListFieldList(&getListFields, FALSE);
   FreeARSortList(&sortList, FALSE);
   FreeARIndexList(&indexList, FALSE);
   FreeARArchiveInfoStruct(&archiveInfo, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARDeleteSchema                             */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteSchema()

{
   unsigned int   deleteOption;
   char          *objectModificationLogLabel = NULL;
   int            result;
   ARNameType     schemaName;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE SCHEMA");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteSchema(GetControlStructPtr(), schemaName, deleteOption, 
                           objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteSchema");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetListSchema                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListSchema()

{
   ARTimestamp        changedSince;
   ARInternalIdList   fieldIdList;
   ARNameType         joinName;
   int                result;
   ARNameList         schemaList;
   unsigned int       schemaType;
   ARPropList         searchableObjPropList = {0, NULL};
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST SCHEMA");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   schemaType = GetUnsignedInt(
       "Type of schema (all,regular,join,view,uplink,downlink,dialog,\n                vendor,allow_mfsearch) (0-6,8-9) (0): ", 0);
   if (GetARBoolean("Include hidden schemas (T): ", TRUE))
      schemaType += AR_HIDDEN_INCREMENT;
   if (((schemaType % AR_HIDDEN_INCREMENT) == AR_LIST_SCHEMA_UPLINK) ||
       ((schemaType % AR_HIDDEN_INCREMENT) == AR_LIST_SCHEMA_DOWNLINK))
      strcpy(joinName, GetChar("Schema name: ", ""));
   else
      joinName[0] = '\0';

   DriverPrintPrompt("Ids of fields, which must be on the schema:\n");
   GetARInternalIdList(&fieldIdList);
   GetARPropList(&searchableObjPropList, FALSE);

   /* Call routine */
   BeginAPICall();
   result = ARGetListSchema(GetControlStructPtr(), changedSince, schemaType,
                            joinName, &fieldIdList, &searchableObjPropList,
                            &schemaList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListSchema");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARNameList("Schema list: ", "   ", &schemaList);
      SaveFirstSchemaListName(&schemaList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&schemaList, FALSE);
   FreeARPropList(&searchableObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeARInternalIdList(&fieldIdList, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetMultipleSchemas                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleSchemas()

{
   ARInternalIdListList          admingrpListList;
   ARArchiveInfoList             archiveInfoList;
   ARPermissionListList          assignedGroupListList;
   ARAuditInfoList               auditInfoList;
   ARTextStringList              changeDiaryList;
   ARTimestamp                   changedSince;
   ARNameList                    defaultVuiList;
   ARBooleanList                 existList;
   ARInternalIdList              fieldIdList;
   AREntryListFieldListList      getListFieldsList;
   ARPermissionListList          groupListList;
   ARTextStringList              helpTextList;
   unsigned int                  i;
   ARIndexListList               indexListList;
   ARAccessNameList              lastChangedList;
   ARNameList                    nameList;
   ARPropListList                objPropListList;
   ARAccessNameList              ownerList;
   int                           result;
   ARSchemaInheritanceListList   schemaInheritanceListList;
   ARCompoundSchemaList          schemaList;
   ARNameList                    schemaNameList;
   ARUnsignedIntList             schemaTypeList;
   ARSortListList                sortListList;
   ARStatusList                  status = {0, NULL};
   ARTimestampList               timestampList;

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE SCHEMAS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get schema type list:\n");
   GetARSchemaTypeList(&schemaTypeList);
   DriverPrintPrompt("Get schemas by name:\n");
   GetARNameList(&nameList, "");
   DriverPrintPrompt("Ids of fields, which must be on the schema:\n");
   GetARInternalIdList(&fieldIdList);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleSchemas(GetControlStructPtr(), changedSince,
                                 &schemaTypeList, &nameList, &fieldIdList,
                                 &existList, &schemaNameList, &schemaList,
                                 &schemaInheritanceListList, &assignedGroupListList,
                                 &groupListList, &admingrpListList,
                                 &getListFieldsList, &sortListList, &indexListList,
                                 &archiveInfoList, &auditInfoList, &defaultVuiList,
                                 &helpTextList, &timestampList, &ownerList,
                                 &lastChangedList, &changeDiaryList,
                                 &objPropListList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleSchemas");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (nameList.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameList.numItems; i++)
         {
            DriverPrintResult("   Schema : %s\n", nameList.nameList[i]);
            PrintARBoolean("    exists: ", existList.booleanList[i]);
         }
      }
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            PrintChar("\nSchema name: ", schemaNameList.nameList[i]);
            PrintARCompoundSchema(&schemaList.compoundSchema[i]);
            PrintChar("Assigned Group List: ", "");
            PrintARPermissionList(&assignedGroupListList.permissionList[i], FALSE);
            PrintChar("Complete Group List: ", "");
            PrintARPermissionList(&groupListList.permissionList[i], FALSE);
            PrintARInternalIdList("Admin group list: ", "   ",
                                  &admingrpListList.internalIdListList[i]);
            PrintAREntryListFieldList(&getListFieldsList.listFieldList[i]);
            PrintARSortList(&sortListList.sortListList[i]);
            PrintARIndexList(&indexListList.indexListList[i]);
            PrintARArchiveInfo(&archiveInfoList.archiveInfoList[i]);
            PrintARAuditInfo(&auditInfoList.auditInfoList[i]);
            PrintChar("Default VUI: ", defaultVuiList.nameList[i]);
            PrintChar("Help Text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Timestamp: ", timestampList.timestampList[i]);
            PrintChar("Owner: ", ownerList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("Object property: ", "   ",
                                  &objPropListList.propsList[i]);
         }
      }
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&schemaNameList, FALSE);
      FreeARCompoundSchemaList(&schemaList, FALSE);
      FreeARPermissionListList(&groupListList, FALSE);
      FreeARInternalIdListList(&admingrpListList, FALSE);
      FreeAREntryListFieldListList(&getListFieldsList, FALSE);
      FreeARSortListList(&sortListList, FALSE);
      FreeARIndexListList(&indexListList, FALSE);
      FreeARArchiveInfoList(&archiveInfoList, FALSE);
      FreeARAuditInfoList(&auditInfoList, FALSE);
      FreeARNameList(&defaultVuiList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownerList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   FreeARUnsignedIntList(&schemaTypeList, FALSE);
   FreeARNameList(&nameList, FALSE);
   FreeARInternalIdList(&fieldIdList, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                               APIARGetField                               */
/*                                                                           */
/*****************************************************************************/

int APIARGetField()

{
   ARPermissionList        assignedGroupList;
   char                   *changeDiary;
   unsigned int            createMode;
   unsigned int            dataType;
   ARValueStruct           defaultVal;
   ARDisplayInstanceList   dInstanceList;
   ARInternalId            fieldId;
   ARFieldMappingStruct    fieldMap;
   ARNameType              fieldName;
   unsigned int            fieldOption = 0;
   ARPermissionList        groupList;
   char                   *helpText;
   ARAccessNameType        lastChanged;
   ARFieldLimitStruct      limit;
   ARPropList              objPropList = {0, NULL};
   unsigned int            option;
   ARAccessNameType        owner;
   int                     result;
   ARNameType              schemaName;
   ARStatusList            status = {0, NULL};
   ARTimestamp             timestamp;
   
   /* Get parameters */
   DriverPrintHeader("GET FIELD");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   fieldId = GetLong("Field id (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = ARGetField(GetControlStructPtr(), schemaName, fieldId, fieldName,
                       &fieldMap, &dataType, &option, &createMode, &fieldOption,
                       &defaultVal, &assignedGroupList, &groupList, &limit,
                       &dInstanceList, &helpText, &timestamp, owner, lastChanged,
                       &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetField");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Field name: ", fieldName);
      PrintARFieldMapping("Field mapping: ", &fieldMap);
      PrintUInt("Datatype: ", dataType);
      PrintUInt("Field option: ", option);
      PrintUInt("Field create mode: ", createMode);
      PrintUInt("Field Bit Option: ", fieldOption);
      PrintARValueStruct("", &defaultVal);
      PrintChar("Assigned Group List: ", "");
      PrintARPermissionList(&assignedGroupList, TRUE);
      PrintChar("Complete Group List: ", "");
      PrintARPermissionList(&groupList, TRUE);
      PrintARFieldLimitStruct(&limit);
      PrintARDisplayInstanceList(&dInstanceList);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARValueStruct(&defaultVal, FALSE);
      FreeARPermissionList(&groupList, FALSE);
      FreeARPermissionList(&assignedGroupList, FALSE);
      FreeARFieldLimitStruct(&limit, FALSE);
      FreeARDisplayInstanceList(&dInstanceList, FALSE);
      FreeARPropList(&objPropList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                               APIARSetField                               */
/*                                                                           */
/*****************************************************************************/

int APIARSetField(ARBoolean imageFileFlag)

{
   char                    changeDiary[1024];
   char                   *changeDiaryPtr;
   unsigned int            createMode;
   unsigned int           *createModePtr;
   unsigned int            dataType;
   ARValueStruct           defaultVal;
   ARValueStruct          *defaultValPtr;
   ARDisplayInstanceList   dInstanceList;
   ARDisplayInstanceList  *dInstanceListPtr;
   ARInternalId            fieldId;
   ARFieldMappingStruct    fieldMap;
   ARFieldMappingStruct   *fieldMapPtr;
   ARNameType              fieldName;
   char                   *fieldNamePtr;
   unsigned int            fieldOption;
   unsigned int           *fieldOptionPtr;
   char                    helpText[1024];
   char                   *helpTextPtr;
   ARFieldLimitStruct      limit;
   ARFieldLimitStruct     *limitPtr;
   ARPropList              objPropList;
   ARPropList             *objPropListPtr;
   unsigned int            opFlags = 0;  /* set me via debugger */
   unsigned int            option;
   unsigned int           *optionPtr;
   ARAccessNameType        owner;
   char                   *ownerPtr;
   ARPermissionList        permissions;
   ARPermissionList       *permissionsPtr;
   int                     result;
   ARNameType              schemaName;
   ARStatusList            status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET FIELD");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   fieldId = GetLong("Field id (1): ", 1);
   dataType = GetUnsignedInt(
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/\n"
"         attach/currency/date/timeofday/join/trim/control/Table/Column/\n"
"         ulong/coords/view/display (0-14, 30-34, 40-43) (2): ",
                     AR_DATA_TYPE_INTEGER);
   if (GetARBoolean("Change fieldName? (F): ", FALSE))
   {
      strcpy(fieldName, GetChar("Field name (): ", ""));
      fieldNamePtr = fieldName;
   }
   else
      fieldNamePtr = NULL;
   if (GetARBoolean("Change field mapping? (F): ", FALSE))
   {
      GetARFieldMapping(&fieldMap);
      fieldMapPtr = &fieldMap;
   }
   else
      fieldMapPtr = NULL;
   if (GetARBoolean("Have new option? (F): ", FALSE))
   {
      option = GetUnsignedInt("Required, Optional, System, or Display-only (1-4) (2): ", 2);
      optionPtr = &option;
   }
   else
      optionPtr = NULL;
   if (GetARBoolean("Have new create mode? (F): ", FALSE))
   {
      createMode = GetUnsignedInt("Open or Protected at create (1 or 2) (1): ", 1);
      createModePtr = &createMode;
   }
   else
      createModePtr = NULL;

   if (GetARBoolean("Have new field option? (F): ", FALSE))
   {
      fieldOption = GetUnsignedInt("Field option for Audit/Copy (0): ", 0);
      fieldOptionPtr = &fieldOption;
   }
   else
      fieldOptionPtr = NULL;

   if ((dataType != AR_DATA_TYPE_ATTACH) &&
       (dataType != AR_DATA_TYPE_TABLE) &&
       (dataType != AR_DATA_TYPE_COLUMN) &&
       (GetARBoolean("Have new default value? (F): ", FALSE)))
   {
      defaultVal.dataType = dataType;
      GetValue(&defaultVal, imageFileFlag, "");
      defaultValPtr = &defaultVal;
   }
   else
      defaultValPtr = NULL;
   if (GetARBoolean("Have new permissions? (F): ", FALSE))
   {
      GetARPermissionList(TRUE, &permissions);
      permissionsPtr = &permissions;
   }
   else
      permissionsPtr = NULL;
   if (GetARBoolean("Have new field limits? (F): ", FALSE))
   {
      GetARFieldLimitStruct(dataType, &limit);
      limitPtr = &limit;
   }
   else
      limitPtr = NULL;
   if (GetARBoolean("Have new display instance info? (F): ", FALSE))
   {
      GetARDisplayInstanceList(&dInstanceList, imageFileFlag);
      dInstanceListPtr = &dInstanceList;
   }
   else
      dInstanceListPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARSetField(GetControlStructPtr(), schemaName, fieldId, fieldNamePtr,
                       fieldMapPtr, optionPtr, createModePtr, fieldOptionPtr,
                       defaultValPtr, permissionsPtr, limitPtr, dInstanceListPtr,
                       helpTextPtr, ownerPtr, changeDiaryPtr, opFlags, objPropListPtr,
                       &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetField");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (defaultValPtr != NULL)
      FreeARValueStruct(&defaultVal, FALSE);
   if (permissionsPtr != NULL)
      FreeARPermissionList(&permissions, FALSE);
   if (limitPtr != NULL)
      FreeARFieldLimitStruct(&limit, FALSE);
   if (dInstanceListPtr != NULL)
      FreeARDisplayInstanceList(&dInstanceList, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARSetMultipleFields                           */
/*                                                                           */
/*****************************************************************************/

int APIARSetMultipleFields()

{
   ARTextStringList               changeDiaryList;
   ARTextStringList              *changeDiaryListPtr;
   ARUnsignedIntPtrList           createModePtrList;
   ARUnsignedIntPtrList          *createModePtrListPtr;
   ARValuePtrList                 defaultValuePtrList;
   ARValuePtrList                *defaultValuePtrListPtr;
   ARDisplayInstanceListPtrList   displayInstanceListPtrList;
   ARDisplayInstanceListPtrList  *displayInstanceListPtrListPtr;
   ARInternalIdList               fieldIdList;
   ARFieldMappingPtrList          fieldMapPtrList;
   ARFieldMappingPtrList         *fieldMapPtrListPtr;
   ARNamePtrList                  fieldNamePtrList;
   ARNamePtrList                 *fieldNamePtrListPtr;
   ARUnsignedIntPtrList           fieldOptionPtrList;
   ARUnsignedIntPtrList          *fieldOptionPtrListPtr;
   ARTextStringList               helpTextList;
   ARTextStringList              *helpTextListPtr;
   ARFieldLimitPtrList            limitPtrList;
   ARFieldLimitPtrList           *limitPtrListPtr;
   ARPropListList                 objPropList;
   ARPropListList                *objPropListPtr;
   ARUnsignedIntPtrList           optionPtrList;
   ARUnsignedIntPtrList          *optionPtrListPtr;
   ARAccessNamePtrList            ownerPtrList;
   ARAccessNamePtrList           *ownerPtrListPtr;
   ARPermissionListPtrList        permissionListPtrList;
   ARPermissionListPtrList       *permissionListPtrListPtr;
   int                            result;
   ARNameType                     schemaName;
   ARUnsignedIntList              setFieldOptionList;
   ARUnsignedIntList             *setFieldOptionListPtr;
   ARStatusListList               setFieldStatusList = {0, NULL};
   ARStatusList                   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET MULTIPLE FIELDS");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   DriverPrintPrompt("Ids of fields to set:\n");
   GetARInternalIdList(&fieldIdList);
   if (GetARBoolean("Change field names? (F): ", FALSE))
   {
      GetARNamePtrList(&fieldNamePtrList, "");
      fieldNamePtrListPtr = &fieldNamePtrList;
   }
   else
      fieldNamePtrListPtr = NULL;
   if (GetARBoolean("Change field mappings? (F): ", FALSE))
   {
      GetARFieldMappingPtrList(&fieldMapPtrList);
      fieldMapPtrListPtr = &fieldMapPtrList;
   }
   else
      fieldMapPtrListPtr = NULL;
   if (GetARBoolean("Change options? (F): ", FALSE))
   {
      GetARUnsignedIntPtrList(&optionPtrList);
      optionPtrListPtr = &optionPtrList;
   }
   else
      optionPtrListPtr = NULL;
   if (GetARBoolean("Change create modes? (F): ", FALSE))
   {
      GetARUnsignedIntPtrList(&createModePtrList);
      createModePtrListPtr = &createModePtrList;
   }
   else
      createModePtrListPtr = NULL;
   if (GetARBoolean("Change field options? (F): ", FALSE))
   {
      GetARUnsignedIntPtrList(&fieldOptionPtrList);
      fieldOptionPtrListPtr = &fieldOptionPtrList;
   }
   else
      fieldOptionPtrListPtr = NULL;
   if (GetARBoolean("Change default values? (F): ", FALSE))
   {
      GetARValuePtrList(&defaultValuePtrList);
      defaultValuePtrListPtr = &defaultValuePtrList;
   }
   else
      defaultValuePtrListPtr = NULL;
   if (GetARBoolean("Change field permissions? (F): ", FALSE))
   {
      GetARPermissionListPtrList(TRUE, &permissionListPtrList);
      permissionListPtrListPtr = &permissionListPtrList;
   }
   else
      permissionListPtrListPtr = NULL;
   if (GetARBoolean("Change field limits? (F): ", FALSE))
   {
      GetARFieldLimitPtrList(NULL, &limitPtrList);
      limitPtrListPtr = &limitPtrList;
   }
   else
      limitPtrListPtr = NULL;
   if (GetARBoolean("Change field display instances? (F): ", FALSE))
   {
      GetARDisplayInstanceListPtrList(&displayInstanceListPtrList);
      displayInstanceListPtrListPtr = &displayInstanceListPtrList;
   }
   else
      displayInstanceListPtrListPtr = NULL;
   if (GetARBoolean("Change field help text? (F): ", FALSE))
   {
      GetARTextStringList(&helpTextList);
      helpTextListPtr = &helpTextList;
   }
   else
      helpTextListPtr = NULL;
   if (GetARBoolean("Change field owners? (F): ", FALSE))
   {
      GetARAccessNamePtrList(&ownerPtrList, "");
      ownerPtrListPtr = &ownerPtrList;
   }
   else
      ownerPtrListPtr = NULL;
   if (GetARBoolean("Change field change diaries? (F): ", FALSE))
   {
      GetARTextStringList(&changeDiaryList);
      changeDiaryListPtr = &changeDiaryList;
   }
   else
      changeDiaryListPtr = NULL;
   if (GetARBoolean("Have set field options? (F): ", FALSE))
   {
      GetARUnsignedIntList(&setFieldOptionList);
      setFieldOptionListPtr = &setFieldOptionList;
   }
   else
      setFieldOptionListPtr = NULL;

   if (GetARBoolean("Change field object properties? (F): ", FALSE))
   {
      GetARPropListPtrList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;


   /* Call routine */
   BeginAPICall();
   result = ARSetMultipleFields(GetControlStructPtr(), schemaName,
                                &fieldIdList, fieldNamePtrListPtr,
                                fieldMapPtrListPtr, optionPtrListPtr,
                                createModePtrListPtr, fieldOptionPtrListPtr,
                                defaultValuePtrListPtr, permissionListPtrListPtr,
                                limitPtrListPtr, displayInstanceListPtrListPtr,
                                helpTextListPtr, ownerPtrListPtr,
                                changeDiaryListPtr, setFieldOptionListPtr,
                                objPropListPtr, &setFieldStatusList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetMultipleFields");
   PrintReturnCode(result);
   PrintARStatusListList(&setFieldStatusList);
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARInternalIdList(&fieldIdList, FALSE);
   FreeARNamePtrList(fieldNamePtrListPtr, FALSE);
   FreeARFieldMappingPtrList(fieldMapPtrListPtr, FALSE);
   FreeARUnsignedIntPtrList(optionPtrListPtr, FALSE);
   FreeARUnsignedIntPtrList(createModePtrListPtr, FALSE);
   FreeARUnsignedIntPtrList(fieldOptionPtrListPtr, FALSE);
   FreeARValuePtrList(defaultValuePtrListPtr, FALSE);
   FreeARPermissionListPtrList(permissionListPtrListPtr, FALSE);
   FreeARFieldLimitPtrList(limitPtrListPtr, FALSE);
   FreeARDisplayInstanceListPtrList(displayInstanceListPtrListPtr, FALSE);
   FreeARTextStringList(helpTextListPtr, FALSE);
   FreeARAccessNamePtrList(ownerPtrListPtr, FALSE);
   FreeARTextStringList(changeDiaryListPtr, FALSE);
   FreeARUnsignedIntList(setFieldOptionListPtr, FALSE);
   FreeARPropListList(objPropListPtr, FALSE);
   FreeARStatusListList(&setFieldStatusList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARCreateField                              */
/*                                                                           */
/*****************************************************************************/

int APIARCreateField(ARBoolean imageFileFlag)

{
   char                    changeDiary[1024];
   unsigned int            createMode;
   unsigned int            dataType;
   ARValueStruct           defaultVal;
   ARDisplayInstanceList   dInstanceList;
   ARInternalId            fieldId;
   ARFieldMappingStruct    fieldMap;
   ARNameType              fieldName;
   unsigned int            fieldOption;
   char                    helpText[1024];
   ARFieldLimitStruct      limit;
   ARPropList              objPropList = {0, NULL};
   unsigned int            option;
   ARAccessNameType        owner;
   ARPermissionList        permissions;
   ARBoolean               requiredIdOK;
   int                     result;
   ARNameType              schemaName;
   ARStatusList            status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("CREATE FIELD");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   fieldId = GetLong("Field id (0): ", 0);
   requiredIdOK = GetARBoolean("Create even if ID is reserved? (F): ", FALSE);
   strcpy(fieldName, GetChar("Field name: ", ""));
   dataType = GetUnsignedInt(
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/\n"
"         attach/currency/date/timeofday/join/trim/control/Table/Column/\n"
"         ulong/coords/view/display (0-14, 30-34, 40-43) (2): ",
                             AR_DATA_TYPE_INTEGER);
   option = GetUnsignedInt("Required, Optional, System, or Display-only (1-4) (2): ",2);
   createMode = GetUnsignedInt("Open or Protected at create (1 or 2) (1): ", 1);
   fieldOption = GetUnsignedInt("Field option for Audit/Copy (0): ", 0);
   if ((dataType != AR_DATA_TYPE_ATTACH) &&
       (dataType != AR_DATA_TYPE_TABLE) &&
       (dataType != AR_DATA_TYPE_COLUMN) &&
       (GetARBoolean("Have default value? (T): ", TRUE)))
   {
      defaultVal.dataType = dataType;
      GetValue(&defaultVal, imageFileFlag, "");
   }
   else
   {
      memset(&defaultVal, '\0', sizeof(defaultVal));
      defaultVal.dataType = AR_DEFAULT_VALUE_NONE;
   }
   GetARPermissionList(TRUE, &permissions);
   if (GetARBoolean("Have field limits? (T): ", TRUE))
      GetARFieldLimitStruct(dataType, &limit);
   else
      limit.dataType = AR_FIELD_LIMIT_NONE;
   GetARDisplayInstanceList(&dInstanceList, imageFileFlag);
   strcpy(helpText, GetChar("Help Text: ", ""));
   strcpy(owner, GetChar("Owner: ", GetControlStructPtr()->user));
   strcpy(changeDiary, GetChar("Change Diary: ", ""));
   GetARFieldMapping(&fieldMap);
   GetARPropList(&objPropList, FALSE);

   /* Call routine */
   BeginAPICall();
   result = ARCreateField(GetControlStructPtr(), schemaName, &fieldId,
                          requiredIdOK, fieldName, &fieldMap, dataType, option,
                          createMode, fieldOption, &defaultVal, &permissions, &limit,
                          &dInstanceList, helpText, owner, changeDiary, &objPropList,
                          &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateField");
   PrintReturnCode(result);
   PrintARInternalId("Field id: ", fieldId);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARValueStruct(&defaultVal, FALSE);
   FreeARPermissionList(&permissions, FALSE);
   FreeARFieldLimitStruct(&limit, FALSE);
   FreeARDisplayInstanceList(&dInstanceList, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARCreateMultipleFields                         */
/*                                                                           */
/*****************************************************************************/

int APIARCreateMultipleFields()

{
   ARTextStringList               changeDiaryList;
   ARTextStringList              *changeDiaryListPtr;
   ARUnsignedIntList              createModeList;
   ARUnsignedIntList              dataTypeList;
   ARValuePtrList                 defaultValuePtrList;
   ARValuePtrList                *defaultValuePtrListPtr;
   ARDisplayInstanceListPtrList   displayInstanceListPtrList;
   ARDisplayInstanceListPtrList  *displayInstanceListPtrListPtr;
   ARInternalIdList               fieldIdList;
   ARFieldMappingList             fieldMapList;
   ARNamePtrList                  fieldNamePtrList;
   ARNamePtrList                 *fieldNamePtrListPtr;
   ARUnsignedIntList              fieldOptionList;
   ARTextStringList               helpTextList;
   ARTextStringList              *helpTextListPtr;
   ARFieldLimitPtrList            limitPtrList;
   ARFieldLimitPtrList           *limitPtrListPtr;
   ARPropListList                 objPropList;
   ARPropListList                *objPropListPtr;
   ARUnsignedIntList              optionList;
   ARAccessNamePtrList            ownerPtrList;
   ARAccessNamePtrList           *ownerPtrListPtr;
   ARPermissionListPtrList        permissionListPtrList;
   ARPermissionListPtrList       *permissionListPtrListPtr;
   int                            result;
   ARBooleanList                  reservedIdOKList;
   ARNameType                     schemaName;
   ARStatusList                   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("CREATE MULTIPLE FIELDS");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   DriverPrintPrompt("Ids of fields to create:\n");
   GetARInternalIdList(&fieldIdList);
   DriverPrintPrompt("Reserved Id OK flags for fields to create:\n");
   GetARBooleanList(&reservedIdOKList);
   if (GetARBoolean("Have field names? (T): ", TRUE))
   {
      GetARNamePtrList(&fieldNamePtrList, "");
      fieldNamePtrListPtr = &fieldNamePtrList;
   }
   else
      fieldNamePtrListPtr = NULL;
   DriverPrintPrompt("Field mappings for fields to create:\n");
   GetARFieldMappingList(&fieldMapList);
   DriverPrintPrompt("Data types of fields to create:\n");
   GetARUnsignedIntList(&dataTypeList);
   DriverPrintPrompt("Options of fields to create:\n");
   GetARUnsignedIntList(&optionList);
   DriverPrintPrompt("Create modes of fields to create:\n");
   GetARUnsignedIntList(&createModeList);
   DriverPrintPrompt("Field options of fields to create:\n");
   GetARUnsignedIntList(&fieldOptionList);
   if (GetARBoolean("Have default values? (T): ", TRUE))
   {
      GetARValuePtrList(&defaultValuePtrList);
      defaultValuePtrListPtr = &defaultValuePtrList;
   }
   else
      defaultValuePtrListPtr = NULL;
   if (GetARBoolean("Have field permissions? (T): ", TRUE))
   {
      GetARPermissionListPtrList(TRUE, &permissionListPtrList);
      permissionListPtrListPtr = &permissionListPtrList;
   }
   else
      permissionListPtrListPtr = NULL;
   if (GetARBoolean("Have field limits? (T): ", TRUE))
   {
      GetARFieldLimitPtrList(&dataTypeList, &limitPtrList);
      limitPtrListPtr = &limitPtrList;
   }
   else
      limitPtrListPtr = NULL;
   if (GetARBoolean("Have field display instances? (T): ", TRUE))
   {
      GetARDisplayInstanceListPtrList(&displayInstanceListPtrList);
      displayInstanceListPtrListPtr = &displayInstanceListPtrList;
   }
   else
      displayInstanceListPtrListPtr = NULL;
   if (GetARBoolean("Have field help text? (T): ", TRUE))
   {
      GetARTextStringList(&helpTextList);
      helpTextListPtr = &helpTextList;
   }
   else
      helpTextListPtr = NULL;

   if (GetARBoolean("Have field owners? (T): ", TRUE))
   {
      GetARAccessNamePtrList(&ownerPtrList, "");
      ownerPtrListPtr = &ownerPtrList;
   }
   else
      ownerPtrListPtr = NULL;

   if (GetARBoolean("Have field change diaries? (T): ", TRUE))
   {
      GetARTextStringList(&changeDiaryList);
      changeDiaryListPtr = &changeDiaryList;
   }
   else
      changeDiaryListPtr = NULL;

   if (GetARBoolean("Have field object properties? (F): ", FALSE))
   {
      GetARPropListPtrList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
 
   /* Call routine */
   BeginAPICall();
   result = ARCreateMultipleFields(GetControlStructPtr(), schemaName,
                                   &fieldIdList, &reservedIdOKList,
                                   fieldNamePtrListPtr, &fieldMapList,
                                   &dataTypeList, &optionList,
                                   &createModeList, &fieldOptionList,
                                   defaultValuePtrListPtr, permissionListPtrListPtr,
                                   limitPtrListPtr, displayInstanceListPtrListPtr,
                                   helpTextListPtr, ownerPtrListPtr,
                                   changeDiaryListPtr, objPropListPtr,
                                   &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateMultipleFields");
   PrintReturnCode(result);
   PrintARInternalIdList("Field Id List:", "              ", &fieldIdList);
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARInternalIdList(&fieldIdList, FALSE);
   FreeARBooleanList(&reservedIdOKList, FALSE);
   FreeARNamePtrList(fieldNamePtrListPtr, FALSE);
   FreeARFieldMappingList(&fieldMapList, FALSE);
   FreeARUnsignedIntList(&dataTypeList, FALSE);
   FreeARUnsignedIntList(&optionList, FALSE);
   FreeARUnsignedIntList(&createModeList, FALSE);
   FreeARUnsignedIntList(&fieldOptionList, FALSE);
   FreeARValuePtrList(defaultValuePtrListPtr, FALSE);
   FreeARPermissionListPtrList(permissionListPtrListPtr, FALSE);
   FreeARFieldLimitPtrList(limitPtrListPtr, FALSE);
   FreeARDisplayInstanceListPtrList(displayInstanceListPtrListPtr, FALSE);
   FreeARTextStringList(helpTextListPtr, FALSE);
   FreeARAccessNamePtrList(ownerPtrListPtr, FALSE);
   FreeARTextStringList(changeDiaryListPtr, FALSE);
   FreeARPropListList(objPropListPtr, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARDeleteField                              */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteField()

{
   unsigned int   deleteOption;
   ARInternalId   fieldId;
   int            result;
   ARNameType     schemaName;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE FIELD");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   fieldId = GetLong("Field id (0): ", 0);
   deleteOption = GetUnsignedInt("Delete option - clean, data, cascade (0 - 2) (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteField(GetControlStructPtr(), schemaName, fieldId,
                          deleteOption, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteField");
   PrintChar("Schema name:", schemaName);
   PrintARInternalId("Field id: ", fieldId);
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARDeleteMultipleFields                         */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteMultipleFields()

{
   unsigned int       deleteOption;
   ARInternalIdList   fieldIdList;
   int                result;
   ARNameType         schemaName;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE MULTIPLE FIELDS");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   DriverPrintPrompt("Ids of fields to delete:\n");
   GetARInternalIdList(&fieldIdList);
   deleteOption = GetUnsignedInt("Delete option - clean, data, cascade (0 - 2) (0): ", 0);
 
   /* Call routine */
   BeginAPICall();
   result = ARDeleteMultipleFields(GetControlStructPtr(), schemaName,
                                   &fieldIdList, deleteOption, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteMultipleFields");
   PrintReturnCode(result);
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARInternalIdList(&fieldIdList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetListField                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListField()

{
   ARTimestamp        changedSince;
   ARInternalIdList   fieldIdList;
   unsigned long      fieldType;
   int                result;
   ARNameType         schemaName;
   ARStatusList       status = {0, NULL};
   ARPropList         serverObjPropList = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST FIELD");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   fieldType = GetLong("Field Type - data, trim, control, page, page holder, table, column, attach,\n             attach pool  (1, 2, 4, 8, 16, 32, 64, 128, 256) (1): ", 1);
   
   /* Call routine */
   BeginAPICall();
   result = ARGetListField(GetControlStructPtr(), schemaName, fieldType,
                           changedSince, &serverObjPropList, &fieldIdList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListField");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARInternalIdList("Field id list: ", "   ", &fieldIdList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARInternalIdList(&fieldIdList, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetMultipleFields                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleFields()

{
   ARPermissionListList        assignedGroupList;
   ARTextStringList            changeDiary;
   ARUnsignedIntList           createMode;
   ARUnsignedIntList           dataType;
   ARValueList                 defaultVal;
   ARDisplayInstanceListList   dInstanceList;
   ARBooleanList               existList;
   ARInternalIdList            fieldIdIn;
   ARInternalIdList            fieldIdOut;
   ARFieldMappingList          fieldMap;
   ARNameList                  fieldName;
   ARUnsignedIntList           fieldOption;
   ARPermissionListList        groupList;
   ARTextStringList            helpText;
   unsigned int                i;
   ARAccessNameList            lastChanged;
   ARFieldLimitList            limit;
   ARPropListList              objPropListList;
   ARUnsignedIntList           option;
   ARAccessNameList            owner;
   int                         result;
   ARNameType                  schemaName;
   ARStatusList                status = {0, NULL};
   ARTimestampList             timestamp;
   
   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE FIELDS");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   DriverPrintPrompt("Ids of fields to retrieve:\n");
   GetARInternalIdList(&fieldIdIn);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleFields(GetControlStructPtr(), schemaName, &fieldIdIn,
                                &existList, &fieldIdOut, &fieldName, &fieldMap,
                                &dataType, &option, &createMode, &fieldOption,
                                &defaultVal, &assignedGroupList, &groupList,
                                &limit, &dInstanceList, &helpText, &timestamp,
                                &owner, &lastChanged, &changeDiary,
                                &objPropListList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleFields");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARBooleanList("Field exists list", &existList);
      PrintARInternalIdList("Field ID list", "   ", &fieldIdOut);
      PrintARNameList("Field name list", "   ", &fieldName);
      PrintARFieldMappingList("Field mapping list", &fieldMap);
      PrintARUnsignedIntList("Datatype list", "   ", &dataType);
      PrintARUnsignedIntList("Field option list", "   ", &option);
      PrintARUnsignedIntList("Field create mode list", "   ", &createMode);
      PrintARUnsignedIntList("Field bit option list", "   ", &fieldOption);
      PrintARValueList("", &defaultVal);
      PrintARPermissionListList("Assigned Group List", &assignedGroupList);
      PrintARPermissionListList("Complete Group List", &groupList);
      PrintARFieldLimitList("Field limits", &limit);
      PrintARDisplayInstanceListList("Display Instances", &dInstanceList);
      PrintARTextStringList("Help text list", "   Help text: ", &helpText);
      PrintARTimestampList("Last update time list: ", "   ", &timestamp);
      PrintARAccessNameList("Owner list", "   ", &owner);
      PrintARAccessNameList("Last changed list", "   ", &lastChanged);
      APIPrintDiaryList("Change Diary list", "   Change diary: ", &changeDiary);
      for (i = 0; i < objPropListList.numItems; i++)
         PrintARObjectPropList("", "", &objPropListList.propsList[i]);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARInternalIdList(&fieldIdIn, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARInternalIdList(&fieldIdOut, FALSE);
      FreeARNameList(&fieldName, FALSE);
      FreeARFieldMappingList(&fieldMap, FALSE);
      FreeARUnsignedIntList(&dataType, FALSE);
      FreeARUnsignedIntList(&option, FALSE);
      FreeARUnsignedIntList(&createMode, FALSE);
      FreeARUnsignedIntList(&fieldOption, FALSE);
      FreeARValueList(&defaultVal, FALSE);
      FreeARPermissionListList(&assignedGroupList, FALSE);
      FreeARPermissionListList(&groupList, FALSE);
      FreeARFieldLimitList(&limit, FALSE);
      FreeARDisplayInstanceListList(&dInstanceList, FALSE);
      FreeARTextStringList(&helpText, FALSE);
      FreeARTimestampList(&timestamp, FALSE);
      FreeARAccessNameList(&owner, FALSE);
      FreeARAccessNameList(&lastChanged, FALSE);
      FreeARTextStringList(&changeDiary, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                                APIARGetVUI                                */
/*                                                                           */
/*****************************************************************************/

int APIARGetVUI()

{
   char              *changeDiary;
   ARPropList         dPropList;
   char              *helpText;
   ARAccessNameType   lastChanged;
   ARLocaleType       locale;
   ARPropList         objPropList = {0, NULL};
   ARAccessNameType   owner;
   int                result;
   ARNameType         schemaName;
   ARStatusList       status = {0, NULL};
   ARTimestamp        timestamp;
   ARInternalId       vuiId;
   ARNameType         vuiName;
   unsigned int       vuiType;

   /* Get parameters */
   DriverPrintHeader("GET VUI");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   vuiId = GetLong("VUI id (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = ARGetVUI(GetControlStructPtr(), schemaName, vuiId, vuiName,
                     locale, &vuiType, &dPropList, &helpText, &timestamp,
                     owner, lastChanged, &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetVUI");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("VUI name: ", vuiName);
      PrintChar("Locale: ", locale);
      PrintUInt("VUI-Type: ", vuiType);
      PrintARDisplayPropList("", "", &dPropList);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARPropList(&dPropList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
      FreeARPropList(&objPropList, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                                APIARSetVUI                                */
/*                                                                           */
/*****************************************************************************/

int APIARSetVUI()

{
   char               changeDiary[1024];
   char              *changeDiaryPtr;
   ARPropList         dPropList;
   ARPropList        *dPropListPtr;
   char               helpText[1024];
   char              *helpTextPtr;
   ARLocaleType       locale;
   char              *localePtr;
   ARPropList         objPropList;
   ARPropList        *objPropListPtr = NULL;
   ARAccessNameType   owner;
   char              *ownerPtr;
   int                result;
   ARNameType         schemaName;
   ARStatusList       status = {0, NULL};
   ARInternalId       vuiId;
   ARNameType         vuiName;
   char              *vuiNamePtr;
   unsigned int       vuiType;
   unsigned int      *vuiTypePtr;

   /* Get parameters */
   DriverPrintHeader("SET VUI");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   vuiId = GetLong("VUI id (1): ", 1);
   if (GetARBoolean("Change VUI name? (F): ", FALSE))
   {
      strcpy(vuiName, GetChar("VUI name(): ", ""));
      vuiNamePtr = vuiName;
   }
   else
      vuiNamePtr = NULL;
   if (GetARBoolean("Change Locale? (F): ", FALSE))
   {
      strcpy(locale, GetChar("Locale(): ", ""));
      localePtr = locale;
   }
   else
      localePtr = NULL;
   if (GetARBoolean("Change VUI-Type? (F): ", FALSE))
   {
      vuiType = GetUnsignedInt("VUI-Type (0): ", 0);
      vuiTypePtr = &vuiType;
   }
   else
      vuiTypePtr = NULL;
   if (GetARBoolean("Have new Display Prop List info? (F): ", FALSE))
   {
      GetARPropList(&dPropList, FALSE);
      dPropListPtr = &dPropList;
   }
   else
      dPropListPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARSetVUI(GetControlStructPtr(), schemaName, vuiId, vuiNamePtr,
                     localePtr, vuiTypePtr, dPropListPtr, helpTextPtr,
                     ownerPtr, changeDiaryPtr, objPropListPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetVUI");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (dPropListPtr != NULL)
      FreeARPropList(&dPropList, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARCreateVUI                               */
/*                                                                           */
/*****************************************************************************/

int APIARCreateVUI()

{
   char               changeDiary[1024];
   ARPropList         dPropList;
   char               helpText[1024];
   ARLocaleType       locale;
   ARPropList         objPropList = {0, NULL};
   ARAccessNameType   owner;
   int                result;
   ARNameType         schemaName;
   ARStatusList       status = {0, NULL};
   ARInternalId       vuiId;
   ARNameType         vuiName;
   unsigned int       vuiType;

   /* Get parameters */
   DriverPrintHeader("CREATE VUI");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   vuiId = GetLong("VUI id (0): ", 0);
   strcpy(vuiName, GetChar("VUI name: ", ""));
   strcpy(locale, GetChar("Locale: ", ""));
   vuiType = GetUnsignedInt("VUI-Type (0): ", 0);
   GetARPropList(&dPropList, FALSE);
   strcpy(helpText, GetChar("Help Text: ", ""));
   strcpy(owner, GetChar("Owner: ", GetControlStructPtr()->user));
   strcpy(changeDiary, GetChar("Change Diary: ", ""));
   
   /* Call routine */
   BeginAPICall();
   result = ARCreateVUI(GetControlStructPtr(), schemaName, &vuiId, vuiName,
                        locale, vuiType, &dPropList, helpText, owner,
                        changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateVUI");
   PrintReturnCode(result);
   PrintARInternalId("VUI id: ", vuiId);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARPropList(&dPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeARPropList(&objPropList, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARDeleteVUI                               */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteVUI()

{
   int            result;
   ARNameType     schemaName;
   ARStatusList   status = {0, NULL};
   ARInternalId   vuiId;

   /* Get parameters */
   DriverPrintHeader("DELETE VUI");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   vuiId = GetLong("VUI id (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteVUI(GetControlStructPtr(), schemaName, vuiId, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteVUI");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARGetListVUI                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetListVUI()

{
   ARTimestamp        changedSince;
   int                result;
   ARNameType         schemaName;
   ARPropList         searchableObjPropList = {0, NULL};
   ARStatusList       status = {0, NULL};
   ARInternalIdList   vuiIdList;

   /* Get parameters */
   DriverPrintHeader("GETLIST VUI");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetListVUI(GetControlStructPtr(), schemaName, changedSince,
                         &searchableObjPropList, &vuiIdList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListVUI");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARInternalIdList("VUI id list: ", "   ", &vuiIdList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARInternalIdList(&vuiIdList, FALSE);
   FreeARStatusList(&status, FALSE);
   FreeARPropList(&searchableObjPropList, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetMultipleVUIs                            */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleVUIs()

{
   ARTextStringList    changeDiary;
   ARTimestamp         changedSince;
   ARPropListList      dPropListList;
   ARBooleanList       existList;
   ARInternalIdList    gotList;
   ARTextStringList    helpText;
   unsigned int        i;
   ARAccessNameList    lastChanged;
   ARLocaleList        localeList;
   ARNameList          nameList;
   ARPropListList      objPropListList;
   ARAccessNameList    owner;
   int                 result;
   ARNameType          schemaName;
   ARStatusList        status = {0, NULL};
   ARTimestampList     timestamp;
   ARUnsignedIntList   vuiType;
   ARInternalIdList    wantList = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE VUIS");
   strcpy(schemaName, GetChar("Schema name: ", ""));
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Ids of vuis to retrieve:\n");
   GetARInternalIdList(&wantList);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleVUIs(GetControlStructPtr(),
                              schemaName, &wantList, changedSince,
                              &existList, &gotList, &nameList, &localeList,
                              &vuiType, &dPropListList, &helpText, &timestamp,
                              &owner, &lastChanged, &changeDiary, 
                              &objPropListList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleVUIs");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARBooleanList("VUI exists list", &existList);
      PrintARInternalIdList("VUI ID list", "   ", &gotList);
      PrintARNameList("VUI name list", "   ", &nameList);
      PrintARLocaleList("VUI locale list", "   ", &localeList);
      PrintARUnsignedIntList("Datatype list", "   ", &vuiType);
      PrintARTextStringList("Help text list", "   Help text: ", &helpText);
      PrintARTimestampList("Last update time list: ", "   ", &timestamp);
      PrintARAccessNameList("Owner list", "   ", &owner);
      PrintARAccessNameList("Last changed list", "   ", &lastChanged);
      APIPrintDiaryList("Change Diary list", "   Change diary: ", &changeDiary);
      PrintARDisplayPropListList("Display Properties list", "   ",
                                 &dPropListList);
      for (i = 0; i < objPropListList.numItems; i++)
         PrintARObjectPropList("", "", &objPropListList.propsList[i]);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARInternalIdList(&wantList, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARInternalIdList(&gotList, FALSE);
      FreeARNameList(&nameList, FALSE);
      FreeARUnsignedIntList(&vuiType, FALSE);
      FreeARTextStringList(&helpText, FALSE);
      FreeARTimestampList(&timestamp, FALSE);
      FreeARAccessNameList(&owner, FALSE);
      FreeARAccessNameList(&lastChanged, FALSE);
      FreeARTextStringList(&changeDiary, FALSE);
      FreeARPropListList(&dPropListList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetCharMenu                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetCharMenu()

{
   char              *changeDiary;
   char              *helpText;
   ARAccessNameType   lastChanged;
   ARCharMenuStruct   menuDefn;
   ARNameType         name;
   ARPropList         objPropList;
   ARAccessNameType   owner;
   unsigned int       refreshCode;
   int                result;
   ARStatusList       status = {0, NULL};
   ARTimestamp        timestamp;

   /* Get parameters */
   DriverPrintHeader("GET CHAR MENU");
   strcpy(name, GetChar("Character Menu name: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetCharMenu(GetControlStructPtr(), name, &refreshCode, &menuDefn,
                          &helpText, &timestamp, owner, lastChanged,
                          &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetCharMenu");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintUInt("Refresh code: ", refreshCode);
      PrintARCharMenuStruct("", &menuDefn);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARCharMenuStruct(&menuDefn, FALSE);
      FreeARPropList(&objPropList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARSetCharMenu                              */
/*                                                                           */
/*****************************************************************************/

int APIARSetCharMenu()

{
   char               changeDiary[1024];
   char              *changeDiaryPtr;
   char               helpText[1024];
   char              *helpTextPtr;
   ARCharMenuStruct   menuDefn;
   ARCharMenuStruct  *menuDefnPtr;
   ARNameType         name;
   ARNameType         newName;
   char              *newNamePtr;
   char              *objectModificationLogLabel = NULL;
   ARPropList         objPropList;
   ARPropList        *objPropListPtr;
   ARAccessNameType   owner;
   char              *ownerPtr;
   unsigned int       refreshCode;
   unsigned int      *refreshCodePtr;
   int                result;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET CHAR MENU");
   strcpy(name, GetChar("Character Menu name: ", ""));
   if (GetARBoolean("Change character menu name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change refresh code? (F): ", FALSE))
   {
      refreshCode = GetUnsignedInt("Refresh on connect, open, interval (1 - 3) (1): ", 1);
      refreshCodePtr = &refreshCode;
   }
   else
      refreshCodePtr = NULL;
   if (GetARBoolean("Change menu definition(s)? (F): ", FALSE))
   {
      DriverPrintPrompt("Character menu struct:\n");
      GetARCharMenuStruct(&menuDefn);
      menuDefnPtr = &menuDefn;
   }
   else
      menuDefnPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetCharMenu(GetControlStructPtr(), name, newNamePtr, refreshCodePtr,
                          menuDefnPtr, helpTextPtr, ownerPtr, changeDiaryPtr,
                          objPropListPtr, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetCharMenu");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (menuDefnPtr != NULL)
      FreeARCharMenuStruct(&menuDefn, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
  
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARCreateCharMenu                            */
/*                                                                           */
/*****************************************************************************/

int APIARCreateCharMenu()

{
   char               changeDiary[1024];
   char               helpText[1024];
   ARCharMenuStruct   menuDefn;
   ARNameType         name;
   char              *objectModificationLogLabel = NULL;
   ARPropList         objPropList;
   ARAccessNameType   owner;
   unsigned int       refreshCode;
   int                result;
   ARStatusList       status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("CREATE CHAR MENU");
   strcpy(name, GetChar("Character Menu name: ", ""));
   refreshCode = GetUnsignedInt("Refresh on connect, open, interval (1 - 3) (1): ", 1);
   DriverPrintPrompt("Character menu struct:\n");
   GetARCharMenuStruct(&menuDefn);
   strcpy(helpText, GetChar("Help Text (): ", ""));
   strcpy(owner, GetChar("Owner (): ", ""));
   strcpy(changeDiary, GetChar("Change Diary (): ", ""));
   GetARPropList(&objPropList, FALSE);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateCharMenu(GetControlStructPtr(), name, refreshCode, &menuDefn,
                             helpText, owner, changeDiary, &objPropList,
                             objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateCharMenu");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARCharMenuStruct(&menuDefn, FALSE);
   FreeARPropList(&objPropList, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARDeleteCharMenu                            */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteCharMenu()

{
   unsigned int   deleteOption;
   ARNameType     name;
   char          *objectModificationLogLabel = NULL;
   int            result; 
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE CHAR MENU");
   strcpy(name, GetChar("Character Menu name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteCharMenu(GetControlStructPtr(), name, deleteOption, 
                             objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteCharMenu");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetListCharMenu                            */
/*                                                                           */
/*****************************************************************************/

int APIARGetListCharMenu()

{
   ARNameList     actLinkNameList = {0, NULL};
   ARTimestamp    changedSince;
   ARNameList     nameList;
   int            result;
   ARNameList     schemaNameList = {0, NULL};
   ARPropList     serverObjPropList = {0, NULL};
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST CHAR MENU");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);

   DriverPrintPrompt("Get menus associated with schemas:\n");
   GetARNameList(&schemaNameList, "");

   DriverPrintPrompt("Get menus associated with active links:\n");
   GetARNameList(&actLinkNameList, "");

   GetARPropList(&serverObjPropList, FALSE);

   /* Call routine */
   BeginAPICall();
   result = ARGetListCharMenu(GetControlStructPtr(), changedSince,
                              &schemaNameList, &actLinkNameList,
                              &serverObjPropList, &nameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListCharMenu");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("Character Menu list: ", "   ", &nameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&nameList, FALSE);
   FreeARNameList(&schemaNameList, FALSE);
   FreeARNameList(&actLinkNameList, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARGetMultipleCharMenus                         */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleCharMenus()
{
   ARTextStringList       changeDiaryList = {0, 0};
   ARTimestamp            changedSince = 0;
   ARBooleanList          existList = {0, 0};
   ARTextStringList       helpTextList = {0, 0};
   unsigned int           i;
   ARAccessNameList       lastChangedList = {0, 0};
   ARCharMenuStructList   menuDefnList = {0, 0};
   ARNameList             nameListIn = {0, 0};
   ARNameList             nameListOut = {0, 0};
   ARPropListList         objPropListList = {0, 0};
   ARAccessNameList       ownersList = {0, 0};
   ARQualifierList        queryList = {0, 0};
   ARUnsignedIntList      refreshCodeList = {0, 0};
   int                    result;
   ARStatusList           status = {0, NULL};
   ARTimestampList        timestampList = {0, 0};
   
   DriverPrintHeader("GET MULTIPLE CHAR MENUS");
                                       /* get parameter */
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get char menu by name:\n");
   GetARNameList(&nameListIn, "");

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleCharMenus(GetControlStructPtr(), changedSince,
                                   &nameListIn, &existList, &nameListOut,
                                   &refreshCodeList, &menuDefnList,
                                   &helpTextList, &timestampList, &ownersList,
                                   &lastChangedList, &changeDiaryList,
                                   &objPropListList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleCharMenus");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (nameListIn.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameListIn.numItems; i++)
         {
            DriverPrintResult("   Char Menu : %s\n", nameListIn.nameList[i]);
            PrintARBoolean("    exists: ", existList.booleanList[i]);
         }
      }
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            PrintChar("\n Char Menu name: ", nameListOut.nameList[i]);
            PrintUInt("Refresh Code: ", refreshCodeList.intList[i]);
            PrintARCharMenuStruct("", &menuDefnList.list[i]);
            PrintChar("Help text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Last update time: ", timestampList.timestampList[i]);
            PrintChar("Owner: ", ownersList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change Diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("", "", &objPropListList.propsList[i]);
         }
      }
   }
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&nameListIn, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&nameListOut, FALSE);
      FreeARUnsignedIntList(&refreshCodeList, FALSE);
      FreeARCharMenuStructList(&menuDefnList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownersList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARExpandCharMenu                            */
/*                                                                           */
/*****************************************************************************/

int APIARExpandCharMenu()

{
   ARCharMenuStruct   menu;
   ARCharMenuStruct   menuDefn; 
   int                result;
   unsigned int       maxRetrieve;
   unsigned int      *numMatchesPtr = NULL;
   unsigned int       numMatches;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("EXPAND CHAR MENU");
   DriverPrintPrompt("Character menu struct:\n");
   GetARCharMenuStruct(&menuDefn);

   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ",
                                500);

   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;

   /* Call routine */
   BeginAPICall();
   result = ARExpandCharMenu(GetControlStructPtr(), &menuDefn, maxRetrieve, &menu, 
                             numMatchesPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARExpandCharMenu");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARCharMenuStruct("", &menu);
 
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARCharMenuStruct(&menuDefn, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARCharMenuStruct(&menu, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/**********                                                         **********/
/**********                         FILTER                          **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                              APIARGetFilter                               */
/*                                                                           */
/*****************************************************************************/

int APIARGetFilter()

{
   ARFilterActionList        actionList;
   char                     *changeDiary;
   ARFilterActionList        elseList;
   unsigned int              enable;
   ARNameType                errorFilterName;
   unsigned int              errorFilterOptions;
   char                     *helpText;
   ARAccessNameType          lastChanged;
   ARNameType                name;
   ARPropList                objPropList;
   unsigned int              opSet;
   unsigned int              order;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result;
   ARStatusList              status = {0, NULL};
   ARTimestamp               timestamp;
   ARWorkflowConnectStruct   workflowConnect;
   
   /* Get parameters */
   DriverPrintHeader("GET FILTER");
   strcpy(name, GetChar("Filter name: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetFilter(GetControlStructPtr(), name, &order, &workflowConnect,
                        &opSet, &enable, &query, &actionList, &elseList,
                        &helpText, &timestamp, owner, lastChanged, &changeDiary,
                        &objPropList, &errorFilterOptions, errorFilterName, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetFilter");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintUInt("Filter order: ", order);
      PrintUInt("Workflow connect type: ", workflowConnect.type);
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         PrintUInt("Number of schema list items is: ",
                   workflowConnect.u.schemaList->numItems);
         PrintARNameList("Schema list: ", " ", workflowConnect.u.schemaList);    
      }
      PrintUInt("Operations bitmask: ", opSet);
      PrintUInt("Enable flag : ", enable);
      PrintARQualifierStruct("", &query);
      PrintARFilterActionList(&actionList, TRUE);
      PrintARFilterActionList(&elseList, FALSE);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
      PrintUInt("ErrorFilter Options bitmask: ", errorFilterOptions);
      PrintChar("ErrorFilter Name: ", errorFilterName);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARQualifierStruct(&query, FALSE);
      FreeARFilterActionList(&actionList, FALSE);
      FreeARFilterActionList(&elseList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
      FreeARPropList(&objPropList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARSetFilter                               */
/*                                                                           */
/*****************************************************************************/

int APIARSetFilter()

{
   ARFilterActionList        actionList;
   ARFilterActionList       *actionListPtr;
   char                      changeDiary[1024];
   char                     *changeDiaryPtr;
   ARFilterActionList        elseList;
   ARFilterActionList       *elseListPtr;
   unsigned int              enable;
   unsigned int             *enablePtr;
   ARNameType                errorFilterName;
   char                     *errorFilterNamePtr;
   unsigned int              errorFilterOptions;
   unsigned int             *errorFilterOptionsPtr;
   char                      helpText[1024];
   char                     *helpTextPtr;
   ARNameType                name;
   ARNameType                newName;
   char                     *newNamePtr;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARPropList               *objPropListPtr;
   unsigned int              opSet;
   unsigned int             *opSetPtr;
   unsigned int              order;
   unsigned int             *orderPtr;
   ARAccessNameType          owner;
   char                     *ownerPtr;
   ARQualifierStruct         query;
   ARQualifierStruct        *queryPtr;
   int                       result;  
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;
   ARWorkflowConnectStruct  *workflowConnectPtr;

   /* Get parameters */
   DriverPrintHeader("SET FILTER");
   strcpy(name, GetChar("Filter name: ", ""));
   if (GetARBoolean("Change filter name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change filter order? (F): ", FALSE))
   {
      order = GetUnsignedInt("Filter order (0 - 1000) (500): ", 500);
      orderPtr = &order;
   }
   else
      orderPtr = NULL;
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST &&
       GetARBoolean("Change schema or schemaList? (F): ", FALSE))
   {
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
         if (workflowConnect.u.schemaList == NULL)
         {
            DriverPrintMallocCallError("SetFilter");
            return AR_RETURN_ERROR;
         }
         workflowConnect.u.schemaList->numItems = 
            GetUnsignedInt("Number of schemas? (1): ", 1);
         if (workflowConnect.u.schemaList->numItems == 0)
            workflowConnect.u.schemaList->nameList = NULL;
         else
            workflowConnect.u.schemaList->nameList = 
                  (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                        sizeof(ARNameType));
         if (workflowConnect.u.schemaList->nameList == NULL)
         {
            DriverPrintMallocCallError("SetFilter");
            free(workflowConnect.u.schemaList);
            return AR_RETURN_ERROR;
         }
         for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
              numItems++)
         {
            if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                         numItems + 1) < 0)
               tmpString[sizeof(tmpString) - 1] = '\0';
            strcpy(workflowConnect.u.schemaList->nameList[numItems],
                   GetChar(tmpString, ""));
         }
      }
      workflowConnectPtr = &workflowConnect; 
   }
   else
      workflowConnectPtr = NULL;


   if (GetARBoolean("Change operations mask? (F): ", FALSE))
   {
      opSet = GetUnsignedInt("Operations bit mask (0): ", 0);
      opSetPtr = &opSet;
   }
   else
      opSetPtr = NULL;
   if (GetARBoolean("Change enable/disable? (F): ", FALSE))
   {
      enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
      enablePtr = &enable;
   }
   else
      enablePtr = NULL;
   if (GetARBoolean("Change query? (F): ", FALSE))
   {
      GetARQualifierStruct(&query);
      queryPtr = &query;
   }
   else
      queryPtr = NULL;
   if (GetARBoolean("Change action(s)? (F): ", FALSE))
   {
      GetARFilterActionList(TRUE, &actionList);
      actionListPtr = &actionList;
   }
   else
      actionListPtr = NULL;
   if (GetARBoolean("Change else(s)? (F): ", FALSE))
   {
      GetARFilterActionList(FALSE, &elseList);
      elseListPtr = &elseList;
   }
   else
      elseListPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
   if (GetARBoolean("Change ErrorFilter Options bitmask? (F): ", FALSE))
   {
      errorFilterOptions = GetUnsignedInt("ErrorFilter Options bitmask (0): ", 0);
      errorFilterOptionsPtr = &errorFilterOptions;
   }
   else
      errorFilterOptionsPtr = NULL;
   if (GetARBoolean("Change Error Filter name? (F): ", FALSE))
   {
      strcpy(errorFilterName, GetChar("   ErrorFilter name (): ", ""));
      errorFilterNamePtr = errorFilterName;
   }
   else
      errorFilterNamePtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);


   /* Call routine */
   BeginAPICall();
   result = ARSetFilter(GetControlStructPtr(), name, newNamePtr, orderPtr,
                        workflowConnectPtr, opSetPtr, enablePtr, queryPtr,
                        actionListPtr, elseListPtr, helpTextPtr, ownerPtr,
                        changeDiaryPtr, objPropListPtr, errorFilterOptionsPtr,
                        errorFilterNamePtr, objectModificationLogLabel, &status);
   EndAPICall(&status);


   /* Print results */
   DriverPrintResultHeader("ARSetFilter");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (queryPtr != NULL)
      FreeARQualifierStruct(&query, FALSE);
   if (actionListPtr != NULL)
      FreeARFilterActionList(&actionList, FALSE);
   if (elseListPtr != NULL)
      FreeARFilterActionList(&elseList, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   if (workflowConnectPtr != NULL)
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARCreateFilter                             */
/*                                                                           */
/*****************************************************************************/

int APIARCreateFilter()

{
   ARFilterActionList        actionList;
   char                      changeDiary[1024];
   ARFilterActionList        elseList;
   unsigned int              enable;
   ARNameType                errorFilterName;
   unsigned int              errorFilterOptions;
   char                      helpText[1024];
   ARNameType                name;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   unsigned int              opSet;
   unsigned int              order;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result;
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;

   /* Get parameters */
   DriverPrintHeader("CREATE FILTER");
   strcpy(name, GetChar("Filter name: ", ""));
   order = GetUnsignedInt("Filter order (0 - 1000) (500): ", 500);
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
   {
      workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
      if (workflowConnect.u.schemaList == NULL)
      {
         DriverPrintMallocCallError("CreateFilter");
         return AR_RETURN_ERROR;
      }
      workflowConnect.u.schemaList->numItems =
         GetUnsignedInt("Number of schemas (1): ", 1);
      if (workflowConnect.u.schemaList->numItems == 0)
         workflowConnect.u.schemaList->nameList = NULL;
      else
         workflowConnect.u.schemaList->nameList =
               (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                     sizeof(ARNameType));
      if (workflowConnect.u.schemaList->nameList == NULL)
      {
         DriverPrintMallocCallError("CreateFilter");
         free(workflowConnect.u.schemaList);
         return AR_RETURN_ERROR;
      }
      for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
           numItems++)
      {
         if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                      numItems + 1) < 0)
            tmpString[sizeof(tmpString) - 1] = '\0';
         strcpy(workflowConnect.u.schemaList->nameList[numItems],
                GetChar(tmpString, ""));
      }
   }

   opSet = GetUnsignedInt("Operations bit mask (0): ", 0);
   enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
   GetARQualifierStruct(&query);
   GetARFilterActionList(TRUE, &actionList);
   GetARFilterActionList(FALSE, &elseList);
   strcpy(helpText, GetChar("Help Text: ", ""));
   strcpy(owner, GetChar("Owner: ", GetControlStructPtr()->user));
   strcpy(changeDiary, GetChar("Change Diary: ", ""));
   GetARPropList(&objPropList, FALSE);
   errorFilterOptions = GetUnsignedInt("ErrorFilter Options bitmask (0): ", 0);
   strcpy(errorFilterName, GetChar("Error Filter name: ", ""));
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateFilter(GetControlStructPtr(), name, order, &workflowConnect,
                           opSet, enable, &query, &actionList, &elseList,
                           helpText, owner, changeDiary, &objPropList, 
                           errorFilterOptions, errorFilterName,
                           objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateFilter");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&query, FALSE);
   FreeARFilterActionList(&actionList, FALSE);
   FreeARFilterActionList(&elseList, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
 
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARDeleteFilter                             */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteFilter()

{
   unsigned int   deleteOption;
   ARNameType     name;
   char          *objectModificationLogLabel = NULL;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE FILTER");
   strcpy(name, GetChar("Filter name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteFilter(GetControlStructPtr(), name, deleteOption, 
                           objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteFilter");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetListFilter                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListFilter()

{
   ARTimestamp    changedSince;
   ARNameList     nameList;
   int            result;
   ARNameType     schema;
   char          *schemaPtr;
   ARPropList     serverObjPropList = {0, NULL};
   ARStatusList   status = {0, NULL};
      
   /* Get parameters */
   DriverPrintHeader("GETLIST FILTER");
   if (GetARBoolean("For a specific schema? (F): ", FALSE))
   {
      strcpy(schema, GetChar("Schema name (): ", ""));
      schemaPtr = schema;
   }
   else
      schemaPtr = NULL;
   changedSince = GetARTimestamp("Get all changed since (0): ",
                                 (ARTimestamp) 0);

   GetARPropList(&serverObjPropList, FALSE);
   
   /* Call routine */
   BeginAPICall();
   result = ARGetListFilter(GetControlStructPtr(), schemaPtr, changedSince,
                            &serverObjPropList, &nameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListFilter");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("Filter list: ", "   ", &nameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&nameList, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetMultipleFilters                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleFilters()
{
   ARFilterActionListList   actionListList = {0, 0};
   ARTextStringList         changeDiaryList = {0, 0};
   ARTimestamp              changedSince = 0;
   ARFilterActionListList   elseListList = {0, 0};
   ARUnsignedIntList        enableList = {0, 0};
   ARNameList               errorFilterNameList = {0, 0};
   ARUnsignedIntList        errorFilterOptionsList = {0, 0};
   ARBooleanList            existList = {0, 0};
   ARTextStringList         helpTextList = {0, 0};
   unsigned int             i;
   ARAccessNameList         lastChangedList = {0, 0};
   ARNameList               nameListIn = {0, 0};
   ARNameList               nameListOut = {0, 0};
   ARPropListList           objPropListList = {0, 0};
   ARUnsignedIntList        opSetList = {0, 0};
   ARUnsignedIntList        orderList = {0, 0};
   ARAccessNameList         ownersList = {0, 0};
   ARQualifierList          queryList = {0, 0};
   int                      result;
   ARStatusList             status = {0, NULL};
   ARTimestampList          timestampList = {0, 0};
   ARWorkflowConnectList    workflowConnectList = {0, 0};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE FILTERS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get filter by name:\n");
   GetARNameList(&nameListIn, "");
   
   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleFilters(GetControlStructPtr(), changedSince,
                                 &nameListIn, &existList, &nameListOut,
                                 &orderList, &workflowConnectList, &opSetList,
                                 &enableList, &queryList, &actionListList,
                                 &elseListList, &helpTextList, &timestampList,
                                 &ownersList, &lastChangedList,
                                 &changeDiaryList, &objPropListList, 
                                 &errorFilterOptionsList, &errorFilterNameList,
                                 &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleFilters");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (nameListIn.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameListIn.numItems; i++)
         {
            DriverPrintResult("   Filter : %s\n", nameListIn.nameList[i]);
            PrintARBoolean("    exists: ", existList.booleanList[i]);
         }
      }
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            ARWorkflowConnectStruct *const iwfl =
               &workflowConnectList.workflowConnectList[i];

            PrintChar("\n Filter name: ", nameListOut.nameList[i]);
            PrintUInt("Execution order: ", orderList.intList[i]);
            PrintUInt("Workflow connect type: ", iwfl->type);
            if (iwfl->type == AR_WORKFLOW_CONN_SCHEMA_LIST &&
                iwfl->u.schemaList)
            {
               PrintUInt("Number of schema list items is: ",
                         iwfl->u.schemaList->numItems);
               PrintARNameList("Schema list: ", " ", iwfl->u.schemaList); 
            }
            PrintUInt("Enable flag : ", enableList.intList[i]);
            PrintARQualifierStruct("", &queryList.qualifierList[i]);
            PrintARFilterActionList(&actionListList.actionListList[i], TRUE);
            PrintARFilterActionList(&elseListList.actionListList[i], FALSE);
            PrintChar("Help text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Last update time: ",
                             timestampList.timestampList[i]);
            PrintChar("Owner: ", ownersList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change Diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("", "", &objPropListList.propsList[i]);
            if (errorFilterOptionsList.numItems > 0)
               PrintUInt("ErrorFilter Options bitmask: ",
                         errorFilterOptionsList.intList[i]);
            if (errorFilterNameList.numItems > 0)
               PrintChar("ErrorFilter Name: ", errorFilterNameList.nameList[i]);
         }
      }
   }
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&nameListIn, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&nameListOut, FALSE);
      FreeARUnsignedIntList(&orderList, FALSE);
      FreeARWorkflowConnectList(&workflowConnectList, FALSE);
      FreeARUnsignedIntList(&enableList, FALSE);
      FreeARQualifierList(&queryList, FALSE);
      FreeARFilterActionListList(&actionListList, FALSE);
      FreeARFilterActionListList(&elseListList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownersList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
      FreeARUnsignedIntList(&errorFilterOptionsList, FALSE);
      FreeARNameList(&errorFilterNameList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/**********                                                         **********/
/**********                       ACTIVE LINK                       **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                            APIARGetActiveLink                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetActiveLink()

{
   ARActiveLinkActionList    actionList;
   ARInternalIdList          assignedGroupList;
   char                     *changeDiary;
   ARInternalId              controlField;
   ARActiveLinkActionList    elseList;
   unsigned int              enable;
   ARNameType                errorActlinkName;
   unsigned int              errorActlinkOptions;
   unsigned int              executeMask;
   ARInternalId              focusField;
   ARInternalIdList          groupList;
   char                     *helpText;
   ARAccessNameType          lastChanged;
   ARNameType                name;
   ARPropList                objPropList;
   unsigned int              order;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result; 
   ARStatusList              status = {0, NULL};
   ARTimestamp               timestamp;
   ARWorkflowConnectStruct   workflowConnect = {AR_WORKFLOW_CONN_SCHEMA_LIST, NULL};

   /* Get parameters */
   DriverPrintHeader("GET ACTIVE LINK");
   strcpy(name, GetChar("Active link name: ", ""));
   
   /* Call routine */
   BeginAPICall();
   result = ARGetActiveLink(GetControlStructPtr(), name, &order,
                            &workflowConnect, &assignedGroupList, &groupList,
                            &executeMask, &controlField, &focusField, &enable,
                            &query, &actionList, &elseList, &helpText, &timestamp,
                            owner, lastChanged, &changeDiary, &objPropList,
                            &errorActlinkOptions, errorActlinkName, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetActiveLink");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintUInt("Execution order: ", order);
      PrintUInt("Workflow connect type: ", workflowConnect.type);
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         PrintUInt("Number of schema list items is: ",
                   workflowConnect.u.schemaList->numItems);
         PrintARNameList("Schema list: ", " ", workflowConnect.u.schemaList); 
      }
      PrintARInternalIdList("Assigned Group list: ", "   ", &assignedGroupList);
      PrintARInternalIdList("Complete Group list: ", "   ", &groupList);
      PrintUInt("Execute bitmask: ", executeMask);
      PrintARInternalId("Control Field: ", controlField);
      PrintARInternalId("Focus Field: ", focusField);
      PrintUInt("Enable flag : ", enable);
      PrintARQualifierStruct("", &query);
      PrintARActiveLinkActionList(&actionList, TRUE);
      PrintARActiveLinkActionList(&elseList, FALSE);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
      PrintUInt("errorActlink Options bitmask: ", errorActlinkOptions);
      PrintChar("errorActlink Name: ", errorActlinkName);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARInternalIdList(&groupList, FALSE);
      FreeARQualifierStruct(&query, FALSE);
      FreeARActiveLinkActionList(&actionList, FALSE);
      FreeARActiveLinkActionList(&elseList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
      FreeARPropList(&objPropList, FALSE);
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARSetActiveLink                             */
/*                                                                           */
/*****************************************************************************/

int APIARSetActiveLink()

{
   ARActiveLinkActionList    actionList;
   ARActiveLinkActionList   *actionListPtr;
   char                      changeDiary[1024];
   char                     *changeDiaryPtr;
   ARInternalId              controlField;
   ARInternalId             *controlFieldPtr;
   ARActiveLinkActionList    elseList;
   ARActiveLinkActionList   *elseListPtr;
   unsigned int              enable;
   unsigned int             *enablePtr;
   ARNameType                errorActlinkName;
   char                     *errorActlinkNamePtr;
   unsigned int              errorActlinkOptions;
   unsigned int             *errorActlinkOptionsPtr;
   unsigned int              executeMask;
   unsigned int             *executeMaskPtr;
   ARInternalId              focusField;
   ARInternalId             *focusFieldPtr;
   ARInternalIdList          groupList;
   ARInternalIdList         *groupListPtr;
   char                      helpText[1024];
   char                     *helpTextPtr;
   ARNameType                name;
   ARNameType                newName;
   char                     *newNamePtr;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARPropList               *objPropListPtr;
   unsigned int              order;
   unsigned int             *orderPtr;
   ARAccessNameType          owner;
   char                     *ownerPtr;
   ARQualifierStruct         query;
   ARQualifierStruct        *queryPtr;
   int                       result;
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;
   ARWorkflowConnectStruct  *workflowConnectPtr;

   /* Get parameters */
   DriverPrintHeader("SET ACTIVE LINK");
   strcpy(name, GetChar("Active link name: ", ""));
   if (GetARBoolean("Change active link name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change execution order? (F): ", FALSE))
   {
      order = GetUnsignedInt("Execution order (1 - 1000) (1): ", 1);
      orderPtr = &order;
   }
   else
      orderPtr = NULL;
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST &&
       GetARBoolean("Change schemaList? (F): ", FALSE))
   {
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
         if (workflowConnect.u.schemaList == NULL)
         {
            DriverPrintMallocCallError("SetActiveLink");
            return AR_RETURN_ERROR;
         }
         workflowConnect.u.schemaList->numItems =
            GetUnsignedInt("Number of schemas? (1): ", 1);
         if (workflowConnect.u.schemaList->numItems == 0)
            workflowConnect.u.schemaList->nameList = NULL;
         else
            workflowConnect.u.schemaList->nameList =
                  (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                        sizeof(ARNameType));
         if (workflowConnect.u.schemaList->nameList == NULL)
         {
            DriverPrintMallocCallError("SetActiveLink");
            free(workflowConnect.u.schemaList);
            return AR_RETURN_ERROR;
         }
         for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
              numItems++)
         {
            if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                         numItems + 1) < 0)
               tmpString[sizeof(tmpString) - 1] = '\0';
            strcpy(workflowConnect.u.schemaList->nameList[numItems],
                   GetChar(tmpString, ""));
         }
      }
      workflowConnectPtr = &workflowConnect;
   }
   else
      workflowConnectPtr = NULL; 
   if (GetARBoolean("Change group list? (F): ", FALSE))
   {
      DriverPrintPrompt("Ids of groups allowed to perform active link:\n");
      GetARInternalIdList(&groupList);
      groupListPtr = &groupList;
   }
   else
      groupListPtr = NULL;
   if (GetARBoolean("Change execute mask? (F): ", FALSE))
   {
      executeMask = GetUnsignedInt("Execute bit mask (1): ", 1);
      executeMaskPtr = &executeMask;
   }
   else
      executeMaskPtr = NULL;
   if (GetARBoolean("Change control field? (F): ", FALSE))
   {
      controlField = GetLong("Control Field to tie active link to (0): ", 0);
      controlFieldPtr = &controlField;
   }
   else
      controlFieldPtr = NULL;
   if (GetARBoolean("Change focus field? (F): ", FALSE))
   {
      focusField = GetLong("Focus Field to tie active link to (0): ", 0);
      focusFieldPtr = &focusField;
   }
   else
      focusFieldPtr = NULL;
   if (GetARBoolean("Change enable/disable? (F): ", FALSE))
   {
      enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
      enablePtr = &enable;
   }
   else
      enablePtr = NULL;
   if (GetARBoolean("Change query? (F): ", FALSE))
   {
      GetARQualifierStruct(&query);
      queryPtr = &query;
   }
   else
      queryPtr = NULL;
   if (GetARBoolean("Change action(s)? (F): ", FALSE))
   {
      GetARActiveLinkActionList(TRUE, &actionList);
      actionListPtr = &actionList;
   }
   else
      actionListPtr = NULL;
   if (GetARBoolean("Change else(s)? (F): ", FALSE))
   {
      GetARActiveLinkActionList(FALSE, &elseList);
      elseListPtr = &elseList;
   }
   else
      elseListPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;

   if (GetARBoolean("Change Error Actlink Options bitmask? (F): ", FALSE))
   {
      errorActlinkOptions = GetUnsignedInt("errorActlink Options bitmask (0): ", 0);
      errorActlinkOptionsPtr = &errorActlinkOptions;
   }
   else
      errorActlinkOptionsPtr = NULL;
   if (GetARBoolean("Change Error Actlink name? (F): ", FALSE))
   {
      strcpy(errorActlinkName, GetChar("   errorActlink name (): ", ""));
      errorActlinkNamePtr = errorActlinkName;
   }
   else
      errorActlinkNamePtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetActiveLink(GetControlStructPtr(), name, newNamePtr, orderPtr,
                            workflowConnectPtr, groupListPtr, executeMaskPtr,
                            controlFieldPtr, focusFieldPtr, enablePtr, queryPtr,
                            actionListPtr, elseListPtr, helpTextPtr, ownerPtr,
                            changeDiaryPtr, objPropListPtr, errorActlinkOptionsPtr,
                            errorActlinkNamePtr, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetActiveLink");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (groupListPtr != NULL)
      FreeARInternalIdList(&groupList, FALSE);
   if (queryPtr != NULL)
      FreeARQualifierStruct(&query, FALSE);
   if (actionListPtr != NULL)
      FreeARActiveLinkActionList(&actionList, FALSE);
   if (elseListPtr != NULL)
      FreeARActiveLinkActionList(&elseList, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   if (workflowConnectPtr != NULL)
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARCreateActiveLink                           */
/*                                                                           */
/*****************************************************************************/

int APIARCreateActiveLink()

{
   ARActiveLinkActionList    actionList;
   char                      changeDiary[1024];
   ARInternalId              controlField;
   ARActiveLinkActionList    elseList;
   unsigned int              enable;
   ARNameType                errorActlinkName;
   unsigned int              errorActlinkOptions;
   unsigned int              executeMask;
   ARInternalId              focusField;
   ARInternalIdList          groupList;
   char                      helpText[1024];
   ARNameType                name;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   unsigned int              order;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result;
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;

   /* Get parameters */
   DriverPrintHeader("CREATE ACTIVE LINK");
   strcpy(name, GetChar("Active link name: ", ""));
   order = GetUnsignedInt("Execution order (1 - 1000) (1): ", 1);
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
   {
      workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
      if (workflowConnect.u.schemaList == NULL)
      {
         DriverPrintMallocCallError("CreateActiveLink");
         return AR_RETURN_ERROR;
      }
      workflowConnect.u.schemaList->numItems =
         GetUnsignedInt("Number of schemas (1): ", 1);
      if (workflowConnect.u.schemaList->numItems == 0)
         workflowConnect.u.schemaList->nameList = NULL;
      else
         workflowConnect.u.schemaList->nameList = 
               (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                     sizeof(ARNameType));
      if (workflowConnect.u.schemaList->nameList == NULL)
      {
         DriverPrintMallocCallError("CreateActiveLink");
         free(workflowConnect.u.schemaList);
         return AR_RETURN_ERROR;
      }
      for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
           numItems++)
      {
         if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                      numItems + 1) < 0)
            tmpString[sizeof(tmpString) - 1] = '\0';
         strcpy(workflowConnect.u.schemaList->nameList[numItems],
                GetChar(tmpString, ""));
      }
   }
   DriverPrintPrompt("Ids of groups allowed to perform active link:\n");
   GetARInternalIdList(&groupList);
   executeMask = GetUnsignedInt("Execute bit mask (1): ", 1);
   controlField = GetLong("Control Field to tie active link to (0): ", 0);
   focusField = GetLong("Focus Field to tie active link to (0): ", 0);
   enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
   GetARQualifierStruct(&query);
   GetARActiveLinkActionList(TRUE, &actionList);
   GetARActiveLinkActionList(FALSE, &elseList);
   strcpy(helpText, GetChar("Help Text: ", ""));
   strcpy(owner, GetChar("Owner: ", GetControlStructPtr()->user));
   strcpy(changeDiary, GetChar("Change Diary: ", ""));
   GetARPropList(&objPropList, FALSE);
   errorActlinkOptions = GetUnsignedInt("Error Actlink Options bitmask (0): ", 0);
   strcpy(errorActlinkName, GetChar("Error Actlink name: ", ""));
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateActiveLink(GetControlStructPtr(), name, order,
                               &workflowConnect, &groupList, executeMask,
                               &controlField, &focusField, enable, &query,
                               &actionList, &elseList, helpText, owner,
                               changeDiary, &objPropList, 
                               errorActlinkOptions, errorActlinkName, 
                               objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateActiveLink");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARInternalIdList(&groupList, FALSE);
   FreeARQualifierStruct(&query, FALSE);
   FreeARActiveLinkActionList(&actionList, FALSE);
   FreeARActiveLinkActionList(&elseList, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARDeleteActiveLink                           */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteActiveLink()

{
   unsigned int   deleteOption;
   ARNameType     name;
   char          *objectModificationLogLabel = NULL;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE ACTIVE LINK");
   strcpy(name, GetChar("Active link name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteActiveLink(GetControlStructPtr(), name, deleteOption, 
                               objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteActiveLink");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetListActiveLink                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetListActiveLink()

{
   ARTimestamp    changedSince;
   ARNameList     nameList;
   int            result;
   ARNameType     schema;
   char          *schemaPtr;
   ARPropList     serverObjPropList = {0, NULL};
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GETLIST ACTIVE LINK");
   if (GetARBoolean("For a specific schema? (F): ", FALSE))
   {
      strcpy(schema, GetChar("Schema name (): ", ""));
      schemaPtr = schema;
   }
   else
      schemaPtr = NULL;
   changedSince = GetARTimestamp("Get all changed since (0): ",
                                 (ARTimestamp) 0);
   GetARPropList(&serverObjPropList, FALSE);
  
   /* Call routine */
   BeginAPICall();
   result = ARGetListActiveLink(GetControlStructPtr(), schemaPtr, changedSince,
                                &serverObjPropList, &nameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListActiveLink");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("Active link list: ", "   ", &nameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&nameList, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetMultipleActiveLinks                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleActiveLinks()

{
   ARActiveLinkActionListList   actionListList;
   ARInternalIdListList         assignedGroupListList;
   ARTextStringList             changeDiaryList;
   ARTimestamp                  changedSince;
   ARInternalIdList             controlFieldList;
   ARActiveLinkActionListList   elseListList;
   ARUnsignedIntList            enableList;
   ARNameList                   errorActlinkNameList = {0, 0};
   ARUnsignedIntList            errorActlinkOptionsList = {0, 0};
   ARUnsignedIntList            executeMaskList;
   ARBooleanList                existList;
   ARInternalIdList             focusFieldList;
   ARInternalIdListList         groupListList;
   ARTextStringList             helpTextList;
   unsigned int                 i;
   ARAccessNameList             lastChangedList;
   ARNameList                   nameListIn;
   ARNameList                   nameListOut;
   ARPropListList               objPropListList;
   ARUnsignedIntList            orderList;
   ARAccessNameList             ownersList;
   ARQualifierList              queryList;
   int                          result;
   ARStatusList                 status = {0, NULL};
   ARTimestampList              timestampList;
   ARWorkflowConnectList        workflowConnectList;

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE ACTIVE LINKS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get active link by name:\n");
   GetARNameList(&nameListIn, "");
   
   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleActiveLinks(GetControlStructPtr(), changedSince,
                                     &nameListIn, &existList, &nameListOut,
                                     &orderList, &workflowConnectList,
                                     &assignedGroupListList,
                                     &groupListList, &executeMaskList,
                                     &controlFieldList, &focusFieldList,
                                     &enableList, &queryList, &actionListList,
                                     &elseListList, &helpTextList, 
                                     &timestampList, &ownersList,
                                     &lastChangedList, &changeDiaryList,
                                     &objPropListList, &errorActlinkOptionsList,
                                     &errorActlinkNameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleActiveLinks");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (nameListIn.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameListIn.numItems; i++)
         {
            DriverPrintResult("   Active Link : %s\n", nameListIn.nameList[i]);
            PrintARBoolean("    exists: ", existList.booleanList[i]);
         }
      } 
      for (i = 0; i < existList.numItems; i++)
      { 
         if (existList.booleanList[i])
         {
            PrintChar("\n Active Link name: ", nameListOut.nameList[i]);
            PrintUInt("Execution order: ", orderList.intList[i]);
            PrintUInt("Workflow connect type: ", workflowConnectList.workflowConnectList[i].type);
            if (workflowConnectList.workflowConnectList[i].type == AR_WORKFLOW_CONN_SCHEMA_LIST &&
                workflowConnectList.workflowConnectList[i].u.schemaList)
            {
               PrintUInt("Number of schema list items is: ",
                         workflowConnectList.workflowConnectList[i].u.schemaList->numItems);
               PrintARNameList("Schema list: ", " ",
                               workflowConnectList.workflowConnectList[i].u.schemaList); 
            }
            if (assignedGroupListList.internalIdListList)
               PrintARInternalIdList("Assigned Group list: ", "   ",
                                     &assignedGroupListList.internalIdListList[i]);
            if (groupListList.internalIdListList)
               PrintARInternalIdList("Complete Group list: ", "   ",
                                     &groupListList.internalIdListList[i]);
            PrintUInt("Execute bitmask: ", executeMaskList.intList[i]);
            PrintARInternalId("Control Field: ", controlFieldList.internalIdList[i]);
            PrintARInternalId("Focus Field: ", focusFieldList.internalIdList[i]);
            PrintUInt("Enable flag : ", enableList.intList[i]);
            PrintARQualifierStruct("", &queryList.qualifierList[i]);
            PrintARActiveLinkActionList(&actionListList.actionListList[i], TRUE);
            PrintARActiveLinkActionList(&elseListList.actionListList[i], FALSE);
            PrintChar("Help text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Last update time: ", timestampList.timestampList[i]);
            PrintChar("Owner: ", ownersList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change Diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("", "", &objPropListList.propsList[i]);
            if (errorActlinkOptionsList.numItems > 0)
               PrintUInt("errorActlink Options bitmask: ",
                         errorActlinkOptionsList.intList[i]);
            if (errorActlinkNameList.numItems > 0)
               PrintChar("errorActlink Name: ", errorActlinkNameList.nameList[i]);
         }
      }
   }
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&nameListIn, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&nameListOut, FALSE);
      FreeARUnsignedIntList(&orderList, FALSE);
      FreeARWorkflowConnectList(&workflowConnectList, FALSE);
      FreeARInternalIdListList(&groupListList, FALSE);
      FreeARUnsignedIntList(&executeMaskList, FALSE);
      FreeARInternalIdList(&controlFieldList, FALSE);
      FreeARInternalIdList(&focusFieldList, FALSE);
      FreeARUnsignedIntList(&enableList, FALSE);
      FreeARQualifierList(&queryList, FALSE);
      FreeARActiveLinkActionListList(&actionListList, FALSE);
      FreeARActiveLinkActionListList(&elseListList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownersList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
      FreeARUnsignedIntList(&errorActlinkOptionsList, FALSE);
      FreeARNameList(&errorActlinkNameList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;
}
 

/*****************************************************************************/
/**********                                                         **********/
/**********                       SUPPORT FILE                      **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                            APIARGetSupportFile                            */
/*                                                                           */
/*****************************************************************************/

int APIARGetSupportFile()

{
   ARInternalId   fileId;
   char           filename[256];
   FILE          *filePtr;
   unsigned int   fileType;
   ARInternalId   id2;
   ARNameType     name;
   int            result;
   ARStatusList   status = {0, NULL};
   ARTimestamp    timestamp;

   /* Get parameters */
   DriverPrintHeader("GET SUPPORT FILE");
   fileType = GetUnsignedInt("File type -- external report (1) (1): ", 1);
   strcpy(name, GetChar("Name of associated object: ", ""));
   id2 = GetLong("Supporting ID for object (0): ", 0);
   fileId = GetLong("ID for the file (1): ", 1);

   filePtr = NULL;
   while (filePtr == NULL)
   {
      strcpy(filename, GetChar("Filename to hold returned file: ", ""));
#ifdef _WIN32
      filePtr = fopen(filename, "wb");
#else /* _WIN32 */
      filePtr = fopen(filename, "w");
#endif /* _WIN32 */
      if (filePtr == NULL)
         DriverPrintError("   ***  File open failure\n");
   }

   /* Call routine */
   BeginAPICall();
   result = ARGetSupportFile(GetControlStructPtr(), fileType, name, id2, fileId,
                             filePtr, &timestamp, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetSupportFile");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARTimestamp("Last update time: ", timestamp);
   PrintARStatusList(&status);

   /* Cleanup */
   fclose(filePtr);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARSetSupportFile                            */
/*                                                                           */
/*****************************************************************************/

int APIARSetSupportFile()

{
   ARInternalId   fileId;
   char           filename[256];
   FILE          *filePtr;
   unsigned int   fileType;
   ARInternalId   id2;
   ARNameType     name;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET SUPPORT FILE");
   fileType = GetUnsignedInt("File type -- external report (1) (1): ", 1);
   strcpy(name, GetChar("Name of associated object: ", ""));
   id2 = GetLong("Supporting ID for object (0): ", 0);
   fileId = GetLong("ID for the file (1): ", 1);

   filePtr = NULL;
   while (filePtr == NULL)
   {
      strcpy(filename, GetChar("Filename to load to server: ", ""));
#ifdef _WIN32
      filePtr = fopen(filename, "rb");
#else /* _WIN32 */
      filePtr = fopen(filename, "r");
#endif /* _WIN32 */
      if (filePtr == NULL)
         DriverPrintError("   ***  File open failure\n");
   }

   /* Call routine */
   BeginAPICall();
   result = ARSetSupportFile(GetControlStructPtr(), fileType, name, id2, fileId,
                             filePtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetSupportFile");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   fclose(filePtr);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARCreateSupportFile                           */
/*                                                                           */
/*****************************************************************************/

int APIARCreateSupportFile()

{
   ARInternalId   fileId;
   char           filename[256];
   FILE          *filePtr;
   unsigned int   fileType;
   ARInternalId   id2;
   ARNameType     name;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("CREATE SUPPORT FILE");
   fileType = GetUnsignedInt("File type -- external report (1) (1): ", 1);
   strcpy(name, GetChar("Name of associated object: ", ""));
   id2 = GetLong("Supporting ID for object (0): ", 0);
   fileId = GetLong("ID for the file (1): ", 1);

   filePtr = NULL;
   while (filePtr == NULL)
   {
      strcpy(filename, GetChar("Filename to load to server: ", ""));
#ifdef _WIN32
      filePtr = fopen(filename, "rb");
#else /* _WIN32 */
      filePtr = fopen(filename, "r");
#endif /* _WIN32 */
      if (filePtr == NULL)
         DriverPrintError("   ***  File open failure\n");
   }

   /* Call routine */
   BeginAPICall();
   result = ARCreateSupportFile(GetControlStructPtr(), fileType, name, id2,
                                fileId, filePtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateSupportFile");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   fclose(filePtr);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARDeleteSupportFile                           */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteSupportFile()

{
   ARInternalId   fileId;
   unsigned int   fileType;
   ARInternalId   id2;
   ARNameType     name;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE SUPPORT FILE");
   fileType = GetUnsignedInt("File type -- external report (1) (1): ", 1);
   strcpy(name, GetChar("Name of associated object: ", ""));
   id2 = GetLong("Supporting ID for object (0): ", 0);
   fileId = GetLong("ID for the file (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteSupportFile(GetControlStructPtr(), fileType, name, id2,
                                fileId, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteSupportFile");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetListSupportFile                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetListSupportFile()

{
   ARTimestamp        changedSince;
   ARInternalIdList   fileIdList;
   unsigned int       fileType;
   ARInternalId       id2;
   ARNameType         name;
   int                result;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST SUPPORT FILE");
   fileType = GetUnsignedInt("File type -- external report (1) (1): ", 1);
   strcpy(name, GetChar("Name of associated object: ", ""));
   id2 = GetLong("Supporting ID for object (0): ", 0);
   changedSince = GetARTimestamp("Get all changed since (0): ",
                                 (ARTimestamp) 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetListSupportFile(GetControlStructPtr(), fileType, name, id2,
                                 changedSince, &fileIdList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListSupportFile");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARInternalIdList("File ID list: ", "   ", &fileIdList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARInternalIdList(&fileIdList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/**********                                                         **********/
/**********                       GROUP/USER                        **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                             APIARGetListGroup                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListGroup()

{
   ARGroupInfoList    groupList;
   ARPasswordType     password;
   char              *passwordPtr;
   int                result;
   ARStatusList       status = {0, NULL};
   ARAccessNameType   user;
   char              *userPtr;
   
   /* Get parameters */
   DriverPrintHeader("GETLIST GROUP");
   if (GetARBoolean("For a specific user? (F): ", FALSE))
   {
      strcpy(user, GetChar("Name of the user: ",
                           GetControlStructPtr()->user));
      userPtr = user;

      strcpy(password, GetChar("Password of the user: ",
                               GetControlStructPtr()->password));
      passwordPtr = password;
   }
   else
   {
      userPtr = NULL;
      passwordPtr = NULL;
   }

   /* Call routine */
   BeginAPICall();
   result = ARGetListGroup(GetControlStructPtr(), userPtr, passwordPtr,
                           &groupList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListGroup");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARGroupInfoList(&groupList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARGroupInfoList(&groupList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetListRole                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetListRole()

{
   ARNameType         applicationName;
   ARPasswordType     password;
   char              *passwordPtr;
   int                result;
   ARRoleInfoList     roleList;
   ARStatusList       status = {0, NULL};
   ARAccessNameType   user;
   char              *userPtr;
   
   /* Get parameters */
   DriverPrintHeader("GETLIST ROLE");

   strcpy(applicationName, GetChar("Name of the application: ", ""));

   if (GetARBoolean("For a specific user? (F): ", FALSE))
   {
      strcpy(user, GetChar("Name of the user: ",
                           GetControlStructPtr()->user));
      userPtr = user;

      strcpy(password, GetChar("Password of the user: ",
                               GetControlStructPtr()->password));
      passwordPtr = password;
   }
   else
   {
      userPtr = NULL;
      passwordPtr = NULL;
   }

   /* Call routine */
   BeginAPICall();
   result = ARGetListRole(GetControlStructPtr(), applicationName, userPtr, passwordPtr,
                           &roleList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListRole");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARRoleInfoList(&roleList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARRoleInfoList(&roleList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARGetListApplicationState                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetListApplicationState()

{
   int            result;
   ARNameList     stateNameList;        
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GETLIST APPLICATION STATE");

   /* Call routine */
   BeginAPICall();
   result = ARGetListApplicationState(GetControlStructPtr(), &stateNameList,
                                      &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListApplicationState");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("   State Names:", "      ", &stateNameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&stateNameList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARGetApplicationState                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetApplicationState()

{
   ARNameType     applicationName;
   ARNameType     currentState;
   int            result;
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GET APPLICATION STATE");
   strcpy(applicationName, GetChar("Name of the application: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetApplicationState(GetControlStructPtr(), applicationName,
                                  currentState, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetApplicationState");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      DriverPrintResult("   Application: \"%s\" is currently in the \"%s\" state.\n",
                        applicationName, currentState);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARSetApplicationState                          */
/*                                                                           */
/*****************************************************************************/

int APIARSetApplicationState()

{
   ARNameType     applicationName;
   ARNameType     newState;
   int            result;
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("SET APPLICATION STATE");
   strcpy(applicationName, GetChar("Name of the application: ", ""));
   strcpy(newState, GetChar("Name of the new state: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARSetApplicationState(GetControlStructPtr(), applicationName,
                                  newState, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetApplicationState");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      DriverPrintResult("   Application: \"%s\" is now in the \"%s\" state.\n",
                        applicationName, newState);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetListUser                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetListUser()

{
   ARTimestamp      changedSince;
   int              result;
   ARStatusList     status = {0, NULL};
   ARUserInfoList   userList;
   unsigned int     userListType;

   /* Get parameters */
   DriverPrintHeader("GETLIST USER");
   userListType = GetUnsignedInt(
                 "User List Type (myself, registered, current, invalid, or application) (0-4)(1): ", 1);
   changedSince = GetARTimestamp("Get registered users changed since (0 means all): ",
                                 (ARTimestamp) 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetListUser(GetControlStructPtr(), userListType, changedSince,
                          &userList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListUser");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARUserInfoList(&userList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARUserInfoList(&userList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/**********                                                         **********/
/**********                      MISCELLANEOUS                      **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                              APIARVerifyUser                              */
/*                                                                           */
/*****************************************************************************/

int APIARVerifyUser()

{
   ARBoolean      adminFlag;
   ARBoolean      customFlag;
   int            result;
   ARStatusList   status = {0, NULL};
   ARBoolean      subadminFlag;

   /* Get parameters */
   DriverPrintHeader("VERIFY USER");

   /* Call routine */
   BeginAPICall();
   result = ARVerifyUser(GetControlStructPtr(), &adminFlag, &subadminFlag,
                         &customFlag, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARVerifyUser");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARBoolean("Admin flag: ", adminFlag);
      PrintARBoolean("SubAdmin flag: ", subadminFlag);
      PrintARBoolean("Customize flag: ", customFlag);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}

int APIARVerifyUser2()
{
   ARBoolean      adminFlag, subadminFlag, customFlag;
   ARBoolean      stadminFlag, stsubadminFlag, overlayFlag, boverlayFlag;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("VERIFY USER2");

   /* Call routine */
   BeginAPICall();
   result = ARVerifyUser2(GetControlStructPtr(),
               &adminFlag, &subadminFlag, &customFlag,
               &stadminFlag, &stsubadminFlag, &overlayFlag, &boverlayFlag,
               &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARVerifyUser");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARBoolean("Admin flag: ", adminFlag);
      PrintARBoolean("SubAdmin flag: ", subadminFlag);
      PrintARBoolean("Customize flag: ", customFlag);
      PrintARBoolean("Struct Admin flag: ", stadminFlag);
      PrintARBoolean("Struct SubAdmin flag: ", stsubadminFlag);
      PrintARBoolean("Overlay Group flag: ", overlayFlag);
      PrintARBoolean("Base Overlay Group flag: ", boverlayFlag);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                                APIARExport                                */
/*                                                                           */
/*****************************************************************************/

struct exportctl {
   ARBoolean tofile;
   char header[8];
   char rheader[24];
};
static int APIARExport1(const struct exportctl *);

int APIARExport(void) {
   static const struct exportctl ctl = {
      0, "EXPORT",
      "ARExport"
   };
   return APIARExport1(&ctl);
}

int APIARExportToFile(void) {
   static const struct exportctl ctl = {
      1, "EXPORTF",
      "ARExportToFile"
   };
   return APIARExport1(&ctl);
}

static int APIARExport1(const struct exportctl *ctl)
{
   char                  *buff = NULL;
   ARNameType             displayTag;
   char                  *fileExt = NULL;
   char                   filename[256];
   FILE                  *filePtr = NULL;
   unsigned int           i;
   unsigned int           len = 0;
   ARWorkflowLockStruct   lockInfo;
   int                    result;
   ARStatusList           status = {0, NULL};
   ARStructItemList       structItems;
   unsigned int           vuiType;
   unsigned int           exportOption = 0;
   unsigned int           workflowOption = 0;
   unsigned int           exportSettings = 0;
   
   /* Get parameters */
   DriverPrintHeader(ctl->header);
   GetARStructItemList(&structItems,&exportSettings);

   /* If exporting any VIEWs, prompt for the display tag and vui-type. */
   strcpy(displayTag, "");
   vuiType = 0;
   for (i = 0; i < structItems.numItems; i++)
   {
      if (structItems.structItemList[i].type == AR_STRUCT_ITEM_SCHEMA_VIEW ||
          structItems.structItemList[i].type == AR_STRUCT_ITEM_SCHEMA_VIEW_2)
      {
         strcpy(displayTag, GetChar("Display Tag: ", ""));
         vuiType = GetUnsignedInt("VUI-Type (0): ", 0);
         break;
      }
   }

   exportOption |= exportSettings;

   /* If applying lock to exported objects get the lock information */
   GetARWorkflowLockStruct(&lockInfo);

   filePtr = NULL;
   strcpy(filename, GetChar("Filename for exported data: ", ""));
   if (filename[0] != '\0')
   {
      /* consider export format only if file extension provided */
      /* doesn't fall in one of valid export format def or xml  */
      /* this is in sync with WAT/devstudio behavior            */
      len = strlen(filename);      
      if ((len > 4) && (fileExt = &filename[strlen(filename) - 4]) && 
         (!strcasecmp(fileExt, ".def") || !strcasecmp(fileExt, ".xml")))
      {
         /* set export type as per file extension */
         if (!strcasecmp(fileExt, ".def") && gExportFormat == 2)
         {
            for (i = 0; i < structItems.numItems; i++)
               structItems.structItemList[i].type ^= AR_STRUCT_XML_OFFSET;
         }
         else if (!strcasecmp(fileExt, ".xml") && gExportFormat == 1)
         {
            for (i = 0; i < structItems.numItems; i++)
               structItems.structItemList[i].type |= AR_STRUCT_XML_OFFSET;
         }
      }  
      else
      {
         /* append appropiate extension as per export type */
         if (gExportFormat == 1)
            strncat(filename, ".def", 256);
         else
            strncat(filename, ".xml", 256);
      }
      
      filePtr = fopen(filename, "w");
   }
   if (filePtr == NULL)
   {
      DriverPrintError("\n   ***  File open failure: %s\n", filename);
      return AR_RETURN_ERROR;
   }

   /* Call routine */
   if (ctl->tofile)
   {
      BeginAPICall();
      result = ARExportToFile(GetControlStructPtr(), &structItems, displayTag,
                              vuiType, exportOption, &lockInfo, filePtr, &status);
      EndAPICall(&status);
   }
   else
   {
      BeginAPICall();
      result = ARExport(GetControlStructPtr(), &structItems, displayTag,
                        vuiType, exportOption, &lockInfo, &buff, &status);
      EndAPICall(&status);
      if ((result < AR_RETURN_ERROR) && (buff != NULL))
         fwrite(buff, strlen(buff), 1, filePtr);
   }

   /* Print results */
   DriverPrintResultHeader(ctl->rheader);
   PrintReturnCode(result);

   fclose(filePtr);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (buff != NULL)
         free(buff);
   }
   else
      unlink(filename);
   FreeARStructItemList(&structItems, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                                APIARImport                                */
/*                                                                           */
/*****************************************************************************/

int APIARImport()

{
   int                amtRead;
   int                amtRemaining;
   long               fileLen;
   char               filename[256];
   FILE              *filePtr;
   char              *importBuf;
   unsigned int       importOption;
   char              *objectModificationLogLabel = NULL;
   int                result;
   ARStatusList       status = {0, NULL};
   ARStructItemList   structItems;

   /* Get parameters */
   DriverPrintHeader("IMPORT");
   GetARStructItemList(&structItems, NULL);
   
   strcpy(filename, GetChar("Filename containing import data: ", ""));
   importOption = GetUnsignedInt("The import option : (0) ", 0);

   filePtr = NULL;
   if (filename[0] != '\0')
      filePtr = fopen(filename, "r");
   if (filePtr == NULL)
   {
      DriverPrintError("\n   ***  File open failure: %s\n", filename);
      return AR_RETURN_ERROR;
   }

   fseek(filePtr, 0L, 2);
   fileLen = ftell(filePtr);
   importBuf = (char *) malloc((unsigned) (fileLen + 1));
   if (importBuf == NULL)
   {
      fclose(filePtr);
      DriverPrintError("**** malloc failure for import operation ****\n");
      return AR_RETURN_ERROR;
   }
   rewind(filePtr);

   /* Note that the length of the file (fileLen) may actually be */
   /* more than what can be read.  Some standard io libraries    */
   /* will compress carriage returns (\r) and new lines (\n) on  */
   /* fread(), but this may not be reflected in ftell().         */
   for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
   {
      result = fread(&importBuf[amtRead], 1, amtRemaining, filePtr);
      if (result <= 0)
         break;
      amtRemaining -= result;
      amtRead += result;
   }
   importBuf[amtRead] = '\0';
   fclose(filePtr);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARImport(GetControlStructPtr(), &structItems, importBuf,
                     importOption, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARImport");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   free(importBuf);
   FreeARStructItemList(&structItems, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                               APIARUnImport                               */
/*                                                                           */
/*****************************************************************************/

int APIARUnImport()

{
   int                amtRead;
   int                amtRemaining;
   long               fileLen;
   char               filename[256];
   FILE              *filePtr;
   int                result;
   ARStatusList       status = {0, NULL};
   ARStructItemList   structItems;
   char              *unImportBuf;
   unsigned int       unImportOption;

   /* Get parameters */
   DriverPrintHeader("UNIMPORT");
   GetARStructItemList(&structItems, NULL);
   
   strcpy(filename, GetChar("Filename containing the unimport data: ", ""));
   unImportOption = GetUnsignedInt("The unimport option : (0) ", 0);

   filePtr = NULL;
   if (filename[0] != '\0')
      filePtr = fopen(filename, "r");
   if (filePtr == NULL)
   {
      DriverPrintError("\n   ***  File open failure: %s\n", filename);
      return AR_RETURN_ERROR;
   }

   fseek(filePtr, 0L, 2);
   fileLen = ftell(filePtr);
   unImportBuf = (char *) malloc((unsigned) (fileLen + 1));
   if (unImportBuf == NULL)
   {
      fclose(filePtr);
      DriverPrintError("**** malloc failure for unimport operation ****\n");
      return AR_RETURN_ERROR;
   }
   rewind(filePtr);

   /* Note that the length of the file (fileLen) may actually be */
   /* more than what can be read.  Some standard io libraries    */
   /* will compress carriage returns (\r) and new lines (\n) on  */
   /* fread(), but this may not be reflected in ftell().         */
   for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
   {
      result = fread(&unImportBuf[amtRead], 1, amtRemaining, filePtr);
      if (result <= 0)
         break;
      amtRemaining -= result;
      amtRead += result;
   }
   unImportBuf[amtRead] = '\0';
   fclose(filePtr);

   /* Call routine */
   BeginAPICall();
   result = ARUnImport(GetControlStructPtr(), &structItems, unImportBuf,
                       unImportOption, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARUnImport");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   free(unImportBuf);
   FreeARStructItemList(&structItems, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetListServer                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetListServer()

{
   ARServerNameList   nameList;
   int                result;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST SERVER");

   /* Call routine */
   BeginAPICall();
   result = ARGetListServer(GetControlStructPtr(), &nameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListServer");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARServerNameList("Server list: ", "   ", &nameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARServerNameList(&nameList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARInitialization                            */
/*                                                                           */
/*****************************************************************************/

int APIARInitialization()

{
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("INITIALIZATION");

   /* Call routine */
   BeginAPICall();
   result = ARInitialization(GetControlStructPtr(), &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARInitialization");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   if (gSetServerPort)
      APIARSetServerPort(FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARTermination                              */
/*                                                                           */
/*****************************************************************************/

int APIARTermination()

{
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("TERMINATION");

   /* Call routine */
   BeginAPICall();
   result = ARTermination(GetControlStructPtr(), &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARTermination");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetServerInfo                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetServerInfo()

{
   ARServerInfoRequestList   requestList;
   int                       result;
   ARServerInfoList          serverInfoList;
   ARStatusList              status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET SERVER INFO");
   GetARServerInfoRequestList(AR_MAX_SERVER_INFO_USED, &requestList);

   /* Call routine */
   BeginAPICall();
   result = ARGetServerInfo(GetControlStructPtr(), &requestList,
                            &serverInfoList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetServerInfo");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARServerInfoList(&serverInfoList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARServerInfoList(&serverInfoList, FALSE);
   FreeARServerInfoRequestList(&requestList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARSetServerInfo                             */
/*                                                                           */
/*****************************************************************************/

int APIARSetServerInfo()

{
   int                result;
   ARServerInfoList   serverInfoList;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET SERVER INFO");
   GetARServerInfoList(&serverInfoList);

   /* Call routine */
   BeginAPICall();
   result = ARSetServerInfo(GetControlStructPtr(), &serverInfoList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetServerInfo");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARServerInfoList(&serverInfoList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARSetServerPort                             */
/*                                                                           */
/*****************************************************************************/

int APIARSetServerPort(ARBoolean prompt)
{
   unsigned int   portNumber;
   unsigned int   privateSocket;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   (prompt) ? DriverPrintHeader("SET SERVER PORT")
            : DriverPrintHeader("\nSET SERVER PORT");

   if (prompt)
   {
      portNumber = GetUnsignedInt("The port number of server (0): ", 0);
      privateSocket = GetUnsignedInt("Using private socket? (0): ", 0);
   }
   else
   {
      portNumber    = gPortNumber;
      privateSocket = gSocketNumber;
   }

   /* Call routine */
   BeginAPICall();
   result = ARSetServerPort(GetControlStructPtr(),
                            GetControlStructPtr()->server, portNumber,
                            privateSocket, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetServerPort");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARGetServerStatistics                          */
/*                                                                           */
/*****************************************************************************/

int APIARGetServerStatistics()

{
   ARServerInfoRequestList   requestList;
   int                       result; 
   ARServerInfoList          serverInfoList;
   ARStatusList              status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET SERVER STATISTICS");
   GetARServerInfoRequestList(AR_MAX_SERVER_STAT_USED, &requestList);

   /* Call routine */
   BeginAPICall();
   result = ARGetServerStatistics(GetControlStructPtr(), &requestList,
                                  &serverInfoList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetServerStatistics");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARServerInfoList2(&serverInfoList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARServerInfoList(&serverInfoList, FALSE);
   FreeARServerInfoRequestList(&requestList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetTextForErrorMessage                        */
/*                                                                           */
/*****************************************************************************/

void APIARGetTextForErrorMessage()

{
   char  *messageBuf;
   int    messageId;

   /* Get parameters */
   DriverPrintHeader("GET TEXT FOR ERROR MESSAGE");
   messageId = GetInt("The message identifier for the message (0): ", 0);

   /* Call routine */
   BeginAPICall();
   messageBuf = ARGetTextForErrorMessage(messageId);
   EndAPICall(NULL);

   /* Print results */
   DriverPrintResultHeader("ARGetTextForErrorMessage");
   PrintChar("Message Text: ", messageBuf);
   
   /* Cleanup */
   if (messageBuf)
      free(messageBuf);
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARGetListSQL                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetListSQL()

{
   unsigned int      maxRetrieve;
   unsigned int      numMatches;
   unsigned int     *numMatchesPtr;
   int               result;
   char             *sqlCommand;
   ARStatusList      status = {0, NULL};
   ARValueListList   valueListList;

   /* Get parameters */
   DriverPrintHeader("GETLIST SQL");
   GetMallocChar("SQL command: ", "", &sqlCommand);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ", 500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARGetListSQL(GetControlStructPtr(), sqlCommand, maxRetrieve,
                         &valueListList, numMatchesPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListSQL");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARValueListList(&valueListList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (sqlCommand != NULL)
      free(sqlCommand);
   if (result < AR_RETURN_ERROR)
      FreeARValueListList(&valueListList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARExecuteProcess                            */
/*                                                                           */
/*****************************************************************************/

int APIARExecuteProcess()

{
   char           command[1024];
   int            result;
   int            returnStatus;
   int           *returnStatusPtr;
   char          *returnString;
   char         **returnStringPtr;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("EXECUTE PROCESS");
   strcpy(command, GetChar("Command: ", ""));
   if (GetARBoolean("Wait for process to complete? (F): ", FALSE))
   {
      returnStatusPtr = &returnStatus;
      returnStringPtr = &returnString;
   }
   else
   {
      returnStatusPtr = NULL;
      returnStringPtr = NULL;
   }

   /* Call routine */
   BeginAPICall();
   result = ARExecuteProcess(GetControlStructPtr(), command, returnStatusPtr,
                             returnStringPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARExecuteProcess");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (returnStatusPtr != NULL)
         PrintInt("Return Status: ", *returnStatusPtr);
      if (returnStringPtr != NULL)
         PrintChar("Process output: ", *returnStringPtr);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (returnStringPtr != NULL)
         free(*returnStringPtr);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/**********                                                         **********/
/**********                       ESCALATION                        **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                            APIARGetEscalation                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetEscalation()

{
   ARFilterActionList        actionList;
   char                     *changeDiary;
   ARFilterActionList        elseList;
   unsigned int              enable;
   AREscalationTmStruct      escalationTm;
   char                     *helpText;
   ARAccessNameType          lastChanged;
   ARNameType                name;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result;
   ARStatusList              status = {0, NULL};
   ARTimestamp               timestamp;
   ARWorkflowConnectStruct   workflowConnect;

   /* Get parameters */
   DriverPrintHeader("GET ESCALATION");
   strcpy(name, GetChar("Escalation name: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetEscalation(GetControlStructPtr(), name, &escalationTm,
                            &workflowConnect, &enable, &query, &actionList,
                            &elseList, &helpText, &timestamp, owner,
                            lastChanged, &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetEscalation");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (escalationTm.escalationTmType == AR_ESCALATION_TYPE_INTERVAL)
         PrintLong("Escalation interval: ", escalationTm.u.interval);
      else
      {
         PrintLong("Escalation monthday: ", escalationTm.u.date.monthday);
         PrintLong("Escalation weekday: ", escalationTm.u.date.weekday);
         PrintLong("Escalation hourmask: ", escalationTm.u.date.hourmask);
         PrintUInt("Escalation minute: ", escalationTm.u.date.minute);
      }
      PrintUInt("Workflow connect type: ",workflowConnect.type);
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         PrintUInt("Number of schema list items is: ",
                   workflowConnect.u.schemaList->numItems);
         PrintARNameList("Schema list: ", " ", workflowConnect.u.schemaList);
      }
      PrintUInt("Enable flag : ", enable);
      PrintARQualifierStruct("", &query);
      PrintARFilterActionList(&actionList, TRUE);
      PrintARFilterActionList(&elseList, FALSE);
      PrintChar("Help text: ", helpText);
      PrintARTimestamp("Last update time: ", timestamp);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARQualifierStruct(&query, FALSE);
      FreeARFilterActionList(&actionList, FALSE);
      FreeARFilterActionList(&elseList, FALSE);
      if (helpText != NULL)
         free(helpText);
      if (changeDiary != NULL)
         free(changeDiary);
      FreeARPropList(&objPropList, FALSE);
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARSetEscalation                             */
/*                                                                           */
/*****************************************************************************/

int APIARSetEscalation()

{
   ARFilterActionList        actionList;
   ARFilterActionList       *actionListPtr;
   char                      changeDiary[1024];
   char                     *changeDiaryPtr;
   int                       i, day, date;
   ARFilterActionList        elseList;
   ARFilterActionList       *elseListPtr;
   unsigned int              enable;
   unsigned int             *enablePtr;
   AREscalationTmStruct      escalationTm;
   AREscalationTmStruct     *escalationTmPtr;
   char                      helpText[1024];
   char                     *helpTextPtr;
   ARNameType                name;
   ARNameType                newName;
   char                     *newNamePtr;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARPropList               *objPropListPtr;
   ARAccessNameType          owner;
   char                     *ownerPtr;
   ARQualifierStruct         query;
   ARQualifierStruct        *queryPtr;
   int                       result;
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;
   ARWorkflowConnectStruct  *workflowConnectPtr;

   /* Get parameters */
   DriverPrintHeader("SET ESCALATION");
   strcpy(name, GetChar("Escalation name: ", ""));
   if (GetARBoolean("Change escalation name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }
   else
      newNamePtr = NULL;
   if (GetARBoolean("Change escalation time ? (F): ", FALSE))
   {
      escalationTm.escalationTmType =
         GetUnsignedInt("Escalation time format (1/2) (1): ", 1);
      if (escalationTm.escalationTmType == AR_ESCALATION_TYPE_INTERVAL)
         escalationTm.u.interval = GetLong("Escalation interval (> 0) (300): ",
                                           300);
      else
      {
         escalationTm.u.date.monthday = 0;
         escalationTm.u.date.weekday = 0;
         escalationTm.u.date.hourmask = 0;
         escalationTm.u.date.minute = 0;
         day = GetInt("how many days to set in day/month format? (> 0) (1): ",1);
         i = 0;
         while (i < day)
         {
            date = GetInt("date of the day, 0 to 30 for month/day (0): ", 0);
            escalationTm.u.date.monthday |= AR_SETDAY(date);
            i++;
         }
         day = GetInt("how many days to set in day/week format? (> 0) (1): ", 1);
         i = 0;
         while (i < day)
         {
            date = GetInt("date of the day, 0 to 6 for sunday to saturday (0): ", 0);
            escalationTm.u.date.weekday |= AR_SETDAY(date);
            i++;
         }
         day = GetInt("how many hour marks to set in a day? (> 0) (1): ", 1);
         i = 0;
         while (i < day)
         {
            date = GetInt("hour of the day, 0 to 23 for 24 hours of a day (0): ", 0);
            escalationTm.u.date.hourmask |= AR_SETHOUR(date);
            i++;
         }
         day = GetInt("set minute mark of the hour ? (0-59) (0): ", 0);
         escalationTm.u.date.minute = day;
      }
      escalationTmPtr = &escalationTm;
   }
   else
      escalationTmPtr = NULL;
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST &&
       GetARBoolean("Change schema or schemaList? (F): ", FALSE))
   {
      if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
      {
         workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
         if (workflowConnect.u.schemaList == NULL)
         {
            DriverPrintMallocCallError("SetEscalation");
            return AR_RETURN_ERROR;
         }
         workflowConnect.u.schemaList->numItems =
            GetUnsignedInt("Number of schemas? (1): ", 1);
         if (workflowConnect.u.schemaList->numItems == 0)
            workflowConnect.u.schemaList->nameList = NULL;
         else
            workflowConnect.u.schemaList->nameList =
                  (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                       sizeof(ARNameType));
         if (workflowConnect.u.schemaList->nameList == NULL)
         {
            DriverPrintMallocCallError("SetEscalation");
            free(workflowConnect.u.schemaList);
            return AR_RETURN_ERROR;
         }
         for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
              numItems++)
         {
            if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                         numItems + 1) < 0)
               tmpString[sizeof(tmpString) - 1] = '\0';
            strcpy(workflowConnect.u.schemaList->nameList[numItems],
                   GetChar(tmpString, ""));
         }
      }
      workflowConnectPtr = &workflowConnect;
   }
   else
      workflowConnectPtr = NULL;

   if (GetARBoolean("Change enable flag? (F): ", FALSE))
   {
      enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
      enablePtr = &enable;
   }
   else
      enablePtr = NULL;
   if (GetARBoolean("Change query? (F): ", FALSE))
   {
      GetARQualifierStruct(&query);
      queryPtr = &query;
   }
   else
      queryPtr = NULL;
   if (GetARBoolean("Change action(s)? (F): ", FALSE))
   {
      GetARFilterActionList(TRUE, &actionList);
      actionListPtr = &actionList;
   }
   else
      actionListPtr = NULL;
   if (GetARBoolean("Change else(s)? (F): ", FALSE))
   {
      GetARFilterActionList(FALSE, &elseList);
      elseListPtr = &elseList;
   }
   else
      elseListPtr = NULL;
   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }
   else
      helpTextPtr = NULL;
   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }
   else
      ownerPtr = NULL;
   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }
   else
      changeDiaryPtr = NULL;
   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   else
      objPropListPtr = NULL;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetEscalation(GetControlStructPtr(), name, newNamePtr,
                            escalationTmPtr, workflowConnectPtr, enablePtr,
                            queryPtr, actionListPtr, elseListPtr, helpTextPtr,
                            ownerPtr, changeDiaryPtr, objPropListPtr, 
                            objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetEscalation");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (queryPtr != NULL)
      FreeARQualifierStruct(&query, FALSE);
   if (actionListPtr != NULL)
      FreeARFilterActionList(&actionList, FALSE);
   if (elseListPtr != NULL)
      FreeARFilterActionList(&elseList, FALSE);
   if (objPropListPtr != NULL)
      FreeARPropList(&objPropList, FALSE);
   if (workflowConnectPtr != NULL)
      FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);

   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARCreateEscalation                           */
/*                                                                           */
/*****************************************************************************/

int APIARCreateEscalation()

{
   ARFilterActionList        actionList;
   char                      changeDiary[1024];
   int                       i, day, date;
   ARFilterActionList        elseList;
   unsigned int              enable;
   AREscalationTmStruct      escalationTm;
   char                      helpText[1024];
   ARNameType                name;
   unsigned int              numItems;
   char                     *objectModificationLogLabel = NULL;
   ARPropList                objPropList;
   ARAccessNameType          owner;
   ARQualifierStruct         query;
   int                       result;
   ARStatusList              status = {0, NULL};
   char                      tmpString[24];
   ARWorkflowConnectStruct   workflowConnect;
   
   /* Get parameters */
   DriverPrintHeader("CREATE ESCALATION");
   strcpy(name, GetChar("Escalation name: ", ""));
   escalationTm.escalationTmType =
      GetUnsignedInt("Escalation time format (1/2) (1): ", 1);
   if (escalationTm.escalationTmType == AR_ESCALATION_TYPE_INTERVAL)
      escalationTm.u.interval = GetLong("Escalation interval (> 0) (300): ",
                                        300);
   else
   {
      escalationTm.u.date.monthday = 0;
      escalationTm.u.date.weekday = 0;
      escalationTm.u.date.hourmask = 0;
      escalationTm.u.date.minute = 0;
      day = GetInt("how many days to set in day/month format? (> 0) (1): ", 1);
      i = 0;
      while (i < day)
      {
         date = GetInt("date of the day, 0 to 30 for month/day (0): ", 0);
         escalationTm.u.date.monthday |= AR_SETDAY(date);
         i++;
      }
      day = GetInt("how many days to set in day/week format? (> 0) (1): ", 1);
      i = 0;
      while (i < day)
      {
         date = GetInt("date of the day, 0 to 6 for sunday to saturday (0): ", 0);
         escalationTm.u.date.weekday |= AR_SETDAY(date);
         i++;
      }
      day = GetInt("how many hour marks to set in a day? (> 0) (1): ", 1);
      i = 0;
      while (i < day)
      {
         date = GetInt("hour of the day, 0 to 23 for 24 hours of a day (0): ", 0);
         escalationTm.u.date.hourmask |= AR_SETHOUR(date);
         i++;
      }
      day = GetInt("set minute mark of the hour ? (0-59) (0): ", 0);
      escalationTm.u.date.minute = day;
   }
   workflowConnect.type = GetUnsignedInt("Workflow type (1): ", 1);
   if (workflowConnect.type == AR_WORKFLOW_CONN_SCHEMA_LIST)
   {
      workflowConnect.u.schemaList = (ARNameList *) malloc(sizeof(ARNameList));
      if (workflowConnect.u.schemaList == NULL)
      {
         DriverPrintMallocCallError("CreateEscalation");
         return AR_RETURN_ERROR;
      }
      workflowConnect.u.schemaList->numItems =
         GetUnsignedInt("Number of schemas (1): ", 1);
      if (workflowConnect.u.schemaList->numItems == 0)
         workflowConnect.u.schemaList->nameList = NULL;
      else
         workflowConnect.u.schemaList->nameList =
               (ARNameType *) malloc(workflowConnect.u.schemaList->numItems *
                                     sizeof(ARNameType));
      if (workflowConnect.u.schemaList->nameList == NULL)
      {
         DriverPrintMallocCallError("CreateEscalation");
         free(workflowConnect.u.schemaList);
         return AR_RETURN_ERROR;
      }
      for (numItems = 0; numItems < workflowConnect.u.schemaList->numItems;
           numItems++)
      {
         if (snprintf(tmpString, sizeof(tmpString) - 1, "Schema name %u: ",
                      numItems + 1) < 0)
            tmpString[sizeof(tmpString) - 1] = '\0';
         strcpy(workflowConnect.u.schemaList->nameList[numItems],
                GetChar(tmpString, ""));
      }
   }

   enable = GetUnsignedInt("Disable/Enable (0-1) (1): ", 1);
   GetARQualifierStruct(&query);
   GetARFilterActionList(TRUE, &actionList);
   GetARFilterActionList(FALSE, &elseList);
   strcpy(helpText, GetChar("Help Text: ", ""));
   strcpy(owner, GetChar("Owner: ", GetControlStructPtr()->user));
   strcpy(changeDiary, GetChar("Change Diary: ", ""));
   GetARPropList(&objPropList, FALSE);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateEscalation(GetControlStructPtr(), name, &escalationTm,
                               &workflowConnect, enable, &query, &actionList,
                               &elseList, helpText, owner, changeDiary,
                               &objPropList, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateEscalation");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARQualifierStruct(&query, FALSE);
   FreeARFilterActionList(&actionList, FALSE);
   FreeARFilterActionList(&elseList, FALSE);
   FreeARPropList(&objPropList, FALSE);
   FreeARWorkflowConnectStruct(&workflowConnect, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARDeleteEscalation                           */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteEscalation()

{
   unsigned int   deleteOption;
   ARNameType     name;
   char          *objectModificationLogLabel = NULL;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE ESCALATION");
   strcpy(name, GetChar("Escalation name: ", ""));
   deleteOption = GetUnsignedInt("Delete option ? (0): ", 0);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteEscalation(GetControlStructPtr(), name, deleteOption, 
                               objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteEscalation");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetListEscalation                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetListEscalation()

{
   ARTimestamp    changedSince;
   ARNameList     nameList;
   int            result;
   ARNameType     schema;
   char          *schemaPtr;
   ARPropList     serverObjPropList = {0, NULL};
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GETLIST ESCALATION");
   if (GetARBoolean("For a specific schema? (F): ", FALSE))
   {
      strcpy(schema, GetChar("Schema name (): ", ""));
      schemaPtr = schema;
   }
   else
      schemaPtr = NULL;
   changedSince = GetARTimestamp("Get all changed since (0 means all): ",
                                 (ARTimestamp) 0);

   GetARPropList(&serverObjPropList, FALSE);
   
   /* Call routine */
   BeginAPICall();
   result = ARGetListEscalation(GetControlStructPtr(), schemaPtr, changedSince,
                                &serverObjPropList, &nameList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListEscalation");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("Escalation list: ", "   ", &nameList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&nameList, FALSE);
   FreeARPropList(&serverObjPropList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetMultipleEscalations                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleEscalations()

{
   ARFilterActionListList   actionListList = {0, 0};
   ARTextStringList         changeDiaryList = {0, 0};
   ARTimestamp              changedSince = 0;
   ARFilterActionListList   elseListList = {0, 0};
   ARUnsignedIntList        enableList = {0, 0};
   AREscalationTmList       esclTmList = {0, 0};
   ARBooleanList            existList = {0, 0};
   ARTextStringList         helpTextList = {0, 0};
   unsigned int             i;
   ARAccessNameList         lastChangedList = {0, 0};
   ARNameList               nameListIn = {0, 0};
   ARNameList               nameListOut = {0, 0};
   ARPropListList           objPropListList = {0, 0};
   ARUnsignedIntList        orderList = {0, 0};
   ARAccessNameList         ownersList = {0, 0};
   ARQualifierList          queryList = {0, 0};
   int                      result;
   ARStatusList             status = {0, NULL};
   ARTimestampList          timestampList = {0, 0};
   ARWorkflowConnectList    workflowConnectList = {0, 0};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE ESCALATIONS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get escalation by name:\n");
   GetARNameList(&nameListIn, "");
   
   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleEscalations(GetControlStructPtr(), changedSince,
                                     &nameListIn, &existList, &nameListOut,
                                     &esclTmList, &workflowConnectList,
                                     &enableList, &queryList, &actionListList,
                                     &elseListList, &helpTextList,
                                     &timestampList, &ownersList,
                                     &lastChangedList, &changeDiaryList,
                                     &objPropListList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleEscalations");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (nameListIn.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameListIn.numItems; i++)
         {
            DriverPrintResult("   Escalation : %s\n", nameListIn.nameList[i]);
            PrintARBoolean("    exists: ", existList.booleanList[i]);
         }
      }
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            ARWorkflowConnectStruct *const iwfl =
               &workflowConnectList.workflowConnectList[i];

            PrintChar("\n Escalation name: ", nameListOut.nameList[i]);
            {
               AREscalationTmStruct *const e = &esclTmList.escalationTmList[i];
               char b[256];

               switch (e->escalationTmType)
               {
               case AR_ESCALATION_TYPE_INTERVAL:
                  PrintUInt("Fire interval: ", e->u.interval);
                  break;
               case AR_ESCALATION_TYPE_TIMEMARK:
                  sprintf(b, "mdy 0x%0*x wkdy 0x%0*x hr 0x%0*x min %u",
                          2 * (int)sizeof(ARULong32), e->u.date.monthday,
                          2 * (int)sizeof(ARULong32), e->u.date.weekday,
                          2 * (int)sizeof(ARULong32), e->u.date.hourmask,
                          e->u.date.minute);
                  PrintChar("Fire times: ", b);
                  break;
               default:
                  PrintUInt("Unknown escalation type: ", e->escalationTmType);
                  break;
               }
            }
            PrintUInt("Workflow connect type: ", iwfl->type);
            if (iwfl->type==AR_WORKFLOW_CONN_SCHEMA_LIST && iwfl->u.schemaList)
            {
               PrintUInt("Number of schema list items is: ",
                         iwfl->u.schemaList->numItems);
               PrintARNameList("Schema list: ", " ", iwfl->u.schemaList); 
            }
            PrintUInt("Enable flag : ", enableList.intList[i]);
            PrintARQualifierStruct("", &queryList.qualifierList[i]);
            PrintARFilterActionList(&actionListList.actionListList[i], TRUE);
            PrintARFilterActionList(&elseListList.actionListList[i], FALSE);
            PrintChar("Help text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Last update time: ",
                             timestampList.timestampList[i]);
            PrintChar("Owner: ", ownersList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change Diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("", "", &objPropListList.propsList[i]);
         }
      }
   }
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&nameListIn, FALSE);
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&nameListOut, FALSE);
      FreeARUnsignedIntList(&orderList, FALSE);
      FreeARWorkflowConnectList(&workflowConnectList, FALSE);
      FreeARUnsignedIntList(&enableList, FALSE);
      FreeARQualifierList(&queryList, FALSE);
      FreeARFilterActionListList(&actionListList, FALSE);
      FreeARFilterActionListList(&elseListList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARAccessNameList(&ownersList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARRunEscalation                            */
/*                                                                           */
/*****************************************************************************/

int APIARRunEscalation()
{
   char          *escName = NULL;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("RUN ESCALATION");

   GetMallocChar("  Escalation to run: ", "", &escName);

   /* Call routine */
   BeginAPICall();
   result = ARRunEscalation(GetControlStructPtr(), escName, &status);
   EndAPICall();

   /* Print results */
   DriverPrintResultHeader("ARRunEscalation");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (escName != NULL)
      free(escName);
   
   return result;   
}


/*****************************************************************************/
/**********                                                         **********/
/**********                         IMAGE                           **********/
/**********                                                         **********/
/*****************************************************************************/

/*****************************************************************************/
/*                                                                           */
/*                               APIARGetImage                               */
/*                                                                           */
/*****************************************************************************/

int APIARGetImage()
{
   char               *changeDiary = NULL;
   char               *checkSum = NULL;
   ARImageDataStruct   data;
   char               *description = NULL;
   char                fileFlags[4];
   char                filename[256] = "\0";
   FILE               *filePtr = NULL;
   char               *helpText = NULL;
   char               *imageType = NULL;
   ARAccessNameType    lastChanged = "\0";
   ARNameType          name;
   ARPropList          objPropList = {0, NULL};
   ARAccessNameType    owner = "\0";
   int                 result = AR_RETURN_OK;
   ARStatusList        status = {0, NULL};
   ARTimestamp         timestamp = 0;
   
#ifdef _WIN32
   strcpy(fileFlags, "wb");
#else
   strcpy(fileFlags, "w");
#endif

   /* Get parameters */
   DriverPrintHeader("GET IMAGE");
   strcpy(name, GetChar("Name of image: ", ""));
   if (name[0] == '\0')
   {
      DriverPrintError("An image name is required");
      return AR_RETURN_ERROR;
   }

   while (filePtr == NULL)
   {
      strcpy(filename, GetChar("Filename to hold returned image: ", ""));
      if (filename[0] != '\0')
         filePtr = fopen(filename, fileFlags);
      if (filePtr == NULL)
      {
         DriverPrintError("   ***  File open failure\n");
         if (GetARBoolean("Reenter filename? (F): ", FALSE) == FALSE)
         {
            DriverPrintError("A valid filename is required");
            return AR_RETURN_ERROR;
         }
      }
   }

   /* Call routine */
   BeginAPICall();
   result = ARGetImage(GetControlStructPtr(), name, &data, &imageType, 
                       &timestamp, &checkSum, &description, &helpText, owner,
                       lastChanged, &changeDiary, &objPropList, &status);
   EndAPICall(&status);

   /* Save results to file */
   if ((result < AR_RETURN_ERROR) && (data.numItems > 0) &&
       fwrite(data.bytes, data.numItems, 1, filePtr) == 0)
   {
     DriverPrintError("Error saving image to file %s\n", filename);
   }
   fclose(filePtr);

   /* Print results */
   DriverPrintResultHeader("ARGetImage");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARTimestamp("Last update:",timestamp);
      PrintChar("Filename: ", filename);
      PrintChar("Description: ", description);
      PrintChar("Type: ", imageType);
      PrintChar("CheckSum: ", checkSum);
      PrintInt ("Size: ",  data.numItems);
      PrintChar("Help text: ", helpText);
      PrintChar("Owner: ", owner);
      PrintChar("Last changed: ", lastChanged);
      APIPrintDiary("Change Diary: ", changeDiary);
      PrintARObjectPropList("", "", &objPropList);
      DriverPrintHeader("");
   }
      
   PrintARStatusList(&status);

   /* Cleanup */
   if (imageType)
      free(imageType);
   if (checkSum)
      free(checkSum);
   if (description)
      free(description);
   if (helpText != NULL)
      free(helpText);
   if (changeDiary != NULL)
      free(changeDiary);
   FreeARPropList(&objPropList, FALSE);
   FreeARImageDataStruct(&data, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARSetImage                                */
/*                                                                           */
/*****************************************************************************/

int APIARSetImage()
{
   char                changeDiary[1024];
   char               *changeDiaryPtr = NULL;
   char                helpText[1024] = "\0";
   char               *helpTextPtr = NULL;
   ARNameType          name;
   char                newDescription[1024] = "\0";
   char               *newDescriptionPtr = NULL;
   ARImageDataStruct  *newImageData = NULL;
   char                newImageType[32] = "\0";
   char               *newImageTypePtr = NULL;
   ARNameType          newName = "\0";
   char               *newNamePtr = NULL;
   char               *objectModificationLogLabel = NULL;
   ARPropList          objPropList = {0,NULL};
   ARPropList         *objPropListPtr = NULL;
   ARAccessNameType    owner = "\0";
   char               *ownerPtr = NULL;
   int                 result = AR_RETURN_OK;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("SET IMAGE");
   strcpy(name, GetChar("Image name: ", ""));
   if (GetARBoolean("Change image name? (F): ", FALSE))
   {
      strcpy(newName, GetChar("New name (): ", ""));
      newNamePtr = newName;
   }

   if (GetARBoolean("Change image type? (F): ", FALSE))
   {
      DriverPrintHeader("Enter Image type (i.e., \"jpg\", \"bmp\", etc.)");
      strcpy(newImageType, GetChar("New image type (): ", ""));
      newImageTypePtr = newImageType;
   }

   if (GetARBoolean("Change image description? (F): ", FALSE))
   {
      strcpy(newDescription, GetChar("New image description (): ", ""));
      newDescriptionPtr = newDescription;
   }

   if (GetARBoolean("Change image? (F): ", FALSE))
   {
      GetARImageData(&newImageData);
      if (!newImageData || newImageData->numItems == 0)
      {
         DriverPrintError("A valid image is required");
         return AR_RETURN_ERROR;
      }
   }

   if (GetARBoolean("Change help text? (F): ", FALSE))
   {
      strcpy(helpText, GetChar("   Help Text (): ", ""));
      helpTextPtr = helpText;
   }

   if (GetARBoolean("Change owner? (F): ", FALSE))
   {
      strcpy(owner, GetChar("   Owner (): ", ""));
      ownerPtr = owner;
   }

   if (GetARBoolean("Add to change diary? (F): ", FALSE))
   {
      strcpy(changeDiary, GetChar("   Change Diary (): ", ""));
      changeDiaryPtr = changeDiary;
   }

   if (GetARBoolean("Change object properties? (F): ", FALSE))
   {
      GetARPropList(&objPropList, FALSE);
      objPropListPtr = &objPropList;
   }
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARSetImage(GetControlStructPtr(), name, newNamePtr, newImageData,
                       newImageTypePtr, newDescriptionPtr, helpTextPtr, ownerPtr,
                       changeDiaryPtr, objPropListPtr, objectModificationLogLabel,
                       &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetImage");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARImageDataStruct(newImageData, TRUE);
   FreeARPropList(&objPropList, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARCreateImage                               */
/*                                                                           */
/*****************************************************************************/

int APIARCreateImage()
{
   char                changeDiary[1024] = "\0";
   char                description[1024] = "\0";
   char                helpText[1024] = "\0";
   ARImageDataStruct  *imageData = NULL;
   char                imageType[32] = "\0";
   ARNameType          name = "\0";
   char               *objectModificationLogLabel = NULL;
   ARPropList          objPropList = {0, NULL};
   ARAccessNameType    owner = "\0";
   int                 result = AR_RETURN_OK;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("CREATE IMAGE");
   strcpy(name, GetChar("Image name: ", ""));
   if (name[0] == '\0')
   {
      DriverPrintError("A name is required to create an image");
      return AR_RETURN_ERROR;
   }
   DriverPrintHeader("Enter Image type (i.e., \"jpg\", \"bmp\", etc.)");
   strcpy(imageType, GetChar("Image type (\"jpg\"): ", "jpg"));
   strcpy(description, GetChar("Image description: ", ""));

   GetARImageData(&imageData);
   if (imageData->numItems == 0)
   {
      DriverPrintError("A valid image is required");
      return AR_RETURN_ERROR;
   }

   strcpy(helpText, GetChar("Help Text (): ", ""));
   strcpy(owner, GetChar("Owner (): ", ""));
   strcpy(changeDiary, GetChar("Change Diary (): ", ""));
   GetARPropList(&objPropList, FALSE);
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateImage(GetControlStructPtr(), name, imageData, imageType,
                          description, helpText, owner, changeDiary,
                          &objPropList, objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateImage");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARImageDataStruct(imageData, TRUE);
   FreeARPropList(&objPropList, FALSE);
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARDeleteImage                               */
/*                                                                           */
/*****************************************************************************/
int APIARDeleteImage()
{
   ARNameType     name = "\0";
   char          *objectModificationLogLabel = NULL;
   ARBoolean      updateRef = FALSE;
   int            result = AR_RETURN_OK;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE IMAGE");

   strcpy(name, GetChar("Image to delete: ", ""));
   if (name[0] == '\0')
   {
      DriverPrintError("A name is required to delete an image");
      return AR_RETURN_ERROR;
   }

   if (GetARBoolean("Delete image references in field display props? (F): ", FALSE))
      updateRef = TRUE;
   else
      updateRef = FALSE;
   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARDeleteImage(GetControlStructPtr(), name, updateRef, 
                          objectModificationLogLabel, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteImage");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetListImage                              */
/*                                                                           */
/*****************************************************************************/

int APIARGetListImage()

{
   ARTimestamp    changedSince = 0;
   ARNameList     imageList = {0, NULL};
   char           imageType[32] = "\0";
   int            result = AR_RETURN_OK;
   ARNameList     schemaList = {0, NULL};
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GETLIST IMAGE");
   if (GetARBoolean("Search all schemas for associated images? (T): ", TRUE) == FALSE)
   {
      DriverPrintPrompt("Get schemas by name:\n");
      GetARNameList(&schemaList, "");
   }

   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   strcpy(imageType, 
          GetChar("Get specific image type (\"jpg\", \"bmp\", etc.) (\"\"): ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetListImage(GetControlStructPtr(), &schemaList, changedSince,
                           imageType, &imageList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListImage");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARNameList("Image list: ", "   ", &imageList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARNameList(&imageList, FALSE);
   FreeARNameList(&schemaList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetMultipleImages                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleImages()
{
   ARTextStringList   changeDiaryList = {0, NULL};
   ARTimestamp        changedSince;
   ARTextStringList   checkSumList = {0, NULL};
   ARTextStringList   descriptionList = {0, NULL};
   ARBooleanList      existList = {0, NULL};
   char               fileDir[256] = "\0";
   char               filename[512] = "\0";
   ARTextStringList   helpTextList = {0, NULL};
   unsigned int       i;
   ARImageDataList    imageDataList = {0, NULL};
   ARNameList         imageNameList = {0, NULL};
   ARTextStringList   imageTypeList = {0, NULL};
   ARAccessNameList   lastChangedList = {0, NULL};
   ARNameList         nameList;
   ARPropListList     objPropListList = {0, NULL};
   ARAccessNameList   ownerList = {0, NULL};
   int                result = AR_RETURN_OK;
   ARStatusList       status = {0, NULL};
   ARTimestampList    timestampList = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE IMAGES");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get images by name:\n");
   GetARNameList(&nameList, "");

   DriverPrintHeader("Images are returned in files based on image name and type");
   strcpy(fileDir, GetChar("Directory to place resulting images: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleImages(GetControlStructPtr(), changedSince, &nameList,
                                &existList, &imageNameList, &imageTypeList, 
                                &timestampList, &descriptionList, &helpTextList,
                                &ownerList, &lastChangedList, &changeDiaryList, 
                                &objPropListList, &checkSumList, &imageDataList, 
                                &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleImages");
   PrintReturnCode(result);

   if (result < AR_RETURN_ERROR)
   {
      if (nameList.numItems)
      {
         PrintChar("Exist list:", NULL);
         for (i = 0; i < nameList.numItems; i++)
         {
            DriverPrintResult("   Image : %s\n", nameList.nameList[i]);
            PrintARBoolean("   exists: ", existList.booleanList[i]);
         }
      }
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            PrintChar("\nImage name:", imageNameList.nameList[i]);
            PrintInt("Image Size:", imageDataList.imageList[i].numItems);
            PrintChar("Image Type:", imageTypeList.stringList[i]);
            PrintChar("CheckSum:", checkSumList.stringList[i]);
            PrintChar("Description:", descriptionList.stringList[i]);
            PrintChar("Help Text: ", helpTextList.stringList[i]);
            PrintARTimestamp("Timestamp: ", timestampList.timestampList[i]);
            PrintChar("Owner: ", ownerList.nameList[i]);
            PrintChar("Last changed: ", lastChangedList.nameList[i]);
            APIPrintDiary("Change Diary: ", changeDiaryList.stringList[i]);
            PrintARObjectPropList("Object property: ", "   ", 
                                  &objPropListList.propsList[i]);
         }
      }
      DriverPrintResult(" \n");

      /* Save results to file */
      for (i = 0; i < existList.numItems; i++)
      {
         if (existList.booleanList[i])
         {
            FILE *filePtr = NULL;
#ifdef _WIN32
            sprintf(filename, "%s\\%s.%s", fileDir, imageNameList.nameList[i],
                    imageTypeList.stringList[i]);
            filePtr = fopen(filename, "wb");
#else /* _WIN32 */
            sprintf(filename, "%s/%s.%s", fileDir, imageNameList.nameList[i],
                    imageTypeList.stringList[i]);
            filePtr = fopen(filename, "w");
#endif /* _WIN32 */
            if (filePtr == NULL)
            {
               DriverPrintError("\n   ***  File open failure (%s)", filename);
            }
            else
            {
               if ((imageDataList.imageList[i].numItems > 0) &&
                   fwrite(imageDataList.imageList[i].bytes, 
                          imageDataList.imageList[i].numItems, 1, filePtr) == 0)
                  DriverPrintError("\nError saving image to file %s\n",filename);
               else
                  DriverPrintResult("Image \"%s\" saved to %s\n",
                                    imageNameList.nameList[i], filename);

               fclose(filePtr);
            }
         }
      }
      DriverPrintResult(" \n");
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARBooleanList(&existList, FALSE);
      FreeARNameList(&imageNameList, FALSE);
      FreeARTextStringList(&imageTypeList, FALSE);
      FreeARTextStringList(&checkSumList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARTextStringList(&descriptionList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARAccessNameList(&ownerList, FALSE);
      FreeARAccessNameList(&lastChangedList, FALSE);
      FreeARTextStringList(&changeDiaryList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
      FreeARImageDataList(&imageDataList, FALSE);
   }

   FreeARNameList(&nameList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARCloseNetworkConnections                        */
/*                                                                           */
/*****************************************************************************/

int APIARCloseNetworkConnections()

{
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("CLOSE NETWORK CONNECTIONS");

   /* Call routine */
   BeginAPICall();
   result = ARCloseNetworkConnections(GetControlStructPtr(), &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCloseNetworkConnections");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                              APIARSetLogging                              */
/*                                                                           */
/*****************************************************************************/
 
int APIARSetLogging()
 
{
   char                 filename[256];
   FILE                *filePtr = NULL;
   unsigned long        logTypeMask;
   int                  result;
   ARStatusList         status = {0, NULL};
   ThreadControlBlock  *threadControlBlockPtr;
   unsigned long        whereToWriteMask;

   threadControlBlockPtr = GetThreadControlBlockPtr();

   /* Get parameters */
   DriverPrintHeader("SET LOGGING");
   logTypeMask = GetLong("NONE(0), SQL(1), Filter(2), API(16) (0): ", 0);
   
   /* if logging option is turned off ignore other values */
   if (logTypeMask == AR_DEBUG_SERVER_NONE)
   {
      whereToWriteMask = 0;
   }
   else
   {
      whereToWriteMask = 
         GetLong("Return logging records to file(1), status list(2)? (1): ",
                 AR_WRITE_TO_FILE);

      /* the file should be left open until the caller closes it with a call */
      /* to this function with logTypeMask equal to AR_DEBUG_SERVER_NONE     */
      if (whereToWriteMask & AR_WRITE_TO_FILE)
      {
         while (filePtr == NULL)
         {
            strcpy(filename,
                   GetChar("Name of file to hold returned records: ", ""));
#ifdef _WIN32
            filePtr = fopen(filename, "wb");
#else /* _WIN32 */
            filePtr = fopen(filename, "w");
#endif /* _WIN32 */
            if (filePtr == NULL)
               DriverPrintError("   ***  File open failure\n");
         }
      }
   }

   /* Call routine */
   BeginAPICall();
   result = ARSetLogging(GetControlStructPtr(), logTypeMask, whereToWriteMask,
                         filePtr, &status);
   EndAPICall(&status);
 
   /* Print results */
   DriverPrintResultHeader("ARSetLogging");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR && logTypeMask != AR_DEBUG_SERVER_NONE)
      threadControlBlockPtr->loggingFile = filePtr;
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (logTypeMask == AR_DEBUG_SERVER_NONE &&
          threadControlBlockPtr->loggingFile != NULL)
      {
         fclose(threadControlBlockPtr->loggingFile);
         threadControlBlockPtr->loggingFile = NULL;
      } 
   }
   else
   { 
      if (filePtr != NULL)
         fclose(filePtr);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                                APIARSignal                                */
/*                                                                           */
/*****************************************************************************/

int APIARSignal()
{
   int            result;
   ARSignalList   signalList;
   ARStatusList   status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("SIGNAL");
   GetARSignalList(&signalList);

   /* Call routine */
   BeginAPICall();
   result = ARSignal(GetControlStructPtr(), &signalList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSignal");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARSignalList(&signalList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARValidateFormCache                           */
/*                                                                           */
/*****************************************************************************/

int APIARValidateFormCache()

{
   ARTimestamp    groupsLastChanged;
   ARNameList     guideChanged;
   ARNameList     menusChanged;
   ARTimestamp    mostRecentActiveLink;
   ARTimestamp    mostRecentGuide;
   ARTimestamp    mostRecentMenu;
   int            numActiveLinksChanged;
   int            numActiveLinksTotal;
   int            result;
   ARNameType     schema;
   ARTimestamp    schemaLastModified;
   ARStatusList   status = {0, NULL};
   ARTimestamp    userLastChanged;

   /* Get parameters */
   DriverPrintHeader("VALIDATE FORM CACHE");
   strcpy(schema, GetChar("Schema name (): ", ""));
   mostRecentActiveLink = GetARTimestamp("Get active links changed since (0): ",
                                         (ARTimestamp) 0);
   mostRecentMenu = GetARTimestamp("Get menus changed since (0): ",
                                   (ARTimestamp) 0);

   mostRecentGuide = GetARTimestamp("Get guides changed since (0): ",
                                    (ARTimestamp) 0);

   /* Call routine */
   BeginAPICall();
   result = ARValidateFormCache(GetControlStructPtr(), schema,
                                mostRecentActiveLink, mostRecentMenu,
                                mostRecentGuide, &schemaLastModified,
                                &numActiveLinksTotal, &numActiveLinksChanged,
                                &menusChanged, &groupsLastChanged,
                                &userLastChanged, &guideChanged, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARValidateFormCache");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARTimestamp("Schema last modified: ", schemaLastModified);
      PrintInt("Number of active links for schema: ", numActiveLinksTotal);
      PrintInt("Number of active links changed: ", numActiveLinksChanged);
      PrintARNameList("Character menus changed: ", "   ", &menusChanged);
      PrintARTimestamp("Groups last changed: ", groupsLastChanged);
      PrintARTimestamp("User last changed: ", userLastChanged);
      PrintARNameList("Guides changed: ", "   ", &guideChanged);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&menusChanged, FALSE);
      FreeARNameList(&guideChanged, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARGetListSchemaWithAlias                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetListSchemaWithAlias()

{
   ARNameList         aliasList;
   ARTimestamp        changedSince;
   ARInternalIdList   fieldIdList;
   ARNameType         joinName;
   int                result;
   ARNameList         schemaList;
   unsigned int       schemaType;
   ARStatusList       status = {0, NULL};
   ARNameType         vuiLabel;

   /* Get parameters */
   DriverPrintHeader("GETLIST SCHEMA WITH ALIAS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   schemaType = GetUnsignedInt(
       "Type of schema (all,regular,join,view,uplink,downlink,dialog,allow_mfsearch) (0-6,9) (0): ", 0);
   if (GetARBoolean("Include hidden schemas (T): ", TRUE))
      schemaType += AR_HIDDEN_INCREMENT;
   if (((schemaType % AR_HIDDEN_INCREMENT) == AR_LIST_SCHEMA_UPLINK) ||
       ((schemaType % AR_HIDDEN_INCREMENT) == AR_LIST_SCHEMA_DOWNLINK))
      strcpy(joinName, GetChar("Schema name: ", ""));
   else
      joinName[0] = '\0';

   strcpy(vuiLabel, GetChar("Vui desired (): ", ""));

   DriverPrintPrompt("Ids of fields, which must be on the schema:\n");
   GetARInternalIdList(&fieldIdList);

   /* Call routine */
   BeginAPICall();
   result = ARGetListSchemaWithAlias(GetControlStructPtr(), changedSince,
                                     schemaType, joinName, &fieldIdList,
                                     vuiLabel, NULL, &schemaList, &aliasList,
                                     &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListSchemaWithAlias");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARNameList("Schema list: ", "   ", &schemaList);
      PrintARNameList("Alias list: ", "   ", &aliasList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&schemaList, FALSE);
      FreeARNameList(&aliasList, FALSE);
   }
   FreeARInternalIdList(&fieldIdList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARGetLocalizedValue                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetLocalizedValue()
{
   ARLocalizedRequestStruct   lms;
   ARTimestamp               *ptrTimestamp;
   ARValueStruct             *ptrValue = NULL;
   int                        result;
   ARStatusList               status = {0, NULL};
   ARTimestamp                timestamp;
   ARValueStruct              value;
   

   /* Get parameters */
   DriverPrintHeader("GET LOCALIZED TEXT");
   GetARLocalizedRequestStruct(&lms);
   
   if (GetARBoolean("   Get value struct? (T): ", TRUE))
      ptrValue = &value;
   else
      ptrValue = NULL;

   if (GetARBoolean("   Get timestamp? (T): ", TRUE))
      ptrTimestamp = &timestamp;
   else
      ptrTimestamp = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARGetLocalizedValue(GetControlStructPtr(), &lms, ptrValue,
                                ptrTimestamp, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetLocalizedValue");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (ptrValue)
         PrintARValueStruct("Localized Text: ", &value);
      if (ptrTimestamp)
         PrintARTimestamp("Timestamp: ", timestamp);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (ptrValue)
         FreeARValueStruct(ptrValue, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                      APIARGetMultipleLocalizedValues                      */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleLocalizedValues()
{
   unsigned int             i;
   ARLocalizedRequestList   lml;
   ARTimestampList         *ptrTimestampList;
   ARValueList             *ptrValueList;
   int                      result;
   ARStatusList             status = {0, NULL};
   ARTimestampList          timestampList;
   ARValueList              valueList;

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE LOCALIZED VALUES");
   GetARLocalizedRequestList(&lml);
   if (GetARBoolean("   Get value list? (T): ", TRUE))
      ptrValueList = &valueList;
   else
      ptrValueList = NULL;

   if (GetARBoolean("   Get timestamp list? (T): ", TRUE))
      ptrTimestampList = &timestampList;
   else
      ptrTimestampList = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleLocalizedValues(GetControlStructPtr(), &lml,
                                         ptrValueList, ptrTimestampList,
                                         &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetLocalizedValue");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (ptrValueList)
      {
         for (i = 0; i < valueList.numItems; i++)
         {
            PrintARValueStruct("Localized Text: ", &valueList.valueList[i]);
         }
      }

      if (ptrTimestampList)
      {
         for (i = 0; i < timestampList.numItems; i++)
         {
            PrintARTimestamp("Timestamp: ", timestampList.timestampList[i]);
         }
      }
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (ptrValueList)
         FreeARValueList(ptrValueList, FALSE);
      if (ptrTimestampList)
         FreeARTimestampList(ptrTimestampList, FALSE);
   }
   if (lml.localizedRequestList != NULL)
      free(lml.localizedRequestList);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARCreateAlertEvent                           */
/*                                                                           */
/*****************************************************************************/

int APIARCreateAlertEvent()

{
   int                alertPriority;
   ARNameType         alertSource;
   char               alertText[1024];
   AREntryIdType      entryId;
   ARNameType         formName;
   char               objectId[256];
   int                result;
   ARServerNameType   serverName;
   ARStatusList       status = {0, NULL};
   ARAccessNameType   user;

   /* Get parameters */
   DriverPrintHeader("CREATE ALERT EVENT");
   strcpy(user, GetChar("User name (): ", ""));
   strcpy(alertText, GetChar("Alert text (): ", ""));
   alertPriority = GetInt("Alert priority (0): ", 0);
   strcpy(alertSource, GetChar("Alert source (): ", ""));
   strcpy(serverName, GetChar("Server name (): ", ""));
   strcpy(formName, GetChar("Form name (): ", ""));
   strcpy(objectId, GetChar("Object Id (): ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARCreateAlertEvent(GetControlStructPtr(), user, alertText,
                               alertPriority, alertSource, serverName,
                               formName, objectId, entryId, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateAlertEvent");
   PrintReturnCode(result);
   PrintChar("Entry id: ", entryId);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARRegisterForAlerts                           */
/*                                                                           */
/*****************************************************************************/

int APIARRegisterForAlerts()

{
   int            clientPort;
   unsigned int   registrationFlags;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("REGISTER FOR ALERTS");
   clientPort = GetInt("Client port (0): ", 0);
   registrationFlags = GetUnsignedInt("Registration flags (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARRegisterForAlerts(GetControlStructPtr(), clientPort,
                                registrationFlags, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARRegisterForAlerts");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARDeregisterForAlerts                          */
/*                                                                           */
/*****************************************************************************/

int APIARDeregisterForAlerts()

{
   int            clientPort;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DEREGISTER FOR ALERTS");
   clientPort = GetInt("Client port (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARDeregisterForAlerts(GetControlStructPtr(), clientPort, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeregisterForAlerts");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetListAlertUser                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetListAlertUser()

{
   int                result;
   ARStatusList       status = {0, NULL};
   ARAccessNameList   userList;

   /* Get parameters */
   DriverPrintHeader("GET LIST ALERT USER");

   /* Call routine */
   BeginAPICall();
   result = ARGetListAlertUser(GetControlStructPtr(), &userList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListAlertUser");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARAccessNameList("User list: ", "   ", &userList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARAccessNameList(&userList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARGetAlertCount                             */
/*                                                                           */
/*****************************************************************************/

int APIARGetAlertCount()

{
   unsigned int        count;
   int                 result;
   ARQualifierStruct   qualifier;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET ALERT COUNT");
   GetARQualifierStruct(&qualifier);

   /* Call routine */
   BeginAPICall();
   result = ARGetAlertCount(GetControlStructPtr(), &qualifier, &count, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetAlertCount");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintUInt("Alert Count: ", count);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARDecodeAlertMessage                          */
/*                                                                           */
/*****************************************************************************/

int APIARDecodeAlertMessage()

{
   char           *alertText = NULL;
   char           *formName = NULL;
   unsigned char  *messagePtr;
   char           *objectId = NULL;
   unsigned int    priority;
   int             result;
   char           *serverName = NULL;
   char           *serverAddr = NULL;
   char           *sourceTag = NULL;
   unsigned int    sourceType;
   ARStatusList    status = {0, NULL};
   ARTimestamp     timestamp;

   /* Get parameters */
   DriverPrintHeader("DECODE ALERT MESSAGE");
   messagePtr = (unsigned char *) GetChar("Alert message: ", "");

   /* Call routine - note that we cannot properly exercise this function */
   /* because we cannot pass in the expected binary data so we improvise */
   BeginAPICall();
   result = ARDecodeAlertMessage(GetControlStructPtr(), messagePtr,
                                 strlen((char *) messagePtr) + 1, &timestamp,
                                 &sourceType, &priority, &alertText, &sourceTag,
                                 &serverName, &serverAddr, &formName, &objectId,
                                 &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDecodeAlertMessage");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARTimestamp("Timestamp: ", timestamp);
      PrintUInt("Source type: ", sourceType);
      PrintUInt("Priority: ", priority);
      PrintChar("Alert text: ", alertText);
      PrintChar("Source tag: ", sourceTag);
      PrintChar("Server name: ", serverName);
      PrintChar("Server Address: ", serverAddr);
      PrintChar("Form name: ", formName);
      PrintChar("Object Id: ", objectId);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (alertText)
         free(alertText);
      if (sourceTag)
         free(sourceTag);
      if (serverName)
         free(serverName);
      if (serverAddr)
         free(serverAddr);
      if (formName)
         free(formName);
      if (objectId)
         free(objectId);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARDecodeARQualifierStruct                        */
/*                                                                           */
/*****************************************************************************/

int APIARDecodeARQualifierStruct()
{
   ARQualifierStruct   qualStruct;
   char               *qualTextPtr;
   int                 result;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DECODE STRING TO ARQUALIFIER STRUCT");
   qualTextPtr = GetChar("Qualifier Text: ", "");

   /* Call routine */
   result = ARDecodeARQualifierStruct(GetControlStructPtr(), qualTextPtr,
                                      &qualStruct, &status);
   /* Print results */
   DriverPrintResultHeader("ARDecodeARQualifierStruct");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARQualifierStruct("", &qualStruct);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARQualifierStruct(&qualStruct, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIAREncodeARQualifierStruct                        */
/*                                                                           */
/*****************************************************************************/

int APIAREncodeARQualifierStruct()
{
   ARQualifierStruct   qualStruct;
   char               *qualTextPtr = NULL;
   int                 result;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("ENCODE ARQUALIFIER STRUCT TO STRING");
   GetARQualifierStruct(&qualStruct);

   /* Call routine */
   result = AREncodeARQualifierStruct(GetControlStructPtr(), &qualStruct,
                                      &qualTextPtr, &status);
   /* Print results */
   DriverPrintResultHeader("AREncodeARQualifierStruct");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("Qualifier Text: ", qualTextPtr);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR && qualTextPtr)
      free(qualTextPtr);
   FreeARQualifierStruct(&qualStruct, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARDecodeARAssignStruct                         */
/*                                                                           */
/*****************************************************************************/

int APIARDecodeARAssignStruct()
{
   ARAssignStruct   assignStruct;
   char            *assignTextPtr;
   int              result;
   ARStatusList     status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DECODE STRING TO ARASSIGN STRUCT");
   assignTextPtr = GetChar("Assign Text: ", "");

   /* Call routine */
   result = ARDecodeARAssignStruct(GetControlStructPtr(), assignTextPtr,
                                   &assignStruct, &status);
   /* Print results */
   DriverPrintResultHeader("ARDecodeARAssignStruct");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARAssignStruct("", &assignStruct);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARAssignStruct(&assignStruct, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIAREncodeARAssignStruct                         */
/*                                                                           */
/*****************************************************************************/

int APIAREncodeARAssignStruct()
{
   ARAssignStruct   assignStruct;
   char            *assignTextPtr = NULL;
   int              result;
   ARStatusList     status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("ENCODE ARASSIGN STRUCT TO STRING");
   GetARAssignStruct(&assignStruct);

   /* Call routine */
   result = AREncodeARAssignStruct(GetControlStructPtr(), &assignStruct,
                                   &assignTextPtr, &status);
   /* Print results */
   DriverPrintResultHeader("AREncodeARAssignStruct");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("Assign Text: ", assignTextPtr);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR && assignTextPtr)
      free(assignTextPtr);
   FreeARAssignStruct(&assignStruct, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIAREncodeStatusHistory                          */
/*                                                                           */
/*****************************************************************************/

int APIAREncodeStatusHistory()
{
   int                   result;
   ARStatusHistoryList   statHistList;
   char                 *statHistString = NULL;
   ARStatusList          status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("ENCODE STATUS HISTORY LIST TO STRING");
   GetARStatusHistoryList(&statHistList);

   /* Call routine */
   result = AREncodeStatusHistory(GetControlStructPtr(), &statHistList,
                                  &statHistString, &status);
   /* Print results */
   DriverPrintResultHeader("AREncodeStatusHistory");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("Status History String: ", statHistString);
   PrintARStatusList(&status);

   /* Cleanup */
   if (statHistString)
      free(statHistString);
   FreeARStatusHistoryList(&statHistList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIAREncodeDiary                              */
/*                                                                           */
/*****************************************************************************/

int APIAREncodeDiary()
{
   ARDiaryList    diaryList;
   char          *diaryString = NULL;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("ENCODE DIARY LIST TO STRING");
   GetARDiaryList(&diaryList);

   /* Call routine */
   result = AREncodeDiary(GetControlStructPtr(), &diaryList, &diaryString,
                          &status);
                                  
   /* Print results */
   DriverPrintResultHeader("AREncodeDiary");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("Diary String: ", diaryString);
   PrintARStatusList(&status);

   /* Cleanup */
   if (diaryString)
      free(diaryString);
   FreeARDiaryList(&diaryList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARExpandSSMenu                             */
/*                                                                           */
/*****************************************************************************/

int APIARExpandSSMenu()

{
   ARCharMenuStruct     menu;
   ARCharMenuSSStruct   menuDefn; 
   int                  result;
   ARStatusList         status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("EXPAND SS MENU");
   DriverPrintPrompt("Server-side menu struct:\n");
   GetARCharMenuSSStruct(&menuDefn);

   /* Call routine */
   BeginAPICall();
   result = ARExpandSSMenu(GetControlStructPtr(), &menuDefn, &menu, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARExpandSSMenu");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARCharMenuStruct("", &menu);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARCharMenuSSStruct(&menuDefn, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARCharMenuStruct(&menu, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                      APIARGetListExtSchemaCandidates                      */
/*                                                                           */
/*****************************************************************************/

int APIARGetListExtSchemaCandidates()
{
   int                    result;
   ARCompoundSchemaList   schema;
   unsigned int           schemaType;
   ARStatusList           status = {0, NULL};

   schema.numItems = 0;

   /* Get parameters */
   DriverPrintHeader("GET LIST EXTERNAL SCHEMA CANDIDATES");
   schemaType = GetUnsignedInt("Schema Type (0): ", 0);

   /* Call routine */
   result = ARGetListExtSchemaCandidates(GetControlStructPtr(), schemaType,
                                         &schema, &status);
                                  
   /* Print results */
   DriverPrintResultHeader("ARGetListExtSchemaCandidates");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARCompoundSchemaList(&schema);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARCompoundSchemaList(&schema, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                    APIARGetMultipleExtFieldCandidates                     */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleExtFieldCandidates()
{
   ARUnsignedIntList    dataType;
   ARFieldMappingList   fieldMapping;
   unsigned int         i;
   ARFieldLimitList     limit;
   int                  result;
   ARCompoundSchema     schema;
   ARStatusList         status = {0, NULL};
   
   memset(&schema, 0, sizeof(schema));

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE EXTERNAL FIELD CANDIDATES");
   GetARCompoundSchema(&schema);

   /* Call routine */
   result = ARGetMultipleExtFieldCandidates(GetControlStructPtr(), &schema,
                                            &fieldMapping, &limit, &dataType,
                                            &status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleExtFieldCandidates");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      for (i = 0; i < fieldMapping.numItems; i++)
      {
         PrintARFieldMapping("", &fieldMapping.mappingList[i]);
         PrintARFieldLimitStruct(&limit.fieldLimitList[i]);
         PrintUInt("Datatype: ", dataType.intList[i]);
      }
   }
   
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARFieldMappingList(&fieldMapping, FALSE);
   FreeARFieldLimitList(&limit, FALSE);
   FreeARUnsignedIntList(&dataType, FALSE);
   FreeARCompoundSchema(&schema, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARValidateLicense                            */
/*                                                                           */
/*****************************************************************************/

int APIARValidateLicense()

{
   ARLicenseValidStruct   licenseValid;
   ARLicenseNameType      licType;
   int                    result;
   ARStatusList           status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("VALIDATE LICENSE");
   strcpy(licType, GetChar("License Type: ", ""));

   memset(&status, 0, sizeof(ARStatusList));
   memset(&licenseValid, 0, sizeof(ARLicenseValidStruct));

   /* Call routine */
   BeginAPICall();
   result = ARValidateLicense(GetControlStructPtr(), licType, &licenseValid,
                              &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARValidateLicense");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintInt("Number Of Licenses: ", licenseValid.numLicenses);
      PrintARBoolean("            IsDemo: ", licenseValid.isDemo);
      PrintInt("Expire Date: month: ", licenseValid.expireDate.month);
      PrintInt("               day: ", licenseValid.expireDate.day);
      PrintInt("              year: ", licenseValid.expireDate.year);
      PrintChar("        Token List: ", licenseValid.tokenList);
   }
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   FreeARLicenseValidStruct(&licenseValid, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARValidateMultipleLicenses                       */
/*                                                                           */
/*****************************************************************************/

int APIARValidateMultipleLicenses()

{
   unsigned int           i;
   unsigned int           j;
   ARLicenseNameList      licenseTypeList;
   ARLicenseValidList     licenseValidList;
   unsigned int           num;
   ARLicenseValidStruct  *temp;
   ARLicenseNameType     *tmpNameType;
   int                    result;
   ARStatusList           status = {0, NULL};
   
   memset(&status, 0, sizeof(ARStatusList));
   memset(&licenseValidList, 0, sizeof(ARLicenseValidList));
   memset(&licenseTypeList, 0, sizeof(ARLicenseNameList));

   /* Get parameters */
   DriverPrintHeader("VALIDATE MULTIPLE LICENSES");
   num = GetUnsignedInt("Number of licenses to validate (1): ", 1);

   if (num == 0)
   {
      /* don't want to validate anything */
      return AR_RETURN_ERROR;
   }
   else
   {
      licenseTypeList.nameList =
         (ARLicenseNameType *) calloc(num, sizeof(ARLicenseNameType));
      if (licenseTypeList.nameList == NULL)
      {
         DriverPrintMallocCallError("ValidateMultipleLicenses");
         return AR_RETURN_ERROR;
      }

      for (i = 0; i < num; i++)
      {
         strcpy(licenseTypeList.nameList[i], GetChar("License Type: ", ""));
         licenseTypeList.numItems++;
      }
   }

   /* Call routine */
   BeginAPICall();
   result = ARValidateMultipleLicenses(GetControlStructPtr(), &licenseTypeList, 
                                       &licenseValidList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARValidateMultipleLicenses");
   PrintReturnCode(result);

   if (result < AR_RETURN_ERROR)
   {
      temp = licenseValidList.licenseValidateInfoList;
      tmpNameType = licenseTypeList.nameList;
      for (j = 0; j < licenseValidList.numItems; j++)
      {
         PrintChar("License Type: ", tmpNameType[j]);
         PrintInt("  Number Of Licenses: ", temp[j].numLicenses);
         PrintARBoolean("              IsDemo: ", temp[j].isDemo);
         PrintInt("  Expire Date: month: ", temp[j].expireDate.month);
         PrintInt("                 day: ", temp[j].expireDate.day);
         PrintInt("                year: ", temp[j].expireDate.year);
         PrintChar("          Token List: ", temp[j].tokenList);  
         PrintChar("", NULL);
      }
   }

   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARLicenseNameList(&licenseTypeList, FALSE);
   FreeARLicenseValidList(&licenseValidList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}
 

/*****************************************************************************/
/*                                                                           */
/*                            APIARGetListLicense                            */
/*                                                                           */
/*****************************************************************************/

int APIARGetListLicense()

{
   unsigned int          i;
   ARLicenseInfoList     licenseInfoList;
   ARLicenseNameType     licType;
   int                   result;
   ARStatusList          status = {0, NULL};
   ARLicenseInfoStruct  *tempInfo;

                                       /* initialize */
   memset(&status, 0L, sizeof(ARStatusList));
   memset(&licenseInfoList, 0L, sizeof(ARLicenseInfoList));

   /* Get parameters */
   DriverPrintHeader("GET LIST LICENSE");
   strcpy(licType, GetChar("License Type: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetListLicense(GetControlStructPtr(), licType, &licenseInfoList,
                             &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListLicense");
   PrintReturnCode(result);

   if (result < AR_RETURN_ERROR)
   {
      tempInfo = licenseInfoList.licenseInfoList;
      PrintUInt("Total Number of Licenses: ", licenseInfoList.numItems);
      for (i = 0; i < licenseInfoList.numItems; i++)
      {
         PrintChar("              License Type: ", tempInfo[i].licType);
         PrintChar("               License Key: ", tempInfo[i].licKey);
         PrintChar("          License Sub Type: ", tempInfo[i].licSubtype);
         PrintInt ("License Issued Date: month: ", tempInfo[i].issuedDate.month);
         PrintInt ("                       day: ", tempInfo[i].issuedDate.day);
         PrintInt ("                      year: ", tempInfo[i].issuedDate.year);
         PrintInt ("License Expire Date: month: ", tempInfo[i].expireDate.month);
         PrintInt ("                       day: ", tempInfo[i].expireDate.day);
         PrintInt ("                      year: ", tempInfo[i].expireDate.year);
         PrintChar("                 Site Name: ", tempInfo[i].siteName);
         PrintChar("                   Host ID: ", tempInfo[i].hostId);
         PrintInt ("        Number of Licenses: ", tempInfo[i].numLicenses);
         PrintChar("                Token List: ", tempInfo[i].tokenList);
         PrintChar("                   Comment: ", tempInfo[i].comment);
         PrintChar("", NULL);
      }
   }

   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARLicenseInfoList(&licenseInfoList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}
 

/*****************************************************************************/
/*                                                                           */
/*                            APIARCreateLicense                             */
/*                                                                           */
/*****************************************************************************/

int APIARCreateLicense()

{
   char                  buffer[80];
   ARLicenseInfoStruct   licenseInfoStruct;
   int                   result;
   ARStatusList          status = {0, NULL};

   memset(&licenseInfoStruct, 0L, sizeof(ARLicenseInfoStruct));
   memset(&buffer, 0L, 80);

   /* Get parameters */
   DriverPrintHeader("CREATE LICENSE");
   strcpy(licenseInfoStruct.licKey, GetChar("License Key: ", ""));
   strcpy(licenseInfoStruct.licType, GetChar("License Type: ", ""));
   strcpy(licenseInfoStruct.licSubtype, GetChar("License Sub Type: ", ""));
   licenseInfoStruct.issuedDate.month = GetInt("Issued Month (January-December, 0-11): (0)", 0);
   licenseInfoStruct.issuedDate.day = GetInt("Issued Day (1-31): (1)", 1);
   licenseInfoStruct.issuedDate.year = GetInt("Issued Year (yyyy): (2002)", 2002);
   licenseInfoStruct.issuedDate.year = licenseInfoStruct.issuedDate.year - 1900;

   /* "never expire" day would be Jan-01-1970 that's the default time. */
   licenseInfoStruct.expireDate.month = GetInt("Expire Month (January-December, 0-11): (0)", 0);
   licenseInfoStruct.expireDate.day = GetInt("Expire Day (1-31): (1)", 1);
   licenseInfoStruct.expireDate.year = GetInt("Expire Year (yyyy): (1970)", 1970);
   licenseInfoStruct.expireDate.year = licenseInfoStruct.expireDate.year - 1900;
   
   strcpy(buffer, GetChar("Site Name: ", ""));
   licenseInfoStruct.siteName = strdup(buffer);
   memset(buffer, 0L, 80);

   strcpy(buffer, GetChar("Host ID:", ""));
   licenseInfoStruct.hostId = strdup(buffer);
   memset(buffer, 0L, 80);

   licenseInfoStruct.numLicenses = GetInt("Number of Licenses: (1)", 1);

   strcpy(buffer, GetChar("Token List: ", ""));
   licenseInfoStruct.tokenList = strdup(buffer);
   memset(buffer, 0L, 80);

   strcpy(buffer, GetChar("Comment: ", ""));
   licenseInfoStruct.comment = strdup(buffer);
   memset(buffer, 0L, 80);
   
   /* Call routine */
   BeginAPICall();
   result = ARCreateLicense(GetControlStructPtr(), &licenseInfoStruct, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARCreateLicense");
   PrintReturnCode(result);
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARLicenseInfoStruct(&licenseInfoStruct, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}
 

/*****************************************************************************/
/*                                                                           */
/*                            APIARDeleteLicense                             */
/*                                                                           */
/*****************************************************************************/

int APIARDeleteLicense()

{
   ARLicenseKeyType    licKey;
   ARLicenseNameType   licType;
   int                 result;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DELETE LICENSE");
   strcpy(licType, GetChar("License Type: ", ""));
   strcpy(licKey, GetChar("License key: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARDeleteLicense(GetControlStructPtr(), licType, licKey, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARDeleteLicense");
   PrintReturnCode(result);
   PrintARStatusList(&status);
 
   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARImportLicense                             */
/*                                                                           */
/*****************************************************************************/

int APIARImportLicense()

{
   int            amtRead;
   int            amtRemaining;
   long           fileLen;
   char           filename[256];
   FILE          *filePtr;
   char          *importBuf;
   unsigned int   importOption;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("IMPORT LICENSE");
   strcpy(filename, GetChar("Filename containing license data: ", ""));
   importOption = GetUnsignedInt("Import option [Append=0, Overwrite=1, Fix Server=2]: (0) ", 0);

   filePtr = NULL;
   if (filename[0] != '\0')
      filePtr = fopen(filename, "r");
   if (filePtr == NULL)
   {
      DriverPrintError("\n   ***  File open failure: %s\n", filename);
      return AR_RETURN_ERROR;
   }

   fseek(filePtr, 0L, SEEK_END);
   fileLen = ftell(filePtr);
   importBuf = (char *) calloc(1, (unsigned) (fileLen + 100));
   if (importBuf == NULL)
   {
      fclose(filePtr);
      DriverPrintError("**** malloc failure for import operation ****\n");
      return AR_RETURN_ERROR;
   }
   rewind(filePtr);

   /* Note that the length of the file (fileLen) may actually be */
   /* more than what can be read.  Some standard io libraries    */
   /* will compress carriage returns (\r) and new lines (\n) on  */
   /* fread(), but this may not be reflected in ftell().         */
   for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
   {
      result = fread(&importBuf[amtRead], 1, amtRemaining, filePtr);
      if (result <= 0)
         break;
      amtRemaining -= result;
      amtRead += result;
   }
   importBuf[amtRead] = '\0';
   fclose(filePtr);

   /* Call routine */
   BeginAPICall();
   result = ARImportLicense(GetControlStructPtr(), importBuf, importOption,
                            &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARImportLicense");
   PrintReturnCode(result);
   PrintARStatusList(&status);
 
   /* Cleanup */
   free(importBuf);
   FreeARStatusList(&status, FALSE);

   return result;   
}
 

/*****************************************************************************/
/*                                                                           */
/*                            APIARExportLicense                             */
/*                                                                           */
/*****************************************************************************/

int APIARExportLicense()

{
   char          *exportBuf;
   char           filename[256];
   FILE          *filePtr;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("EXPORT LICENSE");
   filePtr = NULL;
   strcpy(filename, GetChar("Filename for exported licenses: ", ""));
   if (filename[0] != '\0')
      filePtr = fopen(filename, "w");
   if (filePtr == NULL)
   {
      DriverPrintError("\n   ***  File open failure: %s\n", filename);
      return AR_RETURN_ERROR;
   }

   /* Call routine */
   BeginAPICall();
   result = ARExportLicense(GetControlStructPtr(), &exportBuf, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARExportLicense");
   PrintReturnCode(result);
   if ((result < AR_RETURN_ERROR) && (exportBuf != NULL))
      fwrite(exportBuf, strlen(exportBuf), 1, filePtr);
   fclose(filePtr);
   PrintARStatusList(&status);
 
   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (exportBuf != NULL)
         free(exportBuf);
   }
   else
      unlink(filename);
   FreeARStatusList(&status, FALSE);

   return result;   
}
 

/*****************************************************************************/
/*                                                                           */
/*                       APIARGetListSQLForActiveLink                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetListSQLForActiveLink()

{
   unsigned int       actionIndex;
   unsigned int       actionType;
   ARNameType         actlinkName;
   ARFieldValueList   keywordList;
   unsigned int       maxRetrieve;
   unsigned int       numMatches;
   unsigned int      *numMatchesPtr;
   ARFieldValueList   parameterList;
   int                result;
   char              *returnCommand = NULL;
   char             **returnCommandPtr;
   ARStatusList       status = {0, NULL};
   ARTimestamp        timestamp;
   ARValueListList    valueListList;

   /* Get parameters */
   DriverPrintHeader("GETLIST SQL FOR ACTIVE LINK");
   strcpy(actlinkName, GetChar("Active Link Name: ", ""));
   actionIndex = GetUnsignedInt("Action Index (0): ", 0);
   actionType = GetUnsignedInt("Action Type (0): ", 0);
   timestamp = GetARTimestamp("Timestamp (0): ", 0);
   DriverPrintPrompt("Keyword List:\n");
   GetARFieldValueList(&keywordList);
   DriverPrintPrompt("Parameter List:\n");
   GetARFieldValueList(&parameterList);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ",
                                500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Get return command? (F): ", FALSE))
      returnCommandPtr = &returnCommand;
   else
      returnCommandPtr = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARGetListSQLForActiveLink(GetControlStructPtr(), actlinkName,
                                      actionIndex, actionType, timestamp,
                                      &keywordList, &parameterList, maxRetrieve, 
                                      &valueListList, numMatchesPtr,
                                      returnCommandPtr, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListSQLForActiveLink");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARValueListList(&valueListList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);
      if (returnCommandPtr != NULL)
         PrintChar("Returned Command: ", returnCommand);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARValueListList(&valueListList, FALSE);
      if (returnCommandPtr != NULL)
         free(*returnCommandPtr);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                     APIARExecuteProcessForActiveLink                      */
/*                                                                           */
/*****************************************************************************/

int APIARExecuteProcessForActiveLink()

{
   unsigned int       actionIndex;
   unsigned int       actionType;
   ARNameType         actlinkName;
   unsigned int       fieldid;
   ARFieldValueList   keywordList;
   ARFieldValueList   parameterList;
   int                result;
   char              *returnCommand;
   char             **returnCommandPtr;
   int                returnStatus;
   int               *returnStatusPtr;
   char              *returnString;
   char             **returnStringPtr;
   ARStatusList       status = {0, NULL};
   ARTimestamp        timestamp;

   /* Get parameters */
   DriverPrintHeader("EXECUTE PROCESS FOR ACTIVE LINK");
   strcpy(actlinkName, GetChar("Active Link Name: ", ""));
   actionIndex = GetUnsignedInt("Action Index (0): ", 0);
   actionType = GetUnsignedInt("Action Type (0): ", 0);
   fieldid = GetInt("fieldid (0): ", 0);
   timestamp = GetARTimestamp("Timestamp (0): ", 0);
   DriverPrintPrompt("Keyword List:\n");
   GetARFieldValueList(&keywordList);
   DriverPrintPrompt("Parameter List:\n");
   GetARFieldValueList(&parameterList);
   if (GetARBoolean("Wait for process to complete? (F): ", FALSE))
   {
      returnStatusPtr = &returnStatus;
      returnStringPtr = &returnString;
   }
   else
   {
      returnStatusPtr = NULL;
      returnStringPtr = NULL;
   }
   
   if (GetARBoolean("Get return command? (F): ", FALSE))
      returnCommandPtr = &returnCommand;
   else
      returnCommandPtr = NULL;

   /* Call routine */
   BeginAPICall();
   result = ARExecuteProcessForActiveLink(GetControlStructPtr(), actlinkName,
                                          actionIndex, actionType, fieldid,
                                          timestamp, &keywordList,
                                          &parameterList, returnStatusPtr,
                                          returnStringPtr, returnCommandPtr,
                                          &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARExecuteProcessForActiveLink");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      if (returnStatusPtr != NULL)
         PrintInt("Return Status: ", *returnStatusPtr);
      if (returnStringPtr != NULL)
         PrintChar("Process output: ", *returnStringPtr);
      if (returnCommandPtr != NULL)
         PrintChar("Return command: ", *returnCommandPtr);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      if (returnStringPtr != NULL)
         free(*returnStringPtr);
      if (returnCommandPtr != NULL)
         free(*returnCommandPtr);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARGetSessionConfiguration                        */
/*                                                                           */
/*****************************************************************************/

int APIARGetSessionConfiguration()
{
   int             result;
   ARStatusList    status = {0, NULL};
   unsigned int    variableId;
   ARValueStruct   variableValue;

   /* Get parameters */
   DriverPrintHeader("GET SESSION CONFIGURATION");

   variableId = GetUnsignedInt("Session variable Id (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetSessionConfiguration(GetControlStructPtr(), variableId,
                                      &variableValue, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetSessionConfiguration");
   PrintReturnCode(result);

   if (result < AR_RETURN_ERROR)
      PrintARValueStruct("", &variableValue);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARValueStruct(&variableValue, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARSetSessionConfiguration                        */
/*                                                                           */
/*****************************************************************************/

int APIARSetSessionConfiguration()
{
   unsigned int    dataType;
   int             result;
   ARStatusList    status = {0, NULL};
   unsigned int    variableId;
   ARValueStruct   variableValue;

   /* Get parameters */
   DriverPrintHeader("SET SESSION CONFIGURATION");
   variableId = GetUnsignedInt("Session variable Id (0): ", 0);
   dataType = GetUnsignedInt(
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/\n"
"         attach/currency/date/timeofday/join/trim/control/Table/Column/\n"
"         ulong/coords/view/display (0-14, 30-34, 40-43) (2): ",
                     AR_DATA_TYPE_INTEGER);

   variableValue.dataType = dataType;
   GetValue(&variableValue, FALSE, "");

   /* Call routine */
   BeginAPICall();
   result = ARSetSessionConfiguration(GetControlStructPtr(), variableId,
                                      &variableValue, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetSessionConfiguration");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARValueStruct(&variableValue, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARDateToJulianDate                           */
/*                                                                           */
/*****************************************************************************/

void APIARDateToJulianDate()
{
   int            jd;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("DATE TO JULIAN DATE");

   jd = GetDate("Date Value () [YYYY-MM-DD]: ", 0);

   /* Print results */
   DriverPrintResultHeader("ARDateToJulianDate");
   PrintReturnCode(AR_RETURN_OK);
   PrintInt("(julian date)  ", jd);
   PrintARStatusList(&status);
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARJulianDateToDate                           */
/*                                                                           */
/*****************************************************************************/

void APIARJulianDateToDate()
{
   int            jd;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("JULIAN DATE TO DATE");

   jd = GetInt("Julian Date Value (0): ", 0);

   /* Print results */
   DriverPrintResultHeader("ARJulianDateToDate");
   PrintReturnCode(AR_RETURN_OK);
   PrintARDate("(date)  ", jd);  
   PrintARStatusList(&status);
}


/*****************************************************************************/
/*                                                                           */
/*                     APIARGetMultipleCurrencyRatioSets                     */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleCurrencyRatioSets()

{
   ARTextStringList   currencyRatios;
   ARTimestampList    ratioTimestamps;
   int                result;
   ARStatusList       status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE CURRENCY RATIO SETS");
   GetARTimestampList(&ratioTimestamps);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleCurrencyRatioSets(GetControlStructPtr(),
                                           &ratioTimestamps,
                                           &currencyRatios, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleCurrencyRatioSets");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARTextStringList("Currency Ratio Sets", "   Ratio Set: ",
                            &currencyRatios);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARTextStringList(&currencyRatios, FALSE);
   }
   FreeARTimestampList(&ratioTimestamps, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetCurrencyRatio                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetCurrencyRatio()

{
   ARValueStruct        currencyRatio;
   char                 currencyRatios[1024];
   ARCurrencyCodeType   fromCurrencyCode;
   int                  result;
   ARStatusList         status = {0, NULL};
   ARCurrencyCodeType   toCurrencyCode;
   
   /* Get parameters */
   DriverPrintHeader("GET CURRENCY RATIO");
   strcpy(currencyRatios, GetChar("Currency ratios string: ", ""));
   strcpy(fromCurrencyCode, GetChar("From currency code: ", ""));
   strcpy(toCurrencyCode, GetChar("To currency code: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARGetCurrencyRatio(GetControlStructPtr(), currencyRatios,
                               fromCurrencyCode, toCurrencyCode, &currencyRatio,
                               &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetCurrencyRatio");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARValueStruct("", &currencyRatio);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARValueStruct(&currencyRatio, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARXMLCreateEntry                            */
/*                                                                           */
/*****************************************************************************/

int APIARXMLCreateEntry()
{
   int            amtRead, amtRemaining;
   long           fileLen;
   char          *optionDocBuf = NULL;
   char           optionDocFile[256];
   FILE          *optionDocFilePtr = NULL;
   char          *outputDoc = NULL;
   char          *outputMappingBuf = NULL;
   char           outputMappingFile[256];
   FILE          *outputMappingFilePtr = NULL;
   int            result;
   ARStatusList   status = {0, NULL};
   char          *xmlInputDocBuf = NULL;
   char           xmlInputDocFile[256];
   FILE          *xmlInputDocFilePtr = NULL;
   char          *xmlInputMappingBuf = NULL;
   char           xmlInputMappingFile[256];
   FILE          *xmlInputMappingFilePtr = NULL;

   /* Get parameters */
   DriverPrintHeader("XML CREATE ENTRY");
   strcpy(xmlInputMappingFile, GetChar("Filename containing XML input mapping: ", ""));
   strcpy(xmlInputDocFile, GetChar("Filename containing XML input document: ", ""));
   strcpy(outputMappingFile, GetChar("Filename containing XML output mapping: ", ""));
   strcpy(optionDocFile, GetChar("Filename containing XML option document: ", ""));

   /* GET XML Mapping */
   if (xmlInputMappingFile[0] != '\0')
   {
      xmlInputMappingFilePtr = fopen(xmlInputMappingFile, "r");
      if (xmlInputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input mapping: %s  ****\n",
                          xmlInputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputMappingFilePtr);
      xmlInputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputMappingBuf == NULL)
      {
         fclose(xmlInputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading input mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputMappingBuf[amtRead], 1, amtRemaining,
                        xmlInputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputMappingBuf[amtRead] = '\0';
      fclose(xmlInputMappingFilePtr);
   }

   /* GET XML Document */
   if (xmlInputDocFile[0] != '\0')
   {
      xmlInputDocFilePtr = fopen(xmlInputDocFile, "r");
      if (xmlInputDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input document: %s  ****\n",
                          xmlInputDocFile);
         if (xmlInputMappingBuf)
            free(xmlInputMappingBuf);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputDocFilePtr);
      xmlInputDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputDocBuf == NULL)
      {
         fclose(xmlInputDocFilePtr);
         DriverPrintError("**** malloc failure for reading input document file ****\n");
         if (xmlInputMappingBuf)
            free(xmlInputMappingBuf);
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputDocBuf[amtRead], 1, amtRemaining,
                        xmlInputDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputDocBuf[amtRead] = '\0';
      fclose(xmlInputDocFilePtr);
   }

   /* GET XML Mapping */
   if (outputMappingFile[0] != '\0')
   {
      outputMappingFilePtr = fopen(outputMappingFile, "r");
      if (outputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML output mapping: %s  ****\n",
                          outputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(outputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(outputMappingFilePtr);
      outputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (outputMappingBuf == NULL)
      {
         fclose(outputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading output mapping file ****\n");
         if (xmlInputMappingBuf)
            free(xmlInputMappingBuf);
         return AR_RETURN_ERROR;
      }
      rewind(outputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&outputMappingBuf[amtRead], 1, amtRemaining,
                        outputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      outputMappingBuf[amtRead] = '\0';
      fclose(outputMappingFilePtr);
   }

   /* GET XML Document */
   if (optionDocFile[0] != '\0')
   {
      optionDocFilePtr = fopen(optionDocFile, "r");
      if (optionDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML option document: %s  ****\n",
                          optionDocFile);
         if (xmlInputMappingBuf)
            free(xmlInputMappingBuf);
         if (outputMappingBuf)
            free(outputMappingBuf);
         return AR_RETURN_ERROR;
      }
      fseek(optionDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(optionDocFilePtr);
      optionDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (optionDocBuf == NULL)
      {
         fclose(optionDocFilePtr);
         DriverPrintError("**** malloc failure for reading option document file ****\n");
         if (xmlInputMappingBuf)
            free(xmlInputMappingBuf);
         if (outputMappingBuf)
            free(outputMappingBuf);
         return AR_RETURN_ERROR;
      }
      rewind(optionDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&optionDocBuf[amtRead], 1, amtRemaining,
                        optionDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      optionDocBuf[amtRead] = '\0';
      fclose(optionDocFilePtr);
   }

   /* Call routine */
   BeginAPICall();
   result = ARXMLCreateEntry(GetControlStructPtr(), xmlInputMappingBuf,
                             xmlInputDocBuf, outputMappingBuf, optionDocBuf,
                             &outputDoc, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARXMLCreateEntry");
   PrintReturnCode(result);
   fprintf(stdout, "%s\n", outputDoc);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (xmlInputMappingBuf != NULL)
      free(xmlInputMappingBuf);
   if (xmlInputDocBuf != NULL)
      free(xmlInputDocBuf);
   if (optionDocBuf != NULL)
      free(optionDocBuf);
   if (outputMappingBuf)
      free(outputMappingBuf);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARXMLGetEntry                              */
/*                                                                           */
/*****************************************************************************/

int APIARXMLGetEntry()
{
   int            amtRead, amtRemaining;
   long           fileLen;
   char          *optionDocBuf = NULL;
   char           optionDocFile[256];
   FILE          *optionDocFilePtr = NULL;
   char          *outputDoc=NULL;
   char          *outputMappingBuf = NULL;
   char           outputMappingFile[256];
   FILE          *outputMappingFilePtr = NULL;
   int            result;
   ARStatusList   status = {0, NULL};
   char          *xmlQueryDocBuf = NULL;
   char           xmlQueryDocFile[256];
   FILE          *xmlQueryDocFilePtr = NULL;
   char          *xmlQueryMappingBuf = NULL;
   char           xmlQueryMappingFile[256];
   FILE          *xmlQueryMappingFilePtr = NULL;

   /* Get parameters */
   DriverPrintHeader("XML GET ENTRY");
   strcpy(xmlQueryMappingFile, GetChar("Filename containing XML query mapping: ", ""));
   strcpy(xmlQueryDocFile, GetChar("Filename containing XML query document: ", ""));
   strcpy(outputMappingFile, GetChar("Filename containing XML output mapping: ", ""));
   strcpy(optionDocFile, GetChar("Filename containing XML option document: ", ""));

   /* GET XML Mapping */
   if (xmlQueryMappingFile[0] != '\0')
   {
      xmlQueryMappingFilePtr = fopen(xmlQueryMappingFile, "r");
      if (xmlQueryMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML query mapping: %s  ****\n",
                          xmlQueryMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlQueryMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlQueryMappingFilePtr);
      xmlQueryMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlQueryMappingBuf == NULL)
      {
         fclose(xmlQueryMappingFilePtr);
         DriverPrintError("**** malloc failure for reading query mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlQueryMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlQueryMappingBuf[amtRead], 1, amtRemaining,
                        xmlQueryMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlQueryMappingBuf[amtRead] = '\0';
      fclose(xmlQueryMappingFilePtr);
   }

   /* GET XML Document */
   if (xmlQueryDocFile[0] != '\0')
   {
      xmlQueryDocFilePtr = fopen(xmlQueryDocFile, "r");
      if (xmlQueryDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML query document: %s  ****\n",
                          xmlQueryDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlQueryDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlQueryDocFilePtr);
      xmlQueryDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlQueryDocBuf == NULL)
      {
         fclose(xmlQueryDocFilePtr);
         DriverPrintError("**** malloc failure for reading query document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlQueryDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlQueryDocBuf[amtRead], 1, amtRemaining,
                        xmlQueryDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlQueryDocBuf[amtRead] = '\0';
      fclose(xmlQueryDocFilePtr);
   }

   /* GET XML Mapping */
   if (outputMappingFile[0] != '\0')
   {
      outputMappingFilePtr = fopen(outputMappingFile, "r");
      if (outputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML output mapping: %s  ****\n",
                          outputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(outputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(outputMappingFilePtr);
      outputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (outputMappingBuf == NULL)
      {
         fclose(outputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading output mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(outputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&outputMappingBuf[amtRead], 1, amtRemaining,
                        outputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      outputMappingBuf[amtRead] = '\0';
      fclose(outputMappingFilePtr);
   }

   /* GET XML Document */
   if (optionDocFile[0] != '\0')
   {
      optionDocFilePtr = fopen(optionDocFile, "r");
      if (optionDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML option document: %s  ****\n",
                          optionDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(optionDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(optionDocFilePtr);
      optionDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (optionDocBuf == NULL)
      {
         fclose(optionDocFilePtr);
         DriverPrintError("**** malloc failure for reading option document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(optionDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&optionDocBuf[amtRead], 1, amtRemaining,
                        optionDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      optionDocBuf[amtRead] = '\0';
      fclose(optionDocFilePtr);
   }

   /* Call routine */
   BeginAPICall();
   result = ARXMLGetEntry(GetControlStructPtr(), xmlQueryMappingBuf,
                          xmlQueryDocBuf, outputMappingBuf, optionDocBuf,
                          &outputDoc, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARXMLGetEntry");
   PrintReturnCode(result);
   fprintf(stdout, "%s\n", outputDoc);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (xmlQueryMappingBuf != NULL)
      free(xmlQueryMappingBuf);
   if (xmlQueryDocBuf != NULL)
      free(xmlQueryDocBuf);
   if (outputMappingBuf != NULL)
      free(outputMappingBuf);
   if (optionDocBuf != NULL)
      free(optionDocBuf);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARXMLSetEntry                              */
/*                                                                           */
/*****************************************************************************/

int APIARXMLSetEntry()
{
   int            amtRead, amtRemaining;
   long           fileLen;
   char          *optionDocBuf = NULL;
   char           optionDocFile[256];
   FILE          *optionDocFilePtr = NULL;
   char          *outputDoc = NULL;
   char          *outputMappingBuf = NULL;
   char           outputMappingFile[256];
   FILE          *outputMappingFilePtr = NULL;
   int            result;
   ARStatusList   status = {0, NULL};
   char          *xmlInputDocBuf = NULL;
   char           xmlInputDocFile[256];
   FILE          *xmlInputDocFilePtr = NULL;
   char          *xmlInputMappingBuf = NULL;
   char           xmlInputMappingFile[256];
   FILE          *xmlInputMappingFilePtr = NULL;
   char          *xmlQueryDocBuf = NULL;
   char           xmlQueryDocFile[256];
   FILE          *xmlQueryDocFilePtr = NULL;
   char          *xmlQueryMappingBuf = NULL;
   char           xmlQueryMappingFile[256];
   FILE          *xmlQueryMappingFilePtr = NULL;

   /* Get parameters */
   DriverPrintHeader("XML SET ENTRY");
   strcpy(xmlQueryMappingFile, GetChar("Filename containing XML query mapping: ", ""));
   strcpy(xmlQueryDocFile, GetChar("Filename containing XML query document: ", ""));
   strcpy(xmlInputMappingFile, GetChar("Filename containing XML input mapping: ", ""));
   strcpy(xmlInputDocFile, GetChar("Filename containing XML input document: ", ""));
   strcpy(outputMappingFile, GetChar("Filename containing XML output mapping: ", ""));
   strcpy(optionDocFile, GetChar("Filename containing XML option document: ", ""));

   /* GET XML Mapping */
   if (xmlInputMappingFile[0] != '\0')
   {
      xmlInputMappingFilePtr = fopen(xmlInputMappingFile, "r");
      if (xmlInputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input mapping: %s  ****\n",
                          xmlInputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputMappingFilePtr);
      xmlInputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputMappingBuf == NULL)
      {
         fclose(xmlInputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading input mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputMappingBuf[amtRead], 1, amtRemaining,
                        xmlInputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputMappingBuf[amtRead] = '\0';
      fclose(xmlInputMappingFilePtr);
   }

   /* GET XML Document */
   if (xmlInputDocFile[0] != '\0')
   {
      xmlInputDocFilePtr = fopen(xmlInputDocFile, "r");
      if (xmlInputDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input document: %s  ****\n",
                          xmlInputDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputDocFilePtr);
      xmlInputDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputDocBuf == NULL)
      {
         fclose(xmlInputDocFilePtr);
         DriverPrintError("**** malloc failure for reading input document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputDocBuf[amtRead], 1, amtRemaining,
                        xmlInputDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputDocBuf[amtRead] = '\0';
      fclose(xmlInputDocFilePtr);
   }

   /* GET XML Mapping */
   if (xmlQueryMappingFile[0] != '\0')
   {
      xmlQueryMappingFilePtr = fopen(xmlQueryMappingFile, "r");
      if (xmlQueryMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML query mapping: %s  ****\n",
                          xmlQueryMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlQueryMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlQueryMappingFilePtr);
      xmlQueryMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlQueryMappingBuf == NULL)
      {
         fclose(xmlQueryMappingFilePtr);
         DriverPrintError("**** malloc failure for reading query mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlQueryMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlQueryMappingBuf[amtRead], 1, amtRemaining,
                        xmlQueryMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlQueryMappingBuf[amtRead] = '\0';
      fclose(xmlQueryMappingFilePtr);
   }

   /* GET XML Document */
   if (xmlQueryDocFile[0] != '\0')
   {
      xmlQueryDocFilePtr = fopen(xmlQueryDocFile, "r");
      if (xmlQueryDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML query document: %s  ****\n",
                          xmlQueryDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlQueryDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlQueryDocFilePtr);
      xmlQueryDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlQueryDocBuf == NULL)
      {
         fclose(xmlQueryDocFilePtr);
         DriverPrintError("**** malloc failure for reading query document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlQueryDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlQueryDocBuf[amtRead], 1, amtRemaining,
                        xmlQueryDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlQueryDocBuf[amtRead] = '\0';
      fclose(xmlQueryDocFilePtr);
   }

   /* GET XML Mapping */
   if (outputMappingFile[0] != '\0')
   {
      outputMappingFilePtr = fopen(outputMappingFile, "r");
      if (outputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML output mapping: %s  ****\n",
                          outputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(outputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(outputMappingFilePtr);
      outputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (outputMappingBuf == NULL)
      {
         fclose(outputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading output mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(outputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&outputMappingBuf[amtRead], 1, amtRemaining,
                        outputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      outputMappingBuf[amtRead] = '\0';
      fclose(outputMappingFilePtr);
   }

   /* GET XML Document */
   if (optionDocFile[0] != '\0')
   {
      optionDocFilePtr = fopen(optionDocFile, "r");
      if (optionDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML option document: %s  ****\n",
                          optionDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(optionDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(optionDocFilePtr);
      optionDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (optionDocBuf == NULL)
      {
         fclose(optionDocFilePtr);
         DriverPrintError("**** malloc failure for reading option document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(optionDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&optionDocBuf[amtRead], 1, amtRemaining,
                        optionDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      optionDocBuf[amtRead] = '\0';
      fclose(optionDocFilePtr);
   }

   /* Call routine */
   BeginAPICall();
   result = ARXMLSetEntry(GetControlStructPtr(), xmlQueryMappingBuf,
                          xmlQueryDocBuf, xmlInputMappingBuf, xmlInputDocBuf,
                          outputMappingBuf, optionDocBuf,  &outputDoc, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARXMLSetEntry");
   PrintReturnCode(result);
   fprintf(stdout, "%s\n", outputDoc);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (xmlInputMappingBuf != NULL)
      free(xmlInputMappingBuf);
   if (xmlInputDocBuf != NULL)
      free(xmlInputDocBuf);
   if (xmlQueryMappingBuf != NULL)
      free(xmlQueryMappingBuf);
   if (xmlQueryDocBuf != NULL)
      free(xmlQueryDocBuf);
   if (outputMappingBuf != NULL)
      free(outputMappingBuf);
   if (optionDocBuf != NULL)
      free(optionDocBuf);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARXMLServiceEntry                            */
/*                                                                           */
/*****************************************************************************/

int APIARXMLServiceEntry()
{
   int            amtRead, amtRemaining;
   long           fileLen;
   char          *optionDocBuf = NULL;
   char           optionDocFile[256];
   FILE          *optionDocFilePtr = NULL;
   char          *outputDoc = NULL;
   char          *outputMappingBuf = NULL;
   char           outputMappingFile[256];
   FILE          *outputMappingFilePtr = NULL;
   int            result;
   ARStatusList   status = {0, NULL};
   char          *xmlInputDocBuf = NULL;
   char           xmlInputDocFile[256];
   FILE          *xmlInputDocFilePtr = NULL;
   char          *xmlInputMappingBuf = NULL;
   char           xmlInputMappingFile[256];
   FILE          *xmlInputMappingFilePtr = NULL;

   /* Get parameters */
   DriverPrintHeader("XML SERVICE ENTRY");
   strcpy(xmlInputMappingFile, GetChar("Filename containing XML input mapping: ", ""));
   strcpy(xmlInputDocFile, GetChar("Filename containing XML input document: ", ""));
   strcpy(outputMappingFile, GetChar("Filename containing XML output mapping: ", ""));
   strcpy(optionDocFile, GetChar("Filename containing XML option document: ", ""));

   /* GET XML Mapping */
   if (xmlInputMappingFile[0] != '\0')
   {
      xmlInputMappingFilePtr = fopen(xmlInputMappingFile, "r");
      if (xmlInputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input mapping: %s  ****\n",
                          xmlInputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputMappingFilePtr);
      xmlInputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputMappingBuf == NULL)
      {
         fclose(xmlInputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading input mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputMappingBuf[amtRead], 1, amtRemaining,
                        xmlInputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputMappingBuf[amtRead] = '\0';
      fclose(xmlInputMappingFilePtr);
   }

   /* GET XML Document */
   if (xmlInputDocFile[0] != '\0')
   {
      xmlInputDocFilePtr = fopen(xmlInputDocFile, "r");
      if (xmlInputDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML input document: %s  ****\n",
                          xmlInputDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(xmlInputDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(xmlInputDocFilePtr);
      xmlInputDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (xmlInputDocBuf == NULL)
      {
         fclose(xmlInputDocFilePtr);
         DriverPrintError("**** malloc failure for reading input document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(xmlInputDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&xmlInputDocBuf[amtRead], 1, amtRemaining,
                        xmlInputDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      xmlInputDocBuf[amtRead] = '\0';
      fclose(xmlInputDocFilePtr);
   }

   /* GET XML Mapping */
   if (outputMappingFile[0] != '\0')
   {
      outputMappingFilePtr = fopen(outputMappingFile, "r");
      if (outputMappingFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML output mapping: %s  ****\n",
                          outputMappingFile);
         return AR_RETURN_ERROR;
      }
      fseek(outputMappingFilePtr, 0L, SEEK_END);
      fileLen = ftell(outputMappingFilePtr);
      outputMappingBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (outputMappingBuf == NULL)
      {
         fclose(outputMappingFilePtr);
         DriverPrintError("**** malloc failure for reading output mapping file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(outputMappingFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&outputMappingBuf[amtRead], 1, amtRemaining,
                        outputMappingFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      outputMappingBuf[amtRead] = '\0';
      fclose(outputMappingFilePtr);
   }

   /* GET XML Document */
   if (optionDocFile[0] != '\0')
   {
      optionDocFilePtr = fopen(optionDocFile, "r");
      if (optionDocFilePtr == NULL)
      {
         DriverPrintError("\n**** Cannot open file for XML option document: %s  ****\n",
                          optionDocFile);
         return AR_RETURN_ERROR;
      }
      fseek(optionDocFilePtr, 0L, SEEK_END);
      fileLen = ftell(optionDocFilePtr);
      optionDocBuf = (char *) malloc((unsigned) (fileLen + 1));
      if (optionDocBuf == NULL)
      {
         fclose(optionDocFilePtr);
         DriverPrintError("**** malloc failure for reading option document file ****\n");
         return AR_RETURN_ERROR;
      }
      rewind(optionDocFilePtr);
      for (amtRead = 0, amtRemaining = fileLen; amtRead < fileLen; )
      {
         result = fread(&optionDocBuf[amtRead], 1, amtRemaining,
                        optionDocFilePtr);
         if (result <= 0)
            break;
         amtRemaining -= result;
         amtRead += result;
      }
      optionDocBuf[amtRead] = '\0';
      fclose(optionDocFilePtr);
   }

   /* Call routine */
   BeginAPICall();
   result = ARXMLServiceEntry(GetControlStructPtr(), NULL, NULL,
                              xmlInputMappingBuf, xmlInputDocBuf,
                              outputMappingBuf, optionDocBuf,
                              &outputDoc, &status);
   EndAPICall(&status);
   
   /* Print results */
   DriverPrintResultHeader("ARXMLServiceEntry");
   PrintReturnCode(result);
   fprintf(stdout, "%s\n", outputDoc);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   if (xmlInputMappingBuf != NULL)
      free(xmlInputMappingBuf);
   if (xmlInputDocBuf != NULL)
      free(xmlInputDocBuf);
   if (outputMappingBuf != NULL)
      free(outputMappingBuf);
   if (optionDocBuf != NULL)
      free(optionDocBuf);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARGetMultipleEntryPoints                         */
/*                                                                           */
/*****************************************************************************/

int APIARGetMultipleEntryPoints()
{
                               /* input parameters */
   ARNameList                  appNameList;
   unsigned int                attribute = AR_ATTRIB_VISIBLE;
   ARTimestamp                 changedSince;
   ARNameType                  displayTag;
   ARReferenceTypeList         refTypeList;
   unsigned int                vuiType;

                               /* return parameters */
   ARTextStringList            descriptionList;
   ARTextStringList            entryPointDLabelList;
   ARNameList                  entryPointNameList;
   ARUnsignedIntList           entryPointTypeList;
   ARPermissionListList        groupListList;
   ARTextStringList            helpTextList;
   ARPropListList              objPropListList;
   ARTextStringList            ownerAppDLabelList;
   ARNameList                  ownerAppNameList;
   ARContainerOwnerObjListList ownerObjListList;
   ARReferenceListList         referencesList;
   ARTimestampList             timestampList;

                               /* local variables */
   unsigned int                i;
   int                         result;
   ARStatusList                status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET MULTIPLE ENTRY POINTS");
   changedSince = GetARTimestamp("Get all changed since (0): ", (ARTimestamp) 0);
   DriverPrintPrompt("Get entry points in applications:\n");
   GetARNameList(&appNameList, "");

   GetARReferenceTypeList(&refTypeList, "");

   strcpy(displayTag, GetChar("Display Tag: ", ""));

   vuiType = GetInt("Get the vui type (0): ", 0);

   attribute = GetUnsignedInt("Entry point attribute (visible, hidden) (0-1) (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetMultipleEntryPoints(GetControlStructPtr(),
                                     changedSince,
                                     &appNameList,
                                     &refTypeList,
                                     &displayTag,
                                     &vuiType,
                                     attribute,
                                     &entryPointNameList,
                                     &entryPointTypeList,
                                     &entryPointDLabelList,
                                     &ownerAppNameList,
                                     &ownerAppDLabelList,
                                     &groupListList,
                                     &ownerObjListList,
                                     &descriptionList,
                                     &referencesList,
                                     &helpTextList,
                                     &timestampList,
                                     &objPropListList,
                                     &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetMultipleEntryPoints");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      for (i = 0; i < entryPointNameList.numItems; i++)
      {
         PrintChar("Entry point name: ", entryPointNameList.nameList[i]);
         PrintInt("Entry point type: ", entryPointTypeList.intList[i]);
         PrintChar("Entry point display label: ",
                   entryPointDLabelList.stringList[i]);
         PrintChar("Owner application name: ", ownerAppNameList.nameList[i]);
         PrintChar("Owner application display label: ",
                   ownerAppDLabelList.stringList[i]);
         PrintARPermissionList(&groupListList.permissionList[i], FALSE);
         PrintARContainerOwnerObjList(&ownerObjListList.ownerObjListList[i]);
         PrintChar("Description: ", descriptionList.stringList[i]);
         PrintARReferenceList(&referencesList.referenceListList[i]);
         PrintChar("Help Text: ", helpTextList.stringList[i]);
         PrintARTimestamp("Last update time: ", timestampList.timestampList[i]);
         PrintARObjectPropList("Object properties: ", "   ",
                               &objPropListList.propsList[i]);
         PrintChar("", "");
      }
   }
   
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARNameList(&entryPointNameList, FALSE);
      FreeARUnsignedIntList(&entryPointTypeList, FALSE);
      FreeARTextStringList(&entryPointDLabelList, FALSE);
      FreeARNameList(&ownerAppNameList, FALSE);
      FreeARTextStringList(&ownerAppDLabelList, FALSE);
      FreeARPermissionListList(&groupListList, FALSE);
      FreeARContainerOwnerObjListList(&ownerObjListList, FALSE);
      FreeARTextStringList(&descriptionList, FALSE);
      FreeARReferenceListList(&referencesList, FALSE);
      FreeARTextStringList(&helpTextList, FALSE);
      FreeARTimestampList(&timestampList, FALSE);
      FreeARPropListList(&objPropListList, FALSE);
   }

                           /* free input parameters */
   FreeARNameList(&appNameList, FALSE);
   FreeARReferenceTypeList(&refTypeList, FALSE);

   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                      APIARBeginBulkEntryTransaction                       */
/*                                                                           */
/*****************************************************************************/

int APIARBeginBulkEntryTransaction()

{
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("BEGIN BULK ENTRY TRANSACTION");

   /* Call routine */
   BeginAPICall();
   result = ARBeginBulkEntryTransaction(GetControlStructPtr(), &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARBeginBulkEntryTransaction");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                        APIAREndBulkEntryTransaction                       */
/*                                                                           */
/*****************************************************************************/

int APIAREndBulkEntryTransaction()

{
   unsigned int            action;
   ARBulkEntryReturnList   bulkEntryReturnList = {0, NULL};
   int                     result;
   ARStatusList            status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("END BULK ENTRY TRANSACTION");
   action = GetUnsignedInt("Action Send/Cancel (1-2) (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = AREndBulkEntryTransaction(GetControlStructPtr(), action,
                                      &bulkEntryReturnList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("AREndBulkEntryTransaction");
   PrintReturnCode(result);
   PrintARBulkEntryReturnList(&bulkEntryReturnList);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARBulkEntryReturnList(&bulkEntryReturnList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIARSetImpersonatedUser                         */
/*                                                                           */
/*****************************************************************************/

int APIARSetImpersonatedUser()

{
   int                result;
   ARStatusList       status = {0, NULL};
   ARAccessNameType   user;

   /* Get parameters */
   DriverPrintHeader("SET IMPERSONATED USER");
   strcpy(user, GetChar("User to impersonate: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARSetImpersonatedUser(GetControlStructPtr(), user, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetImpersonatedUser");
   PrintReturnCode(result);
   PrintARStatusList(&status);
   
   /* Clean up */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetClientCharSet                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetClientCharSet()
{
   char           charSet[AR_MAX_LANG_SIZE + 1] = "";
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET CLIENT CHARSET");

   /* Call routine */
   BeginAPICall();
   result = ARGetClientCharSet(GetControlStructPtr(), charSet, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetClientCharSet");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("char-set: ", charSet);
   PrintARStatusList(&status);

   /* Clean up */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARGetServerCharSet                           */
/*                                                                           */
/*****************************************************************************/

int APIARGetServerCharSet()
{
   char           charSet[AR_MAX_LANG_SIZE + 1] = "";
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET SERVER CHARSET");

   /* Call routine */
   BeginAPICall();
   result = ARGetServerCharSet(GetControlStructPtr(), charSet, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetServerCharSet");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintChar("char-set: ", charSet);
   PrintARStatusList(&status);

   /* Clean up */
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIARParseXMLDocument                           */
/*                                                                           */
/*****************************************************************************/

int APIARParseXMLDocument()

{
   ARNameList          appBlockNameList;
   ARXMLInputDoc       inputDoc = { AR_XML_DOC_FILE_NAME, };
   ARStructItemList    objectsToParse;
   ARStructItemList    parsedObjects;
   ARXMLParsedStream   parsedStream;
   int                 result;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("XML PARSE DOCUMENT");
   GetMallocChar("Path to the file: ", "", &inputDoc.u.fileName);
   GetARStructItemList(&objectsToParse, NULL);

   /* Call routine */
   BeginAPICall();
   result = ARParseXMLDocument(GetControlStructPtr(), &inputDoc, &objectsToParse,
                               &parsedStream, &parsedObjects, &appBlockNameList, 
                               &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARParseXMLDocument");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Parsed objects: ", "");
      PrintARStructItemList(&parsedObjects);
      PrintARNameList("Application Block Name List: ", "   ", &appBlockNameList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARXMLInputDoc(&inputDoc, FALSE);
   FreeARStructItemList(&objectsToParse, FALSE);
   if (result < AR_RETURN_ERROR)
   {
      FreeARXMLParsedStream(&parsedStream, FALSE);
      FreeARStructItemList(&parsedObjects, FALSE);
      FreeARNameList(&appBlockNameList, FALSE);
   }
   FreeARStatusList(&status, FALSE);

   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                    APIARBeginClientManagedTransaction                     */
/*                                                                           */
/*****************************************************************************/

int APIARBeginClientManagedTransaction()
{
   int            result = AR_RETURN_OK;
   ARStatusList   status = {0, NULL};
   char          *transactionHandle = NULL;

   /* Get parameters */
   DriverPrintHeader("BEGIN CLIENT MANAGED TRANSACTION");

   /* Call routine */
   BeginAPICall();
   result = ARBeginClientManagedTransaction(GetControlStructPtr(),
                                            &transactionHandle,  &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARBeginClientManagedTransaction");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Transaction Handle: ", transactionHandle);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      free(transactionHandle);
   }
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIAREndClientManagedTransaction                   */
/*                                                                           */
/*****************************************************************************/

int APIAREndClientManagedTransaction()
{
   int            option;  
   int            result = AR_RETURN_OK;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("END CLIENT MANAGED TRANSACTION");
   option = GetInt ("Commit or roll back (commit, rollback) (1-2) (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = AREndClientManagedTransaction(GetControlStructPtr(), option, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("AREndClientManagedTransaction");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                        APIARSetClientManagedTransaction                   */
/*                                                                           */
/*****************************************************************************/

int APIARSetClientManagedTransaction()
{
   int            result = AR_RETURN_OK;
   ARStatusList   status = {0, NULL};
   char           transactionHandle[256];

   /* Get parameters */
   DriverPrintHeader("SET CLIENT MANAGED TRANSACTION");
   strcpy(transactionHandle, GetChar("TransactionHandle: ", ""));

   /* Call routine */
   BeginAPICall();
   result = ARSetClientManagedTransaction(GetControlStructPtr(), transactionHandle,
                                          &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARSetClientManagedTransaction");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                    APIARRemoveClientManagedTransaction                    */
/*                                                                           */
/*****************************************************************************/

int APIARRemoveClientManagedTransaction()
{
   int            result = AR_RETURN_OK;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("REMOVE CLIENT MANAGED TRANSACTION");

   /* Call routine */
   BeginAPICall();
   result = ARRemoveClientManagedTransaction(GetControlStructPtr(),  &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("APIARRemoveClientManagedTransaction");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
  
   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                         APIARGetObjectChangeTimes                         */
/*                                                                           */
/*****************************************************************************/

int APIARGetObjectChangeTimes()
{
   int                           result = AR_RETURN_OK;
   ARStatusList                  status = {0, NULL};
   ARObjectChangeTimestampList   tsList = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("GET OBJECT CHANGE TIMES");

   /* Call routine */
   BeginAPICall();
   result = ARGetObjectChangeTimes(GetControlStructPtr(), &tsList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetObjectChangeTimes");
   PrintReturnCode(result);
   PrintARObjectChangeTimestampList(&tsList);
   PrintARStatusList(&status);

   /* Cleanup */
   if (tsList.numItems && tsList.objectChanges)
      free(tsList.objectChanges);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                                APIWFDExecute                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDExecute()

{
   unsigned int   extraInfo1, extraInfo2;
   int            mode = 0;
   ARStatusList   status = {0, NULL};
   unsigned int   result;
   int            returnCode;

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD EXECUTE");

   /* Call routine */
   BeginAPICall();
   returnCode = ARWfdExecute(GetControlStructPtr(), mode, &result,
                             &extraInfo1, &extraInfo2, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdExecute");
   PrintReturnCode(returnCode);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return returnCode;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIWFDGetStackLocation                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetStackLocation()

{
   unsigned int           depth;
   ARWfdCurrentLocation   location = {0};
   int                    result;
   ARStatusList           status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("WFD GET CURRENT LOCATION");
   depth = GetUnsignedInt("Stack frame ? (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARWfdGetCurrentLocation(GetControlStructPtr(), depth, &location, &status);
   EndAPICall(&status);

   /* Print results */
   if (NOT_WFD_QUIET)
      DriverPrintResultHeader("ARWfdGetCurrentLocation");

   if (result < AR_RETURN_ERROR)
   {
      PrintWFDLocationStruct("Current Location:\n", &location); 
   }

   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARWfdCurrentLocation(&location, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIWFDGetFieldValues                             */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetFieldValues()

{
   ARFieldValueList   dbFieldList;
   unsigned int       depth;
   int                result;
   ARStatusList       status = {0, NULL};
   ARFieldValueList   trFieldList;

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD GET FIELD VALUE LIST");
   depth = GetUnsignedInt("Stack depth ? (0): ", 0);
   
   /* Call routine */
   BeginAPICall();
   result = ARWfdGetFieldValues(GetControlStructPtr(), depth, &trFieldList,
                                &dbFieldList, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdGetFieldValues");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      DriverPrintHeader( "Transaction Fields - " );
      PrintARFieldValueList(&trFieldList);
      DriverPrintHeader( "Database Fields - " );
      PrintARFieldValueList(&dbFieldList);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
   {
      FreeARFieldValueList(&trFieldList, FALSE);
      FreeARFieldValueList(&dbFieldList, FALSE);
   }
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                          APIWFDSetFieldValues                             */
/*                                                                           */
/*****************************************************************************/

int APIWFDSetFieldValues()

{
   ARFieldValueList   dbFieldList = {0, NULL};
   int                result;
   ARStatusList       status = {0, NULL};
   ARFieldValueList   trFieldList = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD SET FIELD VALUE LIST");

   DriverPrintPrompt("Field/value pairs to set in transaction list:\n");
   GetARFieldValueList(&trFieldList);

   DriverPrintPrompt("Field/value pairs to set in database list:\n");
   GetARFieldValueList(&dbFieldList);

   /* Call routine */
   BeginAPICall();
   result = ARWfdSetFieldValues(GetControlStructPtr(), &trFieldList, &dbFieldList,
                                &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdSetFieldValues");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARFieldValueList(&trFieldList, FALSE);
   FreeARFieldValueList(&dbFieldList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDGetDebugMode                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetDebugMode()

{
   unsigned int   debugMode;
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD GET DEBUG MODE");

   /* Call routine */
   BeginAPICall();
   result = ARWfdGetDebugMode(GetControlStructPtr(), &debugMode, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdGetDebugMode");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      DriverPrintPrompt("Current DebugMode (stop at):\n%s%s%s%s%s%s%s%s\n",
         (WfdStopAtStartAPI(debugMode) ? "   Begin API\n" : ""),
         (WfdStopAtQualifier(debugMode) ? "   Qualfier\n" : ""),
         (WfdStopAtP1Action(debugMode) ? "   Phase 1\n" : ""),
         (WfdStopAtP2Action(debugMode) ? "   Phase 2\n" : ""),
         (WfdStopAtP3Action(debugMode) ? "   Phase 3\n" : ""),
         (WfdStopAtEndAPI(debugMode) ?    "   End API\n" : ""),
         (WfdStopAtEndAPI(debugMode) ?    "   Esc Action\n" : ""),
         (WfdStopAtCMDB(debugMode) ?      "   CMDB\n" : "") );
   }

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDDoSetDebugMode                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDDoSetDebugMode(newMode)

{
   int            result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD SET DEBUG MODE");

   /* Call routine */
   BeginAPICall();
   result = ARWfdSetDebugMode(GetControlStructPtr(), newMode, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdSetDebugMode");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDSetDebugMode                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDSetDebugMode()

{
   unsigned int   debugMode = 0;
   int            result = 0;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD SET DEBUG MODE");

   DriverPrintHeader( "Debug Mode to set (stop at)? : " );
   if (GetARBoolean("   Begin API (T): ", TRUE))
      debugMode |= WFD_STOP_START_API;
   else
      debugMode &= ~WFD_STOP_START_API;

   if (GetARBoolean("   Qualifier (T): ", TRUE))
      debugMode |= WFD_STOP_QUALIFIER;
   else
      debugMode &= ~WFD_STOP_QUALIFIER;

   if (GetARBoolean("   Phase 1 (T): ", TRUE))
      debugMode |= WFD_STOP_P1_ACTION;
   else
      debugMode &= ~WFD_STOP_P1_ACTION;

   if (GetARBoolean("   Phase 2 (T): ", TRUE))
      debugMode |= WFD_STOP_P2_ACTION;
   else
      debugMode &= ~WFD_STOP_P2_ACTION;

   if (GetARBoolean("   Phase 3 (T): ", TRUE))
      debugMode |= WFD_STOP_P3_ACTION;
   else
      debugMode &= ~WFD_STOP_P3_ACTION;

   if (GetARBoolean("   End API (T): ", TRUE))
      debugMode |= WFD_STOP_END_API;
   else
      debugMode &= ~WFD_STOP_END_API;
   
   if (GetARBoolean("   Esc Action (T): ", TRUE))
      debugMode |= WFD_STOP_ESC_ACTION;
   else
      debugMode &= ~WFD_STOP_ESC_ACTION;
   
   if (GetARBoolean("   CMDB (T): ", TRUE))
      debugMode |= WFD_STOP_CMDB;
   else
      debugMode &= ~WFD_STOP_CMDB;
   
   /* Call routine */
   return APIWFDDoSetDebugMode(debugMode);
}


/*****************************************************************************/
/*                                                                           */
/*                          APIWFDGetFilterQual                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetFilterQual()

{
   ARQualifierStruct   outputQualfier;
   int                 result;
   ARStatusList        status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD GET FILTER QUALIFIER");

   /* Call routine */
   BeginAPICall();
   result = ARWfdGetFilterQual(GetControlStructPtr(), &outputQualfier, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdGetFilterQual");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
      PrintARQualifierStruct("    ", &outputQualfier);
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARQualifierStruct(&outputQualfier, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                         APIWFDSetQualifierResult                          */
/*                                                                           */
/*****************************************************************************/

int APIWFDSetQualifierResult()

{
   ARBoolean      result;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD SET QUALIFIER RESULT");
   result = GetARBoolean( "Set Qualifier Result, T/F? (T): ", TRUE );

   /* Call routine */
   BeginAPICall();
   result = ARWfdSetQualifierResult(GetControlStructPtr(), result, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdSetQualifierResult");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDTerminateAPI                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDTerminateAPI()

{
   int            result = 0;
   ARStatusList   status = {0, NULL};
   unsigned int   termCode;

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD TERMINATE API");
   termCode = GetInt("Error Code to return (AR_ERROR_TERMINATED_BY_DEBUGGER): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARWfdTerminateAPI(GetControlStructPtr(), termCode, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdTerminateAPI");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIWFDGetKeyword                              */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetKeyword()

{
   int             kwId = 0;
   int             result;
   ARStatusList    status = {0, NULL};
   ARValueStruct   value;

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD GET KEYWORD");
   kwId = GetInt("Keyword to get (1): ", 1);

   /* Call routine */
   BeginAPICall();
   result = ARWfdGetKeywordValue(GetControlStructPtr(), kwId, &value, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdGetKeywordValue");
   PrintARValueStruct("", &value);

   if (NOT_WFD_QUIET)
   {
      PrintReturnCode(result);
      PrintARStatusList(&status);
   }

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDGetUserContext                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDGetUserContext()

{
   int                mask = 0;
   int                result;
   ARStatusList       status = {0, NULL};
   ARWfdUserContext   userInfo;

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD GET USER CONTEXT");
   mask = GetInt("User Context Mask (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARWfdGetUserContext(GetControlStructPtr(), mask, &userInfo, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdGetUserContext");

   PrintARWfdUserContext(&userInfo);

   if (NOT_WFD_QUIET)
   {
      PrintReturnCode(result);
      PrintARStatusList(&status);
   }

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                            APIWFDRmtSetBreakPt                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDRmtSetBreakPt()

{
   ARWfdRmtBreakpoint  *bp;
   char                 filterString[sizeof(ARNameType)];
   int                  result = AR_ERROR_NONE;
   ARStatusList         status = {0, NULL};

   /* Get parameters */
   DriverPrintHeader("WFD SET REMOTE BREAK POINT");

   bp = calloc(1, sizeof(ARWfdRmtBreakpoint));

   WFDFillBreakPt(bp, FALSE);
   strncpy(filterString, (const char *) bp->filter, sizeof(ARNameType));
   if ((filterString[0] == '\0') || (bp->schema == '\0') ||
       (bp->stage < WFD_BEFORE_API) || (bp->stage > WFD_BEFORE_CMDB))
   {
      if (bp->filter)
         free( bp->filter);
      if (bp->schema)
         free(bp->schema);
      free(bp);
      DriverPrintError("Invalid Breakpoint definition. Breakpoint not set.\n");
      result = AR_WARN_WFD_NO_SUCH_BREAKPOINT;
   }
   else
   {
      result = ARWfdSetBreakpoint(GetControlStructPtr(), bp, &status);
   
      DriverPrintResultHeader("ARWfdSetBreakpoint");
      PrintReturnCode(result);
      PrintARStatusList(&status);
      FreeARStatusList(&status, FALSE);
   }

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIWFDRmtClrBreakPt                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDRmtClrBreakPt()

{
   unsigned int   bpId;
   int            result = 0;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD CLEAR REMOTE BREAKPOINT");

   bpId = GetUnsignedInt("Breakpoint to clear (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARWfdClearBreakpoint(GetControlStructPtr(), bpId, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdClearBreakpoint");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDRmtListBreakPt                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDRmtListBreakPt()

{
   ARWfdRmtBreakpointList   bp;
   unsigned int             i;
   int                      result = 0;
   ARStatusList             status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD LIST REMOTE BREAKPOINTS");

   /* Call routine */
   BeginAPICall();
   result = ARWfdListBreakpoints(GetControlStructPtr(), &bp, &status);
   EndAPICall(&status);

   /* Print results */
   if (NOT_WFD_QUIET)
      DriverPrintResultHeader("ARWfdListBreakpoints");

   for (i = 0; i < bp.numItems; i++)
      PrintWFDBreakpoint(&(bp.bpList[i]), FALSE);

   if (NOT_WFD_QUIET || result)
   {
      PrintReturnCode(result);
      PrintARStatusList(&status);
   }

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                           APIWFDRmtClearBpList                            */
/*                                                                           */
/*****************************************************************************/

int APIWFDRmtClearBpList()

{
   int            result = 0;
   ARStatusList   status = {0, NULL};

   /* Get parameters */
   if (NOT_WFD_QUIET)
      DriverPrintHeader("WFD CLEAR ALL REMOTE BREAKPOINTS");

   /* Call routine */
   BeginAPICall();
   result = ARWfdClearAllBreakpoints(GetControlStructPtr(), &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARWfdClearAllBreakpoints");
   PrintReturnCode(result);
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                  APIARGetListEntryWithMultiSchemaFields                   */
/*                                                                           */
/*****************************************************************************/

int APIARGetListEntryWithMultiSchemaFields()

{
   ARMultiSchemaFieldFuncValueListList   fieldFuncValueListList;
   unsigned int                          firstRetrieve;
   ARMultiSchemaFuncQueryFromList        fromList = {0, NULL};
   ARMultiSchemaFieldFuncList            getListFieldFuncs = {0, NULL};
   unsigned int                          maxRetrieve;
   unsigned int                          numMatches;
   unsigned int                         *numMatchesPtr;
   ARMultiSchemaQualifierStruct          qualifier = {AR_COND_OP_NONE, NULL};
   int                                   result = 0;
   ARMultiSchemaSortList                 sortList = {0, NULL};
   ARStatusList                          status = {0, NULL};
   ARBoolean                             useLocale = FALSE;
   
   /* Get parameters */
   DriverPrintHeader("GETLIST ENTRY WITH MULTISCHEMA FIELDS");

   GetARMultiSchemaQueryFromList(&fromList, FALSE, "");
   GetARMultiSchemaFieldIdList(&getListFieldFuncs, "");
   GetARMultiSchemaQualifierStruct(&qualifier, "Qualifier struct", FALSE, "");
   GetARMultiSchemaSortList(&sortList);
   firstRetrieve = GetUnsignedInt("First Retrieve (0): ", 0);
   maxRetrieve = GetUnsignedInt("Maximum number of entries to retrieve (500): ",
                                500);
   if (GetARBoolean("Get number of matches? (F): ", FALSE))
      numMatchesPtr = &numMatches;
   else
      numMatchesPtr = NULL;

   if (GetARBoolean("Use Locale for search? (F): ", FALSE))
      useLocale = TRUE;

   /* Call routine */
   BeginAPICall();
   result = ARGetListEntryWithMultiSchemaFields(GetControlStructPtr(), &fromList,
                                                &getListFieldFuncs, &qualifier, &sortList,
                                                firstRetrieve, maxRetrieve, useLocale,
                                                (ARMultiSchemaFieldIdList *) NULL,
                                                (ARMultiSchemaFuncQualifierStruct *) NULL, 
                                                &fieldFuncValueListList, numMatchesPtr,
                                                &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetListEntryWithMultiSchemaFields");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARMultiSchemaFieldValueListList(&fieldFuncValueListList);
      if (numMatchesPtr != NULL)
         PrintUInt("Number of matches: ", numMatches);      
   }
   PrintARStatusList(&status);

   /* Cleanup */
   FreeARMultiSchemaFuncQueryFromList(&fromList, FALSE);
   FreeARMultiSchemaQualifierStruct(&qualifier, FALSE);
   FreeARMultiSchemaFieldFuncList(&getListFieldFuncs, FALSE);
   FreeARMultiSchemaSortList(&sortList, FALSE);
   if (result < AR_RETURN_ERROR)
      FreeARMultiSchemaFieldFuncValueListList(&fieldFuncValueListList, FALSE);
   FreeARStatusList(&status, FALSE);
   
   return result;   
}


/*****************************************************************************/
/*                                                                           */
/*                             APIARGetCacheEvent                            */
/*                                                                           */
/*****************************************************************************/

int APIARGetCacheEvent()

{
   unsigned int       cacheCount = 0;
   ARInternalIdList   cacheEventIdList = {0, NULL};
   ARInternalIdList   cacheEventIdOccurredList = {0, NULL};
   int                result;
   unsigned int       returnOption = 0;
   ARStatusList       status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("GET CACHE EVENT");

   GetARInternalIdList(&cacheEventIdList);
   returnOption = GetUnsignedInt("Return Option: Immediate, Next (0-1) (0): ", 0);

   /* Call routine */
   BeginAPICall();
   result = ARGetCacheEvent(GetControlStructPtr(), &cacheEventIdList, returnOption,
                            &cacheEventIdOccurredList, &cacheCount, &status);
   EndAPICall(&status);

   /* Print results */
   DriverPrintResultHeader("ARGetCacheEvent");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintARInternalIdList("Events occurred: ", "", &cacheEventIdOccurredList);
      PrintUInt("Number of caches in the server: ", cacheCount);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (result < AR_RETURN_ERROR)
      FreeARInternalIdList(&cacheEventIdOccurredList, FALSE);
   FreeARInternalIdList(&cacheEventIdList, FALSE);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                            APIARCreateOverlay                             */
/*                                                                           */
/*****************************************************************************/

int APIARCreateOverlay()
{
   char              *objectModificationLogLabel = NULL;
   unsigned int       objType = 0;
   AROverlaidStruct   overlaid;
   ARInternalId       overlayId;
   ARNameType         overlayName;
   int                result;
   ARStatusList       status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("CREATE OVERLAY");
   objType = GetUnsignedInt("Type of the object to overlay: \nform(1), filter(5), activelink(6), menu(8), escalation(9),\ncontainer(12), view(14), field(15), image(17): ", 0);
   overlaid.objType = objType;
   
   if (objType == AR_STRUCT_ITEM_FIELD || objType == AR_STRUCT_ITEM_VUI)
   {
      /* schema name is applicable for field and view only */
      strncpy(overlaid.schemaName,
              GetChar("Schema of the Vui/Field to overlay: ", ""),
              AR_MAX_NAME_SIZE);
      overlaid.schemaName[AR_MAX_NAME_SIZE] = '\0';
      overlaid.id = GetUnsignedInt("Id of the Vui/Field to overlay: ", 0);
      overlaid.name[0] = '\0';
   }
   else
   {
      strncpy(overlaid.name, GetChar("Name of object to overlay: ", ""),
              AR_MAX_NAME_SIZE);
      overlaid.name[AR_MAX_NAME_SIZE] = '\0';
      overlaid.schemaName[0] = '\0';
      overlaid.id = 0;
   }

   GetTaskAndOMLLabel(&objectModificationLogLabel);

   /* Call routine */
   BeginAPICall();
   result = ARCreateOverlay(GetControlStructPtr(), &overlaid, objectModificationLogLabel,
                            overlayName, &overlayId, &status);
   EndAPICall(&status);
   
   /* Print results */
   DriverPrintResultHeader("ARCreateOverlay");
   PrintReturnCode(result);
   if (result < AR_RETURN_ERROR)
   {
      PrintChar("Overlay Name: ", overlayName);
      PrintInt("Overlay Id: ", overlayId);
   }
   PrintARStatusList(&status);

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}


/*****************************************************************************/
/*                                                                           */
/*                       APIARCreateOverlayFromObject                        */
/*                                                                           */
/*****************************************************************************/

int APIARCreateOverlayFromObject()
{
   AROverlaidStruct   baseObj, customObj;
   AROverlaidStruct  *baseObjPtr = NULL, *customObjPtr = NULL;
   char              *objectModificationLogLabel = NULL;
   unsigned int       objType = 0;
   unsigned int       operation = 0;
   ARInternalId       overlayId = 0;
   ARNameType         overlayName;
   int                result; 
   ARStatusList       status = {0, NULL};
   
   /* Get parameters */
   DriverPrintHeader("CREATE OVERLAY/CUSTOM FROM OBJECT");

#define AR_OVERLAY_CONVERT_TO_CUSTOM   1
#define AR_OVERLAY_CONVERT_TO_OVERLAY  2
#define AR_OVERLAY_CONVERT_TO_BASE     3

   operation = GetUnsignedInt("Type of operation - (Convert object to custom - 1, Convert custom object to overlay - 2, Convert custom object to base - 3) (1): ", 1);

   switch (operation)
   {
   case AR_OVERLAY_CONVERT_TO_CUSTOM: /* convert existing object to custom; only custom object is required. */
      /* Get name of custom object */
      strncpy(customObj.name, GetChar("Name of base object: ", ""),
              sizeof(ARNameType) - 1);
      customObj.name[sizeof(ARNameType) - 1] = '\0';
      
      /* Get type of object to overlay */
      objType = GetUnsignedInt("Type of the base object: \nform(1), filter(5), activelink(6), menu(8),\nescalation(9), container(12), view(14), field(15), image(17): ", 0);
      customObj.objType = objType;
      if (objType == AR_STRUCT_ITEM_FIELD || objType == AR_STRUCT_ITEM_VUI)
      {
         /* schema name is applicable for field and view only */
         strncpy(customObj.schemaName, GetChar("Schema of the Vui/Field: ", ""),
                 sizeof(ARNameType) - 1);
         customObj.schemaName[sizeof(ARNameType) - 1] = '\0';
         customObj.id = GetUnsignedInt("Id of the Vui/Field: ", 0);
      }
      else
      {
         customObj.schemaName[0] = '\0';
         customObj.id = 0;
      }
      customObjPtr = &customObj;
      break;

   case AR_OVERLAY_CONVERT_TO_OVERLAY: /* convert existing object to overlay; both base and custom are required. */
      /* Get name of base object */
      strncpy(baseObj.name, GetChar("Name of base object: ", ""),
              sizeof(ARNameType) - 1);
      baseObj.name[sizeof(ARNameType) - 1] = '\0';
      
      /* Get type of object to overlay */
      objType = GetUnsignedInt("Type of the base object to overlay: \nform(1), filter(5), activelink(6), menu(8),\nescalation(9), container(12), view(14), field(15), image(17): ", 0);
      baseObj.objType = objType;
      if (objType == AR_STRUCT_ITEM_FIELD || objType == AR_STRUCT_ITEM_VUI)
      {
         /* schema name is applicable for field and view only */
         strncpy(baseObj.schemaName, GetChar("Schema of the Vui/Field: ", ""),
                 sizeof(ARNameType) - 1);
         baseObj.schemaName[sizeof(ARNameType) - 1] = '\0';
         baseObj.id = GetUnsignedInt("Id of the Vui/Field: ", 0);
      }
      else
      {
         baseObj.schemaName[0] = '\0';
         baseObj.id = 0;
      }
      baseObjPtr = &baseObj;

      strncpy(customObj.name, GetChar("Name of custom object: ", ""),
              sizeof(ARNameType) - 1);
      customObj.name[sizeof(ARNameType) - 1] = '\0';
      
      /* Get type of object to overlay */
      objType = GetUnsignedInt("Type of the custom object: \nform(1), filter(5), activelink(6), menu(8),\nescalation(9), container(12), view(14), field(15), image(17): ", 0);
      customObj.objType = objType;
      if (objType == AR_STRUCT_ITEM_FIELD || objType == AR_STRUCT_ITEM_VUI)
      {
         /* schema name is applicable for field and view only */
         strncpy(customObj.schemaName, GetChar("Schema of the Vui/Field: ", ""),
                 sizeof(ARNameType) - 1);
         customObj.schemaName[sizeof(ARNameType) - 1] = '\0';
         customObj.id = GetUnsignedInt("Id of the Vui/Field: ", 0);
      }
      else
      {
         customObj.schemaName[0] = '\0';
         customObj.id = 0;
      }
      customObjPtr = &customObj;
      break;

   case AR_OVERLAY_CONVERT_TO_BASE: /* convert existing custom object to base; only custom object is required. */

      /* As of now, we are using baseObj to take inputs for custom object. This is */
      /* purposely to differentiate this case with case 1 where base is NULL.      */
      /* API checks if base param is null, then converts custom to overlay and if  */
      /* custom is NULL, converts custom to base.                                  */

      /* Get name of base object */
      strncpy(baseObj.name, GetChar("Name of custom object: ", ""),
              sizeof(ARNameType) - 1);
      baseObj.name[sizeof(ARNameType) - 1] = '\0';
      
      /* Get type of object to overlay */
      objType = GetUnsignedInt("Type of the custom object: \nform(1), filter(5), activelink(6), menu(8),\nescalation(9), container(12), view(14), field(15), image(17): ", 0);
      baseObj.objType = objType;
      if (objType == AR_STRUCT_ITEM_FIELD || objType == AR_STRUCT_ITEM_VUI)
      {
         /* schema name is applicable for field and view only */
         strncpy(baseObj.schemaName, GetChar("Schema of the Vui/Field: ", ""),
                 sizeof(ARNameType) - 1);
         baseObj.schemaName[sizeof(ARNameType) - 1] = '\0';
         baseObj.id = GetUnsignedInt("Id of the Vui/Field: ", 0);
      }
      else
      {
         baseObj.schemaName[0] = '\0';
         baseObj.id = 0;
      }
      baseObjPtr = &baseObj;
      break;

   default:
      PrintUInt("Unknown operation type: ", operation);
      break;
   }
   
   if (operation == AR_OVERLAY_CONVERT_TO_CUSTOM || 
       operation == AR_OVERLAY_CONVERT_TO_OVERLAY ||
       operation == AR_OVERLAY_CONVERT_TO_BASE)
   {
      GetTaskAndOMLLabel(&objectModificationLogLabel);

      /* Call routine */
      BeginAPICall();
      result = ARCreateOverlayFromObject(GetControlStructPtr(), baseObjPtr, customObjPtr,
                                         objectModificationLogLabel, overlayName,
                                         &overlayId, &status);
      EndAPICall(&status);
            
      /* Print results */
      DriverPrintResultHeader("ARCreateOverlayFromObject");
      PrintReturnCode(result);
      if (result < AR_RETURN_ERROR)
      {
         PrintChar("Overlay Name: ", overlayName);
         PrintInt("Overlay Id: ", overlayId);
      }
      PrintARStatusList(&status);
   }

   /* Cleanup */
   if (objectModificationLogLabel != NULL)
      free(objectModificationLogLabel);
   FreeARStatusList(&status, FALSE);

   return result;
}

