     /* File:  print.c */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifdef _WIN32
#ifndef snprintf
#define  snprintf _snprintf
#endif
#endif

#include "ar.h"
#include "arstruct.h"
#include "globals.h"

#include "api.h"
#include "print.h"
#include "util.h"

#ifndef OUTPUT_MODE
void PrintAuxillaryStatus()
{
   /* perform any generic printing operations here */
}
#endif


/*****************************************************************************/
/*                                                                           */
/*                             DriverPrintResult                             */
/*                                                                           */
/*****************************************************************************/

void DriverPrintResult(const char *format, ...)
{
   va_list marker;

   if (!(gQuietMode & SUPPRESS_RESULTS))
   {
      va_start(marker, format);

      vfprintf(((ThreadControlBlock *) GetThreadControlBlockPtr())->outFile,
               format, marker);

      va_end(marker);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          DriverPrintResultHeader                          */
/*                                                                           */
/*****************************************************************************/

void DriverPrintResultHeader(const char *functionName)
{
   if (!(gQuietMode & SUPPRESS_RESULTS))
      fprintf(((ThreadControlBlock *) GetThreadControlBlockPtr())->outFile,
              "\n   %s  results\n", functionName);
}


/*****************************************************************************/
/*                                                                           */
/*                             DriverPrintHeader                             */
/*                                                                           */
/*****************************************************************************/

void DriverPrintHeader(const char *header)
{
   if (!(gQuietMode & SUPPRESS_HEADERS) &&
       ((ThreadControlBlock *) GetThreadControlBlockPtr())->primaryThread)
      printf("%s\n", header);
}


/*****************************************************************************/
/*                                                                           */
/*                            DriverPrintPrompt                              */
/*                                                                           */
/*****************************************************************************/

void DriverPrintPrompt(const char *format, ...)
{
   va_list marker;

   if (!(gQuietMode & SUPPRESS_PROMPTS) &&
       ((ThreadControlBlock *) GetThreadControlBlockPtr())->primaryThread)
   {
      va_start(marker, format);

      vfprintf(stdout, format, marker);

      va_end(marker);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              DriverPrintMenu                              */
/*                                                                           */
/*****************************************************************************/

void DriverPrintMenu(const char *menuLine)
{
   if (!(gQuietMode & SUPPRESS_MENU))
      printf("%s", menuLine);
}


/*****************************************************************************/
/*                                                                           */
/*                             DriverPrintError                              */
/*                                                                           */
/*****************************************************************************/

void DriverPrintError(const char *format, ...)
{
   va_list marker;

   if (!(gQuietMode & SUPPRESS_ERRORS))
   {
      va_start(marker, format);

      vfprintf(((ThreadControlBlock *) GetThreadControlBlockPtr())->outFile,
               format, marker);

      va_end(marker);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            DriverPrintWarning                             */
/*                                                                           */
/*****************************************************************************/

void DriverPrintWarning(const char *format, ...)
{
   va_list marker;

   if (!(gQuietMode & SUPPRESS_WARNINGS))
   {
      va_start(marker, format);

      vfprintf(((ThreadControlBlock *) GetThreadControlBlockPtr())->outFile,
               format, marker);

      va_end(marker);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        DriverPrintMallocCallError                         */
/*                                                                           */
/*****************************************************************************/

void DriverPrintMallocCallError(const char *functionCallName)
{
   DriverPrintError("**** malloc failure for AR%s call ****\n",
                    functionCallName);
}


/*****************************************************************************/
/*                                                                           */
/*                              PrintARBoolean                               */
/*                                                                           */
/*****************************************************************************/

void PrintARBoolean(
char      *header,            /* IN; header string for the value */
ARBoolean  value              /* IN; value to print */
)

{
   DriverPrintResult((value) ? "%s TRUE\n" : "%s FALSE\n", header);
}


/*****************************************************************************/
/*                                                                           */
/*                                 PrintInt                                  */
/*                                                                           */
/*****************************************************************************/

void PrintInt(
char      *header,            /* IN; header string for the value */
int        value              /* IN; value to print */
)

{
   DriverPrintResult("%s %d\n", header, value);
}


/*****************************************************************************/
/*                                                                           */
/*                                 PrintUInt                                 */
/*                                                                           */
/*****************************************************************************/

void PrintUInt(
char          *header,        /* IN; header string for the value */
unsigned int   value          /* IN; value to print */
)

{
   DriverPrintResult("%s %u\n", header, value);
}         


/*****************************************************************************/
/*                                                                           */
/*                                 PrintLong                                 */
/*                                                                           */
/*****************************************************************************/

void PrintLong(
char       *header,           /* IN; header string for the value */
ARLong32    value             /* IN; value to print */
)

{
   DriverPrintResult("%s %d\n", header, value);
}          


/*****************************************************************************/
/*                                                                           */
/*                                PrintULong                                 */
/*                                                                           */
/*****************************************************************************/

void PrintULong(
char          *header,        /* IN; header string for the value */
ARULong32      value          /* IN; value to print */
)

{
   DriverPrintResult("%s %u\n", header, value);
}          


void PrintPtr(
const char *header,
void *value
)
{
   DriverPrintResult("%s %p\n", header, value);
}

/*****************************************************************************/
/*                                                                           */
/*                                 PrintReal                                 */
/*                                                                           */
/*****************************************************************************/

void PrintReal(
char      *header,            /* IN; header string for the value */
double     value              /* IN; value to print */
)

{
   DriverPrintResult("%s %lf\n", header, value);
}          


/*****************************************************************************/
/*                                                                           */
/*                                 PrintChar                                 */
/*                                                                           */
/*****************************************************************************/

void PrintChar(
char      *header,            /* IN; header string for the value */
char      *value              /* IN; value to print */
)

{
   if (value == NULL)
      DriverPrintResult("%s\n", header);
   else
      DriverPrintResult("%s %s\n", header, value);
}          


/*****************************************************************************/
/*                                                                           */
/*                                PrintARTime                                */
/*                                                                           */
/*****************************************************************************/

void PrintARTime(
char       *header,           /* IN; header string for the value */
ARTime      value             /* IN; value to print */
)

{
   char     tod[128];

   GetTODString(value, tod);
   if (!(gQuietMode & SUPPRESS_TIME))
      DriverPrintResult("%s %s\n", header, tod);
   else
      DriverPrintResult("%s **\n", header);
}          


/*****************************************************************************/
/*                                                                           */
/*                             PrintARTimestamp                              */
/*                                                                           */
/*****************************************************************************/

void PrintARTimestamp(
char        *header,          /* IN; header string for the value */
ARTimestamp  value            /* IN; value to print */
)

{
   if (!(gQuietMode & SUPPRESS_TIME))
      DriverPrintResult("%s %ld\n", header, value);
   else
      DriverPrintResult("%s **\n", header);
}          


/*****************************************************************************/
/*                                                                           */
/*                           PrintARTimestampList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARTimestampList(
char             *header,     /* IN; header string for the value */
char             *header2,    /* IN; header string for 2nd+ line */
ARTimestampList  *value       /* IN; value to print */
)

{
   char           buff[82];    /* buffer to build output in */
   char           buff2[16];   /* buffer to hold a converted number */
   unsigned int   i;           /* working index */
   unsigned int   maxBufSize;  /* size of output buffer */
   int            ret;         /* local return variable */
   ARTimestamp   *tempPtr;     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->timestampList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               if (!(gQuietMode & SUPPRESS_TIME))
                  sprintf(buff2, "%u", (unsigned) *tempPtr);
               else
                  strcpy(buff2, "**");
               if ((strlen(buff) + strlen(buff2)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff,header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", buff2);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                                PrintARDate                                */
/*                                                                           */
/*****************************************************************************/

void PrintARDate(
char       *header,        /* IN; header string for the value */
int         value          /* IN; value to print */
)

{
   char  buff[120];

   if (!(gQuietMode & SUPPRESS_DATE))
   {
      GetDateString(value, buff);
      DriverPrintResult("%s %s (%d)\n", header, buff, value);
   }
   else
      DriverPrintResult("%s **\n", header);
}  


/*****************************************************************************/
/*                                                                           */
/*                          PrintARUnsignedIntList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARUnsignedIntList(
char              *header,    /* IN; header string for the value */
char              *header2,   /* IN; header string for 2nd+ line */
ARUnsignedIntList *value      /* IN; value to print */
)

{
   char           buff[82];    /* buffer to build output in */
   char           buff2[16];   /* buffer to hold a converted number */
   unsigned int   i;           /* working index */
   unsigned int   maxBufSize;  /* size of output buffer */
   int            ret;         /* local return variable */
   unsigned int  *tempPtr;     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->intList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               sprintf(buff2, "%u", *tempPtr);
               if ((strlen(buff) + strlen(buff2)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - 1, "%s", buff2);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintAREntryIdList                            */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryIdList(
char          *header,        /* IN; header string for the value */
char          *header2,       /* IN; header string for 2nd+ line */
AREntryIdList *value          /* IN; value to print */
)

{
   char            buff[82];   /* buffer to build output in */
   unsigned int    i;          /* working index */
   unsigned int    maxBufSize; /* size of output buffer */
   int             ret;        /* local return variable */
   AREntryIdType  *tempPtr;    /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->entryIdList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               if ((strlen(buff) + strlen(*tempPtr)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", *tempPtr);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARInternalId                             */
/*                                                                           */
/*****************************************************************************/

void PrintARInternalId(
char         *header,         /* IN; header string for the value */
ARInternalId  value           /* IN; value to print */
)

{
   DriverPrintResult("%s %ld\n", header, value);
}          


/*****************************************************************************/
/*                                                                           */
/*                           PrintARInternalIdList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARInternalIdList(
char             *header,     /* IN; header string for the value */
char             *header2,    /* IN; header string for 2nd+ line */
ARInternalIdList *value       /* IN; value to print */
)

{
   char           buff[82];    /* buffer to build output in */
   char           buff2[16];   /* buffer to hold a converted number */
   unsigned int   i;           /* working index */
   unsigned int   maxBufSize;  /* size of output buffer */
   int            ret;         /* local return variable */
   ARInternalId  *tempPtr;     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->internalIdList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               sprintf(buff2, "%d", (int) *tempPtr);
               if ((strlen(buff) + strlen(buff2)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", buff2);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARLocaleList                              */
/*                                                                           */
/*****************************************************************************/

void PrintARLocaleList(
char         *header,         /* IN; header string for the value */
char         *header2,        /* IN; header string for 2nd+ line */
ARLocaleList *value           /* IN; value to print */
)
{
   enum {
      BFIXED = 32,
      BSZ = AR_MAX_LOCALE_SIZE+BFIXED > 128 ? AR_MAX_LOCALE_SIZE+BFIXED : 128
   };
   char           buff[BSZ];  /* buffer to build output in */
   unsigned int   i;
   unsigned int   maxBufSize;
   int            ret;
   ARLocaleType  *tempPtr;

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%.*s (%u)  ", BSZ-BFIXED, header,
                     value->numItems);
      if (ret > 0)
      {
         for (i = 0, tempPtr = value->localeList; i < value->numItems; ++tempPtr, ++i)
         {
            if (i != 0)
               strcat(buff, ", ");
            if ((strlen(buff) + strlen(*tempPtr)) >= (maxBufSize - 3))
            {
               buff[strlen(buff) - 1] = '\n'; /* over last blank */
               DriverPrintResult("%s", buff);
               if (strlen(header2) >= maxBufSize)
               {
                  strncpy(buff, header2, maxBufSize - 1);
                  buff[maxBufSize - 1] = '\0';
               }
               else
                  strcpy(buff, header2);
            }
            ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3, "%s", *tempPtr);
            if (ret < 0)
               buff[maxBufSize - 1] = '\0';            
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                              PrintARNameList                              */
/*                                                                           */
/*****************************************************************************/

void PrintARNameList(
char       *header,           /* IN; header string for the value */
char       *header2,          /* IN; header string for 2nd+ line */
ARNameList *value             /* IN; value to print */
)

{
   char           buff[AR_MAX_NAME_SIZE + 20]; /* buffer to build output in */
   unsigned int   i;                           /* working index */
   unsigned int   maxBufSize;                  /* size of output buffer */
   int            ret;                         /* local return variable */
   ARNameType    *tempPtr;                     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->nameList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               if ((strlen(buff) + strlen(*tempPtr)) >= (maxBufSize - 3))
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", *tempPtr);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARAccessNameList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARAccessNameList(
char             *header,           /* IN; header string for the value */
char             *header2,          /* IN; header string for 2nd+ line */
ARAccessNameList *value             /* IN; value to print */
)

{
   char              buff[82];   /* buffer to build output in */
   unsigned int      i;          /* working index */
   unsigned int      maxBufSize; /* size of output buffer */
   int               ret;        /* local return variable */
   ARAccessNameType *tempPtr;    /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->nameList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               if ((strlen(buff) + strlen(*tempPtr)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", *tempPtr);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
         else
            buff[maxBufSize - 1] = '\0';
      }
      DriverPrintResult("%s\n", buff);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARServerNameList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARServerNameList(
char             *header,     /* IN; header string for the value */
char             *header2,    /* IN; header string for 2nd+ line */
ARServerNameList *value       /* IN; value to print */
)

{
   char               buff[82];    /* buffer to build output in */
   unsigned int       i;           /* working index */
   unsigned int       maxBufSize;  /* size of output buffer */
   int                ret;         /* local return variable */
   ARServerNameType  *tempPtr;     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(header) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->nameList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               if ((strlen(buff) + strlen(*tempPtr)) >= (size_t) 79)
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s", *tempPtr);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARTextStringList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARTextStringList(header, header2, value)
char             *header;     /* IN; header string for the value */
char             *header2;    /* IN; header string for 2nd+ item */
ARTextStringList *value;      /* IN; value to print */

{
   unsigned int   i;       /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintChar(header2, value->stringList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARControlStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARControlStruct(
ARControlStruct *value        /* IN; value to print */
)

{
   DriverPrintResult("Control record:\n");
   PrintChar("   User name: ", value->user);
   PrintChar("   Password : ", value->password);
   PrintChar("   Locale   : ", value->localeInfo.locale);
   PrintChar("   Time zone: ", value->localeInfo.timeZone);
   PrintARTimestamp("   Operation time:", value->operationTime);
   PrintChar("   Server   : ", value->server);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARStatusStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusStruct(
ARStatusStruct *value         /* IN; value to print */
)

{
   DriverPrintResult("Status Struct :\n");
   DriverPrintResult("   Message type : ");
   switch (value->messageType)
   {
      case AR_RETURN_OK         :
         DriverPrintResult("NOTE\n");
         break;
      case AR_RETURN_WARNING    :
         DriverPrintResult("WARNING\n");
         break;
      case AR_RETURN_ERROR      :
         DriverPrintResult("ERROR\n");
         break;
      default                   :
         DriverPrintResult("<unknown type>\n");
         break;
   }
   PrintLong("   Message number : ", value->messageNum);
   PrintChar("   Message: ", value->messageText);
   if (value->appendedText && value->appendedText[0])
      PrintChar("   Appended: ", value->appendedText);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARStatusList                              */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusList(
ARStatusList *value           /* IN; value to print */
)

{
   unsigned int     i;         /* working index */
   ARStatusStruct  *tempPtr;   /* working pointer */

   DriverPrintResult("Status List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->statusList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARStatusStruct(tempPtr);
         tempPtr++;
      }
   }
   PrintAuxillaryStatus();
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARStatusStructIndent                         */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusStructIndent(
char           *indent,       /* IN; string of blanks for indenting */
ARStatusStruct *value         /* IN; value to print */
)

{
   DriverPrintResult("%sStatus Struct :\n", indent);
   DriverPrintResult("%s   Message type : ", indent);
   switch (value->messageType)
   {
      case AR_RETURN_OK         :
         DriverPrintResult("NOTE\n");
         break;
      case AR_RETURN_WARNING    :
         DriverPrintResult("WARNING\n");
         break;
      case AR_RETURN_ERROR      :
         DriverPrintResult("ERROR\n");
         break;
      default                   :
         DriverPrintResult("<unknown type>\n");
         break;
   }
   DriverPrintResult("%s", indent);
   PrintLong("   Message number : ", value->messageNum);
   DriverPrintResult("%s", indent);
   PrintChar("   Message: ", value->messageText);
   if (value->appendedText && value->appendedText[0])
   {
      DriverPrintResult("%s", indent);
      PrintChar("   Appended: ", value->appendedText);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARStatusListIndent                           */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusListIndent(
char         *indent,         /* IN; string of blanks for indenting */
ARStatusList *value           /* IN; value to print */
)

{
   unsigned int     i;         /* working index */
   ARStatusStruct  *tempPtr;   /* working pointer */

   DriverPrintResult("%sStatus List : %u items\n", indent, value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->statusList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARStatusStructIndent(indent, tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARStatusListList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusListList(
ARStatusListList *value       /* IN; value to print */
)

{
   unsigned int   i;         /* working index */
   ARStatusList  *tempPtr;   /* working pointer */

   DriverPrintResult("List of Status Lists : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->statusListList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARStatusListIndent("      ", tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARByteList                                */
/*                                                                           */
/*****************************************************************************/

void PrintARByteList(
char          *header,        /* IN; header string for the value */
char          *header2,       /* IN; header string for 2nd+ line */
ARByteList    *value          /* IN; value to print */
)

{
   unsigned int    i;
   int             length;
   unsigned char  *tempPtr;

   DriverPrintResult("%s  type - %lu with %u bytes", header, value->type,
                     value->numItems);
   length = (80 - strlen(header2)) / 2;
   if (value->numItems != 0)
   {  /* Print the stream of bytes */
      tempPtr = value->bytes;
      for (i = 0; i < value->numItems; i++)
      {
         if (((int) i % length) == 0)
            DriverPrintResult("\n%s", header2);
         DriverPrintResult("%02X", *tempPtr);
         tempPtr++;
      }
      DriverPrintResult("\n");
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARCoordList                               */
/*                                                                           */
/*****************************************************************************/

void PrintARCoordList(
char          *header,        /* IN; header string for the value */
char          *header2,       /* IN; header string for 2nd+ line */
ARCoordList   *value          /* IN; value to print */
)

{
   unsigned int    i;
   ARCoordStruct  *tempPtr;

   DriverPrintResult("%s %u items\n", header, value->numItems);
   if (value->numItems != 0)
   {
      /* Print the pairs of coordinates */
      tempPtr = value->coords;
      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintResult("%sx: %ld, y: %ld\n", header2, tempPtr->x,
                           tempPtr->y);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARAttachLocStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARAttachLocStruct(
char           *header,        /* IN; header string for the value */
ARLocStruct    *value          /* IN; value to print */
)

{
   DriverPrintResult("      %s :\n", header);

   if (value->locType == 0)
      DriverPrintResult("         Locator Type : (NULL)\n");
   else
   {
      DriverPrintResult("         Locator Type : %ld\n", value->locType);
      if (value->locType == AR_LOC_FILENAME)
         DriverPrintResult("            File Name    : %s\n",
                           value->u.filename);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARAttachProps                             */
/*                                                                           */
/*****************************************************************************/

void PrintARAttachProps(
char              *header,        /* IN; header string for the value */
ARAttachStruct    *value          /* IN; value to print */
)

{
   DriverPrintResult("%s\n", header);

   if (value->name[0] != '\0')
      DriverPrintResult("      Name            : %s\n", value->name);
   else
      DriverPrintResult("      Name            : <null>\n");

   DriverPrintResult("      Original size   : %ld\n", value->origSize);
   DriverPrintResult("      Compressed size : %ld\n", value->compSize);
   PrintARAttachLocStruct("Locator", &value->loc);
}


/*****************************************************************************/
/*                                                                           */
/*                              PrintARCurrency                              */
/*                                                                           */
/*****************************************************************************/

void PrintARCurrency(
char                *header,        /* IN; header string for the value */
ARCurrencyStruct    *value          /* IN; value to print */
)

{
   unsigned int   i;

   DriverPrintResult("%s\n", header);

   if (value->value[0] != '\0')
      DriverPrintResult("      Value          : %s\n", value->value);
   else
      DriverPrintResult("      Value          : <null>\n");

   DriverPrintResult("      Currency Code  : %s\n", value->currencyCode);
   if (!(gQuietMode & SUPPRESS_TIME))
      DriverPrintResult("      Conversion Date: %ld\n", value->conversionDate);
   else
      DriverPrintResult("      Conversion Date: **\n");

   DriverPrintResult("      Functional Currency List: %u items\n",
                     value->funcList.numItems);
   for (i = 0; i < value->funcList.numItems; i++)
   {
      DriverPrintResult("      Functional Currency Struct:\n");
      if (value->funcList.funcCurrencyList[i].value[0] != '\0')
         DriverPrintResult("         Value          : %s\n",
                           value->funcList.funcCurrencyList[i].value);
      else
         DriverPrintResult("         Value          : <null>\n");
      DriverPrintResult("         Currency Code  : %s\n",
                        value->funcList.funcCurrencyList[i].currencyCode);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARValueStruct                             */
/*                                                                           */
/*****************************************************************************/

void PrintARValueStruct(
char           *header,       /* IN; header string for the value */
ARValueStruct  *value         /* IN; value to print */
)

{
   char  buff[82];         /* buffer to hold second line header */

   DriverPrintResult("%sValue:  ", header);
   switch (value->dataType)
   {
      case AR_DATA_TYPE_NULL    :  /* NULL value (incl. no default) */
         DriverPrintResult("NULL\n");
         break;
      case AR_DATA_TYPE_KEYWORD :
         PrintUInt("(keyword)  ", value->u.keyNum);
         break;
      case AR_DATA_TYPE_INTEGER :
         PrintLong("(integer)  ", (int) value->u.intVal);
         break;
      case AR_DATA_TYPE_REAL    :
         PrintReal("(real)  ", value->u.realVal);
         break;
      case AR_DATA_TYPE_CHAR    :
         PrintChar("(char)  ", value->u.charVal);
         break;
      case AR_DATA_TYPE_DIARY   :
         APIPrintDiary("(diary)  ", value->u.diaryVal);
         break;
      case AR_DATA_TYPE_ENUM    :
         PrintLong("(enumerated)  ", (int) value->u.enumVal);
         break;
      case AR_DATA_TYPE_TIME    :
         PrintARTimestamp("(timestamp)  ", value->u.timeVal);
         break;
      case AR_DATA_TYPE_BITMASK :
         PrintLong("(bitmask)  ", (int) value->u.maskVal);
         break;
      case AR_DATA_TYPE_BYTES :
         sprintf(buff, "%s        ", header);
         PrintARByteList("(bytelist)  ", buff, value->u.byteListVal);
         break;
      case AR_DATA_TYPE_JOIN :
         DriverPrintResult("(join)\n");
         break;
      case AR_DATA_TYPE_TRIM :
         DriverPrintResult("(trim)\n");
         break;
      case AR_DATA_TYPE_CONTROL:
         DriverPrintResult("(control)\n");
         break;
      case AR_DATA_TYPE_ULONG :
         PrintULong("(ulong)  ", value->u.ulongVal);
         break;
      case AR_DATA_TYPE_COORDS:
         sprintf(buff, "%s        ", header);
         PrintARCoordList("(coordlist)  ", buff, value->u.coordListVal);
         break;
      case AR_DATA_TYPE_PAGE:
         DriverPrintResult("(Page)\n");
         break;
      case AR_DATA_TYPE_PAGE_HOLDER:
         DriverPrintResult("(PageHolder)\n");
         break;
      case AR_DATA_TYPE_ATTACH_POOL:
         DriverPrintResult("(AttachmentPool)\n");
         break;
      case AR_DATA_TYPE_ATTACH:
         PrintARAttachProps("(attachment)  ", value->u.attachVal);
         break;
      case AR_DATA_TYPE_DECIMAL :
         PrintChar("(decimal)  ", value->u.decimalVal);
         break;
      case AR_DATA_TYPE_CURRENCY :
         PrintARCurrency("(currency)    ", value->u.currencyVal);
         break;
      case AR_DATA_TYPE_VIEW :
         PrintChar("(view)  ", value->u.charVal);  
         break;
      case AR_DATA_TYPE_DISPLAY :
         PrintChar("(display)  ", value->u.charVal);
         break;
      case AR_DATA_TYPE_DATE :
         PrintARDate("(date)  ", value->u.dateVal);
         break;
      case AR_DATA_TYPE_TIME_OF_DAY    :
         PrintARTime("(timeofday)  ", value->u.timeOfDayVal);
         break;
      default                   :
         DriverPrintResult("<undefined data type tag = %u>\n",
                           value->dataType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARValueList                              */
/*                                                                           */
/*****************************************************************************/

void PrintARValueList(
char        *header,          /* IN; header string for the value */
ARValueList *value            /* IN; value to print */
)

{
   char            buff[45];  /* buffer to build nested header in */
   unsigned int    i;         /* working index */
   ARValueStruct  *tempPtr;   /* working pointer */

   DriverPrintResult("%sValue List : %u items\n", header,value->numItems);
   if (value->numItems != 0)
   {
      if (strlen(header) >= (size_t) 40)
         buff[0] = '\0';
      else
         sprintf(buff, "%s   ", header);

      tempPtr = value->valueList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARValueStruct(buff, tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARValueListList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARValueListList(
ARValueListList *value        /* IN; value to print */
)

{
   unsigned int    i;         /* working index */
   ARValueList    *tempPtr;   /* working pointer */

   DriverPrintResult("Value List List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->valueListList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARValueList("   ", tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintAREntryListFieldStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListFieldStruct(
AREntryListFieldStruct *value      /* IN; value to print */
)

{
   DriverPrintResult("   Entry List Field Struct:\n");
   PrintLong("      Field Id :", value->fieldId);
   PrintUInt("      Column Width:", value->columnWidth);
   PrintChar("      Separator:", value->separator);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintAREntryListFieldList                          */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListFieldList(
AREntryListFieldList *value   /* IN; value to print */
)

{
   unsigned int             i;       /* working index */
   AREntryListFieldStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Entry List Field List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->fieldsList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintAREntryListFieldStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARSortList                               */
/*                                                                           */
/*****************************************************************************/

void PrintARSortList(
ARSortList *value       /* IN; value to print */
)

{
   unsigned int   i;      /* working index */

   DriverPrintResult("Sort List : %u items\n", value->numItems);
   for (i = 0; i < value->numItems; i++)
   {
      DriverPrintResult("   Sort Struct:\n");
      PrintLong("      Field Id :", value->sortList[i].fieldId);
      switch (value->sortList[i].sortOrder)
      {
         case AR_SORT_ASCENDING  :
            DriverPrintResult("      Sort Order: ASCENDING\n");
            break;
         case AR_SORT_DESCENDING :
            DriverPrintResult("      Sort Order: DESCENDING\n");
            break;
         default              :
            DriverPrintResult("      Sort Order: <undefined tag value = %u>\n",
                              value->sortList[i].sortOrder);
            break;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintAREntryListStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListStruct(
AREntryListStruct *value      /* IN; value to print */
)

{
   unsigned int i;

   DriverPrintResult("Entry List Struct:\n");
   if (value != NULL && value->entryId.numItems > 0)
   {
      DriverPrintResult("   Entry Id : %s", value->entryId.entryIdList[0]);
      for (i = 1; i < value->entryId.numItems; i++)
         DriverPrintResult("|%s", value->entryId.entryIdList[i]);
      DriverPrintResult("\n");
      PrintChar("   Short Description :", value->shortDesc);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintAREntryListList                            */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListList(
AREntryListList *value        /* IN; value to print */
)

{
   unsigned int        i;       /* working index */
   AREntryListStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Entry List List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->entryList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintAREntryListStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFieldValueStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldValueStruct(
ARFieldValueStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("Field Value Struct:\n");
   PrintARInternalId("   Field Id :", value->fieldId);
   if ((value->fieldId == AR_CORE_STATUS_HISTORY) &&
       (value->value.dataType == AR_DATA_TYPE_CHAR))
      APIPrintStatusHistory("   Value: ", value->value.u.charVal);
   else
      PrintARValueStruct("   ", &value->value);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFieldValueList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldValueList(value)
ARFieldValueList *value;      /* IN; value to print */

{
   unsigned int         i;       /* working index */
   ARFieldValueStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Field Value List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->fieldValueList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARFieldValueStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFieldValueListList                         */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldValueListList(value)
ARFieldValueListList *value;      /* IN; value to print */

{
   unsigned int       i;       /* working index */
   ARFieldValueList  *tempPtr; /* working pointer */

   DriverPrintResult("Field Value List List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->valueListList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARFieldValueList(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                       PrintAREntryListFieldValueStruct                    */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListFieldValueStruct(value)
AREntryListFieldValueStruct *value;     /* IN; value to print */

{
   unsigned int i;

   DriverPrintResult("Entry List Struct:\n");
   if (value != NULL && value->entryId.numItems > 0)
   {
      DriverPrintResult("   Entry Id : %s", value->entryId.entryIdList[0]);
      for (i = 1; i < value->entryId.numItems; i++)
         DriverPrintResult("|%s", value->entryId.entryIdList[i]);
      DriverPrintResult("\n");
      DriverPrintResult("Entry Field/Value List:\n");
      PrintARFieldValueList(value->entryValues);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      PrintAREntryListFieldValueList                       */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryListFieldValueList(value)
AREntryListFieldValueList *value;       /* IN; value to print */

{
   unsigned int                  i;       /* working index */
   AREntryListFieldValueStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Entry List List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->entryList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintAREntryListFieldValueStruct(tempPtr);
         tempPtr++;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                       PrintAREntryListFieldValueStruct                    */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryBlockStruct(value)
AREntryBlockStruct *value;     /* IN; value to print */

{
   DriverPrintResult("Entry Block Struct:\n");
   if (value != NULL)
   {
      DriverPrintResult("   Block Size : %d\n", value->blockSize);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      PrintAREntryBlockList                                */
/*                                                                           */
/*****************************************************************************/

void PrintAREntryBlockList(value)
AREntryBlockList *value;       /* IN; value to print */

{
   unsigned int                  i;       /* working index */
   AREntryBlockStruct            *tempPtr; /* working pointer */
   unsigned int totalCompressedSize = 0;

   DriverPrintResult("Entry Block List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->entryBlockList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintAREntryBlockStruct(tempPtr);
         totalCompressedSize += tempPtr->blockSize;
         tempPtr++;
      }
      DriverPrintResult("Total compressed size : %u bytes\n", totalCompressedSize);

   }
}

/*****************************************************************************/
/*                                                                           */
/*                      PrintARFieldValueOrArithStruct                       */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldValueOrArithStruct(header, value)
char                      *header;/* IN; header string for the value */
ARFieldValueOrArithStruct *value; /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */
   char   buff2[65];          /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sField, Value, or Arithmetic Struct:\n", header);
   switch (value->tag)
   {
      case AR_FIELD        :
         DriverPrintResult("%s   Tag: FIELD\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.fieldId);
         break;
      case AR_VALUE        :
         DriverPrintResult("%s   Tag: VALUE\n", header);
         PrintARValueStruct(buff, &value->u.value);
         break;
      case AR_ARITHMETIC   :
         DriverPrintResult("%s   Tag: ARITHMETIC\n", header);
         PrintARArithOpStruct(buff, (ARArithOpStruct *) value->u.arithOp);
         break;
      case AR_STAT_HISTORY :
         DriverPrintResult("%s   Tag: STAT HISTORY\n", header);
         sprintf(buff2, "%s   Enumerated value index :", header);
         PrintULong(buff2, value->u.statHistory.enumVal);
         switch (value->u.statHistory.userOrTime)
         {
            case AR_STAT_HISTORY_USER :
               DriverPrintResult("%s   User or Time: USER\n", header);
               break;
            case AR_STAT_HISTORY_TIME :
               DriverPrintResult("%s   User or Time: TIME\n", header);
               break;
            default                   :
               DriverPrintResult("%s   User or Time: %s%u>\n", header,
                                 "<undefined value = ",
                                 value->u.statHistory.userOrTime);
               break;
         }
         break;
      case AR_VALUE_SET    :
         DriverPrintResult("%s   Tag: VALUE SET\n", header);
         PrintARValueList(buff, &value->u.valueSet);
         break;
      case AR_CURRENCY_FLD :
         DriverPrintResult("%s   Tag: CURRENCY FIELD\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.currencyField->fieldId);
         sprintf(buff2, "%s   Currency part tag:", header);
         PrintUInt(buff2, value->u.currencyField->partTag);
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            DriverPrintResult("%s   Currency code: %s\n", header,
                              value->u.currencyField->currencyCode);
         break;
      case AR_FIELD_TRAN   :
         DriverPrintResult("%s   Tag: FIELD TRAN\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.fieldId);
         break;
      case AR_FIELD_DB     :
         DriverPrintResult("%s   Tag: FIELD DB\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.fieldId);
         break;
      case AR_LOCAL_VARIABLE :
         DriverPrintResult("%s   Tag: LOCAL VARIABLE\n", header);
         sprintf(buff2, "%s   Local Variable number:", header);
         PrintUInt(buff2, value->u.variable);
         break;
      case AR_QUERY        :
         DriverPrintResult("%s   Tag: QUERY\n", header);
         sprintf(buff2, "%s   Schema :", header);
         PrintChar(buff2, value->u.queryValue->schema);
         sprintf(buff2, "%s   Server :", header);
         PrintChar(buff2, value->u.queryValue->server);
         PrintARQualifierStruct(buff, value->u.queryValue->qualifier);
         PrintLong(buff2, (int) value->u.queryValue->valueField);
         DriverPrintResult("%s   Multi Value Code:", header);
         switch (value->u.queryValue->multiMatchCode)
         {
            case AR_QUERY_VALUE_MULTI_ERROR :
               DriverPrintResult("ERROR\n");
               break;
            case AR_QUERY_VALUE_MULTI_FIRST :
               DriverPrintResult("FIRST\n");
               break;
            case AR_QUERY_VALUE_MULTI_SET   :
               DriverPrintResult("SET\n");
               break;
            default                         :
               DriverPrintResult("<undefined multi-match value = %u>\n",
                                 value->u.queryValue->multiMatchCode);
               break;
         }
         break;
      case AR_CURRENCY_FLD_TRAN :
         DriverPrintResult("%s   Tag: CURRENCY FIELD TRAN\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.currencyField->fieldId);
         sprintf(buff2, "%s   Currency part tag:", header);
         PrintUInt(buff2, value->u.currencyField->partTag);
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            DriverPrintResult("%s   Currency code: %s\n", header,
                              value->u.currencyField->currencyCode);
         break;
      case AR_CURRENCY_FLD_DB :
         DriverPrintResult("%s   Tag: CURRENCY FIELD DB\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.currencyField->fieldId);
         sprintf(buff2, "%s   Currency part tag:", header);
         PrintUInt(buff2, value->u.currencyField->partTag);
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            DriverPrintResult("%s   Currency code: %s\n", header,
                              value->u.currencyField->currencyCode);
         break;
      case AR_CURRENCY_FLD_CURRENT :
         DriverPrintResult("%s   Tag: CURRENCY FIELD (current screen)\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.currencyField->fieldId);
         sprintf(buff2, "%s   Currency part tag:", header);
         PrintUInt(buff2, value->u.currencyField->partTag);
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            DriverPrintResult("%s   Currency code: %s\n", header,
                              value->u.currencyField->currencyCode);
         break;
      case AR_FIELD_CURRENT:
         DriverPrintResult("%s   Tag: FIELD (current screen)\n", header);
         sprintf(buff2, "%s   Field Id :", header);
         PrintARInternalId(buff2, value->u.fieldId);
         break;
      default              :
         DriverPrintResult("%s   Tag: <undefined tag value = %u>\n", header,
                           value->tag);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARArithOpStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARArithOpStruct(header, value)
char            *header;      /* IN; header string for the value */
ARArithOpStruct *value;       /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sArithmetic Operation:\n", header);
   switch (value->operation)
   {
      case AR_ARITH_OP_ADD      :
         DriverPrintResult("%s   Operation: ADD\n", header);
         break;
      case AR_ARITH_OP_SUBTRACT :
         DriverPrintResult("%s   Operation: SUBTRACT\n", header);
         break;
      case AR_ARITH_OP_MULTIPLY :
         DriverPrintResult("%s   Operation: MULTIPLY\n", header);
         break;
      case AR_ARITH_OP_DIVIDE   :
         DriverPrintResult("%s   Operation: DIVIDE\n", header);
         break;
      case AR_ARITH_OP_MODULO   :
         DriverPrintResult("%s   Operation: MODULO\n", header);
         break;
      case AR_ARITH_OP_NEGATE   :
         DriverPrintResult("%s   Operation: NEGATE (unary minus)\n", header);
         break;
      default                   :
         DriverPrintResult("%s   Operation: <undefined operation = %u>\n",
                           header, value->operation);
         break;
   }
   if (value->operation != AR_ARITH_OP_NEGATE)
      PrintARFieldValueOrArithStruct(buff, &value->operandLeft);
   PrintARFieldValueOrArithStruct(buff, &value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARRelOpStruct                             */
/*                                                                           */
/*****************************************************************************/

void PrintARRelOpStruct(header, value)
char          *header;        /* IN; header string for the value */
ARRelOpStruct *value;         /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sRelational Operation:\n", header);
   switch (value->operation)
   {
      case AR_REL_OP_EQUAL         :
         DriverPrintResult("%s   Operation: EQUAL\n", header);
         break;
      case AR_REL_OP_GREATER       :
         DriverPrintResult("%s   Operation: GREATER\n", header);
         break;
      case AR_REL_OP_GREATER_EQUAL :
         DriverPrintResult("%s   Operation: GREATER_EQUAL\n", header);
         break;
      case AR_REL_OP_LESS          :
         DriverPrintResult("%s   Operation: LESS\n", header);
         break;
      case AR_REL_OP_LESS_EQUAL    :
         DriverPrintResult("%s   Operation: LESS_EQUAL\n", header);
         break;
      case AR_REL_OP_NOT_EQUAL     :
         DriverPrintResult("%s   Operation: NOT_EQUAL\n", header);
         break;
      case AR_REL_OP_LIKE          :
         DriverPrintResult("%s   Operation: LIKE\n", header);
         break;
      case AR_REL_OP_IN            :
         DriverPrintResult("%s   Operation: IN\n", header);
         break;
      default                      :
         DriverPrintResult("%s   Operation: <undefined operation = %u>\n",
                           header, value->operation);
         break;
   }
   PrintARFieldValueOrArithStruct(buff, &value->operandLeft);
   PrintARFieldValueOrArithStruct(buff, &value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARQualifierStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARQualifierStruct(header, value)
char              *header;    /* IN; header string for the value */
ARQualifierStruct *value;     /* IN; value to print */

{
   char   buff[255];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sQualifier Struct:\n", header);
   switch (value->operation)
   {
      case AR_COND_OP_NONE   :
         /* is a dummy with no value */
         DriverPrintResult("%s   Operation: <none>\n", header);
         break;
      case AR_COND_OP_AND    :
         DriverPrintResult("%s   Operation: AND\n", header);
         PrintARQualifierStruct(buff, value->u.andor.operandLeft);
         PrintARQualifierStruct(buff, value->u.andor.operandRight);
         break;
      case AR_COND_OP_OR     :
         DriverPrintResult("%s   Operation: OR\n", header);
         PrintARQualifierStruct(buff, value->u.andor.operandLeft);
         PrintARQualifierStruct(buff, value->u.andor.operandRight);
         break;
      case AR_COND_OP_NOT    :
         DriverPrintResult("%s   Operation: NOT\n", header);
         PrintARQualifierStruct(buff, value->u.not);
         break;
      case AR_COND_OP_REL_OP :
         DriverPrintResult("%s   Operation: RELATIONAL OPERATION\n", header);
         PrintARRelOpStruct(buff, value->u.relOp);
         break;
      case AR_COND_OP_FROM_FIELD:
         DriverPrintResult("%s   Operation: EXTERNAL OPERATION\n", header);
         sprintf(buff, "%s   Field Id :", header);
         PrintARInternalId(buff, value->u.fieldId);
         break;
      default             :
         DriverPrintResult("%s   Operation: <undefined operation = %u>\n",
                           header, value->operation);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARQualifierList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARQualifierList(
char            *header,          /* IN; header string for the value */
ARQualifierList *value            /* IN; value to print */
)

{
   char                buff[45];  /* buffer to build nested header in */
   unsigned int        i;         /* working index */
   ARQualifierStruct  *tempPtr;   /* working pointer */

   DriverPrintResult("%sQualifier List : %u items\n", header,value->numItems);
   if (value->numItems != 0)
   {
      if (strlen(header) >= (size_t) 40)
         buff[0] = '\0';
      else
         sprintf(buff, "%s   ", header);

      tempPtr = value->qualifierList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARQualifierStruct(buff, tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARIndexStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARIndexStruct(
ARIndexStruct   *value        /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("Index Struct:\n");
   PrintUInt("   Number of Index Fields:", value->numFields);
   for (i = 0; i < value->numFields; i++)
      PrintLong("   Field Id:", value->fieldIds[i]);
   PrintARBoolean("   Unique flag:", value->unique);
}


/*****************************************************************************/
/*                                                                           */
/*                              PrintARIndexList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARIndexList(
ARIndexList   *value          /* IN; value to print */
)

{
   unsigned int    i;        /* working index */
   ARIndexStruct  *tempPtr;  /* working pointer */

   DriverPrintResult("Index List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->indexList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARIndexStruct(tempPtr);
         tempPtr++;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                              PrintARArchiveInfo                           */
/*                                                                           */
/*****************************************************************************/

void PrintARArchiveInfo(
ARArchiveInfoStruct   *value          /* IN; value to print */
)

{
   DriverPrintResult("Archive Struct:\n");
   PrintUInt("   Enable:", value->enable);
   if (value->archiveType != 0)
   {
      PrintUInt("   Type:", value->archiveType);
      if ((value->archiveType & AR_ARCHIVE_FORM) == AR_ARCHIVE_FORM)
         PrintChar("   Form:", value->u.formName);
      else
      if (((value->archiveType & AR_ARCHIVE_FILE_XML) == AR_ARCHIVE_FILE_XML) ||
         ((value->archiveType & AR_ARCHIVE_FILE_ARX) == AR_ARCHIVE_FILE_ARX))
         PrintChar("   Dir Path:", value->u.dirPath);
      PrintUInt("   Hour:", value->archiveTime.hourmask);
      PrintUInt("   Minute:", value->archiveTime.minute);
      PrintUInt("   Monthday:", value->archiveTime.monthday);
      PrintUInt("   Weekday:", value->archiveTime.weekday);
      PrintARQualifierStruct("  ", &value->query);
   }
   PrintChar("   ArchiveFrom:", value->archiveFrom);

}

/*****************************************************************************/
/*                                                                           */
/*                              PrintARAuditInfo                             */
/*                                                                           */
/*****************************************************************************/

void PrintARAuditInfo(
ARAuditInfoStruct   *value          /* IN; value to print */
)

{
   if (value == NULL)
      return;
   DriverPrintResult("Audit Struct:\n");
   PrintUInt("   Enable:", value->enable);
   if (value->style != 0)
   {
      PrintUInt("   Style:", value->style);
      PrintChar("   Form:", value->formName);
      PrintARQualifierStruct("  ", &value->query);
      PrintUInt("   Audit Mask:",value->auditMask);
   }
   else
      PrintChar("   Form:", value->formName);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCompoundSchema                             */
/*                                                                           */
/*****************************************************************************/

void PrintARCompoundSchema(value)
ARCompoundSchema   *value;         /* IN; value to print */

{
   if (value == NULL)
      return;

   DriverPrintResult("Compound Schema Info:  ");
   switch (value->schemaType)
   {
      case AR_SCHEMA_NONE :
         DriverPrintResult("<none>\n");
         break;
      case AR_SCHEMA_REGULAR :
         DriverPrintResult("REGULAR\n");
         break;
      case AR_SCHEMA_DIALOG :
         DriverPrintResult("DIALOG\n");
         break;
      case AR_SCHEMA_JOIN :
         DriverPrintResult("JOIN\n");
         DriverPrintResult("  Join member A %s \n", value->u.join.memberA);
         DriverPrintResult("  Join member B %s \n", value->u.join.memberB);
         PrintARQualifierStruct("  Join qualification",
                                &value->u.join.joinQual);
         DriverPrintResult("  Join option %d \n", value->u.join.option);
         break;
      case AR_SCHEMA_VIEW :
         DriverPrintResult("VIEW\n");
         DriverPrintResult("  View table name %s \n",
                           value->u.view.tableName);
         break;
      case AR_SCHEMA_VENDOR :
         DriverPrintResult("VENDOR\n");
         DriverPrintResult("  Vendor name %s \n",
                           value->u.vendor.vendorName);
         DriverPrintResult("  Vendor table name %s \n",
                           value->u.vendor.tableName);
         break;
      default             :
         DriverPrintResult("  <undefined schemaType = %u>\n",
                           value->schemaType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCompoundSchemaList                         */
/*                                                                           */
/*****************************************************************************/
void PrintARCompoundSchemaList(
ARCompoundSchemaList   *value          /* IN; value to print */
)

{
   unsigned int i;

   DriverPrintResult("Compound Schema List : %u items\n", value->numItems);
   for (i = 0;i < value->numItems; i++)
      PrintARCompoundSchema(&value->compoundSchema[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARStatisticsResultStruct                      */
/*                                                                           */
/*****************************************************************************/

void PrintARStatisticsResultStruct(header, value)
char                     *header;  /* IN; header string for the value */
ARStatisticsResultStruct *value;   /* IN; value to print */

{
   char           buff[55];    /* buffer to build nested header in */
   unsigned int   i;           /* working index */

   if (value->groupByValues.numItems != 0)
   {
      if (strlen(header) >= (size_t) 40)
         strcpy(buff, "Grouped by ");
      else
         sprintf(buff, "%sGrouped by ", header);

      for (i = 0; i < value->groupByValues.numItems; i++)
      {
         PrintARValueStruct(buff, &value->groupByValues.valueList[i]);

         if (i == 0)
         {
            if (strlen(header) >= (size_t) 40)
               strcpy(buff, " then by ");
            else
               sprintf(buff, "%s then by ", header);
         }
      }
   }

   if (strlen(header) >= (size_t) 40)
      strcpy(buff, "   Stat ");
   else
      sprintf(buff, "%s   Stat ", header);
   PrintARValueStruct(buff, &value->result);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARStatisticsResultList                        */
/*                                                                           */
/*****************************************************************************/

void PrintARStatisticsResultList(header, value)
char                   *header;  /* IN; header string for the value */
ARStatisticsResultList *value;   /* IN; value to print */

{
   char                       buff[45];  /* buffer to build nested header in */
   unsigned int               i;         /* working index */
   ARStatisticsResultStruct  *tempPtr;   /* working pointer */

   DriverPrintResult("%sStatistics Result List : %u items\n", header,
                     value->numItems);
   if (value->numItems != 0)
   {
      if (strlen(header) >= (size_t) 40)
         buff[0] = '\0';
      else
         sprintf(buff, "%s   ", header);

      tempPtr = value->resultList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARStatisticsResultStruct(buff, tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARObjectPropList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARObjectPropList(header, prefix, value)
char       *header;      /* IN; header string for the value */
char       *prefix;      /* IN; prefix to Prop list on first line */
ARPropList *value;       /* IN; value to print */

{
   char           buff[45];    /* buffer to build nested header in */
   unsigned int   i;           /* working index */
   ARPropStruct  *tempPtr;     /* working pointer */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%s%sProp List : %u items\n", header, prefix,
                     value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->props;
      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintResult("%sProp Struct:  ", header);
         switch (tempPtr->prop)
         {
            case AR_OPROP_VENDOR_NAME     :
               DriverPrintResult("VENDOR NAME\n");
               break;
            case AR_OPROP_VENDOR_PRODUCT  :
               DriverPrintResult("VENDOR PRODUCT\n");
               break;
            case AR_OPROP_VENDOR_VERSION     :
               DriverPrintResult("VENDOR VERSION\n");
               break;
            case AR_OPROP_GUID               :
               DriverPrintResult("GUID\n");
               break;
            case AR_OPROP_COPYRIGHT          :
               DriverPrintResult("COPYRIGHT\n");
               break;
            case AR_OPROP_SCC_LOCKED_BY      :
               DriverPrintResult("SCC LOCKED BY\n");
               break;
            case AR_OPROP_SCC_VERSION        :
               DriverPrintResult("SCC VERSION\n");
               break;
            case AR_OPROP_SCC_TIMESTAMP      :
               DriverPrintResult("SCC TIMESTAMP\n");
               break;
            case AR_OPROP_SCC_USER           :
               DriverPrintResult("SCC USER\n");
               break;
            case AR_OPROP_SCC_LOCATION       :
               DriverPrintResult("SCC LOCATION\n");
               break;
            case AR_OPROP_SCC_DATA_LOCKED_BY :
               DriverPrintResult("SCC DATA LOCKED BY\n");
               break;
            case AR_OPROP_SCC_DATA_VERSION   :
               DriverPrintResult("SCC DATA VERSION\n");
               break;
            case AR_OPROP_SCC_DATA_TIMESTAMP :
               DriverPrintResult("SCC DATA TIMESTAMP\n");
               break;
            case AR_OPROP_SCC_DATA_USER      :
               DriverPrintResult("SCC DATA USER\n");
               break;
            case AR_OPROP_SCC_DATA_LOCATION  :
               DriverPrintResult("SCC DATA LOCATION\n");
               break;
            case AR_OPROP_WINDOW_OPEN_IF_SAMPLE_SERVER_SCHEMA:
               DriverPrintResult("WINDOW OPEN IF SAMPLE SERVER SCHEMA\n");
               break;
            case AR_OPROP_WINDOW_OPEN_ELSE_SAMPLE_SERVER_SCHEMA:
               DriverPrintResult("WINDOW OPEN ELSE SAMPLE SERVER SCHEMA\n");
               break;
            case AR_OPROP_FORM_NAME_WEB_ALIAS:
               DriverPrintResult("FORM NAME WEB ALIAS\n");
               break;
            case AR_OPROP_VIEW_LABEL_WEB_ALIAS:
               DriverPrintResult("VIEW LABEL WEB ALIAS\n");
               break;
            case AR_OPROP_APP_WEB_ALIAS      :
               DriverPrintResult("APPLICATION WEB ALIAS\n");
               break;
            case AR_OPROP_INTERVAL_VALUE: 
               DriverPrintResult("ACTIVE LINK TIME INTERVAL\n");
               break;
            case AR_OPROP_INTEGRITY_KEY: 
               DriverPrintResult("SCHEMA INTEGRITY KEY\n");
               break;
            case AR_OPROP_GUIDE_PARAMETERS: 
               DriverPrintResult("GUIDE PARAMETERS\n");
               break;
            case AR_OPROP_NEXT_ID_BLOCK_SIZE: 
               DriverPrintResult("NEXT ID BLOCK SIZE\n");
               break;
            case AR_OPROP_POOL_NUMBER: 
               DriverPrintResult("POOL NUMBER\n");
               break;
            case AR_OPROP_CORE_FIELDS_OPTION_MASK: 
               DriverPrintResult("CORE FIELDS OPTION MASK\n");
               break;
            case AR_OPROP_MAX_VENDOR_TEMP_TABLES:
               DriverPrintResult("MAX VENDOR TEMP TABLES\n");
               break;
            case AR_SMOPROP_OBJECT_VERSION:
               DriverPrintResult("OBJECT VERSION\n");
               break;
            case AR_SMOPROP_APP_OWNER:
               DriverPrintResult("APPLICATION OWNER\n");
               break;
            case AR_SMOPROP_OBJECT_LOCK_TYPE:
               DriverPrintResult("OBJECT LOCK TYPE\n");
               break;
            case AR_SMOPROP_OBJECT_LOCK_KEY:
               DriverPrintResult("OBJECT LOCK KEY\n");
               break;
            case AR_SMOPROP_ENTRYPOINT_DEFAULT_NEW_ORDER:
               DriverPrintResult("ENTRYPOINT DEFAULT NEW ORDER\n");
               break;
            case AR_SMOPROP_ENTRYPOINT_DEFAULT_SEARCH_ORDER:
               DriverPrintResult("ENTRYPOINT DEFAULT SEARCH ORDER\n");
               break;
            case AR_SMOPROP_NO_APP_STATS_LOGGING:
               DriverPrintResult("APPLICATION STATISTICS FORM\n");
               break;
            case AR_SMOPROP_APP_LIC_VERSION:
               DriverPrintResult("APPLICATION LICENSE VENDOR\n");
               break;
            case AR_SMOPROP_APP_LIC_DESCRIPTOR:
               DriverPrintResult("APPLICATION LICENSE VENDOR DESC\n");
               break;
            case AR_SMOPROP_APP_LIC_USER_LICENSABLE:
               DriverPrintResult("APPLICATION LICENSE USER LICENSABLE\n");
               break;
            case AR_SMOPROP_APP_ACCESS_POINT:
               DriverPrintResult("APPLICATION ACCESS POINT\n");
               break;
            case AR_SMOPROP_APP_BSM_TAG:
               DriverPrintResult("APPLICATION BSM FORM\n");
               break;
            case AR_OPROP_STATIC_PERMISSION_INHERITED:
               DriverPrintResult("STATIC PERMISSION INHERITED\n");
               break;
            case AR_OPROP_DYNAMIC_PERMISSION_INHERITED:
               DriverPrintResult("DYNAMIC PERMISSION INHERITED\n");
               break;
            case AR_OPROP_MFS_OPTION_MASK: 
               DriverPrintResult("MULTI-FORM SEARCH OPTION MASK\n");
               break;
            case AR_OPROP_MFS_WEIGHTED_RELEVANCY_FIELDS: 
               DriverPrintResult("MULTI-FORM SEARCH WEIGHTED RELEVANCY FIELDS\n");
               break;
            case AR_SMOPROP_FILTER_GET_DATA_AS_USER: 
               DriverPrintResult("FILTER GET DATA AS USER\n");
               break;
            case AR_OPROP_FT_MFS_INDEX_TABLE_FIELD: 
               DriverPrintResult("MULTI-FORM SEARCH TABLE FIELD INDEXED\n");
               break;
            case AR_SMOPROP_OVERLAY_GROUP: 
               DriverPrintResult("OVERLAY GROUP\n");
               break;
            case AR_SMOPROP_OVERLAY_PROPERTY: 
               DriverPrintResult("OVERLAY PROPERTY\n");
               break;
            case AR_OPROP_FT_FILTER_SEARCH: 
               DriverPrintResult("FILTER USE FTS\n");
               break;
            case AR_OPROP_LOCALIZE_FORM_VIEWS: 
               DriverPrintResult("LOCALIZE FORM VIEWS\n");
               break;
            case AR_OPROP_DRILL_DOWN_IN_WEB_REPORTS: 
               DriverPrintResult("DRILL DOWN IN WEB REPORTS\n");
               break;
            default:
               DriverPrintResult("   <undefined property = %u>\n", tempPtr->prop);
               break;
         }
         PrintARValueStruct(buff, &tempPtr->value);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARDisplayPropList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARDisplayPropList(header, prefix, value)
char       *header;      /* IN; header string for the value */
char       *prefix;      /* IN; prefix to Prop list on first line */
ARPropList *value;       /* IN; value to print */

{
   char           buff[45];    /* buffer to build nested header in */
   unsigned int   i;           /* working index */
   ARPropStruct  *tempPtr;     /* working pointer */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%s%sProp List : %u items\n", header, prefix, value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->props;
      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintResult("%sProp Struct:  ", header);
         switch (tempPtr->prop)
         {
            case AR_DPROP_TRIM_TYPE       :
               DriverPrintResult("TRIM TYPE\n");
               break;
            case AR_DPROP_CNTL_TYPE       :
               DriverPrintResult("CNTL TYPE\n");
               break;
            case AR_DPROP_BBOX            :
               DriverPrintResult("BBOX\n");
               break;
            case AR_DPROP_VISIBLE         :
               DriverPrintResult("VISIBLE\n");
               break;
            case AR_DPROP_ENABLE          :
               DriverPrintResult("ENABLE\n");
               break;
            case AR_DPROP_HELP            :
               DriverPrintResult("HELP\n");
               break;
            case AR_DPROP_Z_ORDER         :
               DriverPrintResult("Z ORDER\n");
               break;
            case AR_DPROP_COLOR_FILL      :
               DriverPrintResult("COLOR FILL\n");
               break;
            case AR_DPROP_DEPTH_EFFECT    :
               DriverPrintResult("DEPTH EFFECT\n");
               break;
            case AR_DPROP_DEPTH_AMOUNT    :
               DriverPrintResult("DEPTH AMOUNT\n");
               break;
            case AR_DPROP_COLOR_LINE      :
               DriverPrintResult("COLOR LINE\n");
               break;
            case AR_DPROP_COLOR_TEXT      :
               DriverPrintResult("COLOR TEXT\n");
               break;
            case AR_DPROP_PROMPT          :
               DriverPrintResult("PROMPT\n");
               break;
            case AR_DPROP_LABEL           :
               DriverPrintResult("LABEL\n");
               break;
            case AR_DPROP_LABEL_BBOX      :
               DriverPrintResult("LABEL BBOX\n");
               break;
            case AR_DPROP_LABEL_FONT_STYLE:
               DriverPrintResult("LABEL FONT STYLE\n");
               break;
            case AR_DPROP_LABEL_FONT_SIZE :
               DriverPrintResult("LABEL FONT SIZE\n");
               break;
            case AR_DPROP_LABEL_COLOR_TEXT:
               DriverPrintResult("LABEL COLOR TEXT\n");
               break;
            case AR_DPROP_LABEL_JUSTIFY   :
               DriverPrintResult("LABEL JUSTIFY\n");
               break;
            case AR_DPROP_LABEL_ALIGN     :
               DriverPrintResult("LABEL ALIGN\n");
               break;
            case AR_DPROP_LABEL_POS_SECTOR :
               DriverPrintResult("LABEL POS SECTOR\n");
               break;
            case AR_DPROP_LABEL_POS_JUSTIFY:
               DriverPrintResult("LABEL POS JUSTIFY\n");
               break;
            case AR_DPROP_LABEL_POS_ALIGN :
               DriverPrintResult("LABEL POS ALIGN\n");
               break;
            case AR_DPROP_LABEL_COLOR_FILL:
               DriverPrintResult("LABEL COLOR FILL\n");
               break;
            case AR_DPROP_LABEL_COLOR_LINE:
               DriverPrintResult("LABEL COLOR LINE\n");
               break;
            case AR_DPROP_COORDS          :
               DriverPrintResult("COORDS\n");
               break;
            case AR_DPROP_LINE_WIDTH      :
               DriverPrintResult("LINE WIDTH\n");
               break;
            case AR_DPROP_LINE_PATTERN    :
               DriverPrintResult("LINE PATTERN\n");
               break;
            case AR_DPROP_JOINT_STYLE     :
               DriverPrintResult("JOINT STYLE\n");
               break;
            case AR_DPROP_ENDCAP_START    :
               DriverPrintResult("ENDCAP START\n");
               break;
            case AR_DPROP_ENDCAP_END      :
               DriverPrintResult("ENDCAP END\n");
               break;
            case AR_DPROP_DATA_ROWS       :
               DriverPrintResult("DATA ROWS\n");
               break;
            case AR_DPROP_DATA_COLS       :
               DriverPrintResult("DATA COLS\n");
               break;
            case AR_DPROP_DATA_SPIN       :
               DriverPrintResult("DATA SPIN\n");
               break;
            case AR_DPROP_DATA_MENU       :
               DriverPrintResult("DATA MENU\n");
               break;
            case AR_DPROP_DATA_RADIO      :
               DriverPrintResult("DATA RADIO\n");
               break;
            case AR_DPROP_DATA_MENU_BBOX  :
               DriverPrintResult("MENU BBOX\n");
               break;
            case AR_DPROP_DATA_EXPAND_BBOX:
               DriverPrintResult("EXPAND BBOX\n");
               break;
            case AR_DPROP_TEXT            :
               DriverPrintResult("TEXT\n");
               break;
            case AR_DPROP_TEXT_FONT_STYLE :
               DriverPrintResult("TEXT FONT STYLE\n");
               break;
            case AR_DPROP_TEXT_FONT_SIZE  :
               DriverPrintResult("TEXT FONT SIZE\n");
               break;
            case AR_DPROP_JUSTIFY         :
               DriverPrintResult("JUSTIFY\n");
               break;
            case AR_DPROP_ALIGN           :
               DriverPrintResult("ALIGN\n");
               break;
            case AR_DPROP_IMAGE           :
               DriverPrintResult("IMAGE\n");
               break;
            case AR_DPROP_PUSH_BUTTON_IMAGE:
               DriverPrintResult("PUSH BUTTON IMAGE\n");
               break;
            case AR_DPROP_BUTTON_TEXT     :
               DriverPrintResult("BUTTON TEXT\n");
               break;
            case AR_DPROP_BUTTON_2D       :
               DriverPrintResult("BUTTON 2D\n");
               break;
            case AR_DPROP_BUTTON_IMAGE_POSITION :
               DriverPrintResult("BUTTON IMAGE POSITION\n");
               break;
            case AR_DPROP_BUTTON_SCALE_IMAGE :
               DriverPrintResult("BUTTON SCALE IMAGE\n");
               break;
            case AR_DPROP_BUTTON_MAINTAIN_RATIO :
               DriverPrintResult("BUTTON MAINTAIN RATIO\n");
               break;
            case AR_DPROP_MENU_TEXT       :
               DriverPrintResult("MENU TEXT\n");
               break;
            case AR_DPROP_MENU_POS        :
               DriverPrintResult("MENU POS\n");
               break;
            case AR_DPROP_MENU_MODE       :
               DriverPrintResult("MENU MODE\n");
               break;
            case AR_DPROP_MENU_PARENT     :
               DriverPrintResult("MENU PARENT\n");
               break;
            case AR_DPROP_MENU_HELP       :
               DriverPrintResult("MENU HELP\n");
               break;
            case AR_DPROP_TOOLTIP         :
               DriverPrintResult("TOOLTIP\n");
               break;
            case AR_DPROP_TOOLBAR_POS     :
               DriverPrintResult("TOOLBAR POS\n");
               break;
            case AR_DPROP_TOOLBAR_MODE    :
               DriverPrintResult("TOOLBAR MODE\n");
               break;
            case AR_DPROP_TOOLBAR_TEXT    :
               DriverPrintResult("TOOLBAR TEXT\n");
               break;
            case AR_DPROP_TAB_MODE        :
               DriverPrintResult("TAB MODE\n");
               break;
            case AR_DPROP_TAB_COORD       :
               DriverPrintResult("TAB COORD\n");
               break;
            case AR_DPROP_TAB_TEXT        :
               DriverPrintResult("TAB TEXT\n");
               break;
            case AR_DPROP_TAB_ORDER       :
               DriverPrintResult("TAB ORDER\n");
               break;
            case AR_DPROP_DATETIME_POPUP  :
               DriverPrintResult("DATETIME POPUP\n");
               break;
            case AR_DPROP_BACKGROUND_MODE :
               DriverPrintResult("BACKGROUND MODE\n");
               break;
            case AR_DPROP_TAB_NEXT        :
               DriverPrintResult("TAB NEXT\n");
               break;
            case AR_DPROP_DATA_BBOX       :
               DriverPrintResult("DATA BBOX\n");
               break;
            case AR_DPROP_VIEW_GRID_BBOX  :
               DriverPrintResult("VIEW GRID BBOX\n");
               break;
            case AR_DPROP_VUI_DEFAULT     :
               DriverPrintResult("VUI DEFAULT\n");
               break;
            case AR_DPROP_PANE_LAYOUT     :
               DriverPrintResult("PANE LAYOUT\n");
               break;
            case AR_DPROP_DETAIL_PANE_VISIBILITY :
               DriverPrintResult("DETAIL PANE VISIBILITY\n");
               break;
            case AR_DPROP_PROMPT_PANE_VISIBILITY :
               DriverPrintResult("PROMPT PANE VISIBILITY\n");
               break;
            case AR_DPROP_RESULT_PANE_VISIBILITY :
               DriverPrintResult("RESULT PANE VISIBILITY\n");
               break;
            case AR_DPROP_DETAIL_PANE_COLOR :
               DriverPrintResult("DETAIL PANE COLOR\n");
               break;
            case AR_DPROP_DETAIL_PANE_IMAGE :
               DriverPrintResult("DETAIL PANE IMAGE\n");
               break;
            case AR_DPROP_IMAGE_ALIGN     :
               DriverPrintResult("IMAGE ALIGN\n");
               break;
            case AR_DPROP_IMAGE_JUSTIFY   :
               DriverPrintResult("IMAGE JUSTIFY\n");
               break;
            case AR_DPROP_DISPLAY_PARENT  :
               DriverPrintResult("DISPLAY PARENT\n");
               break;
            case AR_DPROP_PAGE_ORDER      :
               DriverPrintResult("PAGE ORDER\n");
               break;
            case AR_DPROP_PAGE_LABEL_DISPLAY :
               DriverPrintResult("PAGE LABEL DISPLAY\n");
               break;
            case AR_DPROP_PAGE_ARRANGEMENT:
               DriverPrintResult("PAGE ARRANGEMENT\n");
               break;
            case AR_DPROP_DEFAULT_PAGE    :
               DriverPrintResult("DEFAULT PAGE\n");
               break;
            case AR_DPROP_TITLE_BAR_ICON_IMAGE :
               DriverPrintResult("TITLE BAR ICON IMAGE\n");
               break;
            case AR_DPROP_DETAIL_PANE_WIDTH :
               DriverPrintResult("DETAIL PANE WIDTH\n");
               break;
            case AR_DPROP_DETAIL_PANE_HEIGHT :
               DriverPrintResult("DETAIL PANE HEIGHT\n");
               break;
            case AR_DPROP_DETAIL_BANNER_VISIBILITY :
               DriverPrintResult("DETAIL BANNER VISIBILITY\n");
               break;
            case AR_DPROP_PROMPT_BANNER_VISIBILITY :
               DriverPrintResult("PROMPT BANNER VISIBILITY\n");
               break;
            case AR_DPROP_RESULT_BANNER_VISIBILITY :
               DriverPrintResult("RESULT BANNER VISIBILITY\n");
               break;
            case AR_DPROP_ALIAS_SINGULAR  :
               DriverPrintResult("ALIAS SINGULAR \n");
               break;
            case AR_DPROP_ALIAS_PLURAL    :
               DriverPrintResult("ALIAS PLURAL\n");
               break;
            case AR_DPROP_ALIAS_SHORT_SINGULAR :
               DriverPrintResult("ALIAS SHORT SINGULAR \n");
               break;
            case AR_DPROP_ALIAS_SHORT_PLURAL :
               DriverPrintResult("ALIAS SHORT PLURAL\n");
               break;
            case AR_DPROP_ALIAS_ABBREV_SINGULAR :
               DriverPrintResult("ALIAS ABBREV SINGULAR \n");
               break;
            case AR_DPROP_ALIAS_ABBREV_PLURAL :
               DriverPrintResult("ALIAS ABBREV PLURAL\n");
               break;
            case AR_DPROP_NAMED_SEARCHES  :
               DriverPrintResult("NAMED SEARCHES\n");
               break;
            case AR_DPROP_MENU_ACCESS     :
               DriverPrintResult("MENU ACCESS\n");
               break;
            case AR_DPROP_PANE_VISIBILITY_OPTION :
               DriverPrintResult("PANE VISIBILITY OPTION\n");
               break;
            case AR_DPROP_REQUEST_IDENTIFIER :
               DriverPrintResult("REQUEST IDENTIFIER\n");
               break;
            case AR_DPROP_QUERY_LIST_COLOR:
               DriverPrintResult("QUERY LIST COLOR\n");
               break;
            case AR_DPROP_COLUMN_WIDTH    :
               DriverPrintResult("COLUMN WIDTH\n");
               break;
            case AR_DPROP_COLUMN_ORDER    :
               DriverPrintResult("COLUMN ORDER\n");
               break;
            case AR_DPROP_SORT_SEQ        :
               DriverPrintResult("COLUMN SORT SEQUENCE\n");
               break;
            case AR_DPROP_SORT_DIR        :
               DriverPrintResult("COLUMN SORT DIRECTION\n");
               break;
            case AR_DPROP_DRILL_DOWN      :
               DriverPrintResult("DRILL DOWN\n");
               break;
            case AR_DPROP_REFRESH         :
               DriverPrintResult("REFRESH\n");
               break;
            case AR_DPROP_AUTO_REFRESH    :
               DriverPrintResult("AUTO REFRESH\n");
               break;
            case AR_DPROP_AUTOFIT_COLUMNS :
               DriverPrintResult("AUTOFIT COLUMNS\n");
               break;
            case AR_DPROP_APPLY_DIRTY     :
               DriverPrintResult("APPLY DIRTY\n");
               break;
            case AR_DPROP_IMAGE_CACHE     :
               DriverPrintResult("IMAGE CACHE\n");
               break;
            case AR_DPROP_ENUM_LABELS     :
               DriverPrintResult("DISPLAY LABELS\n");
               break;
            case AR_DPROP_LOCALIZATION_REQUIRED :
               DriverPrintResult("LOCALIZATION REQUIRED\n");
               break;
            case AR_DPROP_FIELD_DRAGGABLE :
               DriverPrintResult("DRAGGABLE\n");
               break;
            case AR_DPROP_FIELD_DROPPABLE :
               DriverPrintResult("DROPPABLE\n");
               break;
            default                       :
               DriverPrintResult("   <undefined property = %u>\n",
                                 tempPtr->prop);
               break;
         }
         PrintARValueStruct(buff, &tempPtr->value);
         tempPtr++;
      }
   }
}

void PrintARDisplayPropListList(
char *header1, 
char *header2,
ARPropListList *value
)
{
   unsigned i;

   DriverPrintResult("%s List : %u items\n", header1, value->numItems);
   for (i = 0; i < value->numItems; ++i)
   {
      char ib[64+sizeof(i)*3+2+4];
      sprintf(ib, "%.64s [%u] ", header2, i);
      PrintARDisplayPropList(ib, "   ", &value->propsList[i]);
   }
}

/*****************************************************************************/
/*                                                                           */
/*                             PrintARDisplayInstanceList                    */
/*                                                                           */
/*****************************************************************************/

void PrintARDisplayInstanceList(value)
ARDisplayInstanceList  *value;        /* IN; value to print */

{
   unsigned int              i;        /* working index */
   ARDisplayInstanceStruct  *tempPtr;  /* working pointer */

   DriverPrintResult("Display Instance List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      PrintARDisplayPropList("   ", "Common ", &value->commonProps);
      tempPtr = value->dInstanceList;
      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintResult("   Display Instance : #%u for VUI %lu\n", i,
                           tempPtr->vui);
         PrintARDisplayPropList("   ", "Instance ", &tempPtr->props);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      PrintARDisplayInstanceListList                       */
/*                                                                           */
/*****************************************************************************/

void PrintARDisplayInstanceListList(
char                      *header, /* IN; header string for the value */
ARDisplayInstanceListList *value   /* IN; value to print */
)

{
   unsigned int   i;               /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARDisplayInstanceList(&value->dInstanceList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARAssignFieldStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARAssignFieldStruct(header, value)
char                *header;  /* IN; header string for the value */
ARAssignFieldStruct *value;   /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%s", buff);
   PrintChar("Server: ", value->server);
   DriverPrintResult("%s", buff);
   PrintChar("Schema: ", value->schema);

   switch (value->tag)
   {
      case AR_FIELD        :
         DriverPrintResult("%sTag: FIELD\n", buff);
         DriverPrintResult("%s", buff);
         PrintARInternalId("Field Id:", value->u.fieldId);
         break;
      case AR_STAT_HISTORY :
         DriverPrintResult("%sTag: STAT_HISTORY\n", buff);
         DriverPrintResult("%s", buff);
         PrintLong("Enumerated value index:",
                   (int) value->u.statHistory.enumVal);
         switch (value->u.statHistory.userOrTime)
         {
            case AR_STAT_HISTORY_USER :
               DriverPrintResult("%sUser or Time: USER\n", buff);
               break;
            case AR_STAT_HISTORY_TIME :
               DriverPrintResult("%sUser or Time: TIME\n", buff);
               break;
            default                   :
               DriverPrintResult("%sUser or Time: %s%u>\n", buff,
                                 "<undefined value = ",
                                 value->u.statHistory.userOrTime);
               break;
         }
         break;
      case AR_CURRENCY_FLD :
         DriverPrintResult("%sTag: CURRENCY FIELD\n", buff);
         DriverPrintResult("%s", buff);
         PrintARInternalId("Field Id:", value->u.currencyField->fieldId);
         DriverPrintResult("%s", buff);
         PrintLong("Currency part tag:", (int) value->u.currencyField->partTag);         
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
         {
            DriverPrintResult("%sCurrency code: %s\n",
                              buff, value->u.currencyField->currencyCode);
         }
         break;
      default              :
         DriverPrintResult("%sTag: <undefined tag = %u>\n", buff, value->tag);
         break;
   }

   PrintARQualifierStruct(header, &value->qualifier);
   switch (value->noMatchOption)
   {
      case AR_NO_MATCH_ERROR    :
         DriverPrintResult("%sNo Match Option: ERROR\n", header);
         break;
      case AR_NO_MATCH_SET_NULL :
         DriverPrintResult("%sNo Match Option: NULL\n", header);
         break;
      case AR_NO_MATCH_NO_ACTION:
         DriverPrintResult("%sNo Match Option: NO ACTION\n", header);
         break;
      case AR_NO_MATCH_SUBMIT   :
         DriverPrintResult("%sNo Match Option: SUBMIT\n", header);
         break;
      default                   :
         DriverPrintResult("%sNo Match Option: %s%u>\n", header,
                           "<undefined value = ", value->noMatchOption);
         break;
   }
   switch (value->multiMatchOption)
   {
      case AR_MULTI_MATCH_ERROR    :
         DriverPrintResult("%sMulti Match Option: ERROR\n", header);
         break;
      case AR_MULTI_MATCH_SET_NULL :
         DriverPrintResult("%sMulti Match Option: NULL\n", header);
         break;
      case AR_MULTI_MATCH_USE_FIRST:
         DriverPrintResult("%sMulti Match Option: FIRST\n", header);
         break;
      case AR_MULTI_MATCH_PICKLIST :
         DriverPrintResult("%sMulti Match Option: PICKLIST\n", header);
         break;
      case AR_MULTI_MATCH_MODIFY_ALL :
         DriverPrintResult("%sMulti Match Option: MODIFY ALL\n", header);
         break;
      case AR_MULTI_MATCH_NO_ACTION :
         DriverPrintResult("%sMulti Match Option: NO ACTION\n", header);
         break;
      default                      :
         DriverPrintResult("%sMulti Match Option: %s%u>\n", header,
                           "<undefined value = ", value->multiMatchOption);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARAssignSQLStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARAssignSQLStruct(
char                *header,  /* IN; header string for the value */
ARAssignSQLStruct   *value    /* IN; value to print */
)

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%s", buff);
   PrintChar("Server: ", value->server);
   DriverPrintResult("%s", buff);
   if (value->sqlCommand == NULL)
      DriverPrintResult("SQL Command: NULL");
   else
      PrintChar("SQL Command: ", value->sqlCommand);

   DriverPrintResult("%s", buff);
   PrintUInt("Value Index:", value->valueIndex);

   switch (value->noMatchOption)
   {
      case AR_NO_MATCH_ERROR    :
         DriverPrintResult("%sNo Match Option: ERROR\n", buff);
         break;
      case AR_NO_MATCH_SET_NULL :
         DriverPrintResult("%sNo Match Option: NULL\n", buff);
         break;
      default                   :
         DriverPrintResult("%sNo Match Option: %s%u>\n", buff,
                           "<undefined value = ", value->noMatchOption);
         break;
   }
   switch (value->multiMatchOption)
   {
      case AR_MULTI_MATCH_ERROR    :
         DriverPrintResult("%sMulti Match Option: ERROR\n", buff);
         break;
      case AR_MULTI_MATCH_SET_NULL :
         DriverPrintResult("%sMulti Match Option: NULL\n", buff);
         break;
      case AR_MULTI_MATCH_USE_FIRST:
         DriverPrintResult("%sMulti Match Option: FIRST\n", buff);
         break;
      case AR_MULTI_MATCH_PICKLIST :
         DriverPrintResult("%sMulti Match Option: PICKLIST\n", buff);
         break;
      default                      :
         DriverPrintResult("%sMulti Match Option: %s%u>\n", buff,
                           "<undefined value = ", value->multiMatchOption);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                       PrintARAssignFilterApiStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARAssignFilterApiStruct(
char                    *header,    /* IN; header string for the value */
ARAssignFilterApiStruct *value
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%s   Assign Filter Api Struct:\n", header);
   DriverPrintResult("%s      Service Name: %s", header, value->serviceName);
   DriverPrintResult("%s      Input Field Value List\n", header);
   DriverPrintResult("%s   Assign List : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARAssignStruct(header, &value->inputValues[i]);
   DriverPrintResult("%s      Value Index: %d\n", header, value->valueIndex);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARAssignStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARAssignStruct(header, value)
char           *header;       /* IN; header string for the value */
ARAssignStruct *value;        /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sAssign Value: ", buff);
   switch (value->assignType)
   {
      case AR_ASSIGN_TYPE_NONE    :
         DriverPrintResult("  NONE\n");
         break;
      case AR_ASSIGN_TYPE_VALUE   :
         DriverPrintResult("  VALUE\n");
         PrintARValueStruct(buff, &value->u.value);
         break;
      case AR_ASSIGN_TYPE_FIELD   :
         DriverPrintResult("  FIELD\n");
         PrintARAssignFieldStruct(buff, value->u.field);
         break;
      case AR_ASSIGN_TYPE_PROCESS :
         DriverPrintResult("  PROCESS\n");
         DriverPrintResult("%s", buff);
         PrintChar("Process: ", value->u.process);
         break;
      case AR_ASSIGN_TYPE_ARITH   :
         DriverPrintResult("  ARITH\n");
         PrintARArithOpAssignStruct(buff, value->u.arithOp);
         break;
      case AR_ASSIGN_TYPE_FUNCTION:
         DriverPrintResult("  FUNCTION\n");
         PrintARFunctionAssignStruct(buff, value->u.function);
         break;
      case AR_ASSIGN_TYPE_DDE     :
         DriverPrintResult("  DDE\n");
         PrintARDDEStruct(buff, value->u.dde);
         break;
      case AR_ASSIGN_TYPE_SQL     :
         DriverPrintResult("  SQL\n");
         PrintARAssignSQLStruct(buff, value->u.sql);
         break;
      case AR_ASSIGN_TYPE_FILTER_API:
         DriverPrintResult("  FILTER API\n");
         PrintARAssignFilterApiStruct(buff, value->u.filterApi);
         break;
      default                   :
         DriverPrintResult("  <undefined assign type tag = %u>\n",
                           value->assignType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFieldAssignStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldAssignStruct(value)
ARFieldAssignStruct *value;   /* IN; value to print */

{
   DriverPrintResult("Field Assign Struct:\n");
   PrintARInternalId("   Field Id :", value->fieldId);
   PrintARAssignStruct("", &value->assignment);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFieldAssignList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldAssignList(value)
ARFieldAssignList *value;     /* IN; value to print */

{
   unsigned int         i;      /* working index */
   ARFieldAssignStruct *tempPtr;/* working pointer */

   DriverPrintResult("Field Assign List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->fieldAssignList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARFieldAssignStruct(tempPtr);
         tempPtr++;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                          PrintARSetFieldsAction                           */
/*                                                                           */
/*****************************************************************************/

void PrintARSetFieldsAction(value)
ARSetFieldsActionStruct *value;     /* IN; value to print */

{
   PrintARFieldAssignList(&value->fieldList);
   PrintChar("Sample Server: ", value->sampleServer);
   PrintChar("Sample Schema: ", value->sampleSchema);
}

/*****************************************************************************/
/*                                                                           */
/*                        PrintARArithOpAssignStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARArithOpAssignStruct(header, value)
char                  *header; /* IN; header string for the value */
ARArithOpAssignStruct *value;  /* IN; value to print */

{
   char   buff[45];           /* buffer to build nested header in */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sArithmetic Operation:\n", header);
   switch (value->operation)
   {
      case AR_ARITH_OP_ADD      :
         DriverPrintResult("%s   Operation: ADD\n", header);
         break;
      case AR_ARITH_OP_SUBTRACT :
         DriverPrintResult("%s   Operation: SUBTRACT\n", header);
         break;
      case AR_ARITH_OP_MULTIPLY :
         DriverPrintResult("%s   Operation: MULTIPLY\n", header);
         break;
      case AR_ARITH_OP_DIVIDE   :
         DriverPrintResult("%s   Operation: DIVIDE\n", header);
         break;
      case AR_ARITH_OP_MODULO   :
         DriverPrintResult("%s   Operation: MODULO\n", header);
         break;
      case AR_ARITH_OP_NEGATE   :
         DriverPrintResult("%s   Operation: NEGATE (unary minus)\n", header);
         break;
      default                   :
         DriverPrintResult("%s   Operation: <undefined operation = %u>\n",
                           header, value->operation);
         break;
   }
   if (value->operation != AR_ARITH_OP_NEGATE)
      PrintARAssignStruct(buff, &value->operandLeft);
   PrintARAssignStruct(buff, &value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARFunctionAssignStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARFunctionAssignStruct(header, value)
char                   *header; /* IN; header string for the value */
ARFunctionAssignStruct *value;  /* IN; value to print */

{
   char           buff[45];     /* buffer to build nested header in */
   unsigned int   i;            /* working index */

   if (strlen(header) >= (size_t) 40)
      buff[0] = '\0';
   else
      sprintf(buff, "%s   ", header);

   DriverPrintResult("%sFunction:\n", header);
   switch (value->functionCode)
   {
      case AR_FUNCTION_DATE     :
         DriverPrintResult("%s   Function code: DATE\n", header);
         break;
      case AR_FUNCTION_TIME     :
         DriverPrintResult("%s   Function code: TIME\n", header);
         break;
      case AR_FUNCTION_MONTH    :
         DriverPrintResult("%s   Function code: MONTH\n", header);
         break;
      case AR_FUNCTION_DAY      :
         DriverPrintResult("%s   Function code: DAY\n", header);
         break;
      case AR_FUNCTION_YEAR     :
         DriverPrintResult("%s   Function code: YEAR\n", header);
         break;
      case AR_FUNCTION_WEEKDAY  :
         DriverPrintResult("%s   Function code: WEEKDAY\n", header);
         break;
      case AR_FUNCTION_HOUR     :
         DriverPrintResult("%s   Function code: HOUR\n", header);
         break;
      case AR_FUNCTION_MINUTE   :
         DriverPrintResult("%s   Function code: MINUTE\n", header);
         break;
      case AR_FUNCTION_SECOND   :
         DriverPrintResult("%s   Function code: SECOND\n", header);
         break;
      case AR_FUNCTION_TRUNC    :
         DriverPrintResult("%s   Function code: TRUNC\n", header);
         break;
      case AR_FUNCTION_ROUND    :
         DriverPrintResult("%s   Function code: ROUND\n", header);
         break;
      case AR_FUNCTION_CONVERT  :
         DriverPrintResult("%s   Function code: CONVERT\n", header);
         break;
      case AR_FUNCTION_LENGTH   :
         DriverPrintResult("%s   Function code: LENGTH\n", header);
         break;
      case AR_FUNCTION_UPPER    :
         DriverPrintResult("%s   Function code: UPPER\n", header);
         break;
      case AR_FUNCTION_LOWER    :
         DriverPrintResult("%s   Function code: LOWER\n", header);
         break;
      case AR_FUNCTION_SUBSTR   :
         DriverPrintResult("%s   Function code: SUBSTR\n", header);
         break;
      case AR_FUNCTION_LEFT     :
         DriverPrintResult("%s   Function code: LEFT\n", header);
         break;
      case AR_FUNCTION_RIGHT    :
         DriverPrintResult("%s   Function code: RIGHT\n", header);
         break;
      case AR_FUNCTION_LTRIM    :
         DriverPrintResult("%s   Function code: LTRIM\n", header);
         break;
      case AR_FUNCTION_RTRIM    :
         DriverPrintResult("%s   Function code: RTRIM\n", header);
         break;
      case AR_FUNCTION_LPAD     :
         DriverPrintResult("%s   Function code: LPAD\n", header);
         break;
      case AR_FUNCTION_RPAD     :
         DriverPrintResult("%s   Function code: RPAD\n", header);
         break;
      case AR_FUNCTION_REPLACE  :
         DriverPrintResult("%s   Function code: REPLACE\n", header);
         break;
      case AR_FUNCTION_STRSTR   :
         DriverPrintResult("%s   Function code: STRSTR\n", header);
         break;
      case AR_FUNCTION_MIN      :
         DriverPrintResult("%s   Function code: MIN\n", header);
         break;
      case AR_FUNCTION_MAX      :
         DriverPrintResult("%s   Function code: MAX\n", header);
         break;
      case AR_FUNCTION_DATEADD   :
         DriverPrintResult("%s   Function code: DATEADD\n", header);
         break;
      case AR_FUNCTION_DATEDIFF  :
         DriverPrintResult("%s   Function code: DATEDIFF\n", header);
         break;
      case AR_FUNCTION_DATENAME  :
         DriverPrintResult("%s   Function code: DATENAME\n", header);
         break;
      case AR_FUNCTION_DATENUM   :
         DriverPrintResult("%s   Function code: DATENUM\n", header);
         break;
      case AR_FUNCTION_CURRCONVERT:
         DriverPrintResult("%s   Function code: CURRCONVERT\n", header);
         break;
      case AR_FUNCTION_CURRSETDATE:
         DriverPrintResult("%s   Function code: CURRSETDATE\n", header);
         break;
      case AR_FUNCTION_CURRSETTYPE:
         DriverPrintResult("%s   Function code: CURRSETTYPE\n", header);
         break;
      case AR_FUNCTION_CURRSETVALUE:
         DriverPrintResult("%s   Function code: CURRSETVALUE\n", header);
         break;
      case AR_FUNCTION_LENGTHC   :
         DriverPrintResult("%s   Function code: LENGTHC\n", header);
         break;
      case AR_FUNCTION_LEFTC     :
         DriverPrintResult("%s   Function code: LEFTC\n", header);
         break;
      case AR_FUNCTION_RIGHTC    :
         DriverPrintResult("%s   Function code: RIGHTC\n", header);
         break;
      case AR_FUNCTION_LPADC     :
         DriverPrintResult("%s   Function code: LPADC\n", header);
         break;
      case AR_FUNCTION_RPADC     :
         DriverPrintResult("%s   Function code: RPADC\n", header);
         break;
      case AR_FUNCTION_STRSTRC   :
         DriverPrintResult("%s   Function code: STRSTRC\n", header);
         break;
      case AR_FUNCTION_SUBSTRC   :
         DriverPrintResult("%s   Function code: SUBSTRC\n", header);
         break;
      case AR_FUNCTION_ENCRYPT   :
         DriverPrintResult("%s   Function code: ENCRYPT\n", header);
         break;
      case AR_FUNCTION_DECRYPT   :
         DriverPrintResult("%s   Function code: DECRYPT\n", header);
         break;
      case AR_FUNCTION_HOVER     :
         DriverPrintResult("%s   Function code: HOVER\n", header);
         break;
      case AR_FUNCTION_TEMPLATE  :
         DriverPrintResult("%s   Function code: TEMPLATE\n", header);
         break;
      case AR_FUNCTION_DROPPEDCOLUMNINDEX:
         DriverPrintResult("%s   Function code: DROPPEDCOLUMNINDEX\n", header);
         break;
      case AR_FUNCTION_DROPPEDROWINDEX :
         DriverPrintResult("%s   Function code: DROPPEDROWINDEX\n", header);
         break;
      case AR_FUNCTION_MAPGET :
         DriverPrintResult("%s   Function code: MAPGET\n", header);
         break;
      case AR_FUNCTION_LISTGET:
         DriverPrintResult("%s   Function code: LISTGET\n", header);
         break;
     case AR_FUNCTION_LISTSIZE:
         DriverPrintResult("%s   Function code: LISTSIZE\n", header);
         break;
     case AR_FUNCTION_STRIPHTML:
         DriverPrintResult("%s   Function code: STRIPHTML\n", header);
         break;
      default                   :
         DriverPrintResult("%s   Function code: <undefined function = %u>\n",
                           header, value->functionCode);
         break;
   }
   DriverPrintResult("%s   Number parameters: %u\n", header, value->numItems);

   if ((value->numItems != 0) && (value->parameterList != NULL))
   {
      for (i = 0; i < value->numItems; i++)
         PrintARAssignStruct(buff, &value->parameterList[i]);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFilterActionNotify                        */
/*                                                                           */
/*****************************************************************************/

void PrintARFilterActionNotify(value)
ARFilterActionNotify *value;  /* IN; value to print */

{
   DriverPrintResult("NOTIFY\n");
   PrintChar("   User:", value->user);
   PrintChar("   Text:", value->notifyText);
   PrintUInt("   Priority:", value->notifyPriority);
   PrintChar("   Subject:", value->subjectText);
   PrintUInt("   Behavior:", value->notifyBehavior);
   PrintUInt("   Permission:", value->notifyPermission);
   switch (value->notifyMechanism)
   {
      case AR_NOTIFY_VIA_NOTIFIER :
         DriverPrintResult("   Mechanism: ALERT\n");
         break;
      case AR_NOTIFY_VIA_EMAIL    :
         DriverPrintResult("   Mechanism: E-MAIL\n");
         break;
      case AR_NOTIFY_VIA_DEFAULT  :
         DriverPrintResult("   Mechanism: DEFAULT\n");
         break;
      case AR_NOTIFY_VIA_XREF     :
         DriverPrintResult("   Mechanism: CROSS-REFERENCE\n");
         DriverPrintResult("   Cross reference field id: %ld\n",
                           value->notifyMechanismXRef);
         break;
      default                     :
         DriverPrintResult("   Mechanism: %u\n", value->notifyMechanism);
         break;
   }
   if (value->notifyAdvanced)
   {
      PrintChar("   From user: ", value->notifyAdvanced->from);
      PrintChar("   Reply To: ", value->notifyAdvanced->replyTo);
      PrintChar("   Cc user: ", value->notifyAdvanced->cc);
      PrintChar("   Bcc user: ", value->notifyAdvanced->bcc);
      PrintChar("   Organization: ", value->notifyAdvanced->organization);
      PrintChar("   Mailbox name: ", value->notifyAdvanced->mailboxName);
      PrintChar("   Header template: ", value->notifyAdvanced->headerTemplate);
      PrintChar("   Footer template: ", value->notifyAdvanced->footerTemplate);
      PrintChar("   Content template: ", value->notifyAdvanced->contentTemplate);
   }
   switch (value->fieldIdListType)
   {
      case AR_FILTER_FIELD_IDS_NONE :
         DriverPrintResult("   Field Id List: NONE\n");
         break;
      case AR_FILTER_FIELD_IDS_ALL  :
         DriverPrintResult("   Field Id List: ALL\n");
         break;
      case AR_FILTER_FIELD_IDS_LIST :
         DriverPrintResult("   Field Id List: LIST\n");
         PrintARInternalIdList("      Field List:", "         ",
                               &value->fieldIdList);
         break;
      case AR_FILTER_FIELD_IDS_CHANGED :
         DriverPrintResult("   Field Id List: CHANGED\n");
         break;
      default                     :
         DriverPrintResult("   Field Id List: <undefined type = %u>\n",
                           value->fieldIdListType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFilterStatusStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARFilterStatusStruct(value)
ARFilterStatusStruct *value;        /* IN; value to print */

{
   DriverPrintResult("Filter Status Struct :\n");
   DriverPrintResult("   Message type : ");
   switch (value->messageType)
   {
      case AR_RETURN_OK         :
         DriverPrintResult("NOTE\n");
         break;
      case AR_RETURN_WARNING    :
         DriverPrintResult("WARNING\n");
         break;
      case AR_RETURN_ERROR      :
         DriverPrintResult("ERROR\n");
         break;
      default                   :
         DriverPrintResult("<unknown type>\n");
         break;
   }
   PrintLong("   Message number : ", value->messageNum);
   PrintChar("   Message: ", value->messageText);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARPushFieldsStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARPushFieldsStruct(value)
ARPushFieldsStruct *value;  /* IN; value to print */

{
   DriverPrintResult("Push Fields Struct:\n");
   PrintARAssignFieldStruct("", &value->field);
   PrintARAssignStruct("", &value->assign);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARPushFieldsList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARPushFieldsList(value)
ARPushFieldsList *value;  /* IN; value to print */

{
   unsigned int    i;    /* working index */

   if ((value->numItems != 0) && (value->pushFieldsList != NULL))
   {
      DriverPrintResult("Push Fields List : %u items\n", value->numItems);
      for (i = 0; i < value->numItems; i++)
         PrintARPushFieldsStruct(&value->pushFieldsList[i]);
   }
}

/*****************************************************************************/
/*                                                                           */
/*                         PrintARPushFieldsAction                           */
/*                                                                           */
/*****************************************************************************/

void PrintARPushFieldsAction(value)
ARPushFieldsActionStruct *value;     /* IN; value to print */

{
   PrintARPushFieldsList(&value->pushFieldsList);
   PrintChar("Sample Server: ", value->sampleServer);
   PrintChar("Sample Schema: ", value->sampleSchema);
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARSQLStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARSQLStruct(
ARSQLStruct *value   /* IN; value to print */
)

{
   DriverPrintResult("SQL Struct:\n");
   PrintChar("Server: ", value->server);
   if (value->command == NULL)
      DriverPrintResult("SQL Command: NULL");
   else
      PrintChar("SQL Command: ", value->command);
}


/*****************************************************************************/
/*                                                                           */
/*                       PrintARGotoActionStruct                             */
/*                                                                           */
/*****************************************************************************/
  
void PrintARGotoActionStruct(
ARGotoActionStruct *value
)
{
   DriverPrintResult("   Goto Action Struct:\n");
   PrintUInt("      Tag (Field Or Value): ", value->tag);
   if (value->tag == AR_GOTO_FIELD_XREF)
      PrintULong("      FieldId: ", value->fieldIdOrValue);
   else if (value->tag == AR_GOTO_ABSOLUTE_ORDER)
      PrintULong("      Execution Order: ", value->fieldIdOrValue);
   else if (value->tag == AR_GOTO_OFFSET_FORWARD)
      PrintULong("      Execution Order Advance By: ", value->fieldIdOrValue);
   else if (value->tag == AR_GOTO_OFFSET_BACKWARD)
      PrintULong("      Execution Order Backup By: ", value->fieldIdOrValue);
   else
      DriverPrintResult("<unknown type>\n");
}


/*****************************************************************************/
/*                                                                           */
/*                       PrintARServiceActionStruct                          */
/*                                                                           */
/*****************************************************************************/
  
void PrintARServiceActionStruct(
ARSvcActionStruct *value
)
{
   DriverPrintResult("   Service Action Struct:\n");

   PrintChar("      ServerName : ",value->serverName);
   PrintChar("      Service Schema : ",value->serviceSchema);
   PrintLong("      Request Id  : ",value->requestIdMap);
   PrintARFieldAssignList(&value->inputFieldMapping);
   PrintARFieldAssignList(&value->outputFieldMapping);
}

/*****************************************************************************/
/*                                                                           */
/*                             PrintARCallGuideStruct                        */
/*                                                                           */
/*****************************************************************************/
  
void PrintARCallGuideStruct(
ARCallGuideStruct *value
)

{
   DriverPrintResult("   Active Link Call Guide Struct:\n");
   PrintChar("      Server Name: ", value->serverName);
   PrintChar("      Guide Name: ", value->guideName);
   PrintInt("      Guide Mode: ", value->guideMode);
   PrintLong("      Guide Table Id:", value->guideTableId);
   PrintARFieldAssignList(&value->inputValueFieldPairs);
   PrintARFieldAssignList(&value->outputValueFieldPairs);
   PrintChar("      Sample Server Name: ", value->sampleServer);
   PrintChar("      Sample Guide Name: ", value->sampleGuide);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARExitGuideStruct                        */
/*                                                                           */
/*****************************************************************************/
  
void PrintARExitGuideStruct(
ARExitGuideStruct *value
)

{
   DriverPrintResult("   Active Link Exit Guide Struct:\n");
   PrintARBoolean("      Close All Windows : ", value->closeAll);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARGotoGuideLabel                         */
/*                                                                           */
/*****************************************************************************/
  
void PrintARGotoGuideLabel(
ARGotoGuideLabelStruct *value
)

{
   DriverPrintResult("   Active Link Goto Guide Label Struct:\n");
   PrintChar("      Label: ", value->label);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFilterActionStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARFilterActionStruct(value)
ARFilterActionStruct *value;  /* IN; value to print */

{
   DriverPrintResult("Filter Action Struct:  ");
   switch (value->action)
   {
      case AR_FILTER_ACTION_NOTIFY  :
         PrintARFilterActionNotify(&value->u.notify);
         break;
      case AR_FILTER_ACTION_MESSAGE :
         DriverPrintResult("MESSAGE\n");
         PrintARFilterStatusStruct(&value->u.message);
         break;
      case AR_FILTER_ACTION_LOG     :
         DriverPrintResult("LOG FILE\n");
         PrintChar("   Filename:", value->u.logFile);
         break;
      case AR_FILTER_ACTION_FIELDS  :
         DriverPrintResult("SET FIELDS\n");
         PrintARSetFieldsAction(&(value->u.setFields));
         break;
      case AR_FILTER_ACTION_PROCESS :
         DriverPrintResult("PROCESS\n");
         PrintChar("   Filename:", value->u.process);
         break;
      case AR_FILTER_ACTION_FIELDP  :
         DriverPrintResult("PUSH FIELDS\n");
         PrintARPushFieldsAction(&(value->u.pushFields));
         break;
      case AR_FILTER_ACTION_SQL     :
         DriverPrintResult("DIRECT SQL\n");
         PrintARSQLStruct(&(value->u.sqlCommand));
         break;
      case AR_FILTER_ACTION_GOTOACTION     :
         DriverPrintResult("GOTO ACTION\n");
         PrintARGotoActionStruct(&(value->u.gotoAction));
         break;
      case AR_FILTER_ACTION_CALLGUIDE     :
         DriverPrintResult("CALL GUIDE\n");
         PrintARCallGuideStruct(&(value->u.callGuide));
         break;
      case AR_FILTER_ACTION_EXITGUIDE     :
         DriverPrintResult("EXIT GUIDE\n");
         PrintARExitGuideStruct(&(value->u.exitGuide));
         break;
      case AR_FILTER_ACTION_GOTOGUIDELABEL:
         DriverPrintResult("GOTO GUIDE LABEL\n");
         PrintARGotoGuideLabel(&(value->u.gotoGuide));
         break;
      case AR_FILTER_ACTION_SERVICE:
         DriverPrintResult("SERVICE\n");   
         PrintARServiceActionStruct(&value->u.serviceAction);
         break;
      default                       :
         DriverPrintResult("   <undefined action = %u>\n", value->action);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFilterActionList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARFilterActionList(
ARFilterActionList *value,     /* IN; value to print */
ARBoolean           actionFlag /* IN; TRUE - action; FALSE - else */
)

{
   unsigned int           i;       /* working index */
   ARFilterActionStruct  *tempPtr; /* working pointer */

   if (actionFlag)
      DriverPrintResult("Filter Action List : %u items\n", value->numItems);
   else
      DriverPrintResult("Filter Else List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->actionList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARFilterActionStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARActiveLinkMacroStruct                      */
/*                                                                           */
/*****************************************************************************/

void PrintARActiveLinkMacroStruct(
ARActiveLinkMacroStruct *value   /* IN; value to print */
)

{
   unsigned int   i;       /* working index */

   DriverPrintResult("   Active Link Macro Struct:\n");
   PrintChar("      Macro name:", value->macroName);
   PrintChar("      Macro text:", value->macroText);
   DriverPrintResult("      Macro Parms:  %u\n", value->macroParms.numItems);
   for (i = 0; i < value->macroParms.numItems; i++)
   {
      DriverPrintResult("      Macro parm (%u): %s", (i + 1),
                        value->macroParms.parms[i].name);
      if (value->macroParms.parms[i].value == NULL)
         DriverPrintResult("\n");
      else
         DriverPrintResult("  =  %s\n", value->macroParms.parms[i].value);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARMessageStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARMessageStruct(
ARMessageStruct *value         /* IN; value to print */
)

{
   DriverPrintResult("Message Struct :\n");
   DriverPrintResult("   Message type : ");
   switch (value->messageType)
   {
      case AR_RETURN_OK         :
         DriverPrintResult("NOTE\n");
         break;
      case AR_RETURN_WARNING    :
         DriverPrintResult("WARNING\n");
         break;
      case AR_RETURN_ERROR      :
         DriverPrintResult("ERROR\n");
         break;
      case AR_RETURN_PROMPT     :
         DriverPrintResult("PROMPT\n");
         break;
      case AR_RETURN_ACCESSIBLE :
         DriverPrintResult("ACCESSIBLE\n");
         break;
      case AR_RETURN_TOOLTIP    :
         DriverPrintResult("TOOLTIP\n");
         break;
      default                   :
         DriverPrintResult("<unknown type>\n");
         break;
   }
   PrintLong("   Message number : ", value->messageNum);
   PrintChar("   Message: ", value->messageText);
   PrintARBoolean("   Use Prompting Pane : ", value->usePromptingPane);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFieldCharacteristics                       */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldCharacteristics(value)
ARFieldCharacteristics *value;  /* IN; value to print */

{
   DriverPrintResult("   Active Link Field Characteristics Struct:\n");
   PrintLong("      Field Id:", value->fieldId);
   if (value->charMenu == NULL)
      DriverPrintResult("      Char menu: NULL\n");
   else
      PrintChar("      Char menu:", value->charMenu);
   PrintARDisplayPropList("      ", "", &value->props);
   PrintUInt("      Reset Focus:", value->focus);
   PrintUInt("      Access option:", value->accessOption);
   PrintUInt("      Option:", value->option);
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARDDEStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARDDEStruct(
char          *header,        /* IN; header string for the value */
ARDDEStruct   *value          /* IN; value to print */
)

{
   DriverPrintResult("   Active Link DDE Struct:\n");
   PrintChar("      Service name:", value->serviceName);
   PrintChar("      Topic:", value->topic);
   PrintUInt("      Action:", value->action);
   PrintChar("      Path to Program:", value->pathToProgram);
   PrintChar("      Command:", value->command);
   if (value->item != NULL)
      PrintChar("      Item:", value->item);
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARAutomationStruct                     */
/*                                                                           */
/*****************************************************************************/
 
void PrintARAutomationStruct(header, value)
char                 *header;        /* IN; header string for the value */
ARAutomationStruct   *value;         /* IN; value to print */
 
{
   DriverPrintResult("   Active Link Outbound Automation Struct:\n");
   PrintChar("      Auto Server name:", value->autoServerName);
   PrintARBoolean("      Automation Server Showing flag :", value->isVisible);
   PrintChar("      Class Id:", value->clsId);
   PrintChar("      Automation Action:", value->action);
   PrintARCOMMethodList(&value->methodList);
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARCOMValueStruct                       */
/*                                                                           */
/*****************************************************************************/
 
void PrintARCOMValueStruct(header, value, indent)
char             *header;       /* IN; header string for the value */
ARCOMValueStruct *value;        /* IN; value to print */
char             *indent;       /* IN; for indentation */
 
{
   char buffer[512];

   DriverPrintResult("      %sCOM %s Value Struct:\n", indent, header);
   sprintf(buffer, "         %sValue IId:", indent);
   PrintChar(buffer, ((value->valueIId)? value->valueIId : "(NULL)"));
   sprintf(buffer, "         %sTransient Id:", indent);
   PrintLong(buffer, value->transId);

   switch (value->valueType)
   {
      case AR_COM_PARM_NULL:
         sprintf(buffer, "         %sValue: <NULL>", indent);
         PrintChar(buffer, "");
         break;
      case AR_COM_PARM_FIELDID: /* AR_COM_METHOD_FIELDID has same type */
         sprintf(buffer, "         %sFieldId:", indent);
         PrintLong(buffer, value->u.fieldId);
         break;
      case AR_COM_PARM_VALUE:
         sprintf(buffer, "         %s", indent);
         PrintARValueStruct(buffer, &value->u.value);
         break;
      default:
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARCOMMethodList                        */
/*                                                                           */
/*****************************************************************************/
 
void PrintARCOMMethodList(value)
ARCOMMethodList *value;         /* IN; value to print */
 
{
   unsigned int        i;
   ARCOMMethodStruct  *tempPtr;
 
   DriverPrintResult("    COM Automation Method List:\n");
   if (value->numItems > 0)
   {
      tempPtr = value->methodList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARCOMMethodStruct(tempPtr);
         tempPtr++;
      }
   }
}
 
 
/*****************************************************************************/
/*                                                                           */
/*                               PrintARCOMMethodStruct                      */
/*                                                                           */
/*****************************************************************************/
 
void PrintARCOMMethodStruct(value)
ARCOMMethodStruct *value;         /* IN; value to print */
 
{
   DriverPrintResult("      COM Method Struct:\n");
   PrintChar("      Method Name:", value->methodName);
   PrintChar("      Method Interface Id:", value->methodIId);
   PrintUInt("      Method Type:", value->methodType);
   PrintARCOMValueStruct("Method", &value->methodValue, "");
   PrintARCOMMethodParmList(&(value->parameterList));
}
 
 
/*****************************************************************************/
/*                                                                           */
/*                        PrintARCOMMethodParmList                           */
/*                                                                           */
/*****************************************************************************/
 
void PrintARCOMMethodParmList(value)
ARCOMMethodParmList *value;         /* IN; value to print */
 
{
   unsigned int            i;
   ARCOMMethodParmStruct  *tempPtr;
 
   DriverPrintResult("      COM Parameter List:\n");
   if (value->numItems > 0)
   {
      tempPtr = value->parameterList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARCOMMethodParmStruct(tempPtr);
         tempPtr++;
      }
   }
}
 
 
/*****************************************************************************/
/*                                                                           */
/*                               PrintARCOMMethodParmStruct                  */
/*                                                                           */
/*****************************************************************************/
 
void PrintARCOMMethodParmStruct(value)
ARCOMMethodParmStruct *value;         /* IN; value to print */
 
{
   DriverPrintResult("         COM Parameter Struct:\n");
   PrintChar("         Parameter Name:", value->parmName);
   PrintUInt("         Parameter Type:", value->parmType);
   PrintARCOMValueStruct("Parameter", &value->parmValue, "   ");
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintAROpenDlgStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintAROpenDlgStruct(value)
AROpenDlgStruct   *value;      /* OUT; loaded with value for buffer */
 
{
   DriverPrintResult("   Active Link Open Window Struct:\n");
   PrintChar("      Server Name: ", value->serverName);
   PrintChar("      Schema Name: ", value->schemaName);
   PrintChar("      Vui Label: ", value->vuiLabel);
   PrintARBoolean("      Close Box Showing flag : ", value->closeBox);
   PrintARFieldAssignList(&value->inputValueFieldPairs);
   PrintARFieldAssignList(&value->outputValueFieldPairs);
   PrintInt("Window Mode: ", value->windowMode);
   PrintChar("TargetLocation: ", value->targetLocation);
   PrintARBoolean("No Match Continue:", value->noMatchContinue);
   PrintARBoolean("Suppress Empty List:", value->suppressEmptyLst);
   PrintLong("Polling Interval :", value->pollinginterval);
   PrintARQualifierStruct("   ", &value->query);
   PrintARMessageStruct(&value->msg);
   PrintChar("   Report String: ", value->reportString);
   PrintARSortList(&value->sortOrderList);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARCloseWndStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARCloseWndStruct(
ARCloseWndStruct   *value       /* OUT; loaded with value for buffer */ 
)

{
   DriverPrintResult("   Active Link Close Window Struct:\n");
   PrintARBoolean("      Close All Showing flag : ", value->closeAll);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARWaitStruct                             */
/*                                                                           */
/*****************************************************************************/
  
void PrintARWaitStruct(
ARWaitStruct *value
)

{
   DriverPrintResult("   Active Link Wait Struct:\n");
   PrintChar("      Continue Button Title: ", value->continueButtonTitle);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARActiveLinkActionStruct                      */
/*                                                                           */
/*****************************************************************************/

void PrintARActiveLinkActionStruct(value)
ARActiveLinkActionStruct *value;  /* IN; value to print */

{
   DriverPrintResult("Active Link Action Struct:  ");
   switch (value->action)
   {
      case AR_ACTIVE_LINK_ACTION_MACRO   :
         DriverPrintResult("MACRO\n");
         PrintARActiveLinkMacroStruct(&value->u.macro);
         break;
      case AR_ACTIVE_LINK_ACTION_FIELDS  :
         DriverPrintResult("FIELDS\n");
         PrintARSetFieldsAction(&value->u.setFields);
         break;
      case AR_ACTIVE_LINK_ACTION_PROCESS :
         DriverPrintResult("PROCESS\n");
         PrintChar("   Process:", value->u.process);
         break;
      case AR_ACTIVE_LINK_ACTION_MESSAGE :
         DriverPrintResult("MESSAGE\n");
         PrintARMessageStruct(&value->u.message);
         break;
      case AR_ACTIVE_LINK_ACTION_SET_CHAR:
         DriverPrintResult("SET FIELD CHARACTERISTICS\n");
         PrintARFieldCharacteristics(&value->u.characteristics);
         break;
      case AR_ACTIVE_LINK_ACTION_DDE     :
         DriverPrintResult("DDE\n");
         PrintARDDEStruct("      ", &value->u.dde);
         break;
      case AR_ACTIVE_LINK_ACTION_FIELDP  :
         DriverPrintResult("PUSH FIELDS\n");
         PrintARPushFieldsAction(&(value->u.pushFields));
         break;
      case AR_ACTIVE_LINK_ACTION_SQL     :
         DriverPrintResult("DIRECT SQL\n");
         PrintARSQLStruct(&(value->u.sqlCommand));
         break;
      case AR_ACTIVE_LINK_ACTION_AUTO    :
         DriverPrintResult("Automation\n");
         PrintARAutomationStruct("      ", &value->u.automation);
         break;
      case AR_ACTIVE_LINK_ACTION_OPENDLG   :
         DriverPrintResult("Open Dialog\n");
         PrintAROpenDlgStruct(&value->u.openDlg);
         break;
      case AR_ACTIVE_LINK_ACTION_COMMITC   :
         DriverPrintResult("Commit Changes\n");
         break;
      case AR_ACTIVE_LINK_ACTION_CLOSEWND :
         DriverPrintResult("Close Window\n");
         PrintARCloseWndStruct(&value->u.closeWnd);
         break;
      case AR_ACTIVE_LINK_ACTION_CALLGUIDE  :
         DriverPrintResult("Call Guide\n");
         PrintARCallGuideStruct(&value->u.callGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_EXITGUIDE  :
         DriverPrintResult("Exit Guide\n");
         PrintARExitGuideStruct(&value->u.exitGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_GOTOGUIDELABEL :
         DriverPrintResult("Goto Guide\n");
         PrintARGotoGuideLabel(&value->u.gotoGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_WAIT:
         DriverPrintResult("Wait Guide\n");
         PrintARWaitStruct(&value->u.waitAction);
         break;
      case AR_ACTIVE_LINK_ACTION_GOTOACTION:
         DriverPrintResult("Goto Action\n");
         PrintARGotoActionStruct(&value->u.gotoAction);
         break;
      case AR_ACTIVE_LINK_ACTION_SERVICE :
         DriverPrintResult("Service\n");
         PrintARServiceActionStruct(&value->u.service);
         break;
      default          :
         DriverPrintResult("   <undefined action = %u>\n", value->action);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARActiveLinkActionList                       */
/*                                                                           */
/*****************************************************************************/

void PrintARActiveLinkActionList(
ARActiveLinkActionList *value,     /* IN; value to print */
ARBoolean               actionFlag /* IN; TRUE - action; FALSE - else */
)

{
   unsigned int               i;       /* working index */
   ARActiveLinkActionStruct  *tempPtr; /* working pointer */

   if (actionFlag)
      DriverPrintResult("Active Link Action List : %u items\n",
                        value->numItems);
   else
      DriverPrintResult("Active Link Else List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->actionList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARActiveLinkActionStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARPermissionStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARPermissionStruct(
ARPermissionStruct *value,    /* IN; value to print */
ARBoolean           fieldFlag /* IN; TRUE - field; FALSE - schema */
)

{
   DriverPrintResult("Permission Struct:\n");
   PrintARInternalId("   Group id:", value->groupId);
   switch (value->permissions)
   {
      case AR_PERMISSIONS_NONE   :
         DriverPrintResult("   Permissions:  NONE\n");
         break;
      case AR_PERMISSIONS_VIEW   :
         if (fieldFlag)
            DriverPrintResult("   Permissions:  VIEW\n");
         else
            DriverPrintResult("   Permissions:  VISIBLE\n");
         break;
      case AR_PERMISSIONS_CHANGE :
         if (fieldFlag)
            DriverPrintResult("   Permissions:  CHANGE\n");
         else
            DriverPrintResult("   Permissions:  HIDDEN\n");
         break;
      default                    :
         DriverPrintResult("   Permissions: <undefined permission = %u>\n",
                           value->permissions);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARPermissionList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARPermissionList(
ARPermissionList *value,      /* IN; value to print */
ARBoolean         fieldFlag   /* IN; TRUE - field; FALSE - schema */
)

{
   unsigned int         i;       /* working index */
   ARPermissionStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Permission List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->permissionList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARPermissionStruct(tempPtr, fieldFlag);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARPermissionListList                         */
/*                                                                           */
/*****************************************************************************/

void PrintARPermissionListList(
char                 *header, /* IN; header string for the value */
ARPermissionListList *value   /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARPermissionList(&value->permissionList[i], TRUE);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARGroupInfoStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARGroupInfoStruct(
ARGroupInfoStruct  *value     /* IN; value to print */
)

{
   DriverPrintResult("   Group Info Struct:\n");
   PrintARInternalId("        Group id: ", value->groupId);
   switch (value->groupType)
   {
      case 0                    :
         DriverPrintResult("      Group type:  NONE\n");
         break;
      case AR_GROUP_TYPE_VIEW   :
         DriverPrintResult("      Group type:  VIEW\n");
         break;
      case AR_GROUP_TYPE_CHANGE :
         DriverPrintResult("      Group type:  CHANGE\n");
         break;
      default                    :
         DriverPrintResult("      Group type:  <undefined group type = %u>\n",
                           value->groupType);
         break;
   }
   switch (value->groupCategory)
   {
      case AR_GROUP_CATEGORY_REGULAR  :
         DriverPrintResult("  Group Category:  REGULAR\n");
         break;
      case AR_GROUP_CATEGORY_DYNAMIC  :
         DriverPrintResult("  Group Category:  DYNAMIC\n");
         break;
      case AR_GROUP_CATEGORY_COMPUTED :
         DriverPrintResult("  Group Category:  COMPUTED\n");
         break;
      default                    :
         DriverPrintResult("  Group Category:  <undefined group category = %u>\n",
                           value->groupCategory);
         break;      
   }
   if( value->groupParent )
      DriverPrintResult("    Group Parent:  %u\n", value->groupParent );
   else
      DriverPrintResult("    Group Parent:  NONE\n" );

   PrintARAccessNameList("      Group name(s): ", "      ", &value->groupName);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARGroupInfoList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARGroupInfoList(
ARGroupInfoList *value       /* IN; value to print */
)

{
   unsigned int        i;       /* working index */
   ARGroupInfoStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Group Info List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->groupList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARGroupInfoStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARRoleInfoList                              */
/*                                                                           */
/*****************************************************************************/

void PrintARRoleInfoList(
ARRoleInfoList  *value       /* IN; value to print */
)

{
   unsigned int   i;         /* working index */

   DriverPrintResult("   Role Info List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      for (i = 0; i < value->numItems; i++)
      {
         PrintInt("   Role Id:", value->roleList[i].roleId);
         PrintChar("   Role Name:", value->roleList[i].roleName);
         switch (value->roleList[i].roleType)
         {
            case 0                    :
               DriverPrintResult("   Role type:  NONE\n");
               break;
            case AR_GROUP_TYPE_VIEW   :
               DriverPrintResult("   Role type:  VIEW\n");
               break;
            case AR_GROUP_TYPE_CHANGE :
               DriverPrintResult("   Role type:  CHANGE\n");
               break;
            default                    :
               DriverPrintResult("   Role type:  <undefined role type= %u>\n",
                                 value->roleList[i].roleType);
               break;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARUserLicenseStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARUserLicenseStruct(
ARUserLicenseStruct *value    /* IN; value to print */
)

{
   char  *tempPtr;            /* working pointer */
   char  *tempPtr2;           /* working pointer */

   DriverPrintResult("         User License Struct:  ");
   switch (value->licenseTag)
   {
      case AR_LICENSE_TAG_WRITE          :
         DriverPrintResult("WRITE\n");
         switch (value->licenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr = "READ";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr = "FLOATING";
               break;
            case AR_LICENSE_TYPE_RESTRICTED_READ :
               tempPtr = "RESTRICTED READ";
               break;
            default                    :
               tempPtr = "<undefined>";
               break;
         }
         switch (value->currentLicenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr2 = "READ";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr2 = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr2 = "FLOATING";
               break;
            case AR_LICENSE_TYPE_RESTRICTED_READ :
               tempPtr2 = "RESTRICTED READ";
               break;
            default                    :
               tempPtr2 = "<undefined>";
               break;
         }
         break;
      case AR_LICENSE_TAG_FULL_TEXT      :
         DriverPrintResult("FULL TEXT\n");
         switch (value->licenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr = "NONE";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr = "FLOATING";
               break;
            default                    :
               tempPtr = "<undefined>";
               break;
         }
         switch (value->currentLicenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr2 = "NONE";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr2 = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr2 = "FLOATING";
               break;
            default                    :
               tempPtr2 = "<undefined>";
               break;
         }
         break;
      case AR_LICENSE_TAG_RESERVED1     :
         DriverPrintResult("RESERVED\n");
         switch (value->licenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr = "NONE";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr = "FLOATING";
               break;
            default                    :
               tempPtr = "<undefined>";
               break;
         }
         switch (value->currentLicenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr2 = "NONE";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr2 = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr2 = "FLOATING";
               break;
            default                    :
               tempPtr2 = "<undefined>";
               break;
         }
         break;
      case AR_LICENSE_TAG_APP          :
         DriverPrintResult("APPLICATION\n");
         switch (value->licenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr = "READ";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr = "FLOATING";
               break;
            default                    :
               tempPtr = "<undefined>";
               break;
         }
         switch (value->currentLicenseType)
         {
            case AR_LICENSE_TYPE_NONE :
               tempPtr2 = "READ";
               break;
            case AR_LICENSE_TYPE_FIXED :
               tempPtr2 = "FIXED";
               break;
            case AR_LICENSE_TYPE_FLOATING :
               tempPtr2 = "FLOATING";
               break;
            default                    :
               tempPtr2 = "<undefined>";
               break;
         }
         break;
      default                       :
         DriverPrintResult("   <undefined operation = %u>\n", value->licenseTag);
         tempPtr = "";
         tempPtr2 = "";
         break;
   }
   PrintChar("            Assigned License Type:", tempPtr);
   PrintChar("            Current License Type:", tempPtr2);
   PrintLong("            License Pool:", value->licensePool);
   PrintChar("            App License Descriptor:", value->appLicenseDescriptor);
   PrintARTimestamp("            Last access:", value->lastAccess);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARUserLicenseList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARUserLicenseList(
ARUserLicenseList *value        /* IN; value to print */
)

{
   unsigned int          i;       /* working index */
   ARUserLicenseStruct  *tempPtr; /* working pointer */

   DriverPrintResult("      User License List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->licenseList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARUserLicenseStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARWfdUserContext                            */
/*                                                                           */
/*****************************************************************************/

   void 
PrintARWfdUserContext(
   ARWfdUserContext  *value )   /* IN; value to print */
{
   DriverPrintResult("   User Context:\n");

   if( value->user[0] == '\0' )
   {
      DriverPrintResult("   ...Not available...\n");
   }
   else
   {
      PrintChar("      User name:", value->user);
      PrintUInt("      Default notify mechanism:", value->notifyMech);
      PrintARUserLicenseList(&value->licenseList);
      PrintUInt("      License Type:", value->licenseType);
      PrintUInt("      Current License Type:", value->currentLicenseType);
      PrintARInternalIdList("      Group List:", "", &value->groupList);
      PrintARBoolean("      Administrator Flag:", value->adminFlag);
      PrintARBoolean("      SubAdministrator Flag:", value->subadminFlag);
   }
}

/*****************************************************************************/
/*                                                                           */
/*                          PrintARUserInfoStruct                            */
/*                                                                           */
/*****************************************************************************/

void PrintARUserInfoStruct(
ARUserInfoStruct  *value     /* IN; value to print */
)

{
   DriverPrintResult("   User Info Struct:\n");
   PrintChar("      User name:", value->userName);
   PrintARUserLicenseList(&value->licenseInfo);
   PrintARTimestamp("      Connect time:", value->connectTime);
   PrintARTimestamp("      Last access:", value->lastAccess);
   PrintChar("      Email address:", value->emailAddr);
   PrintUInt("      Default notify mechanism:", value->defaultNotifyMech);
}

/*****************************************************************************/
/*                                                                           */
/*                           PrintARUserInfoList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARUserInfoList(
ARUserInfoList *value        /* IN; value to print */
)

{
   unsigned int       i;       /* working index */
   ARUserInfoStruct  *tempPtr; /* working pointer */

   DriverPrintResult("User Info List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->userList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARUserInfoStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARIntegerLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARIntegerLimitsStruct(
ARIntegerLimitsStruct *value  /* IN; value to print */
)

{
   DriverPrintResult("INTEGER\n");
   PrintLong("   Low range :", (int)value->rangeLow);
   PrintLong("   High range:", (int)value->rangeHigh);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARDateLimitsStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARDateLimitsStruct(
ARDateLimitsStruct *value  /* IN; value to print */
)

{
   DriverPrintResult("DATE\n");

   PrintARDate("   Minimum date :", value->minDate);
   PrintARDate("   Maximum date :", value->maxDate);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARRealLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARRealLimitsStruct(
ARRealLimitsStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("REAL\n");
   PrintReal("   Low range :", value->rangeLow);
   PrintReal("   High range:", value->rangeHigh);
   PrintInt("   Precision:", value->precision);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARCharLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARCharLimitsStruct(
ARCharLimitsStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("CHARACTER\n");
   PrintUInt("   Length Units:", value->lengthUnits);
   PrintUInt("   Maximum length:", value->maxLength);
   PrintUInt("   Menu style:", value->menuStyle);
   PrintUInt("   QBE match operation:", value->qbeMatchOperation);
   PrintChar("   Character menu:", value->charMenu);
   PrintChar("   Pattern:", value->pattern);
   PrintUInt("   FullText Options:", value->fullTextOptions);
   PrintUInt("   CLOB Storage Option:", value->storageOptionForCLOB);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARDiaryLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARDiaryLimitsStruct(
ARDiaryLimitsStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("DIARY\n");
   PrintUInt("   FullText Options:", value->fullTextOptions);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintAREnumItemList                           */
/*                                                                           */
/*****************************************************************************/

void PrintAREnumItemList(
char           *header,       /* IN; header string for the value */
char           *header2,      /* IN; header string for 2nd+ line */
AREnumItemList *value         /* IN; value to print */
)

{
   char               buff[AR_MAX_NAME_SIZE + 20]; /* buffer to build output */
   unsigned int       i;                           /* working index */
   unsigned int       maxBufSize;                  /* size of output buffer */
   int                ret;                         /* local return variable */
   char               tempBuf[20];                 /* buffer to format int */
   AREnumItemStruct  *tempPtr;                     /* working pointer */

   maxBufSize = sizeof(buff);

   if (strlen(buff) >= maxBufSize)
   {
      strncpy(buff, header, maxBufSize - 1);
      buff[maxBufSize - 1] = '\0';
   }
   else
   {
      ret = snprintf(buff, maxBufSize - 1, "%s (%u)  ", header, value->numItems);
      if (ret > 0)
      {
         if (value->numItems != 0)
         {
            tempPtr = value->enumItemList;
            for (i = 0; i < value->numItems; i++)
            {
               if (i != 0)
                  strcat(buff, ", ");
               sprintf(tempBuf, " (%u)", (unsigned) tempPtr->itemNumber);
               if ((strlen(buff) + strlen(tempPtr->itemName) +
                    strlen(tempBuf)) >= (maxBufSize - 3))
               {
                  buff[strlen(buff) - 1] = '\n'; /* over last blank */
                  DriverPrintResult("%s", buff);
                  if (strlen(header2) >= maxBufSize)
                  {
                     strncpy(buff, header2, maxBufSize - 1);
                     buff[maxBufSize - 1] = '\0';
                  }
                  else
                     strcpy(buff, header2);
               }
               ret = snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3,
                              "%s%s", tempPtr->itemName, tempBuf);
               if (ret < 0)
                  buff[maxBufSize - 1] = '\0';
               else
                  tempPtr++;
            }
         }
      }
      else
         buff[maxBufSize - 1] = '\0';
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintAREnumQueryStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintAREnumQueryStruct(
char              *header,    /* IN; header string for the value */
char              *header2,   /* IN; header string for 2nd+ line */
AREnumQueryStruct *value      /* IN; value to print */
)

{
   char                 buff[55];  /* temporary buffer */

   if (strlen(header2) < (size_t) 45)
   {
      strcpy(buff, header2);
      strcat(buff, "   ");
   }
   else  /* reset to 0 spaces */
      buff[0] = '\0';

   DriverPrintResult("%s\n", header);
   DriverPrintResult("%s", header2);
   PrintChar("   Schema Name:", value->schema);
   DriverPrintResult("%s", header2);
   PrintChar("   Server Name:", value->server);
   PrintARQualifierStruct(buff, &value->qualifier);
   DriverPrintResult("%s", header2);
   PrintLong("   Name Field Id:", value->nameField);
   DriverPrintResult("%s", header2);
   PrintLong("   Number Field Id:", value->nameField);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARAttachLimitsStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARAttachLimitsStruct(
ARAttachLimitsStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("ATTACHMENT\n");
   PrintULong("   Maximum size:", value->maxSize);
   PrintUInt("   Attachment Type:", value->attachType);
   PrintUInt("   FullText Options:", value->fullTextOptions);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARTableLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARTableLimitsStruct(
char                 *header,   /* IN; header of the value */
ARTableLimitsStruct  *value     /* IN; value to print */
)

{
   DriverPrintResult("%s\n", header);
   PrintChar("   Schema Name:", value->schema);
   PrintChar("   Server Name:", value->server);
   PrintARQualifierStruct("   ", &value->qualifier);
   PrintUInt("   Number of Columns:", value->numColumns);
   PrintUInt("   Max Rows to Retrieve:", value->maxRetrieve);
   PrintChar("   Sample Schema Name:", value->sampleSchema);
   PrintChar("   Sample Server Name:", value->sampleServer);
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARColumnLimitsStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARColumnLimitsStruct(
char                  *header,  /* IN; header of the value */
ARColumnLimitsStruct  *value    /* IN; value to print */
)

{
   DriverPrintResult("%s\n", header);
   PrintULong("   Parent Table FieldId:", value->parent);
   PrintULong("   DataField FieldId:", value->dataField);
   PrintUInt("   DataSource:", value->dataSource);
   PrintUInt("   Length to display:", value->colLength);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARDecimalLimitsStruct                        */
/*                                                                           */
/*****************************************************************************/

void PrintARDecimalLimitsStruct(
ARDecimalLimitsStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("DECIMAL\n");
   PrintChar("   Low range:", value->rangeLow);
   PrintChar("   High range:", value->rangeHigh);
   PrintInt("   Precision:", value->precision);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCurrencyDetailStruct                       */
/*                                                                           */
/*****************************************************************************/

void PrintARCurrencyDetailStruct(
ARCurrencyDetailStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("CURRENCY INFO\n");
   PrintChar("   Currency Code:", value->currencyCode);
   PrintInt("   Precision:", value->precision);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCurrencyLimitsStruct                       */
/*                                                                           */
/*****************************************************************************/

void PrintARCurrencyLimitsStruct(
ARCurrencyLimitsStruct *value     /* IN; value to print */
)

{
   unsigned int   i;

   DriverPrintResult("CURRENCY\n");
   PrintChar("   Low range :", value->rangeLow);
   PrintChar("   High range:", value->rangeHigh);
   PrintInt("   Precision:", value->precision);
   DriverPrintResult("   Functional Currencies: %u items\n",
                     value->functionalCurrencies.numItems);
   for (i = 0; i < value->functionalCurrencies.numItems; i++)
   {
      PrintARCurrencyDetailStruct(&value->functionalCurrencies.currencyDetailList[i]);
   }

   DriverPrintResult("   Allowable Currencies: %u items\n",
                     value->allowableCurrencies.numItems);
   for (i = 0; i < value->allowableCurrencies.numItems; i++)
   {
      PrintARCurrencyDetailStruct(&value->allowableCurrencies.currencyDetailList[i]);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARViewLimits                              */
/*                                                                           */
/*****************************************************************************/

void PrintARViewLimits(
ARViewLimits  *value    /* IN; value to print */
)

{
   DriverPrintResult("VIEW\n");
   PrintUInt("   Maximum length:", value->maxLength);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARDisplayLimits                           */
/*                                                                           */
/*****************************************************************************/

void PrintARDisplayLimits(
ARDisplayLimits *value    /* IN; value to print */
)

{
   DriverPrintResult("DISPLAY\n");
   PrintUInt("   Length Units:", value->lengthUnits);
   PrintUInt("   Maximum length:", value->maxLength);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFieldLimitStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldLimitStruct(
ARFieldLimitStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("Field Limit Struct: \n");
   switch (value->dataType)
   {
      case AR_FIELD_LIMIT_NONE  :
         DriverPrintResult("   NO LIMITS\n");
         break;
      case AR_DATA_TYPE_INTEGER :
         PrintARIntegerLimitsStruct(&value->u.intLimits);
         break;
      case AR_DATA_TYPE_REAL    :
         PrintARRealLimitsStruct(&value->u.realLimits);
         break;
      case AR_DATA_TYPE_CHAR    :
         PrintARCharLimitsStruct(&value->u.charLimits);
         break;
      case AR_DATA_TYPE_DIARY   :
         PrintARDiaryLimitsStruct(&value->u.diaryLimits);
         break;
      case AR_DATA_TYPE_ENUM    :
         DriverPrintResult("ENUMERATED\n");
         switch (value->u.enumLimits.listStyle)
         {
            case AR_ENUM_STYLE_REGULAR :
               PrintARNameList("   Enum values:", "      ",
                               &value->u.enumLimits.u.regularList);
               break;
            case AR_ENUM_STYLE_CUSTOM  :
               PrintAREnumItemList("   Enum values:", "      ",
                                   &value->u.enumLimits.u.customList);
               break;
            case AR_ENUM_STYLE_QUERY   :
               PrintAREnumQueryStruct("   Enum values query:", "      ",
                                      &value->u.enumLimits.u.queryList);
               break;
         }
         break;
      case AR_DATA_TYPE_TIME    :
         DriverPrintResult("TIMESTAMP\n");
         break;
      case AR_DATA_TYPE_BITMASK :
         DriverPrintResult("BITMASK\n");
         switch (value->u.maskLimits.listStyle)
         {
            case AR_ENUM_STYLE_REGULAR :
               PrintARNameList("   Bitmask values:", "      ",
                               &value->u.maskLimits.u.regularList);
               break;
            case AR_ENUM_STYLE_CUSTOM  :
               PrintAREnumItemList("   Bitmask values:", "      ",
                                   &value->u.maskLimits.u.customList);
               break;
            case AR_ENUM_STYLE_QUERY   :
               PrintAREnumQueryStruct("   Bitmask values query:", "      ",
                                      &value->u.maskLimits.u.queryList);
               break;
         }
         break;
      case AR_DATA_TYPE_ATTACH :
         PrintARAttachLimitsStruct(&value->u.attachLimits);
         break;
      case AR_DATA_TYPE_DECIMAL:
         PrintARDecimalLimitsStruct(&value->u.decimalLimits);
         break;
      case AR_DATA_TYPE_CURRENCY:
         PrintARCurrencyLimitsStruct(&value->u.currencyLimits);
         break;
      case AR_DATA_TYPE_TABLE   :
         PrintARTableLimitsStruct("   Table values:", &value->u.tableLimits);
         break;
      case AR_DATA_TYPE_COLUMN  :
         PrintARColumnLimitsStruct("   Column values:", &value->u.columnLimits);
         break;
      case AR_DATA_TYPE_VIEW    :
         PrintARViewLimits(&value->u.viewLimits);
         break;
      case AR_DATA_TYPE_DISPLAY  :
         PrintARDisplayLimits(&value->u.displayLimits);
         break;
      case AR_DATA_TYPE_DATE :
         PrintARDateLimitsStruct(&value->u.dateLimits);
         break;
      case AR_DATA_TYPE_TIME_OF_DAY    :
         DriverPrintResult("TIMEOFDAY\n");
         break;
      default                   :
         DriverPrintResult("   <undefined datatype = %u>\n", value->dataType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          PrintARFieldLimitList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldLimitList(
char              *header,    /* IN; header string for the value */
ARFieldLimitList  *value      /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARFieldLimitStruct(&value->fieldLimitList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCharMenuItemStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuItemStruct(
char                 *indent,     /* IN; string of blanks for indenting */
ARCharMenuItemStruct *value       /* IN; value to print */
)

{
   char  buff[55];              /* temporary buffer */

   DriverPrintResult("%sChar Menu Item Struct:\n", indent);
   DriverPrintResult("%s", indent);
   PrintChar("   Label:", value->menuLabel);
   DriverPrintResult("%s", indent);
   PrintUInt("   Type:", value->menuType);
   if (value->menuType == AR_MENU_TYPE_VALUE)
   {
      DriverPrintResult("%s", indent);
      PrintChar("   Value:", value->u.menuValue);
   }
   else if (value->menuType == AR_MENU_TYPE_MENU)
   {
      if (strlen(indent) < (size_t) 50)
      {
         strcpy(buff, indent);
         strcat(buff, "   ");
      }
      else  /* reset to 0 spaces */
         buff[0] = '\0';
      PrintARCharMenuStruct(buff, value->u.childMenu);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARCharMenuQueryStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuQueryStruct(
char                  *indent,     /* IN; string of blanks for indenting */
ARCharMenuQueryStruct *value       /* IN; value to print */
)

{
   char  buff[80];            /* temporary buffer */
   int   i;                   /* working index */

   buff[0] = '\0';

   DriverPrintResult("%sChar Menu Query Struct:\n", indent);
   DriverPrintResult("%s", indent);
   PrintChar("   Schema name:", value->schema);
   DriverPrintResult("%s", indent);
   PrintChar("   Server name:", value->server);
   PrintARQualifierStruct(indent, &value->qualifier);
   for (i = 0; i < 5; i++)
   {
      if (value->labelField[i] == 0)
         break;
      if (i == 0)
         sprintf(&buff[strlen(buff)], "%u", (unsigned) value->labelField[i]);
      else
         sprintf(&buff[strlen(buff)], ", %u", (unsigned) value->labelField[i]);
   }
   DriverPrintResult("%s   Label Field Id: %s\n", indent, buff);
   DriverPrintResult("%s", indent);
   PrintLong("   Value Field Id:", value->valueField);
   DriverPrintResult("%s", indent);
   PrintARBoolean("   Sort on Label:", value->sortOnLabel);
   DriverPrintResult("%s", indent);
   PrintChar("   Sample Schema Name:", value->sampleSchema);
   DriverPrintResult("%s", indent);
   PrintChar("   Sample Server Name:", value->sampleServer);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCharMenuSQLStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuSQLStruct(
char                  *indent,     /* IN; string of blanks for indenting */
ARCharMenuSQLStruct   *value       /* IN; value to print */
)

{
   char  buff[80];            /* temporary buffer */
   int   i;                   /* working index */

   buff[0] = '\0';

   DriverPrintResult("%sChar Menu SQL Struct:\n", indent);
   DriverPrintResult("%s", indent);
   PrintChar("   Server name:", value->server);
   DriverPrintResult("%s", indent);
   PrintChar("   SQL Command:", value->sqlCommand);
   for (i = 0; i < 5; i++)
   {
      if (value->labelIndex[i] == 0)
         break;
      if (i == 0)
         sprintf(&buff[strlen(buff)], "%u", (unsigned) value->labelIndex[i]);
      else
         sprintf(&buff[strlen(buff)], ", %u", (unsigned) value->labelIndex[i]);
   }
   DriverPrintResult("%s   Label Index: %s\n", indent, buff);
   DriverPrintResult("%s", indent);
   PrintLong("   Value Index:", value->valueIndex);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCharMenuSSStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuSSStruct(
char                  *indent,    /* IN; string of blanks for indenting */
ARCharMenuSSStruct    *value      /* IN; value to print */
)

{
   DriverPrintResult("%sChar Menu SS Struct:\n", indent);
   DriverPrintResult("%s", indent);
   PrintChar("   Server name:", value->server);
   DriverPrintResult("%s", indent);
   PrintChar("   Menu Name:", value->menuName);
   DriverPrintResult("%s", indent);
   PrintChar("   Schema:", value->schema);
   DriverPrintResult("%s", indent);
   PrintARFieldValueList(&value->parameterList);
   DriverPrintResult("%s", indent);
   PrintARFieldValueList(&value->keywordList);
   DriverPrintResult("%s   ExternList:", indent);
   PrintARQualifierList("   ", &value->externList);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARCharMenuDDStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuDDStruct(
char                  *indent,     /* IN; string of blanks for indenting */
ARCharMenuDDStruct    *value       /* IN; value to print */
)

{
   DriverPrintResult("%sChar Menu Data-Dictionary Struct:\n", indent);
   DriverPrintResult("%s", indent);
   PrintChar("   Server name: ", value->server);
   DriverPrintResult("%s", indent);
   PrintUInt("   Name type: ", value->nameType);
   DriverPrintResult("%s", indent);
   PrintUInt("   Value format: ", value->valueFormat);
   DriverPrintResult("%s", indent);
   PrintUInt("   Struct type: ", value->structType);
   switch (value->structType)
   {
      case AR_CHAR_MENU_DD_NONE:
         break;
      case AR_CHAR_MENU_DD_FORM:
         DriverPrintResult("%s", indent);
         PrintUInt("   Schema type: ", value->u.formDefn.schemaType);
         DriverPrintResult("%s", indent);
         PrintARBoolean("   Include Hidden Form: ", value->u.formDefn.includeHidden);
         break;
      case AR_CHAR_MENU_DD_FIELD:
         DriverPrintResult("%s", indent);
         PrintUInt("   Field type: ", value->u.fieldDefn.fieldType);
         DriverPrintResult("%s", indent);
         PrintChar("   Schema:", value->u.fieldDefn.schema);
         break;
      default:
         DriverPrintResult("%s", indent);
         DriverPrintResult("   <undefined DD Menu struct type = %u>\n",
                           value->structType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARCharMenuList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuList(
char           *indent,       /* IN; string of blanks for indenting */
ARCharMenuList *value         /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%sCharacter Menu List : %u items\n", indent,
                     value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARCharMenuItemStruct(indent, &value->charMenuList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARCharMenuStruct                           */
/*                                                                           */
/*****************************************************************************/

void PrintARCharMenuStruct(
char             *indent,     /* IN; string of blanks for indenting */
ARCharMenuStruct *value       /* IN; value to print */
)

{
   char  buff[55];            /* temporary buffer */

   if (strlen(indent) < (size_t) 50)
   {
      strcpy(buff, indent);
      strcat(buff, "   ");
   }
   else  /* reset to 0 spaces */
      buff[0] = '\0';

   DriverPrintResult("%sChar Menu Struct:\n", indent);
   DriverPrintResult("%sType of Menu:  ", buff);
   switch (value->menuType)
   {
      case AR_CHAR_MENU_NONE  :
         DriverPrintResult("NONE\n");
         break;
      case AR_CHAR_MENU_LIST  :
         DriverPrintResult("LIST\n");
         PrintARCharMenuList(buff, &value->u.menuList);
         break;
      case AR_CHAR_MENU_QUERY :
         DriverPrintResult("QUERY\n");
         PrintARCharMenuQueryStruct(buff, &value->u.menuQuery);
         break;
      case AR_CHAR_MENU_FILE  :
         DriverPrintResult("FILE\n");
         DriverPrintResult("%s   File Location:  ", buff);
         switch (value->u.menuFile.fileLocation)
         {
            case AR_MENU_FILE_SERVER :
               DriverPrintResult("SERVER\n");
               break;
            case AR_MENU_FILE_CLIENT :
               DriverPrintResult("CLIENT\n");
               break;
            default                   :
               DriverPrintResult("   <undefined file location = %u>\n",
                                 value->u.menuFile.fileLocation);
               break;
         }
         DriverPrintResult("%s   Filename:  %s\n", buff,
                           value->u.menuFile.filename);
         break;
      case AR_CHAR_MENU_SQL   :
         DriverPrintResult("SQL\n");
         PrintARCharMenuSQLStruct(buff, &value->u.menuSQL);
         break;
      case AR_CHAR_MENU_SS:
         DriverPrintResult("Server-side\n");
         PrintARCharMenuSSStruct(buff, &value->u.menuSS);
         break;
      case AR_CHAR_MENU_DATA_DICTIONARY:
         DriverPrintResult("Data-dictionary\n");
         PrintARCharMenuDDStruct(buff, &value->u.menuDD);
         break;
      default                   :
         DriverPrintResult("   <undefined menuType = %u>\n", value->menuType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARStructItemStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARStructItemStruct(
ARStructItemStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("   Structure Item Struct:  ");
   switch (value->type)
   {
      case AR_STRUCT_ITEM_SCHEMA      :
         DriverPrintResult("SCHEMA\n");
         break;
      case AR_STRUCT_ITEM_SCHEMA_DEFN :
         DriverPrintResult("SCHEMA DEFN\n");
         break;
      case AR_STRUCT_ITEM_SCHEMA_VIEW :
         DriverPrintResult("SCHEMA VIEW\n");
         break;
      case AR_STRUCT_ITEM_SCHEMA_MAIL :
         DriverPrintResult("SCHEMA MAIL\n");
         break;
      case AR_STRUCT_ITEM_FILTER      :
         DriverPrintResult("FILTER\n");
         break;
      case AR_STRUCT_ITEM_ACTIVE_LINK :
         DriverPrintResult("ACTIVE LINK\n");
         break;
      case AR_STRUCT_ITEM_ADMIN_EXT   :
         DriverPrintResult("ADMINISTRATOR EXTENSION\n");
         break;
      case AR_STRUCT_ITEM_CHAR_MENU   :
         DriverPrintResult("CHARACTER MENU\n");
         break;
      case AR_STRUCT_ITEM_ESCALATION   :
         DriverPrintResult("ESCALATION\n");
         break;
      case AR_STRUCT_ITEM_DIST_MAP     :
         DriverPrintResult("DISTRIBUTED MAPPING\n");
         break;
      default                         :
         DriverPrintResult("   <undefined type = %u>\n", value->type);
         break;
   }
   PrintChar("      Name:", value->name);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARStructItemList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARStructItemList(
ARStructItemList *value       /* IN; value to print */
)

{
   unsigned int         i;       /* working index */
   ARStructItemStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Structure Item List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->structItemList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARStructItemStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARServerInfoRequestList                       */
/*                                                                           */
/*****************************************************************************/

void PrintARServerInfoRequestList(
char                    *header2,/* IN; header string for 2nd+ line */
ARServerInfoRequestList *value   /* IN; value to print */
)

{
   char           buff[82];    /* buffer to build output in */
   char           buff2[16];   /* buffer to hold a converted number */
   unsigned int   i;           /* working index */
   unsigned int   maxBufSize;  /* size of output buffer */
   unsigned int  *tempPtr;     /* working pointer */

   maxBufSize = sizeof(buff);

   sprintf(buff, "Server Info Request List:  (%u)", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->requestList;
      for (i = 0; i < value->numItems; i++)
      {
         if (i != 0)
            strcat(buff, ", ");
         sprintf(buff2, "%u", *tempPtr);
         if ((strlen(buff) + strlen(buff2)) >= (size_t) 79)
         {
            buff[strlen(buff) - 1] = '\n'; /* over last blank */
            DriverPrintResult("%s", buff);
            if (strlen(header2) >= maxBufSize)
            {
               strncpy(buff, header2, maxBufSize - 1);
               buff[maxBufSize - 1] = '\0';
            }
            else
               strcpy(buff, header2);
         }
         if (snprintf(&buff[strlen(buff)], maxBufSize - strlen(buff) - 3, "%s",
                      buff2) < 0)
            buff[maxBufSize - 1] ='\0';
         else
            tempPtr++;
      }
   }
   DriverPrintResult("%s\n", buff);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARServerInfoStruct                         */
/*                                                                           */
/*****************************************************************************/

void PrintARServerInfoStruct(
ARServerInfoStruct *value     /* IN; value to print */
)

{
   DriverPrintResult("   Server Info Struct:  ");
   switch (value->operation)
   {
      case AR_SERVER_INFO_DB_TYPE        :
         DriverPrintResult("DB TYPE\n");
         break;
      case AR_SERVER_INFO_SERVER_LICENSE :
         DriverPrintResult("SERVER LICENSE\n");
         break;
      case AR_SERVER_INFO_FIXED_LICENSE  :
         DriverPrintResult("FIXED USER LICENSE\n");
         break;
      case AR_SERVER_INFO_VERSION        :
         DriverPrintResult("VERSION\n");
         break;
      case AR_SERVER_INFO_ALLOW_GUESTS   :
         DriverPrintResult("ALLOW GUESTS\n");
         break;
      case AR_SERVER_INFO_GUESTS_RESTRICT_READ:
         DriverPrintResult("GUEST RESTRICTED READ\n");
         break;
      case AR_SERVER_INFO_USE_ETC_PASSWD :
         DriverPrintResult("USE /etc/passwd\n");
         break;
      case AR_SERVER_INFO_XREF_PASSWORDS :
         DriverPrintResult("CROSSREF PASSWORDS\n");
         break;
      case AR_SERVER_INFO_DEBUG_MODE     :
         DriverPrintResult("DEBUG MODE\n");
         break;
      case AR_SERVER_INFO_DB_NAME        :
         DriverPrintResult("DB NAME\n");
         break;
      case AR_SERVER_INFO_DB_PASSWORD    :
         DriverPrintResult("DB PASSWORD\n");
         break;
      case AR_SERVER_INFO_HARDWARE       :
         DriverPrintResult("HARDWARE\n");
         break;
      case AR_SERVER_INFO_OS             :
         DriverPrintResult("OS\n");
         break;
      case AR_SERVER_INFO_SERVER_DIR     :
         DriverPrintResult("SERVER DIRECTORY\n");
         break;
      case AR_SERVER_INFO_DBHOME_DIR     :
         DriverPrintResult("DBHOME DIRECTORY\n");
         break;
      case AR_SERVER_INFO_SET_PROC_TIME  :
         DriverPrintResult("SET FILTER PROCESS TIMEOUT\n");
         break;
      case AR_SERVER_INFO_EMAIL_FROM     :
         DriverPrintResult("EMAIL FROM ADDRESS FOR NOTIFICATIONS\n");
         break;
      case AR_SERVER_INFO_SQL_LOG_FILE   :
         DriverPrintResult("SQL LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_FLOAT_LICENSE  :
         DriverPrintResult("FLOATING USER LICENSE\n");
         break;
      case AR_SERVER_INFO_FLOAT_TIMEOUT  :
         DriverPrintResult("FLOATING LICENSE TIMEOUT\n");
         break;
      case AR_SERVER_INFO_UNQUAL_QUERIES :
         DriverPrintResult("ALLOW UNQUALIFIED QUERIES\n");
         break;
      case AR_SERVER_INFO_FILTER_LOG_FILE:
         DriverPrintResult("FILTER LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_USER_LOG_FILE  :
         DriverPrintResult("USER LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_REM_SERV_ID    :
         DriverPrintResult("AR SYSTEM SERVER ID\n");
         break;
      case AR_SERVER_INFO_MULTI_SERVER   :
         DriverPrintResult("MULTI SERVER FLAG\n");
         break;
      case AR_SERVER_INFO_EMBEDDED_SQL   :
         DriverPrintResult("EMBEDDED SQL FLAG\n");
         break;
      case AR_SERVER_INFO_MAX_SCHEMAS    :
         DriverPrintResult("MAX SCHEMAS LICENSED\n");
         break;
      case AR_SERVER_INFO_DB_VERSION     :
         DriverPrintResult("DB VERSION\n");
         break;
      case AR_SERVER_INFO_MAX_ENTRIES    :
         DriverPrintResult("MAX ENTRIES PER QUERY\n");
         break;
      case AR_SERVER_INFO_MAX_F_DAEMONS  :
         DriverPrintResult("MAX FAST PROCESSES\n");
         break;
      case AR_SERVER_INFO_MAX_L_DAEMONS  :
         DriverPrintResult("MAX LIST PROCESSES\n");
         break;
      case AR_SERVER_INFO_ESCALATION_LOG_FILE:
         DriverPrintResult("ESCALATION LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_ALERT_LOG_FILE:
         DriverPrintResult("ALERT LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_ESCL_DAEMON    :
         DriverPrintResult("ESCALATION DAEMON\n");
         break;
      case AR_SERVER_INFO_SUBMITTER_MODE :
         DriverPrintResult("SUBMITTER MODE\n");
         break;
      case AR_SERVER_INFO_API_LOG_FILE   :
         DriverPrintResult("API LOG FILE\n");
         break;
      case AR_SERVER_INFO_FTEXT_FIXED    :
         DriverPrintResult("FULL TEXT FIXED LICENSES\n");
         break;
      case AR_SERVER_INFO_FTEXT_FLOAT    :
         DriverPrintResult("FULL TEXT FLOATING LICENSES\n");
         break;
      case AR_SERVER_INFO_FTEXT_TIMEOUT  :
         DriverPrintResult("FULL TEXT FLOATING LICENSE TIMEOUT\n");
         break;
      case AR_SERVER_INFO_RESERV1_A     :
         DriverPrintResult("RESERVED FIXED LICENSES\n");
         break;
      case AR_SERVER_INFO_RESERV1_B     :
         DriverPrintResult("RESERVED FLOATING LICENSES\n");
         break;
      case AR_SERVER_INFO_RESERV1_C     :
         DriverPrintResult("RESERVED BUILDER LICENSES\n");
         break;
      case AR_SERVER_INFO_SERVER_IDENT  :
         DriverPrintResult("UNIQUE SERVER IDENTIFIER\n");
         break;
      case AR_SERVER_INFO_DS_SVR_LICENSE :
         DriverPrintResult("DISTRIBUTED SERVER LICENSE\n");
         break;
      case AR_SERVER_INFO_DS_MAPPING :
         DriverPrintResult("DISTRIBUTED SERVER MAPPING SCHEMA\n");
         break;
      case AR_SERVER_INFO_DS_PENDING :
         DriverPrintResult("DISTRIBUTED SERVER PENDING SCHEMA\n");
         break;
      case AR_SERVER_INFO_DS_POOL :
         DriverPrintResult("DISTRIBUTED SERVER POOL SCHEMA\n");
         break;
      case AR_SERVER_INFO_DS_RPC_SOCKET :
         DriverPrintResult("DISTRIBUTED SERVER RPC SOCKET\n");
         break;
      case AR_SERVER_INFO_DS_LOG_FILE   :
         DriverPrintResult("DISTRIBUTED SERVER LOG FILE\n");
         break;
      case AR_SERVER_INFO_SUPPRESS_WARN :
         DriverPrintResult("SUPPRESSED WARNINGS\n");
         break;
      case AR_SERVER_INFO_HOSTNAME      :
         DriverPrintResult("SERVER HOSTNAME\n");
         break;
      case AR_SERVER_INFO_FULL_HOSTNAME :
         DriverPrintResult("SERVER FULL (DNS) HOSTNAME\n");
         break;
      case AR_SERVER_INFO_SAVE_LOGIN    :
         DriverPrintResult("SAVE LOGIN\n");
         break;
      case AR_SERVER_INFO_U_CACHE_CHANGE:
         DriverPrintResult("USER CACHE CHANGE TIME\n");
         break;
      case AR_SERVER_INFO_G_CACHE_CHANGE:
         DriverPrintResult("GROUP CACHE CHANGE TIME\n");
         break;
      case AR_SERVER_INFO_STRUCT_CHANGE :
         DriverPrintResult("STRUCTURE CHANGE TIME\n");
         break;
      case AR_SERVER_INFO_CASE_SENSITIVE:
         DriverPrintResult("CASE SENSITIVITY\n");
         break;
      case AR_SERVER_INFO_SERVER_LANG   :
         DriverPrintResult("SERVER LANGUAGE\n");
         break;
      case AR_SERVER_INFO_ADMIN_ONLY    :
         DriverPrintResult("ADMIN ONLY MODE\n");
         break;
      case AR_SERVER_INFO_CACHE_LOG_FILE :
         DriverPrintResult("SHARED CACHE LOG FILE\n");
         break;
      case AR_SERVER_INFO_FLASH_DAEMON   :
         DriverPrintResult("FLASHBOARDS DAEMON\n");
         break;
      case AR_SERVER_INFO_THREAD_LOG_FILE:
         DriverPrintResult("THREAD LOG FILE\n");
         break;
      case AR_SERVER_INFO_ADMIN_TCP_PORT :
         DriverPrintResult("ADMIN SERVER TCP PORT\n");
         break;
      case AR_SERVER_INFO_ESCL_TCP_PORT  :
         DriverPrintResult("ESCALATION SERVER TCP PORT\n");
         break;
      case AR_SERVER_INFO_FAST_TCP_PORT  :
         DriverPrintResult("FAST SERVERS TCP PORT (initial port)\n");
         break;
      case AR_SERVER_INFO_LIST_TCP_PORT  :
         DriverPrintResult("LIST SERVERS TCP PORT (initial port)\n");
         break;
      case AR_SERVER_INFO_FLASH_TCP_PORT :
         DriverPrintResult("FLASHBOARDS SERVER TCP PORT\n");
         break;
      case AR_SERVER_INFO_TCD_TCP_PORT   :
         DriverPrintResult("TCD SERVER TCP PORT\n");
         break;
      case AR_SERVER_INFO_DSO_DEST_PORT  :
         DriverPrintResult("DSO SERVER DESTINATION TCP PORT\n");
         break;
      case AR_SERVER_INFO_INFORMIX_DBN   :
         DriverPrintResult("INFORMIX DB NAME\n");
         break;
      case AR_SERVER_INFO_INFORMIX_TBC   :
         DriverPrintResult("INFORMIX ONCONFIG\n");
         break;
      case AR_SERVER_INFO_INGRES_VNODE   :
         DriverPrintResult("INGRES VNODE\n");
         break;
      case AR_SERVER_INFO_ORACLE_SID     :
         DriverPrintResult("ORACLE SID\n");
         break;
      case AR_SERVER_INFO_ORACLE_CURSOR_SHARE:
         DriverPrintResult("ORACLE CURSOR SHARING\n");
         break;
      case AR_SERVER_INFO_ORACLE_TWO_T   :
         DriverPrintResult("ORACLE TWO TASK\n");
         break;
      case AR_SERVER_INFO_ORACLE_CLOB_STORE_INROW   :
         DriverPrintResult("ORACLE CLOB STORED IN ROW\n");
         break;
      case AR_SERVER_INFO_SYBASE_CHARSET :
         DriverPrintResult("SYBASE CHARSET\n");
         break;
      case AR_SERVER_INFO_SYBASE_SERV    :
         DriverPrintResult("SYBASE DB SERVER NAME\n");
         break;
      case AR_SERVER_INFO_SHARED_MEM     :
         DriverPrintResult("SHARED MEMORY\n");
         break;
      case AR_SERVER_INFO_SHARED_CACHE   :
         DriverPrintResult("SHARED CACHE\n");
         break;
      case AR_SERVER_INFO_CACHE_SEG_SIZE :
         DriverPrintResult("SHARED CACHE SEGMENT SIZE\n");
         break;
      case AR_SERVER_INFO_DB_USER        :
         DriverPrintResult("DB USER\n");
         break;
      case AR_SERVER_INFO_NFY_TCP_PORT :
         DriverPrintResult("NOTIFIER SERVER TCP PORT\n");
         break;
      case AR_SERVER_INFO_FILT_MAX_TOTAL :
         DriverPrintResult("FILTER MAX TOTAL NUMBER\n");
         break;
      case AR_SERVER_INFO_FILT_MAX_STACK :
         DriverPrintResult("FILTER MAX STACK SIZE\n");
         break;
      case AR_SERVER_INFO_DEFAULT_ORDER_BY :
         DriverPrintResult("DEFAULT ORDER BY ENTRY ID\n");
         break;
      case AR_SERVER_INFO_DELAYED_CACHE :
         DriverPrintResult("DELAYED CACHE\n");
         break;
      case AR_SERVER_INFO_DSO_MERGE_STYLE :
         DriverPrintResult("DSO MERGE STYLE\n");
         break;
      case AR_SERVER_INFO_EMAIL_LINE_LEN :
         DriverPrintResult("EMAIL LINE LENGTH\n");
         break;
      case AR_SERVER_INFO_EMAIL_SYSTEM :
         DriverPrintResult("EMAIL SYSTEM\n");
         break;
      case AR_SERVER_INFO_INFORMIX_RELAY_MOD :
         DriverPrintResult("INFORMIX RELAY MODULE\n");
         break;
      case AR_SERVER_INFO_PS_RPC_SOCKET :
         DriverPrintResult("PRIVATE SERVER RPC SOCKETS / PORTS\n");
         break;
      case AR_SERVER_INFO_REGISTER_PORTMAPPER :
         DriverPrintResult("REGISTER PORTMAPPER\n");
         break;
      case AR_SERVER_INFO_SERVER_NAME :
         DriverPrintResult("SERVER NAME\n");
         break;
      case AR_SERVER_INFO_DBCONF :
         DriverPrintResult("DBCONF\n");
         break;
      case AR_SERVER_INFO_APPL_PENDING :
         DriverPrintResult("APPLICATION PENDING SCHEMA\n");
         break;
      case AR_SERVER_INFO_AP_RPC_SOCKET :
         DriverPrintResult("APPROVAL SERVER RPC SOCKET\n");
         break;
      case AR_SERVER_INFO_AP_LOG_FILE   :
         DriverPrintResult("APPROVAL SERVER LOG FILE\n");
         break;
      case AR_SERVER_INFO_AP_DEFN_CHECK :
         DriverPrintResult("APPLICATION DEFINITION CHECK INTERVAL\n");
         break;
      case AR_SERVER_INFO_MAX_LOG_FILE_SIZE:
         DriverPrintResult("MAXIMUM SIZE OF LOG FILES\n");
         break;
      case AR_SERVER_INFO_CLUSTERED_INDEX :
         DriverPrintResult("CLUSTERED INDEX\n");
         break;
      case AR_SERVER_INFO_ACTLINK_DIR :
         DriverPrintResult("ACTIVE LINK RUN PROCESS DIRECTORY\n");
         break;
      case AR_SERVER_INFO_ACTLINK_SHELL :
         DriverPrintResult("ACTIVE LINK RUN PROCESS SHELL\n");
         break;
      case AR_SERVER_INFO_USER_CACHE_UTILS :
         DriverPrintResult("ALLOW USER CACHE UTILITIES\n");
         break;
      case AR_SERVER_INFO_EMAIL_TIMEOUT :
         DriverPrintResult("EMAIL TIMEOUT\n");
         break;
      case AR_SERVER_INFO_EXPORT_VERSION :
         DriverPrintResult("SERVER EXPORT VERSION\n");
         break;
      case AR_SERVER_INFO_ENCRYPT_AL_SQL :
         DriverPrintResult("ENCRYPT AL DIRECT SQL\n");
         break;
      case AR_SERVER_INFO_SCC_ENABLED         :
         DriverPrintResult("SCC ENABLED\n");
         break;
      case AR_SERVER_INFO_SCC_PROVIDER_NAME   :
         DriverPrintResult("SCC PROVIDER NAME\n");
         break;
      case AR_SERVER_INFO_SCC_TARGET_DIR      :
         DriverPrintResult("SCC TARGET DIRECTORY\n");
         break;
      case AR_SERVER_INFO_SCC_COMMENT_CHECKIN :
         DriverPrintResult("SCC COMMENTS ON CHECKIN\n");
         break;
      case AR_SERVER_INFO_SCC_COMMENT_CHECKOUT:
         DriverPrintResult("SCC COMMENTS ON CHECKOUT\n");
         break;
      case AR_SERVER_INFO_SCC_INTEGRATION_MODE:
         DriverPrintResult("SCC INTEGRATION MODE\n");
         break;
      case AR_SERVER_INFO_EA_RPC_SOCKET    :
         DriverPrintResult("EXTERNAL AUTHENTICATION RPC SOCKET\n");
         break;
      case AR_SERVER_INFO_EA_RPC_TIMEOUT   :
         DriverPrintResult("EXTERNAL AUTHENTICATION RPC TIMEOUT\n");
         break;
      case AR_SERVER_INFO_USER_INFO_LISTS:
         DriverPrintResult("USER HASH LISTS\n");
         break;
      case AR_SERVER_INFO_USER_INST_TIMEOUT:
         DriverPrintResult("USER INSTANCE TIMEOUT\n");
         break;
      case AR_SERVER_INFO_DEBUG_GROUPID :
         DriverPrintResult("DEBUG FROM CLIENT GROUP ID\n");
         break;
      case AR_SERVER_INFO_APPLICATION_AUDIT:
         DriverPrintResult("APPLICATION AUDIT INFO\n");
         break;
      case AR_SERVER_INFO_EA_SYNC_TIMEOUT  :
         DriverPrintResult("EXTERNAL AUTHENTICATION SYNC TIMEOUT\n");
         break;
      case AR_SERVER_INFO_SERVER_TIME:
         DriverPrintResult("SERVER CURRENT TIME\n");
         break;
      case AR_SERVER_INFO_SVR_SEC_CACHE:
         DriverPrintResult("TIE SECURITY CACHE TO SERVER NAME\n");
         break;
      case AR_SERVER_INFO_LOGFILE_APPEND:
         DriverPrintResult("LOGFILE APPEND\n");
         break;
      case AR_SERVER_INFO_MINIMUM_API_VER :
         DriverPrintResult("MINIMUM API VERSION SUPPORTED\n");
         break;
      case AR_SERVER_INFO_MAX_AUDIT_LOG_FILE_SIZE :
         DriverPrintResult("MAXIMUM AUDIT LOG FILE SIZE\n");
         break;
      case AR_SERVER_INFO_CANCEL_QUERY:
         DriverPrintResult("CANCEL QUERY\n");
         break;
      case AR_SERVER_INFO_MULT_ASSIGN_GROUPS :
         DriverPrintResult("MULTIPLE ASSIGN GROUPS\n");
         break;
      case AR_SERVER_INFO_DSO_PLACEHOLDER_MODE :
         DriverPrintResult("DSO PLACEHOLDER MODE\n");
         break;
      case AR_SERVER_INFO_DSO_POLLING_INTERVAL :
         DriverPrintResult("DSO POLLING INTERVAL\n");
         break;
      case AR_SERVER_INFO_DSO_SOURCE_SERVER :
         DriverPrintResult("DSO SOURCE SERVER\n");
         break;
      case AR_SERVER_INFO_DSO_TIMEOUT_NORMAL  :
         DriverPrintResult("DSO TIMEOUT NORMAL\n");
         break;
      case AR_SERVER_INFO_ENC_PUB_KEY :
         DriverPrintResult("SERVER'S PUBLIC KEY\n");
         break;
      case AR_SERVER_INFO_ENC_PUB_KEY_EXP :
         DriverPrintResult("SERVER'S PUBLIC/PRIVATE KEY EXPIRATION\n");
         break;
      case AR_SERVER_INFO_DSO_TARGET_CONNECTION :
         DriverPrintResult("DSO TARGET CONNECTION INFO\n");
         break;
      case AR_SERVER_INFO_ENC_DATA_KEY_EXP :
         DriverPrintResult("API DATA ENCRYPTION KEY EXPIRATION\n");
         break;
      case AR_SERVER_INFO_ENC_DATA_ENCR_ALG :
         DriverPrintResult("API DATA ENCRYPTION ALGORITHM\n");
         break;
      case AR_SERVER_INFO_ENC_SEC_POLICY :
         DriverPrintResult("SERVER'S SECURITY POLICY\n");
         break;
      case AR_SERVER_INFO_ENC_SESS_H_ENTRIES :
         DriverPrintResult("ENCRYPTED API SESSION HASH ENTRIES\n");
         break;
      case AR_SERVER_INFO_PREFERENCE_PRIORITY   :
         DriverPrintResult("PRIORITY NUMBER OF PREFERENCE SERVER\n");
         break;
      case AR_SERVER_INFO_ORACLE_QUERY_ON_CLOB :
         DriverPrintResult("QUERY ON CLOB\n");
         break;
      case AR_SERVER_INFO_MESSAGE_CAT_SCHEMA:
         DriverPrintResult("MESSAGE CATALOG SCHEMA NAME\n");
         break;
      case AR_SERVER_INFO_ALERT_SCHEMA :
         DriverPrintResult("ALERT EVENTS SCHEMA\n");
         break;
      case AR_SERVER_INFO_LOCALIZED_SERVER :
         DriverPrintResult("LOCALIZED SERVER\n");
         break;
      case AR_SERVER_INFO_SVR_EVENT_LIST :
         DriverPrintResult("SERVER EVENTS LIST\n");
         break;
      case AR_SERVER_INFO_DISABLE_ADMIN_OPERATIONS :
         DriverPrintResult("DISABLE ADMIN OPERATIONS\n");
         break;
      case AR_SERVER_INFO_DISABLE_ESCALATIONS :
         DriverPrintResult("DISABLE ESCALATIONS\n");
         break;
      case AR_SERVER_INFO_DISABLE_ALERTS :
         DriverPrintResult("DISABLE ALERTS\n");
         break;
      case AR_SERVER_INFO_CHECK_ALERT_USERS :
         DriverPrintResult("CHECK ALERT USERS\n");
         break;
      case AR_SERVER_INFO_ALERT_SEND_TIMEOUT :
         DriverPrintResult("ALERT SEND TIMEOUT\n");
         break;
      case AR_SERVER_INFO_ALERT_OUTBOUND_PORT :
         DriverPrintResult("ALERT OUTBOUND PORT\n");
         break;
      case AR_SERVER_INFO_ALERT_SOURCE_AR :
         DriverPrintResult("ALERT ACTION REQUEST SOURCE STRING\n");
         break;
      case AR_SERVER_INFO_ALERT_SOURCE_FB :
         DriverPrintResult("ALERT FLASHBOARDS SOURCE STRING\n");
         break; 
      case AR_SERVER_INFO_ARFORK_LOG_FILE :
         DriverPrintResult("ARFORK LOG FILE\n");
         break;
      case AR_SERVER_INFO_PLUGIN_LOG_FILE :
         DriverPrintResult("PLUGIN LOG FILE\n");
         break;
      case AR_SERVER_INFO_SVR_STATS_REC_MODE :
         DriverPrintResult("SERVER STATISTICS RECORDING MODE\n");
         break;
      case AR_SERVER_INFO_SVR_STATS_REC_INTERVAL :
         DriverPrintResult("SERVER STATISTICS RECORDING INTERVAL\n");
         break;
      case AR_SERVER_INFO_DEFAULT_WEB_PATH :
         DriverPrintResult("DEFAULT WEB PATH\n");
         break;
      case AR_SERVER_INFO_DSO_USER_PASSWD    :
         DriverPrintResult("DSO USER PASSWORD\n");
         break;
      case AR_SERVER_INFO_DSO_TARGET_PASSWD    :
         DriverPrintResult("DSO TARGET PASSWORD\n");
         break;
      case AR_SERVER_INFO_APP_SERVICE_PASSWD    :
         DriverPrintResult("APPLICATION SERVICE PASSWORD\n");
         break;
      case AR_SERVER_INFO_MID_TIER_PASSWD    :
         DriverPrintResult("MID TIER PASSWORD\n");
         break;
      case AR_SERVER_INFO_FILTER_API_RPC_TIMEOUT:
         DriverPrintResult("FILTER API TIMEOUT\n");
         break;
      case AR_SERVER_INFO_DISABLED_CLIENT:
         DriverPrintResult("DISABLED CLIENT\n");
         break;
      case AR_SERVER_INFO_PLUGIN_PASSWD:
         DriverPrintResult("PLUGIN SERVER PASSWORD\n");
         break;
      case AR_SERVER_INFO_PLUGIN_ALIAS:
         DriverPrintResult("PLUGIN ALIAS\n");
         break;
      case AR_SERVER_INFO_PLUGIN_TARGET_PASSWD:
         DriverPrintResult("PLUGIN TARGET PASSWORD\n");
         break;
      case AR_SERVER_INFO_PLUGIN_LOOPBACK_RPC:
         DriverPrintResult("PLUGIN LOOPBACK RPC SOCKET\n");
         break;
      case AR_SERVER_INFO_INIT_FORM:
         DriverPrintResult("INITIAL FORM\n");
         break;
      case AR_SERVER_INFO_IP_NAMES:
         DriverPrintResult("IP NAMES\n");
         break;
      case AR_SERVER_INFO_ENC_PUB_KEY_ALG :
         DriverPrintResult("PUBLIC KEY ENCRYPTION ALGORITHM\n");
         break;
      case AR_SERVER_INFO_DSO_MARK_PENDING_RETRY:
         DriverPrintResult("DSO MARK PENDING RETRY\n");
         break;
      case AR_SERVER_INFO_DSO_CACHE_CHK_INTERVAL:
         DriverPrintResult("DSO CACHE CHECK INTERVAL\n");
         break;
      case AR_SERVER_INFO_DSO_MAIN_POLL_INTERVAL:
         DriverPrintResult("DSO MAIN POLL INTERVAL\n");
         break;
      case AR_SERVER_INFO_DSO_RPCPROG_NUM:
         DriverPrintResult("DSO LOCAL RPC PROGRAM NUMBER\n");
         break;
      case AR_SERVER_INFO_DELAY_RECACHE_TIME:
         DriverPrintResult("DELAY RECACHE TIME\n");
         break;
      case AR_SERVER_INFO_DFLT_ALLOW_CURRENCIES:
         DriverPrintResult("DEFAULT ALLOWABLE CURRENCY LIST\n");
         break;
      case AR_SERVER_INFO_CURRENCY_INTERVAL:
         DriverPrintResult("CURRENCY RATIO CLIENT REFRESH INTERVAL\n");
         break;
      case AR_SERVER_INFO_DB2_DB_ALIAS:
         DriverPrintResult("DB2 DATABASE ALIAS\n");
         break;
      case AR_SERVER_INFO_DB2_SERVER:
         DriverPrintResult("DB2 DATABASE SERVER\n");
         break;
      case AR_SERVER_INFO_DFLT_FUNC_CURRENCIES:
         DriverPrintResult("DEFAULT FUNCTIONAL CURRENCY LIST\n");
         break;
      case AR_SERVER_INFO_EMAIL_IMPORT_FORM:
         DriverPrintResult("EMAIL IMPORT FORM BY DEFAULT\n");
         break;
      case AR_SERVER_INFO_EMAIL_AIX_USE_OLD_EMAIL:
         DriverPrintResult("USE OLD EMAIL ON AIX\n");
         break;
      case AR_SERVER_INFO_TWO_DIGIT_YEAR_CUTOFF :
         DriverPrintResult("TWO DIGIT YEAR CUTOFF\n");
         break;
      case AR_SERVER_INFO_ALLOW_BACKQUOTE_IN_PROCESS:
         DriverPrintResult("ALLOW BACKQUOTE IN PROCESS\n");
         break;
      case AR_SERVER_INFO_DB_CONNECTION_TIMEOUT:
         DriverPrintResult("DB CONNECTION TIMEOUT\n");
         break;
      case AR_SERVER_INFO_DB_CONNECTION_RETRIES:
         DriverPrintResult("DB CONNECTION RETRIES\n");
         break;
      case AR_SERVER_INFO_CURR_PART_VALUE_STR:
         DriverPrintResult("CURRENCY PART VALUE STRING\n");
         break;
      case AR_SERVER_INFO_CURR_PART_TYPE_STR:
         DriverPrintResult("CURRENCY PART TYPE STRING\n");
         break;
      case AR_SERVER_INFO_CURR_PART_DATE_STR:
         DriverPrintResult("CURRENCY PART DATE STRING\n");
         break;
      case AR_SERVER_INFO_HOMEPAGE_FORM:
         DriverPrintResult("HOMEPAGE FORM\n");
         break;
      case AR_SERVER_INFO_DISABLE_FTS_INDEXER:
         DriverPrintResult("DISABLE FTS INDEXER\n");
         break;
      case AR_SERVER_INFO_DB_CHAR_SET:
         DriverPrintResult("AR SYSTEM DATABASE CHARACTER SET\n");
         break;
      case AR_SERVER_INFO_DISABLE_ARCHIVE:
         DriverPrintResult("DISABLE ARCHIVE\n");
         break;
      case AR_SERVER_INFO_SERVERGROUP_MEMBER:
         DriverPrintResult("SERVER GROUP MEMBER\n");
         break;
      case AR_SERVER_INFO_SERVERGROUP_LOG_FILE:
         DriverPrintResult("SERVER GROUP LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_SERVERGROUP_INTERVAL:
         DriverPrintResult("SERVER GROUP CHECK INTERVAL\n");
         break;
      case AR_SERVER_INFO_FLUSH_LOG_LINES:
         DriverPrintResult("FLUSH LOG LINES\n");
         break;
      case AR_SERVER_INFO_PER_THREAD_LOGS:
         DriverPrintResult("PER-THREAD LOGGING\n");
         break;
      case AR_SERVER_INFO_JAVA_VM_OPTIONS:
         DriverPrintResult("JAVA VM OPTIONS\n");
         break;
      case AR_SERVER_INFO_CONFIG_FILE:
         DriverPrintResult("CONFIG FILE NAME\n");
         break;
      case AR_SERVER_INFO_SSTABLE_CHUNK_SIZE:
         DriverPrintResult("SSTABLE CHUNK SIZE\n");
         break;
      case AR_SERVER_INFO_SG_EMAIL_STATE:
         DriverPrintResult("SERVER GROUP EMAIL STATE\n");
         break;
      case AR_SERVER_INFO_SG_FLASHBOARDS_STATE:
         DriverPrintResult("SERVER GROUP FLASHBOARDS STATE\n");
         break;
      case AR_SERVER_INFO_SG_AIE_STATE:
         DriverPrintResult("SERVER GROUP ATRIUM INTEGRATION ENGINE STATE\n");
         break;
      case AR_SERVER_INFO_SERVERGROUP_NAME:
         DriverPrintResult("SERVER GROUP NAME\n");
         break;
      case AR_SERVER_INFO_SG_ADMIN_SERVER_NAME:
         DriverPrintResult("SERVER GROUP ADMIN SERVER NAME\n");
         break;
      case AR_SERVER_INFO_SG_ADMIN_SERVER_PORT:
         DriverPrintResult("SERVER GROUP ADMIN SERVER PORT\n");
         break; 
      case AR_SERVER_INFO_LOCKED_WKFLW_LOG_MODE :
         DriverPrintResult("SERVER LOCKED WORKFLOW LOG MODE\n");
         break;
      case AR_SERVER_INFO_ROLE_CHANGE:
         DriverPrintResult("ROLE CHANGE TIME\n");
         break;
      case AR_SERVER_INFO_CACHE_MODE:
         DriverPrintResult("CACHE MODE\n");
         break;
      case AR_SERVER_INFO_DB_FREESPACE:
         DriverPrintResult("DB FREESPACE\n");
         break;
      case AR_SERVER_INFO_GENERAL_AUTH_ERR:
         DriverPrintResult("GENERAL AUTH ERR\n");
         break;
      case AR_SERVER_INFO_AUTH_CHAINING_MODE:
         DriverPrintResult("AUTHENTICATION CHAINING MODE\n");
         break;
      case AR_SERVER_INFO_RPC_NON_BLOCKING_IO:
         DriverPrintResult("RPC NON-BLOCKING IO MODE\n");
         break;
      case AR_SERVER_INFO_SYS_LOGGING_OPTIONS:
         DriverPrintResult("SYSTEM LOGGING OPTIONS\n");
         break;
      case AR_SERVER_INFO_EXT_AUTH_CAPABILITIES:
         DriverPrintResult("AREA RETURN DATA CAPABILITIES\n");
         break;
      case AR_SERVER_INFO_DSO_ERROR_RETRY:
         DriverPrintResult("DSO ERROR RETRY OPTION\n");
         break;
      case AR_SERVER_INFO_PREF_SERVER_OPTION:
         DriverPrintResult("PREFERENCE SERVER OPTION\n");
         break;
      case AR_SERVER_INFO_FTINDEXER_LOG_FILE:
         DriverPrintResult("FULL TEXT INDEXER LOG FILENAME\n");
         break;
      case AR_SERVER_INFO_EXCEPTION_OPTION:
         DriverPrintResult("EXCEPTION DIAGNOSTIC OPTION\n");
         break;
      case AR_SERVER_INFO_ERROR_EXCEPTION_LIST:
         DriverPrintResult("ARERROR EXCEPTION LIST\n");
         break;
      case AR_SERVER_INFO_DSO_MAX_QUERY_SIZE :
         DriverPrintResult("DSO MAXIMUM QUERY SIZE\n");
         break;
      case AR_SERVER_INFO_ADMIN_OP_TRACKING :
         DriverPrintResult("ADMIN OPERATION TRACKING\n");
         break;
      case AR_SERVER_INFO_ADMIN_OP_PROGRESS :
         DriverPrintResult("ADMIN OPERATION PROGRESS\n");
         break;
      case AR_SERVER_INFO_PLUGIN_DEFAULT_TIMEOUT:
         DriverPrintResult("PLUGIN IO TIMEOUT\n");
         break;
      case AR_SERVER_INFO_EA_IGNORE_EXCESS_GROUPS :
         DriverPrintResult("EXTAUTH IGNORE EXCESS GROUPS\n");
         break;
      case AR_SERVER_INFO_EA_GROUP_MAPPING :
         DriverPrintResult("EXTAUTH GROUP MAPPING LIST\n");
         break;
      case AR_SERVER_INFO_PLUGIN_LOG_LEVEL:
         DriverPrintResult("PLUGIN LOG LEVEL\n");
         break;
      case AR_SERVER_INFO_FT_THRESHOLD_LOW:
         DriverPrintResult("FULL TEXT SEARCH THRESHOLD LOW\n");
         break;
      case AR_SERVER_INFO_FT_THRESHOLD_HIGH:
         DriverPrintResult("FULL TEXT SEARCH THRESHOLD HIGH\n");
         break;
      case AR_SERVER_INFO_NOTIFY_WEB_PATH:
         DriverPrintResult("EMAIL NOTIFICATION WEB PATH\n");
         break;
      case AR_SERVER_INFO_DISABLE_NON_UNICODE_CLIENTS:
         DriverPrintResult("DISABLE NON-UNICODE CLIENTS\n");
         break;
      case AR_SERVER_INFO_FT_SERVER_NAME:
         DriverPrintResult("FULL TEXT SEARCH SERVER HOST\n");
         break;
      case AR_SERVER_INFO_FT_SERVER_PORT:
         DriverPrintResult("FULL TEXT SEARCH SERVER PORT\n");
         break;
      case AR_SERVER_INFO_FT_COLLECTION_DIR:
         DriverPrintResult("FULL TEXT COLLECTION DIRECTORY\n");
         break;
      case AR_SERVER_INFO_FT_CONFIGURATION_DIR:
         DriverPrintResult("FULL TEXT CONFIGURATION DIRECTORY\n");
         break;
      case AR_SERVER_INFO_FT_TEMP_DIR:
         DriverPrintResult("FULL TEXT TEMP DIRECTORY\n");
         break;
      case AR_SERVER_INFO_FT_REINDEX:
         DriverPrintResult("FULL TEXT REINDEX\n");
         break;
      case AR_SERVER_INFO_FT_DISABLE_SEARCH:
         DriverPrintResult("FULL TEXT DISABLE SEARCH\n");
         break;
      case AR_SERVER_INFO_FT_CASE_SENSITIVITY:
         DriverPrintResult("FULL TEXT CASE SENSITIVITY OPTION\n");
         break;
      case AR_SERVER_INFO_FT_SEARCH_MATCH_OP:
         DriverPrintResult("FULL TEXT SEARCH MATCH OPTION\n");
         break;
      case AR_SERVER_INFO_FT_STOP_WORDS:
         DriverPrintResult("FULL TEXT STOP WORDS\n");
         break;
      case AR_SERVER_INFO_FT_RECOVERY_INTERVAL:
         DriverPrintResult("FULL TEXT RECOVERY INTERVAL\n");
         break;
      case AR_SERVER_INFO_FT_OPTIMIZE_THRESHOLD:
         DriverPrintResult("FULL TEXT INDEXER OPTIMIZE THRESHOLD\n");
         break;
      case AR_SERVER_INFO_MAX_PASSWORD_ATTEMPTS:
         DriverPrintResult("MAX PASSWORD ATTEMPTS ALLOWED\n");
         break;
      case AR_SERVER_INFO_NEXT_ID_BLOCK_SIZE:
         DriverPrintResult("NEXT ID BLOCK SIZE\n");
         break;
      case AR_SERVER_INFO_NEXT_ID_COMMIT:
         DriverPrintResult("NEXT ID COMMIT\n");
         break;
      case AR_SERVER_INFO_RPC_CLIENT_XDR_LIMIT:
         DriverPrintResult("RPC CLIENT XDR SIZE LIMIT\n");
         break;
      case AR_SERVER_INFO_CACHE_DISP_PROP:
         DriverPrintResult("CACHE DISPLAY PROPERTY OPTION\n");
         break;
      case AR_SERVER_INFO_USE_CON_NAME_IN_STATS:
         DriverPrintResult("USE SERVER CONNECT NAME IN STATS\n");
         break;
      case AR_SERVER_INFO_DB_MAX_ATTACH_SIZE:
         DriverPrintResult("DB MAXIMUM ATTACHMENT SIZE\n");
         break;
      case AR_SERVER_INFO_DB_MAX_TEXT_SIZE:
         DriverPrintResult("DB MAXIMUM TEXT SIZE\n");
         break;
      case AR_SERVER_INFO_GUID_PREFIX:
         DriverPrintResult("GUID PREFIX\n");
         break;
      case AR_SERVER_INFO_MULTIPLE_ARSYSTEM_SERVERS:
         DriverPrintResult("MULTIPLE ARSYSTEM SERVERS\n");
         break;
      case AR_SERVER_INFO_ORACLE_BULK_FETCH_COUNT:
         DriverPrintResult("ORACLE BULK FETCH COUNT\n");
         break;
      case AR_SERVER_INFO_MINIMUM_CMDB_API_VER:
         DriverPrintResult("MINIMUM CMDB API VERSION SUPPORTED\n");
         break;
      case AR_SERVER_INFO_PLUGIN_PORT:
         DriverPrintResult("PLUGIN SERVER DEFAULT PORT\n");
         break;
      case AR_SERVER_INFO_PLUGIN_LIST:
         DriverPrintResult("PLUGIN SERVER LOAD LIST\n");
         break;
      case AR_SERVER_INFO_PLUGIN_PATH_LIST:
         DriverPrintResult("PLUGIN SERVER PATH LIST\n");
         break;
      case AR_SERVER_INFO_SHARED_LIB:
         DriverPrintResult("CMDB SHARED LIBRARY\n");
         break;
      case AR_SERVER_INFO_SHARED_LIB_PATH:
         DriverPrintResult("CMDB SHARED LIBRARY PATH\n");
         break;
      case AR_SERVER_INFO_CMDB_INSTALL_DIR:
         DriverPrintResult("CMDB INSTALL DIRECTORY\n");
         break;
      case AR_SERVER_INFO_RE_LOG_DIR:
         DriverPrintResult("RECONCILIATION ENGINE LOG DIRECTORY\n");
         break;
      case AR_SERVER_INFO_SQL_LOG_FORM:
         DriverPrintResult("SQL LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_API_LOG_FORM:
         DriverPrintResult("API LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_ESCL_LOG_FORM:
         DriverPrintResult("ESCALATION LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_FILTER_LOG_FORM:
         DriverPrintResult("FILTER LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_USER_LOG_FORM:
         DriverPrintResult("USER LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_ALERT_LOG_FORM:
         DriverPrintResult("ALERT LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_SVRGRP_LOG_FORM:
         DriverPrintResult("SERVER GROUP LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_FTINDX_LOG_FORM:
         DriverPrintResult("FULL TEXT INDEX LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_THREAD_LOG_FORM:
         DriverPrintResult("THREAD LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_LOG_TO_FORM:
         DriverPrintResult("LOG TO FORM\n");
         break;
      case AR_SERVER_INFO_FIPS_SERVER_MODE:
         DriverPrintResult("Server FIPS Mode\n");
         break;
      case AR_SERVER_INFO_FIPS_CLIENT_MODE:
         DriverPrintResult("Client FIPS Mode\n");
         break;
      case AR_SERVER_INFO_FIPS_STATUS:
         DriverPrintResult("Server FIPS Status\n");
         break;
      case AR_SERVER_INFO_ENC_LEVEL:
         DriverPrintResult("Encryption Level\n");
         break;
      case AR_SERVER_INFO_ENC_ALGORITHM:
         DriverPrintResult("Encryption Algorithm\n");
         break;
      case AR_SERVER_INFO_FIPS_MODE_INDEX:
         DriverPrintResult("FIPS Mode Index\n");
         break;
      case AR_SERVER_INFO_FIPS_DUAL_MODE_INDEX:
         DriverPrintResult("Client Dual Mode Index\n");
         break;
      case AR_SERVER_INFO_ENC_LEVEL_INDEX:
         DriverPrintResult("Encryption Level Index\n");
         break;
      case AR_SERVER_INFO_RECORD_OBJECT_RELS:
         DriverPrintResult("RECORD OBJECT RELATIONSHIPS\n");
         break;
      case AR_SERVER_INFO_COMMON_LOG_FORM:
         DriverPrintResult("COMMON LOG FORM NAME\n");
         break;
      case AR_SERVER_INFO_LOG_FORM_SELECTED:
         DriverPrintResult("LOG FORM SELECTED\n");
         break;
      case AR_SERVER_INFO_WFD_QUEUES:
         DriverPrintResult("DEBUG QUEUES\n");
         break;
      case AR_SERVER_INFO_MAX_RECURSION_LEVEL:
         DriverPrintResult("MAX RECURSION LEVEL\n");
         break;
      case AR_SERVER_INFO_DISABLE_AUDIT_ONLY_CHANGED_FIELDS:
         DriverPrintResult("DISABLE AUDIT ONLY CHANGED FIELDS\n");
         break;
      case AR_SERVER_INFO_MAX_VENDOR_TEMP_TABLES:
         DriverPrintResult("MAX VENDOR TEMP TABLES\n");
         break;
      case AR_SERVER_INFO_DSO_LOG_LEVEL:
         DriverPrintResult("DSO LOG LEVEL\n");
         break;
      case AR_SERVER_INFO_REGISTRY_LOCATION:
         DriverPrintResult("REGISTRY LOCATION\n");
         break;
      case AR_SERVER_INFO_REGISTRY_USER:
         DriverPrintResult("REGISTRY USER\n");
         break;
      case AR_SERVER_INFO_REGISTRY_PASSWORD:
         DriverPrintResult("REGISTRY PASSWORD\n");
         break;
      case AR_SERVER_INFO_DSO_LOG_ERR_FORM:
         DriverPrintResult("DSO LOG TO PENDING ERRORS FORM\n");
         break;
      case AR_SERVER_INFO_ARSIGNALD_LOG_FILE :
         DriverPrintResult("ARSIGNALD LOG FILE\n");
         break;
      case AR_SERVER_INFO_PRELOAD_NUM_THREADS :
         DriverPrintResult("NUM PRELOAD THREADS\n");
         break;
      case AR_SERVER_INFO_PRELOAD_NUM_SCHEMA_SEGS :
         DriverPrintResult("NUM PRELOAD SCHEMA SEGMENTS\n");
         break;
      case AR_SERVER_INFO_PRELOAD_THREAD_INIT_ONLY :
         DriverPrintResult("PRELOAD THREADS AT INIT ONLY\n");
         break;
      case AR_SERVER_INFO_REG_ENDPOINT_CACHE_FLUSH :
         DriverPrintResult("FLUSH WEBSERVICE REGISTRY ENDPOINT CACHE\n");
         break;
      case AR_SERVER_INFO_CREATE_WKFLW_PLACEHOLDER :
         DriverPrintResult("CREATE WORKFLOW PLACEHOLDER\n");
         break;
      case AR_SERVER_INFO_MFS_TITLE_FIELD_WEIGHT :
         DriverPrintResult("MULTI-FORM SEARCH TITLE FIELD WEIGHT\n");
         break;
      case AR_SERVER_INFO_MFS_ENVIRONMENT_FIELD_WEIGHT :
         DriverPrintResult("MULTI-FORM SEARCH ENVIRONMENT FIELD WEIGHT\n");
         break;
      case AR_SERVER_INFO_MFS_KEYWORDS_FIELD_WEIGHT :
         DriverPrintResult("MULTI-FORM SEARCH KEYWORDS FIELD WEIGHT\n");
         break;
      case AR_SERVER_INFO_COPY_CACHE_LOGGING:
         DriverPrintResult("COPY CACHE LOGGING\n");
         break;
      case AR_SERVER_INFO_DSO_SUPPRESS_NO_SUCH_ENTRY_FOR_DELETE:
         DriverPrintResult("DSO SUPRESS NO SUCH ENTRY FOR DELETE\n");
         break;
      case AR_SERVER_INFO_USE_FTS_IN_WORKFLOW :
         DriverPrintResult("USE FTS IN WORKFLOW\n");
         break;
      case AR_SERVER_INFO_MAX_ATTACH_SIZE:
         DriverPrintResult("AR MAXIMUM ATTACHMENT SIZE\n");
         break;
      case AR_SERVER_INFO_DISABLE_ARSIGNALS:
         DriverPrintResult("DISABLE ARSIGNALS\n");
         break;
      case AR_SERVER_INFO_FT_SEARCH_THRESHOLD:
         DriverPrintResult("FULL TEXT SEARCH RESULT THRESHOLD\n");
         break;
      case AR_SERVER_INFO_REQ_FIELD_IDENTIFIER:
        DriverPrintResult("REQUIRED FIELD IDENTIFIER\n");
        break;
      case AR_SERVER_INFO_REQ_FIELD_IDENTIFIER_LOCATION:
        DriverPrintResult("REQUIRED FIELD IDENTIFIER LOCATION\n");
        break;
      case AR_SERVER_INFO_FT_SIGNAL_DELAY:
         DriverPrintResult("FULL TEXT INDEXER SIGNAL DELAY\n");
         break;
      case AR_SERVER_INFO_ATRIUM_SSO_AUTHENTICATION :
         DriverPrintResult("ATRIUM SSO AUTHENTICATION\n");
         break;
      case AR_SERVER_INFO_OVERLAY_MODE:
         DriverPrintResult("OVERLAY MODE\n");
         break;
      case AR_SERVER_INFO_FT_FORM_REINDEX:
         DriverPrintResult("FULL TEXT FORM REINDEX\n");
         break;
      case AR_SERVER_INFO_SUPPRESS_DOMAIN_IN_URL:
         DriverPrintResult("SUPPRESS DOMAIN IN URL\n");
         break;
      default                            :
         DriverPrintResult("   <undefined operation = %u>\n", value->operation);
         break;
   }
   PrintARValueStruct("      ", &value->value);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARServerInfoList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARServerInfoList(value)
ARServerInfoList *value;      /* IN; value to print */

{
   unsigned int         i;       /* working index */
   ARServerInfoStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Server Info List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->serverInfoList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARServerInfoStruct(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARServerInfoStruct2                        */
/*                                                                           */
/*****************************************************************************/

void PrintARServerInfoStruct2(value)
ARServerInfoStruct *value;    /* IN; value to print */

{
   DriverPrintResult("   Server Info Struct:  ");
   switch (value->operation)
   {
      case AR_SERVER_STAT_START_TIME     :
         DriverPrintResult("START TIME\n");
         break;
      case AR_SERVER_STAT_BAD_PASSWORD   :
         DriverPrintResult("NUMBER BAD PASSWORD LOGIN ATTEMPTS\n");
         break;
      case AR_SERVER_STAT_NO_WRITE_TOKEN :
         DriverPrintResult("FLOATING WRITE TOKEN UNAVAILABLE\n");
         break;
      case AR_SERVER_STAT_NO_FULL_TOKEN  :
         DriverPrintResult("FLOATING FULLTEXT TOKEN UNAVAILABLE\n");
         break;
      case AR_SERVER_STAT_CURRENT_USERS  :
         DriverPrintResult("NUMBER CURRENT USERS\n");
         break;
      case AR_SERVER_STAT_WRITE_FIXED    :
         DriverPrintResult("NUMBER CURRENT WRITE FIXED USERS\n");
         break;
      case AR_SERVER_STAT_WRITE_FLOATING :
         DriverPrintResult("NUMBER CURRENT WRITE FLOATING USERS\n");
         break;
      case AR_SERVER_STAT_WRITE_READ     :
         DriverPrintResult("NUMBER CURRENT WRITE READ-ONLY USERS\n");
         break;
      case AR_SERVER_STAT_FULL_FIXED     :
         DriverPrintResult("NUMBER CURRENT FULLTEXT FIXED USERS\n");
         break;
      case AR_SERVER_STAT_FULL_FLOATING  :
         DriverPrintResult("NUMBER CURRENT FULLTEXT FLOATING USERS\n");
         break;
      case AR_SERVER_STAT_FULL_NONE      :
         DriverPrintResult("NUMBER CURRENT FULLTEXT NONE USERS\n");
         break;
      case AR_SERVER_STAT_API_REQUESTS   :
         DriverPrintResult("NUMBER API REQUESTS\n");
         break;
      case AR_SERVER_STAT_API_TIME       :
         DriverPrintResult("API SECONDS\n");
         break;
      case AR_SERVER_STAT_ENTRY_TIME     :
         DriverPrintResult("ARxxxEntry SECONDS\n");
         break;
      case AR_SERVER_STAT_RESTRUCT_TIME  :
         DriverPrintResult("RESTRUCTURE SECONDS\n");
         break;
      case AR_SERVER_STAT_OTHER_TIME     :
         DriverPrintResult("OTHER SECONDS\n");
         break;
      case AR_SERVER_STAT_CACHE_TIME     :
         DriverPrintResult("CACHE SECONDS\n");
         break;
      case AR_SERVER_STAT_GET_E_COUNT    :
         DriverPrintResult("NUMBER GET ENTRY\n");
         break;
      case AR_SERVER_STAT_GET_E_TIME     :
         DriverPrintResult("GET ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_SET_E_COUNT    :
         DriverPrintResult("NUMBER SET ENTRY\n");
         break;
      case AR_SERVER_STAT_SET_E_TIME     :
         DriverPrintResult("SET ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_CREATE_E_COUNT :
         DriverPrintResult("NUMBER CREATE ENTRY\n");
         break;
      case AR_SERVER_STAT_CREATE_E_TIME  :
         DriverPrintResult("CREATE ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_DELETE_E_COUNT :
         DriverPrintResult("NUMBER DELETE ENTRY\n");
         break;
      case AR_SERVER_STAT_DELETE_E_TIME  :
         DriverPrintResult("DELETE ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_MERGE_E_COUNT  :
         DriverPrintResult("NUMBER MERGE ENTRY\n");
         break;
      case AR_SERVER_STAT_MERGE_E_TIME   :
         DriverPrintResult("MERGE ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_GETLIST_E_COUNT:
         DriverPrintResult("NUMBER GETLIST ENTRY\n");
         break;
      case AR_SERVER_STAT_GETLIST_E_TIME :
         DriverPrintResult("GETLIST ENTRY SECONDS\n");
         break;
      case AR_SERVER_STAT_E_STATS_COUNT  :
         DriverPrintResult("NUMBER ENTRY STATISTICS\n");
         break;
      case AR_SERVER_STAT_E_STATS_TIME   :
         DriverPrintResult("ENTRY STATISTICS SECONDS\n");
         break;
      case AR_SERVER_STAT_FILTER_PASSED  :
         DriverPrintResult("NUMBER FILTERS PASSED QUAL\n");
         break;
      case AR_SERVER_STAT_FILTER_FAILED  :
         DriverPrintResult("NUMBER FILTERS FAILED QUAL\n");
         break;
      case AR_SERVER_STAT_FILTER_DISABLE :
         DriverPrintResult("NUMBER FILTERS DISABLED\n");
         break;
      case AR_SERVER_STAT_FILTER_NOTIFY  :
         DriverPrintResult("NUMBER FILTER NOTIFY ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_MESSAGE :
         DriverPrintResult("NUMBER FILTER MESSAGE ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_LOG     :
         DriverPrintResult("NUMBER FILTER LOG ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_FIELDS  :
         DriverPrintResult("NUMBER FILTER FIELDS ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_PROCESS :
         DriverPrintResult("NUMBER FILTER PROCESS ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_TIME    :
         DriverPrintResult("FILTER SECONDS\n");
         break;
      case AR_SERVER_STAT_ESCL_PASSED    :
         DriverPrintResult("NUMBER ESCALATIONS PASSED QUAL\n");
         break;
      case AR_SERVER_STAT_ESCL_FAILED    :
         DriverPrintResult("NUMBER ESCALATIONS FAILED QUAL\n");
         break;
      case AR_SERVER_STAT_ESCL_DISABLE   :
         DriverPrintResult("NUMBER ESCALATIONS DISABLED\n");
         break;
      case AR_SERVER_STAT_ESCL_NOTIFY    :
         DriverPrintResult("NUMBER ESCALATION NOTIFY ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_LOG       :
         DriverPrintResult("NUMBER ESCALATION LOG ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_FIELDS    :
         DriverPrintResult("NUMBER ESCALATION FIELDS ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_PROCESS   :
         DriverPrintResult("NUMBER ESCALATION PROCESS ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_TIME      :
         DriverPrintResult("ESCALATION SECONDS\n");
         break;
      case AR_SERVER_STAT_TIMES_BLOCKED  :
         DriverPrintResult("TIMES AT LEAST ONE REQUEST IN QUEUE\n");
         break;
      case AR_SERVER_STAT_NUMBER_BLOCKED :
         DriverPrintResult("TOTAL NUMBER IN QUEUE DURING RUN\n");
         break;
      case AR_SERVER_STAT_CPU            :
         DriverPrintResult("CPU TIME FOR SERVER PROCESS\n");
         break;
      case AR_SERVER_STAT_SQL_DB_COUNT   :
         DriverPrintResult("NUMBER SQL DB OPERTIONS\n");
         break;
      case AR_SERVER_STAT_SQL_DB_TIME    :
         DriverPrintResult("SQL DB TIME\n");
         break;
      case AR_SERVER_STAT_FTS_SRCH_COUNT :
         DriverPrintResult("NUMBER FULLTEXT SEARCH OPERTIONS\n");
         break;
      case AR_SERVER_STAT_FTS_SRCH_TIME  :
         DriverPrintResult("SQL FULLTEXT SEARCH TIME\n");
         break;
      case AR_SERVER_STAT_SINCE_START    :
         DriverPrintResult("TIME SINCE START TIME\n");
         break;
      case AR_SERVER_STAT_IDLE_TIME      :
         DriverPrintResult("IDLE TIME BETWEEN CALLS\n");
         break;
      case AR_SERVER_STAT_NET_RESP_TIME  :
         DriverPrintResult("NETWORK TIME SHIPPING RESPONSES\n");
         break;
      case AR_SERVER_STAT_FILTER_FIELDP  :
         DriverPrintResult("NUMBER FILTER PUSH FIELDS ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_FIELDP    :
         DriverPrintResult("NUMBER ESCALATION PUSH FIELDS ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_SQL     :
         DriverPrintResult("NUMBER FILTER SQL ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_SQL       :
         DriverPrintResult("NUMBER ESCALATION SQL ACTIONS\n");
         break;
      case AR_SERVER_STAT_NUM_THREADS    :
         DriverPrintResult("NUMBER OF THREADS IN QUEUE\n");
         break;
      case AR_SERVER_STAT_FILTER_GOTO_ACTION :
         DriverPrintResult("NUMBER FILTER GO TO ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_CALL_GUIDE  :
         DriverPrintResult("NUMBER FILTER CALL GUIDE ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_EXIT_GUIDE  :
         DriverPrintResult("NUMBER FILTER EXIT GUIDE ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_GOTO_GUIDE_LB :
         DriverPrintResult("NUMBER FILTER GOTO LABEL ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_FIELDS_SQL    :
         DriverPrintResult("NUMBER FILTER FIELDS-SQL ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_FIELDS_PROCESS:
         DriverPrintResult("NUMBER FILTER FIELDS-PROCESS ACTIONS\n");
         break;
      case AR_SERVER_STAT_FILTER_FIELDS_FLTAPI :
         DriverPrintResult("NUMBER FILTER FIELDS-FILTER API ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_FIELDS_SQL      :
         DriverPrintResult("NUMBER ESCALATION FIELDS-SQL ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_FIELDS_PROCESS  :
         DriverPrintResult("NUMBER ESCALATION FIELDS-PROCESS ACTIONS\n");
         break;
      case AR_SERVER_STAT_ESCL_FIELDS_FLTAPI   :
         DriverPrintResult("NUMBER ESCALATION FIELDS-FILTER API ACTIONS\n");
         break;
      case AR_SERVER_STAT_WRITE_RESTRICTED_READ   :
         DriverPrintResult("NUMBER CURRENT WRITE RESTRICTED-READ USERS\n");
         break;

      default                       :
         DriverPrintResult("   <undefined operation = %u>\n", value->operation);
         break;
   }
   PrintARValueStruct("      ", &value->value);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARServerInfoList2                         */
/*                                                                           */
/*****************************************************************************/

void PrintARServerInfoList2(value)
ARServerInfoList *value;      /* IN; value to print */

{
   unsigned int         i;       /* working index */
   ARServerInfoStruct  *tempPtr; /* working pointer */

   DriverPrintResult("Server Info List : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->serverInfoList;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARServerInfoStruct2(tempPtr);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARStatusHistory                          */
/*                                                                           */
/*****************************************************************************/

void PrintARStatusHistoryList(
char                *header,     /* IN; header string for the value */
ARStatusHistoryList *value       /* IN; value to print */
)

{
   char                    buff[55];   /* temporary buffer */
   unsigned int            i;          /* working index */
   int                     leadSpaces; /* number of lead spaces in header */
   ARStatusHistoryStruct  *tempPtr;    /* working pointer */

   leadSpaces = 0;
   while (header[leadSpaces] == ' ')
      leadSpaces++;
   leadSpaces += 3;

   if (leadSpaces < (size_t) 50)
   {
      memset(buff, ' ', leadSpaces);
      buff[leadSpaces] = '\0';
   }
   else  /* reset to 0 spaces */
      buff[0] = '\0';

   DriverPrintResult("%s %u stat hist items\n", header, value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->statHistList;
      for (i = 0; i < value->numItems; i++)
      {
         if (tempPtr->user[0] == '\0')
            DriverPrintResult("%s %u - \n", buff, i);
         else
            DriverPrintResult("%s %u - %ld   %s\n", buff, i, tempPtr->timeVal,
                              tempPtr->user);
         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintARDiaryList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARDiaryList(
char             *header,     /* IN; header string for the value */
ARDiaryList      *value       /* IN; value to print */
)

{
   char            buff[55];   /* temporary buffer */
   char            buff2[55];  /* temporary buffer */
   unsigned int    i;          /* working index */
   int             leadSpaces; /* number of lead spaces in header */
   ARDiaryStruct  *tempPtr;    /* working pointer */

   leadSpaces = 0;
   while (header[leadSpaces] == ' ')
      leadSpaces++;
   leadSpaces += 3;

   if (leadSpaces < (size_t) 50)
   {
      memset(buff, ' ', leadSpaces);
      buff[leadSpaces] = '\0';
      memset(buff2, ' ', (leadSpaces + 3));
      buff2[leadSpaces + 3] = '\0';
   }
   else  /* reset to 0 spaces */
   {
      buff[0] = '\0';
      memset(buff2, ' ', 3);
      buff2[3] = '\0';
   }

   DriverPrintResult("%s %u diary entries\n", header, value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->diaryList;
      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintResult("%s %ld   %s\n", buff, tempPtr->timeVal,
                           tempPtr->user);
         PrintChar(buff2, tempPtr->value);

         tempPtr++;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARFieldMapping                             */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldMapping(
char                 *header,
ARFieldMappingStruct *fieldMap     /* IN; value to print */
)

{
   DriverPrintResult("%s\n", header);
   DriverPrintResult("   Field type:  ");
   switch (fieldMap->fieldType)
   {
      case AR_FIELD_NONE :
         DriverPrintResult("<none>\n");
         break;
      case AR_FIELD_REGULAR :
         DriverPrintResult("REGULAR\n");
         break;
      case AR_FIELD_JOIN :
         DriverPrintResult("JOIN\n");
         PrintUInt("   Schema Index: ", fieldMap->u.join.schemaIndex);
         PrintARInternalId("   Field Id :", fieldMap->u.join.realId);
         break;
      case AR_FIELD_VIEW :
         DriverPrintResult("VIEW\n");
         PrintChar("   External Field Name: ", fieldMap->u.view.fieldName);
         break;
      case AR_FIELD_VENDOR :
         DriverPrintResult("VENDOR\n");
         PrintChar("   External Field Name: ", fieldMap->u.vendor.fieldName);
         break;
      default             :
         DriverPrintResult("  <undefined fieldType = %u>\n",
                           fieldMap->fieldType);
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARFieldMappingList                           */
/*                                                                           */
/*****************************************************************************/

void PrintARFieldMappingList(
char               *header,   /* IN; header string for the value */
ARFieldMappingList *value     /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARFieldMapping(" Field mapping: ", &value->mappingList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                            PrintARBooleanList                             */
/*                                                                           */
/*****************************************************************************/

void PrintARBooleanList(
char          *header,        /* IN; header string for the value */
ARBooleanList *value          /* IN; value to print */
)

{
   unsigned int   i;                   /* working index */

   DriverPrintResult("%s : %u items\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
   {
      if (value->booleanList[i])
         DriverPrintResult("   Item %3u : TRUE\n", i);
      else
         DriverPrintResult("   Item %3u : FALSE\n", i);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARContainerInfo                            */
/*                                                                           */
/*****************************************************************************/

void PrintARContainerInfo(
char                *header,        /* IN; header string for the value */
ARContainerInfo     *value          /* IN; value to print */
)

{
   DriverPrintResult("%sContainer Info Name: %s\n", header, value->name);
   DriverPrintResult("%s   Type: %u\n", header, value->type);
   PrintARContainerOwnerObjList(&value->ownerList);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARContainerInfoList                          */
/*                                                                           */
/*****************************************************************************/

void PrintARContainerInfoList(
char                *header,        /* IN; header string for the value */
char                *header2,       /* IN; header string for 2nd+ line */
ARContainerInfoList *value          /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("%s (%u)\n", header, value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARContainerInfo(header2, &value->conInfoList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                         PrintARContainerOwnerObj                          */
/*                                                                           */
/*****************************************************************************/

void PrintARContainerOwnerObj(
char                *header,        /* IN; header string for the value */
ARContainerOwnerObj *value          /* IN; value to print */
)

{
   DriverPrintResult("%sOwner Object Type: %d\n", header, value->type);
   if (value->type != ARCONOWNER_NONE && value->type != ARCONOWNER_ALL)
      DriverPrintResult("%sOwner Object Name: %s\n", header, value->ownerName);
}


/*****************************************************************************/
/*                                                                           */
/*                      PrintARContainerOwnerObjList                         */
/*                                                                           */
/*****************************************************************************/

void PrintARContainerOwnerObjList(
ARContainerOwnerObjList *value          /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("Owner Object List (%u)\n", value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARContainerOwnerObj("   ", &value->ownerObjList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                             PrintARReference                              */
/*                                                                           */
/*****************************************************************************/

void PrintARReferenceStruct(
char                *header,        /* IN; header string for the value */
ARReferenceStruct   *value          /* IN; value to print */
)

{
   char  buf[80];

   sprintf(buf, "%s   ", header);
   DriverPrintResult("%sReference Info:\n", header);
   DriverPrintResult("%s   Label: %s\n", header, value->label);
   DriverPrintResult("%s   Description: %s\n", header, value->description);
   DriverPrintResult("%s   Type: %u\n", header, value->type);
   DriverPrintResult("%s   Data Type: %u\n", header, value->reference.dataType);
   if (value->reference.dataType == ARREF_DATA_ARSREF)
   {
      DriverPrintResult("%s   ARS Reference: %s\n", header,
                        value->reference.u.name);
   }
   else if (value->reference.dataType == ARREF_DATA_EXTREF)
   {
      PrintARExtReferenceStruct(buf, &value->reference.u.extRef);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARExtReferenceStruct                          */
/*                                                                           */
/*****************************************************************************/

void PrintARExtReferenceStruct(
char                 *header,        /* IN; header string for the value */
ARExtReferenceStruct *value          /* IN; value to print */
)

{
   char  buf[80];

   DriverPrintResult("%sExternal Reference Info:\n", header);
   sprintf(buf, "%sGroup List:", header);
   PrintARInternalIdList(buf, header, &value->permittedGroups);
   PrintARValueStruct(header, &value->value);
}


/*****************************************************************************/
/*                                                                           */
/*                           PrintARReferenceList                            */
/*                                                                           */
/*****************************************************************************/

void PrintARReferenceList(
ARReferenceList *value          /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   DriverPrintResult("Reference List (%u)\n", value->numItems);
   for (i = 0; i < value->numItems; i++)
      PrintARReferenceStruct("   ", &value->referenceList[i]);
}


/*****************************************************************************/
/*                                                                           */
/*                        PrintARBulkEntryReturnList                         */
/*                                                                           */
/*****************************************************************************/

void PrintARBulkEntryReturnList(
ARBulkEntryReturnList *bulkEntryReturnList     /* IN; value to print */
)

{
   ARBulkEntryReturn  *bulkEntryReturnPtr;
   unsigned int        i;

   DriverPrintResult("Bulk Entry Result List : %u items\n",
                     bulkEntryReturnList->numItems);
   for (i = 0; i < bulkEntryReturnList->numItems; i++)
   {
      bulkEntryReturnPtr = &bulkEntryReturnList->entryReturnList[i];

      DriverPrintResult("   Entry Call Type : %u\n",
                        bulkEntryReturnPtr->entryCallType);

      switch (bulkEntryReturnPtr->entryCallType)
      {
      case AR_BULK_ENTRY_CREATE:
         PrintChar("      Entry id: ", bulkEntryReturnPtr->u.createEntryReturn.entryId);
         SaveCreateEntryId(bulkEntryReturnPtr->u.createEntryReturn.entryId);
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.createEntryReturn.status);
         break;
      case AR_BULK_ENTRY_SET:
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.setEntryReturn);
         break;
      case AR_BULK_ENTRY_DELETE:
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.deleteEntryReturn);
         break;
      case AR_BULK_ENTRY_MERGE:
         PrintChar("      Entry id: ", bulkEntryReturnPtr->u.mergeEntryReturn.entryId);
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.mergeEntryReturn.status);
         break;
      case AR_BULK_ENTRY_XMLCREATE:
         fprintf(stdout, "      %s\n", bulkEntryReturnPtr->u.xmlCreateEntryReturn.outputDoc);
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.xmlCreateEntryReturn.status);
         break;
      case AR_BULK_ENTRY_XMLSET:
         fprintf(stdout, "      %s\n", bulkEntryReturnPtr->u.xmlSetEntryReturn.outputDoc);
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.xmlSetEntryReturn.status);
         break;
      case AR_BULK_ENTRY_XMLDELETE:
         PrintARStatusListIndent("      ", &bulkEntryReturnPtr->u.xmlDeleteEntryReturn);
         break;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               PrintReturnCode                             */
/*                                                                           */
/*****************************************************************************/

void PrintReturnCode(
int returnCode                /* IN; value to print */
)

{
   DriverPrintResult("ReturnCode:  ");
   switch (returnCode)
   {
      case AR_RETURN_OK         :
         DriverPrintResult("OK\n");
         break;
      case AR_RETURN_WARNING    :
         DriverPrintResult("WARNING\n");
         break;
      case AR_RETURN_ERROR      :
         DriverPrintResult("ERROR\n");
         break;
      case AR_RETURN_FATAL      :
         DriverPrintResult("FATAL\n");
         break;
      case AR_RETURN_BAD_STATUS :
         DriverPrintResult("BAD STATUS\n");
         break;
      default                   :
         DriverPrintResult("<unknown return>\n");
         break;
   }
}

/*****************************************************************************/
/*                                                                           */
/*                         PrintARObjectChangeTimestamps                     */
/*                                                                           */
/*****************************************************************************/

void PrintARObjectChangeTimestamps(
char                     *header,        /* IN; header value to print */
ARObjectChangeTimestamps *value          /* IN; value to print */
)
{
   switch(value->objectType)
   {
      case AR_STRUCT_ITEM_SCHEMA:
         DriverPrintResult("%sSchema:\t", header);
         break;
      case AR_STRUCT_ITEM_FILTER:
         DriverPrintResult("%sFilter:\t", header);
         break;
      case AR_STRUCT_ITEM_ACTIVE_LINK:
         DriverPrintResult("%sActive Link:\t", header);
         break;
      case AR_STRUCT_ITEM_CHAR_MENU:
         DriverPrintResult("%sChar Menu:\t", header);
         break;
      case AR_STRUCT_ITEM_ESCALATION:
         DriverPrintResult("%sEscalation:\t", header);
         break;
      case AR_STRUCT_ITEM_CONTAINER:
         DriverPrintResult("%sContainer:\t", header);
         break;
      case AR_STRUCT_ITEM_IMAGE:
         DriverPrintResult("%sImage:\t", header);
         break;
      default:
         DriverPrintResult("%sUnknown object type: ");
   }

   DriverPrintResult("Last create %d, change %d, delete %d.\n",
                     value->createTime, value->changeTime, value->deleteTime);

}


/*****************************************************************************/
/*                                                                           */
/*                      PrintARObjectChangeTimestampList                     */
/*                                                                           */
/*****************************************************************************/

void PrintARObjectChangeTimestampList(
ARObjectChangeTimestampList *value          /* IN; value to print */
)

{
   unsigned int   i;          /* working index */

   if (value)
   {
      DriverPrintResult("ARObjectChangeTimestamps (%u items)\n", value->numItems);
      for (i = 0; i < value->numItems; i++)
         PrintARObjectChangeTimestamps("   ", &value->objectChanges[i]);
   }
}

/*****************************************************************************/
/*                                                                           */
/*                      WfdGetStageText                                      */
/*                                                                           */
/*****************************************************************************/

   char  *
WfdGetStageText(
   int   stage )
{
   switch( stage )
   {
   case WFD_IDLE:
      return( "Idle..." );
   case WFD_RUNNING:
      return( "Busy..." );
   case WFD_BEFORE_API:
      return( "pre-API" );
   case WFD_BEFORE_QUAL:
      return( "pre-Qual" );
   case WFD_PHASE_1:
      return( "Phase 1" );
   case WFD_PHASE_2:
      return( "Phase 2" );
   case WFD_PHASE_3:
      return( "Phase 3" );
   case WFD_ESCL:
      return( "Escalation" );
   case WFD_AFTER_API:  
      return( "post-API" );
   case WFD_BEFORE_CMDB:
      return( "CMDB" );
   default:
      return( "????" );
   }
}

/*****************************************************************************/
/*                                                                           */
/*                            PrintWFDLocationStruct                         */
/*                                                                           */
/*****************************************************************************/

   void 
PrintWFDLocationStruct(
   char                 *header,
   ARWfdCurrentLocation *location )
{
   char                 fmtLoc[256];

   switch( location->Stage ) 
   {
   case WFD_IDLE:
      strcpy( fmtLoc, "Idle..." );
      break;
   case WFD_RUNNING:
      strcpy( fmtLoc, "Busy..." );
      break;
   case WFD_BEFORE_API:
   case WFD_AFTER_API:
      sprintf( fmtLoc, "(%s) %s",
         WfdGetStageText( location->Stage ),
         location->ApiCall );
      break;
   case WFD_BEFORE_QUAL:
      sprintf( fmtLoc, "(%s) %s->%s->%s",
         WfdGetStageText( location->Stage ),
         location->ApiCall,
         location->SchemaName,
         location->Filter );
      break;
   case WFD_PHASE_1:
   case WFD_PHASE_2:
   case WFD_PHASE_3:
   case WFD_ESCL:
      sprintf( fmtLoc, "(%s) %s->%s->%s%s->action #%d (%s)%s",
         WfdGetStageText( location->Stage ),
         location->ApiCall,
         location->SchemaName,
         location->Filter,
         location->ElsePath ? "(else)" : "",
         location->ActionNo,
         location->ActionStr,
         location->ActionDeferred ? " - deferred" : "" );
      break;
   case WFD_BEFORE_CMDB:
      sprintf( fmtLoc, "(%s) %s->%s%s%s",
         WfdGetStageText( location->Stage ),
         location->ApiCall,
         location->SchemaName,
         location->Filter[0] ? "->" : "",
         location->Filter[0] ? location->Filter : "" );
      break;
   default:
      strcpy( fmtLoc, "????..." );
      break;
   }
   PrintChar( header, fmtLoc );
}

/*****************************************************************************/
/*                                                                           */
/*                            PrintWFDBreakpoint                             */
/*                                                                           */
/*****************************************************************************/
   void
PrintWFDBreakpoint(
   ARWfdRmtBreakpoint   *bp,
   int                  local )
{
   DriverPrintResult( " -- %s %3d: %s",
      local ? "Local Bkpt" : "BreakPoint",
      bp->id,
      bp->disable ? "[disabled]" : "" );
   if( bp->passcount )
      DriverPrintResult( "[passcnt=%d]", bp->passcount );
   DriverPrintResult( "\n" );

   DriverPrintResult( "   Filter:\"%s\"  Schema:\"%s\" (%s) (%s) action #%d\n",
      bp->filter,
      bp->schema,
      WfdGetStageText(bp->stage),
      bp->elsePath ? "else" : "if",
      bp->actioNo );    
   
   if( bp->bpQualifier &&
       bp->bpQualifier->operation != AR_COND_OP_NONE )
      PrintARQualifierStruct( "", bp->bpQualifier );
   DriverPrintResult( "\n" );
}

/*****************************************************************************/
/*                                                                           */
/*                     PrintARMultiSchemaFieldValueStructIndent              */
/*                                                                           */
/*****************************************************************************/

void PrintARMultiSchemaFieldValueStructIndent(
char                          *indent, /* IN; string of blanks for indenting */
ARMultiSchemaFieldFuncValueStruct *value  /* IN; value to print */
)

{   
   DriverPrintResult("    %sMulti schema field value struct:\n", indent);
   if (value != NULL && value->fieldId.fieldId > 0)
   {
      DriverPrintResult("      %sField Id: %u", indent, value->fieldId.fieldId);
      DriverPrintResult("      %sFunc  Id: %u", indent, value->fieldId.funcId);
      DriverPrintResult("      Source: %s", value->fieldId.queryFromAlias);      
      PrintARValueStruct("     ", &value->value);
   }
}

/*****************************************************************************/
/*                                                                           */
/*                PrintARMultiSchemaFieldValueListIndent                     */
/*                                                                           */
/*****************************************************************************/

void PrintARMultiSchemaFieldValueListIndent(
char                          *indent, /* IN; string of blanks for indenting */
ARMultiSchemaFieldFuncValueList   *value   /* IN; value to print */
)

{
   unsigned int                     i;          /* working index */
   ARMultiSchemaFieldFuncValueStruct    *tempPtr;   /* working pointer */

   DriverPrintResult("%sMulti Schema Field Value List : %u items\n", 
      indent, value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->listPtr;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARMultiSchemaFieldValueStructIndent(indent, tempPtr);
         tempPtr++;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                PrintARMultiSchemaFieldValueListList                       */
/*                                                                           */
/*****************************************************************************/

void PrintARMultiSchemaFieldValueListList(value)
ARMultiSchemaFieldFuncValueListList *value;       /* IN; value to print */

{
   unsigned int                  i;         /* working index */
   ARMultiSchemaFieldFuncValueList  *tempPtr;   /* working pointer */

   DriverPrintResult("List of Multi Schema Field Value Lists : %u items\n", value->numItems);
   if (value->numItems != 0)
   {
      tempPtr = value->listPtr;
      for (i = 0; i < value->numItems; i++)
      {
         PrintARMultiSchemaFieldValueListIndent("    ", tempPtr);
         tempPtr++;
      }
   }
}

