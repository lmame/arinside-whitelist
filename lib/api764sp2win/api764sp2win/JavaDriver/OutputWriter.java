package com.bmc.arsys.demo.javadriver;

import java.io.*;

import com.bmc.arsys.api.ObjectPrinter;

public class OutputWriter extends ObjectPrinter {

    public void printResult(String resultString) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_RESULTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(resultString);
            outputFile.flush();
        }
    }

    public void driverPrintHeader(String header) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_HEADERS) == 0) {
            ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
            if (threadControlBlockPtr.getPrimaryThread()) {
                System.out.println(header);
            }
        }
    }

    public void driverPrintPrompt(String prompt) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_PROMPTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
            if (threadControlBlockPtr.getPrimaryThread()) {
                System.out.print(prompt);
            }
        }
    }

    public void driverPrintNotSupportCommand(int commandCode) {
        ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
        String buffer = threadControlBlockPtr.getBuffer();
        String args = threadControlBlockPtr.args;
        StringBuilder strBuf = new StringBuilder();
        strBuf.append(" **** No support for this command: ").append(commandCode).append(", with args: ").append(args)
                .append(", in line: ").append(buffer);
        if (threadControlBlockPtr.getCurrentInputFileName() != null)
            strBuf.append(", from: ").append(threadControlBlockPtr.getCurrentInputFileName());
        strBuf.append(", in driver \n");
        driverPrintError(strBuf.toString());
    }

    public void driverPrintMenu(String menuLine) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_MENU) == 0) {
            System.out.print(menuLine);
        }
    }

    public void driverPrintError(String errorMessage) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_RESULTS) == 0) {
            ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(errorMessage);
            outputFile.flush();
        }
    }

    public void driverPrintException(Exception e) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_RESULTS) == 0) {
            e.printStackTrace();
        }
    }

    public void driverPrintWarning(String warning) {
        if ((JavaDriver.quietMode & JavaDriver.SUPPRESS_WARNINGS) == 0) {
            ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
            PrintWriter outputFile = threadControlBlockPtr.getOutFile();
            outputFile.print(warning);
            outputFile.flush();
        }
    }

    public void closeOutputFile() {
        ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();

        if (threadControlBlockPtr.getIsStdOut()) {
            driverPrintWarning(" **** Output to stdout; cannot close stdout file\n");
        } else {
            // Set the output to stdout
            threadControlBlockPtr.setOutputToStdOut();
        }
    }

    public void openOutputFile(String fileName) {
        if ((fileName == null) || fileName.length() == 0) {
            driverPrintWarning(" **** No filename specified so no change to output file\n");
        } else { /* open the new file for writing */
            if ((JavaDriver.getQuietMode() & JavaDriver.SUPPRESS_RESULTS) != 0) {
                return;
            }

            String absoluteFileName = null;
            String directory = JavaDriver.getResultDirectory();

            if (directory != null && (directory.length() > 0)) {
                absoluteFileName = directory + "\\" + fileName;
            } else {
                absoluteFileName = fileName;
            }

            try {
                ThreadControlBlock threadControlBlockPtr = JavaDriver.getThreadControlBlockPtr();
                threadControlBlockPtr.setOutputFile(absoluteFileName);
            } catch (Exception e) {
                driverPrintWarning(" **** File error during open; no change to output file" + "\n");
            }
        }
    }

    public void driverPrintHelp() {
        System.out.print("         AR System Java API Driver\n");

        System.out.println();

        System.out.print("Container      Form           Char Menu       Form Field       <mult>\n");
        System.out.print("------------   -----------    -----------     -------------    ------\n");
        System.out.print("get    (gco)   get    (gs)    get    (gc)     get    (gsf)\n");
        System.out.print("set    (sco)   set    (ss)    set    (sc)     set    (ssf)\n");
        System.out.print("create (cco)   create (cs)    create (cc)     create (csf)\n");
        System.out.print("delete (dco)   delete (ds)    delete (dc)     delete (dsf)     (dmsf)\n");
        System.out.print("getlist(glco)  getlist(gls)   getlist(glc)    getlist(glsf)\n");
        System.out.print("getmult(gmco)  getmult(gms)   getmult(gmc)    getmult(gmsf)\n");
        System.out.print("                              expand (ec)     setmult(smsf)\n");

        System.out.println();

        System.out.print("Active Link    Filter         Escalation      Entry          		Entry\n");
        System.out.print("------------   -----------    -----------     -----------    		------\n");
        System.out.print("get    (gal)   get    (gf)    get    (ges)    get    (ge)    		merge (me)\n");
        System.out.print("set    (sal)   set    (sf)    set    (ses)    set    (se)    		stats (stat)\n");
        System.out.print("create (cal)   create (cf)    create (ces)    create (ce)    		getlistw/f(glewf)\n");
        System.out.print("delete (dal)   delete (df)    delete (des)    delete (de) 		getonew/f(goewf)\n");
        System.out.print("getlist(glal)  getlist(glf)   getlist(gles)   getlist(gle) 		gle w/msf(glmsf)\n");
        System.out.print("getmult(gmal)  getmult(gmf)   getmult(gmes)   getmult(gme) \n");

        System.out.println();

        System.out.print("VUI            Misc Lists      Alerts              Encode/Decode\n");
        System.out.print("-----------    -----------     ---------------     ---------------\n");
        System.out.print("get    (gv)    server(svr)     Create     (cae)    enquery (ecqal)\n");
        System.out.print("set    (sv)    group (glg)     Register   (rfa)    dequery (dcqal)\n");
        System.out.print("create (cv)    user  (glu)     Deregister (dfa)    enassig (ecasn)\n");
        System.out.print("delete (dv)    sql   (glsql)   gla user   (glau)   deassig (dcasn)\n");
        System.out.print("getlist(glsv)  sqlforAL(sqlal) get Count  (gac)    echstry (echst)\n");
        System.out.print("getmult(gmv)                   Delete     (dae)    ecdiary (ecdia)\n");
        System.out.print("                               Decode     (dcam)   enc date(ecdat)\n");
        System.out.print("                                                   dec date(dcdat)\n");

        System.out.println();

        System.out.print("Init/Term         Control/Logging       Info           Miscellaneous\n");
        System.out.print("---------------   ---------------       ------------   -----------------------------------\n");
        System.out.print("login  (log)      record    (rec)       get svr (gsi)  ver user (ver/2)  impusr 	 (imusr)\n");
        System.out.print("exit   (q)        stop rec  (srec)      set svr (ssi)  export     (exp)  get errmsg  	 (gem)\n");
        System.out.print("help   (h, ?)     open out  (oout)      get FT  (gft)  import     (imp)  set logging 	 (slog)\n");
        System.out.print("begtran(bbet)     close out (cout)      set FT  (sft)  set port   (ssp)  obj change  	 (goct)\n");
        System.out.print("endtran(ebet)     rcrd pmpt (recPrompt) get stat(gss)  load qual  (lqs)  create overlay (co)\n");
        System.out.print("charset(g[sc]cs)  stp recpmt(srecPrompt)               exec proc  (proc) set overlay group name (sogn)\n"); 
        System.out.print("                                                       valid cache(vfc)  create overlay from obj (cofo) \n");
        System.out.println();

        System.out.print("Localized Value   Support File     Image          Session Config        Workflow Debug\n");
        System.out.print("---------------   --------------   -----          --------------        --------------\n");
        System.out.print("get       (glv)   create   (cfl)   get     (gi)   get conf(gsc)         cur loc    (dbcl)\n");
        System.out.print("getmult   (gmlv)  delete   (dfl)   set     (si)   set conf(ssc)         execute    (dbex)\n");
        System.out.print("                  get      (gfl)   create  (ci)   use conn pooling(ucp) get FVL    (dbgf)\n");
        System.out.print("                  set      (sfl)   delete  (di)   set conn limits(scl)  set FVL    (dbsf)\n");
        System.out.print("                  getlist  (glfl)  getlist (gli)  adj conn params(acp)  get qual   (dbgq)\n");
        System.out.print("                                   getmult (gmi)                        set qual   (dbsq)\n");
        System.out.print("                                                                        get mode   (dbgm)\n");

        System.out.print(" Complex Entry    Localized Val    App States     Currency Ratio        set mode   (dbsm)\n");
        System.out.print(" -------------    -------------    ------------   --------------        term API   (dbta)\n");
        System.out.print(" create (xmlce)   get    (glv)     get     (gas)  getmult sets(gmcrs)\n");
        System.out.print(" get    (xmlge)   getmult(gmlv)    set     (sas)  get ratio   (gcr)\n");
        System.out.print(" set    (xmlse)                    getlist (glas) \n");
        System.out.print(" service(sve)\n");
        System.out.print(" xml svc(xmlsve)\n");
        System.out.print(" Client Managed Transaction\n");
        System.out.print(" --------------------------\n");
        System.out.print(" begin client managed transaction (bcmt)\n");
        System.out.print(" end client managed transaction   (ecmt)\n");
        System.out.print(" set client managed transaction   (scmt)\n");
        System.out.print(" remove client managed transaction(rcmt)\n");        
        System.out.println();

    }

}
