
/*  File: ardbcskl.c  */

#ifdef _WIN32
#include <windows.h>
#endif /* _WIN32 */
#include <stdio.h>

#include "ardbc.h"
#include "arerrno.h"

/*****************************************************************************/
/*                                                                           */
/*       Action Request System Database Connectivity (ARDBC) Skeleton        */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/* Description: This is a skeleton ARDBC plug-in.  It is intended to be used */
/*    as a starting point for further plug-in development.                   */
/*                                                                           */
/*    All functions, unless otherwise noted, are optional.  If a function is */
/*    not defined, the plug-in service will return an appropriate response.  */
/*                                                                           */
/*    General notes on memory management:                                    */
/*                                                                           */
/*       1) Do not deallocate (free) memory passed into the ARPlugin or      */
/*          ARDBC functions.  The plug-in service will deallocate memory     */
/*          associated with the input ("IN") arguments.                      */
/*                                                                           */
/*       2) Do not deallocate (free) memory returned by the ARPlugin or      */
/*          ARDBC functions.  The plug-in service will deallocate memory     */
/*          associated with the return ("OUT") arguments using free().       */
/*                                                                           */
/*    There is one exception to the above rules regarding memory management. */
/*    The plug-in has complete control over the allocation and deallocation  */
/*    of the "object" via ARPluginCreateInstance() and                       */
/*    ARPluginDeleteInstance(), respectively.                                */
/*                                                                           */
/*****************************************************************************/

#ifdef _WIN32
                        /* DllMain is only required in Windows environments. */

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  ul_reason_for_call, 
                      LPVOID lpReserved)
{
   return TRUE;
}
#endif /* _WIN32 */


/*****************************************************************************/
/*                                                                           */
/*                               ARPluginIdentify                            */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is not optional.  All plug-ins must define this */
/*      function.                                                            */
/*                                                                           */
/*      This call is used by the plug-in service to identify what kind of    */
/*      plug-in this is going to implement, what version of the ARDBC        */
/*      specification it supports, and its name.                             */
/*                                                                           */
/*      Recommended plug-in naming convention:                               */
/*         <Company Name>.<Plug-In Type>.<Plug-In Name>                      */
/*                                                                           */
/*      E.g., If company XYZ has created the "Greatest Plug-In Ever" (GPE)   */
/*      they might name it "XYZ.ARDBC.GPE".                                  */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginIdentify(
ARPluginIdentification *id,     /* OUT; information about the plug-in */
ARStatusList           *status  /* OUT; status of the operation */
)
{
   id->type = AR_PLUGIN_TYPE_ARDBC;
   strcpy(id->name, "EXAMPLE.ARDBC.SKELETON");
   id->version = ARDBC_PLUGIN_VERSION;

   if (status != NULL)
      memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                            ARPluginInitialization                         */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used by the plug-in service to allow the     */
/*      plug-in to perform initialization of global resources (i.e.          */
/*      resources that will be shared across however many instances of the   */
/*      plug-in are created via ARPluginCreateInstance().                    */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginInitialization(
int            argc,    /* IN;  number of arguments passed into the service */
char         **argv,    /* IN;  arguments passed into the plug-in service at */
                        /*      the command line. */
ARStatusList  *status   /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                             ARPluginTermination                           */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used by the plug-in service to allow the     */
/*      plug-in to perform cleanup of global resources (i.e. resources that  */
/*      were created via ARPluginInitialization() or an instance of the      */
/*      plug-in that have not already been cleaned up).                      */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginTermination(
ARStatusList *status        /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));
   
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                            ARPluginCreateInstance                         */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used by the plug-in service to instantiate   */
/*      the plug-in.  An instance of a plug-in will only be active on one    */
/*      thread at a time.  If the plug-in service is running multiple        */
/*      threads, then many instances will be created.  As this is the case,  */
/*      access to global resources must be done in a thread-safe way.        */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginCreateInstance(
void         **object,   /* OUT; plug-in instance */
ARStatusList  *status    /* OUT; status of the operation */
)
{
   if (status != NULL)
      memset(status, 0, sizeof(*status));

   if (object != NULL)
      *object = NULL;

   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                            ARPluginDeleteInstance                         */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used by the plug-in service to shut down an  */
/*      instance of a plug-in.                                               */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARPluginDeleteInstance(
void         *object,  /* IN;  plug-in instance to delete*/
ARStatusList *status   /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                            ARDBCCreateEntry                               */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to create a new entry in the data source*/
/*      The main difference between this and ARDBCSetEntry() is that this    */
/*      function is expected to return the new entry ID whereas the set-     */
/*      entry call creates/modifies the entry that is specified as an input  */
/*      parameter.                                                           */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCCreateEntry(
void              *object,        /* IN; plug-in instance */
char              *tableName,      /*IN; vendor's table name */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId       transId,       /* IN; transaction ID */
ARFieldValueList  *fieldValueList,/* IN; list of field/value pairs */
AREntryIdList     *entryId,       /* OUT; id of entry just created */
ARStatusList      *status         /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                               ARDBCGetEntry                               */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to get an existing entry from the data  */
/*      source.                                                              */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCGetEntry(
void              *object,  /* IN; plug-in instance */
char             *tableName,/* IN; information about the schema holding entry */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId      transId,  /* IN; transaction ID */
AREntryIdList    *entryId,  /* IN; id of the entry are getting */
ARInternalIdList *idList,   /* IN; list of fields to return; numItems=0 == ALL*/
ARFieldValueList *fieldList,/* OUT; list of field/value pairs retrieved */
ARStatusList     *status    /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                               ARDBCSetEntry                               */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to set an existing entry in the data    */
/*      source or to create a new entry with a specific entry ID.            */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCSetEntry(
void              *object,        /* IN; plug-in instance */
char              *tableName,      /*IN; schema to set */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId       transId,       /* IN; transaction ID */
AREntryIdList     *entryId,       /* IN; key value for the record */
ARFieldValueList  *fieldValueList,/* IN; list of field/value pairs for entry */
ARTimestamp        getTimestamp,  /* IN; time at which get of record was done */
ARStatusList      *status         /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                             ARDBCDeleteEntry                              */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to delete an existing entry from the    */
/*      data source.                                                         */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCDeleteEntry(
void              *object,    /* IN; plug-in instance */
char              *tableName, /* IN; info about the schema holding entry */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId       transId,   /* IN; transaction ID */
AREntryIdList     *entryId,   /* IN; id of the entry to find */
ARStatusList      *status     /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                        ARDBCGetEntryBLOB                                  */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This is where we return the contents of an attachment.    */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCGetEntryBLOB(
void              *object,         /* IN; plug-in instance */
char              *tableName,      /* IN; info about the schema holding entry*/
ARVendorFieldList *vendorFieldList,/* IN; list of vendor field info */
ARInternalId       transId,        /* IN; transaction ID */
AREntryIdList     *entryId,        /* IN; entry id of the entry to get */
ARInternalId       fieldId,        /* IN; field id of the data to get  */
ARLocStruct       *loc,            /* OUT; place to put the data */
ARStatusList      *status          /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                      ARDBCGetListEntryWithFields                          */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to return a list of entry data given    */
/*      a qualification.                                                     */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCGetListEntryWithFields(
void              *object,     /* IN; plug-in instance */
char              *tableName,  /* IN; info about the schema holding entry */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId       transId,    /* IN; transaction ID */
ARQualifierStruct *qualifier,  /* IN; qualification(s) for entries to select */
ARSortList        *sortList,   /* IN; list of fields to sort on in result list*/
AREntryListFieldList *getListFields,/* IN; fields to be returned in data area */
unsigned int       firstRetrieve,/*IN; entry to start with (skip ones before) */
unsigned int       maxRetrieve,/* IN; max number of entries to retrieve */
AREntryListFieldValueList   *entryList,  /* OUT; list of entries selected */
unsigned int      *numMatches, /* OUT; number of matches for qualifier; NULL */
                               /*      if this number not wanted             */
ARStatusList      *status      /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                         ARDBCGetEntryStatistics                           */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This call is used to return statistics covering a range   */
/*      of data that matches the qualification.                              */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCGetEntryStatistics(
void              *object,        /* IN; plug-in instance */
char              *tableName,     /* IN; info about the schema holding entry */
ARVendorFieldList *vendorFieldList,/*IN; list of vendor field info */
ARInternalId       transId,       /* IN; transaction ID */
ARQualifierStruct *qualifier,     /* IN; qual for entries to select */
ARFieldValueOrArithStruct *target,/* IN; per entry operation to perform*/
unsigned int       statistic,     /* IN; statistic to calculate */
ARInternalIdList  *groupByList,   /* IN; fields on which to group statistics*/
ARStatisticsResultList *results,  /* OUT; result for each grouping */
ARStatusList      *status         /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                         ARDBCCommitTransaction                            */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  Once a sequence of operations has been completed we call  */
/*      this function to commit any changes since the last commit/rollback   */
/*      for the transaction Id in question.                                  */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCCommitTransaction(
void              *object,  /* IN; plug-in instance */
ARInternalId       transId, /* IN; transaction ID */
ARStatusList      *status   /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                       ARDBCRollbackTransaction                            */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  Once a sequence of operations has been completed we call  */
/*      this function to roll back any changes since the last commit/rollback*/
/*      for the transaction Id in question.                                  */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCRollbackTransaction(
void              *object,  /* IN; plug-in instance */
ARInternalId       transId, /* IN; transaction ID */
ARStatusList      *status   /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                         ARDBCGetListSchemas                               */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This is used to get a list of candidate forms on which    */
/*      vendor forms can be built.                                           */
/*                                                                           */
/*****************************************************************************/

int ARDBCGetListSchemas(
void                 *object,/* IN;  plug-in instance */
ARCompoundSchemaList *schema,/* OUT; list of forms plug-in exposes */
ARStatusList         *status /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}


/*****************************************************************************/
/*                                                                           */
/*                         ARDBCGetMultipleFields                            */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*   Description:  This is used to get a list of candidate fields to which   */
/*      fields on a vendor forms can be attached.                            */
/*                                                                           */
/*****************************************************************************/

ARPLUGIN_EXPORT int ARDBCGetMultipleFields(
void              *object,  /* IN; plug-in instance */
ARCompoundSchema  *schema,  /* IN; form containing the data */
ARFieldMappingList *mapping,/* OUT; list of fields */
ARFieldLimitList  *limit,   /* OUT; corresponding field limits */
ARUnsignedIntList *dataType,/* OUT; corresponding data types */
ARStatusList      *status   /* OUT; status of the operation */
)
{
   return AR_RETURN_OK;
}
