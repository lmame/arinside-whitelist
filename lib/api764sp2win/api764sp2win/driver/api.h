/*  File: api.h  */

/* External declarations of the dummy API for the AR System driver */

                         /**  Decode specials  **/
int APIPrintStatusHistory();
int APIPrintDiary();
                         /**  Entry  **/
int APIARGetEntry();
int APIARGetMultipleEntries();
int APIARSetEntry();
int APIARCreateEntry();
int APIARDeleteEntry();
int APIARGetListEntry();
int APIARGetListEntryWithFields();
int APIARMergeEntry();
int APIARLoadARQualifierStruct();
int APIARGetEntryStatistics();
int APIARGetEntryBLOB();
int APIARBeginBulkEntryTransaction();
int APIAREndBulkEntryTransaction();
int APIARGetListEntryBlocks();
int APIARServiceEntry();
int APIARGetOneEntryWithFields();
int APIARSetGetEntry();
                         /**  Container  **/
int APIARGetContainer();
int APIARSetContainer();
int APIARCreateContainer();
int APIARDeleteContainer();
int APIARGetListContainer();
int APIARGetMultipleContainers();
                         /**  Schema  **/
int APIARGetSchema();
int APIARSetSchema();
int APIARCreateSchema();
int APIARDeleteSchema();
int APIARGetListSchema();
int APIARGetMultipleSchemas();
int APIARGetListSchemaWithAlias();
int APIARGetField();
int APIARSetField(ARBoolean imageFileFlag);
int APIARSetMultipleFields();
int APIARCreateField(ARBoolean imageFileFlag);
int APIARCreateMultipleFields();
int APIARDeleteField();
int APIARDeleteMultipleFields();
int APIARGetListField();
int APIARGetMultipleFields();
                         /**  VUI  **/
int APIARGetVUI();
int APIARSetVUI();
int APIARCreateVUI();
int APIARDeleteVUI();
int APIARGetListVUI();
int APIARGetMultipleVUIs();
                         /**  Character Menu  **/
int APIARGetCharMenu();
int APIARSetCharMenu();
int APIARCreateCharMenu();
int APIARDeleteCharMenu();
int APIARGetListCharMenu();
int APIARGetMultipleCharMenus();
int APIARExpandCharMenu();
int APIARExpandSSMenu();
                         /**  Filter  **/
int APIARGetFilter();
int APIARSetFilter();
int APIARCreateFilter();
int APIARDeleteFilter();
int APIARGetListFilter();
int APIARGetMultipleFilters();
                        /**  Escalations  **/
int APIARGetEscalation();
int APIARSetEscalation();
int APIARCreateEscalation();
int APIARDeleteEscalation();
int APIARGetListEscalation();
int APIARGetMultipleEscalations();
                         /**  Active Link  **/
int APIARGetActiveLink();
int APIARSetActiveLink();
int APIARCreateActiveLink();
int APIARDeleteActiveLink();
int APIARGetListActiveLink();
int APIARGetMultipleActiveLinks();
                         /**  Support File  **/
int APIARGetSupportFile();
int APIARSetSupportFile();
int APIARCreateSupportFile();
int APIARDeleteSupportFile();
int APIARGetListSupportFile();
                         /**  Group/User  **/
int APIARGetListGroup();
int APIARGetListUser();
int APIARGetListRole();
int APIARGetListApplicationState();
int APIARGetApplicationState();
int APIARSetApplicationState();
                         /**  Currency  **/
int APIARGetMultipleCurrencyRatioSets();
int APIARGetCurrencyRatio();
                         /** Client Managed Transactional API **/
int APIARBeginClientManagedTransaction();
int APIAREndClientManagedTransaction();
int APIARSetClientManagedTransaction();
int APIARRemoveClientManagedTransaction();
                         /**  Images **/
int APIARGetImage();
int APIARSetImage();
int APIARCreateImage();
int APIARDeleteImage();
int APIARGetListImage();
int APIARGetMultipleImages();
                         /** Object Timestamps **/
int APIARGetObjectChangeTimes();

                         /**  Workflow Debugger  **/
int APIWFDExecute();
int APIWFDGetStackLocation();
int APIWFDGetFieldValues();
int APIWFDSetFieldValues();
int APIWFDGetDebugMode();
int APIWFDSetDebugMode();
int APIWFDDoSetDebugMode();
int APIWFDGetFilterQual();
int APIWFDSetQualifierResult();
int APIARRunEscalation();
int APIWFDTerminateAPI();
int APIWFDDumpStack();
int APIWFDGetKeyword();
int APIWFDGetUserContext();
int APIWFDRmtSetBreakPt();
int APIWFDRmtClrBreakPt();
int APIWFDRmtListBreakPt();
int APIWFDRmtClearBpList();

                         /** Multi Schema **/
int APIARGetListEntryWithMultiSchemaFields();

                         /**  Miscellaneous  **/
int APIARVerifyUser();
int APIARVerifyUser2();
int APIARExport();
int APIARExportToFile();
int APIARImport();
int APIARUnImport();
int APIARGetListServer();
int APIARInitialization();
int APIARTermination();
int APIARGetServerInfo();
int APIARSetServerInfo();
int APIARGetServerStatistics();
int APIARGetListSQL();
int APIARGetListSQLForActiveLink();
int APIARExecuteProcess();
int APIARExecuteProcessForActiveLink();
int APIARSetServerPort(ARBoolean);
void APIARGetTextForErrorMessage();
int APIARSetLogging();
int APIARSignal();
int APIARCloseNetworkConnections();
int APIARValidateFormCache();
int APIARCreateAlertEvent();
int APIARRegisterForAlerts();
int APIARDeregisterForAlerts();
int APIARGetListAlertUser();
int APIARGetAlertCount();
int APIARDecodeAlertMessage();
int APIARDecodeARQualifierStruct();
int APIAREncodeARQualifierStruct();
int APIARDecodeARAssignStruct();
int APIAREncodeARAssignStruct();
int APIAREncodeStatusHistory();
int APIAREncodeDiary();
int APIARValidateLicense();
int APIARValidateMultipleLicenses();
int APIARGetListLicense();
int APIARCreateLicense();
int APIARDeleteLicense();
int APIARImportLicense();
int APIARExportLicense();
int APIARGetLocalizedValue();
int APIARGetMultipleLocalizedValues();
int APIARGetListExtSchemaCandidates();
int APIARGetMultipleExtFieldCandidates();
int APIARGetSessionConfiguration();
int APIARSetSessionConfiguration();
void APIARDateToJulianDate();
void APIARJulianDateToDate();
int APIARXMLCreateEntry();
int APIARXMLGetEntry();
int APIARXMLSetEntry();
int APIARXMLServiceEntry();
int APIARGetMultipleEntryPoints();
int APIARSetImpersonatedUser();
int APIARGetClientCharSet();
int APIARGetServerCharSet();
int APIARParseXMLDocument();
int APIARGetCacheEvent();
int APIARCreateOverlay();
int APIARCreateOverlayFromObject();
