/*  File:  util.h  */

#ifndef UTIL_H
#define UTIL_H

#define RELEASE_ONE              1
#define RELEASE_ALL              2

#ifdef _WIN32
#define OS_CRITICAL_SECTION      CRITICAL_SECTION
#define OS_CRITICAL_SECTION_PTR  LPCRITICAL_SECTION
#define OS_TLS_OBJECT            unsigned long
#define OS_TLS_OBJECT_PTR        unsigned long *

typedef struct
{
   unsigned int  releaseType;
   HANDLE        eventHandle;
} OS_EVENT_OBJECT;

#else
#define OS_CRITICAL_SECTION      pthread_mutex_t
#define OS_CRITICAL_SECTION_PTR  pthread_mutex_t *
#define OS_TLS_OBJECT            pthread_key_t
#define OS_TLS_OBJECT_PTR        pthread_key_t *

typedef struct
{
   unsigned int     releaseType;
   pthread_mutex_t  eventMutex;
   pthread_cond_t   eventCondVar;
} OS_EVENT_OBJECT;
#endif /* _WIN32 */

/* External declarations of the Utility procedures */

void  DebugDriver(void);
void  GetInputLine();
int   OpenInputFile();
int   OpenOutputFile();
int   CloseOutputFile();
int   StartRecording();
void  StopRecording();
void  DriverSleep();
void  DriverSleepMsec();
void  DriverInitializeCriticalSection();
void  DriverEnterCriticalSection();
void  DriverLeaveCriticalSection();
void  DriverDeleteCriticalSection();
void  DriverTLSAlloc();
void *DriverTLSGetValue();
void  DriverTLSSetValue();
void  DriverTLSFree();
void  DriverCreateEvent();
void  DriverLockEvent();
void  DriverSetEvent();
void  DriverWaitForEvent();
void  DriverUnlockEvent();
void  DriverDeleteEvent();
void  ReleaseWaitingThreads();
ARBoolean ProcessCommentLine(char *);
void  ProcessResultFile(void);
void  SleepTimer();
void  RandomSleep();
int   RandomSleepTimer();
void  MillisecondSleepTimer();
int   BeginLoop();
int   EndLoop();
void *CreateThreadControlBlock();
void *GetThreadControlBlockPtr();
void  DestroyThreadControlBlock();
ARControlStruct * GetControlStructPtr();
void  SaveEntryListEntryIds();
void  SaveEntryListFieldValueEntryIds();
void  SaveFirstSchemaListName();
void  SaveCreateEntryId();
int   GetDateString(int jd, char *dateStr);
void  GetTODString(int tod, char *todStr);
int   GetJDValue(char *, int *jd);

#endif /* UTIL_H */
