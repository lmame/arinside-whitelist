/* File:  get.c */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#include "ar.h"
#include "arerrno.h"
#include "get.h"
#include "globals.h"
#include "util.h"
#include "print.h"

#ifdef _WIN32
#include <winnt.h>
#include <winbase.h>
#ifndef snprintf
#define  snprintf _snprintf
#endif
#endif /* _WIN32 */


/*****************************************************************************/
/*                                                                           */
/*                                GetARBoolean                               */
/*                                                                           */
/*****************************************************************************/

ARBoolean GetARBoolean(
char       *prompt,            /* IN; prompt to issue for the value */
ARBoolean   defaultVal         /* IN; default if no value */
)

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

   if ((strcmp(threadControlBlockPtr->buffer, "T") == 0) ||
       (strcmp(threadControlBlockPtr->buffer, "t") == 0))
      return TRUE;

   /* if not "T" or "t", assume FALSE */
   return FALSE;
}


/*****************************************************************************/
/*                                                                           */
/*                                   GetInt                                  */
/*                                                                           */
/*****************************************************************************/

int GetInt(prompt, defaultVal)
char  *prompt;            /* IN; prompt to issue for the value */
int    defaultVal;        /* IN; default if no value */

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

   return atoi(threadControlBlockPtr->buffer);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetUnsignedInt                              */
/*                                                                           */
/*****************************************************************************/

unsigned int GetUnsignedInt(prompt, defaultVal)
char          *prompt;            /* IN; prompt to issue for the value */
unsigned int   defaultVal;        /* IN; default if no value */

{
   int                  returnVal;
   ThreadControlBlock  *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   do
   {
      DriverPrintPrompt("%s", prompt);
      GetInputLine();
      if (strlen(threadControlBlockPtr->buffer) == 0)
         returnVal = defaultVal;        /* no value so use default */
      else
         returnVal = atoi(threadControlBlockPtr->buffer);
      
      if (returnVal < 0)
         DriverPrintWarning("Value must be greater than 0\n");
   }
   while (returnVal < 0);

   return ((unsigned int) returnVal);
}            


/*****************************************************************************/
/*                                                                           */
/*                                  GetLong                                  */
/*                                                                           */
/*****************************************************************************/

long GetLong(prompt, defaultVal)
char  *prompt;            /* IN; prompt to issue for the value */
long   defaultVal;        /* IN; default if no value */

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

   return atol(threadControlBlockPtr->buffer);
}          


/*****************************************************************************/
/*                                                                           */
/*                                  GetReal                                  */
/*                                                                           */
/*****************************************************************************/

double GetReal(prompt, defaultVal)
char    *prompt;            /* IN; prompt to issue for the value */
double   defaultVal;        /* IN; default if no value */

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

   return atof(threadControlBlockPtr->buffer);
}       


/*****************************************************************************/
/*                                                                           */
/*                                   GetChar                                 */
/*                                                                           */
/*****************************************************************************/

char *GetChar(prompt, defaultVal)
char  *prompt;            /* IN; prompt to issue for the value */
char  *defaultVal;        /* IN; default if no value */

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

                                /* see if there's a possible keyword */
   if (strchr(threadControlBlockPtr->buffer, '$') != NULL)
   {                            /* see if it's possibly a non-ID keyword */
      if (strstr(threadControlBlockPtr->buffer, "_ID") == NULL)
      {                         /* translate possible non-ID keywords */
         if (strcmp(threadControlBlockPtr->buffer, "$USER$") == 0)
            strcpy(threadControlBlockPtr->buffer, GetControlStructPtr()->user);
         else if (strcmp(threadControlBlockPtr->buffer, "$PASSWORD$") == 0)
            strcpy(threadControlBlockPtr->buffer, GetControlStructPtr()->password);
         else if (strcmp(threadControlBlockPtr->buffer, "$SERVER$") == 0)
            strcpy(threadControlBlockPtr->buffer, GetControlStructPtr()->server);
         else if (strcmp(threadControlBlockPtr->buffer, "$SCHEMA$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstSchemaListName);
      }
      else
      {                         /* translate possible ID keywords */
         if (strcmp(threadControlBlockPtr->buffer, "$CREATE_ENTRY_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->createEntryId);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[0]);

         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEWFListId[4]);

         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEWFListId[4]);

         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEWFListId[4]);

         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$FIRST_GLE_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->firstGLEListId[4]);

         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$SECOND_GLE_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->secondGLEListId[4]);

         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID_1$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[0]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID_2$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[1]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID_3$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[2]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID_4$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[3]);
         else if (strcmp(threadControlBlockPtr->buffer, "$LAST_GLE_LIST_ID_5$") == 0)
            strcpy(threadControlBlockPtr->buffer, threadControlBlockPtr->lastGLEListId[4]);
      }
   }

   return threadControlBlockPtr->buffer;
}          


/*****************************************************************************/
/*                                                                           */
/*                              GetARTimestamp                               */
/*                                                                           */
/*****************************************************************************/

ARTimestamp GetARTimestamp(prompt, defaultVal)
char         *prompt;           /* IN; prompt to issue for the value */
ARTimestamp   defaultVal;       /* IN; default if no value */

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("%s", prompt);
   GetInputLine();
   if (strlen(threadControlBlockPtr->buffer) == 0)
      return defaultVal;        /* no value so use default */

   return atol(threadControlBlockPtr->buffer);
}          



/*****************************************************************************/
/*                                                                           */
/*                                  GetDate                                  */
/*                                                                           */
/*****************************************************************************/

int GetDate(prompt, defaultVal)
char  *prompt;            /* IN; prompt to issue for the value */
int    defaultVal;        /* IN; default if no value */

{
   char  *tempPtr;            /* working pointer */
   int    dateValue; 

   tempPtr = GetChar(prompt, "");
   if (GetJDValue(tempPtr, &dateValue) != AR_ERROR_NONE)
      dateValue = defaultVal;

   return dateValue;
}


/*****************************************************************************/
/*                                                                           */
/*                                GetMallocChar                              */
/*                                                                           */
/*****************************************************************************/

void GetMallocChar(prompt, defaultVal, value)
char  *prompt;            /* IN; prompt to issue for the value */
char  *defaultVal;        /* IN; default if no value */
char **value;             /* IN; pointer to the loaded character string */

{
   char  *tempPtr;            /* working pointer */

   tempPtr = GetChar(prompt, defaultVal);
   if (tempPtr == NULL)
      *value = NULL;
   else
      *value = (char *) strdup(tempPtr);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetAREntryIdList                            */
/*                                                                           */
/*****************************************************************************/

void GetAREntryIdList(value)
AREntryIdList  *value;         /* OUT; loaded with value for buffer */

{
   unsigned int   i;          /* working index */
   AREntryIdType *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of entry ids (0): ", 0);
   if (value->numItems == 0)
      value->entryIdList = NULL;
   else
   {
      value->entryIdList =
         (AREntryIdType *) malloc(sizeof(AREntryIdType) * value->numItems);
      if (value->entryIdList != NULL)
      {
         tempPtr = value->entryIdList;
         for (i = 0; i < value->numItems; i++)
         {
            strcpy(*tempPtr, GetChar("   Entry Id (): ", ""));
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARInternalIdList                          */
/*                                                                           */
/*****************************************************************************/

void GetARInternalIdList(value)
ARInternalIdList  *value;      /* OUT; loaded with value for buffer */

{
   unsigned int   i;          /* working index */
   ARInternalId  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of ids (0): ", 0);
   if (value->numItems == 0)
      value->internalIdList = NULL;
   else
   {
      value->internalIdList =
         (ARInternalId *) malloc(sizeof(ARInternalId) * value->numItems);
      if (value->internalIdList != NULL)
      {
         tempPtr = value->internalIdList;
         for (i = 0; i < value->numItems; i++)
         {
            *tempPtr = GetLong("   Id (1): ", 1L);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARTimestampList                             */
/*                                                                           */
/*****************************************************************************/

void GetARTimestampList(value)
ARTimestampList  *value;      /* OUT; loaded with timestamps */

{
   unsigned int   i;           /* working index */
   ARTimestamp   *tempPtr;     /* working pointer */

   value->numItems = GetUnsignedInt("Number of timestamps (0): ", 0);
   if (value->numItems == 0)
      value->timestampList = NULL;
   else
   {
      value->timestampList =
         (ARTimestamp *) malloc(sizeof(ARTimestamp) * value->numItems);
      if (value->timestampList != NULL)
      {
         tempPtr = value->timestampList;
         for (i = 0; i < value->numItems; i++)
         {
            *tempPtr = GetLong("   Timestamp (0): ", 0L);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARSchemaTypeList                            */
/*                                                                           */
/*****************************************************************************/

void GetARSchemaTypeList(
ARUnsignedIntList  *value
)

{
   char          buffer[64];  /* prompt text */
   unsigned int  i;           /* working index */

   sprintf(buffer, "%s", "   Number of values (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->intList = NULL;
   else
   {
      value->intList = calloc(value->numItems, sizeof(unsigned int));
      if (value->intList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            value->intList[i] = GetUnsignedInt(
                "Type of schema (all,regular,join,view,uplink,downlink,dialog,vendor) (0-6,8) (0): ", 0);
            if (GetARBoolean("Include hidden schemas (T): ", TRUE))
               value->intList[i] += AR_HIDDEN_INCREMENT;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                                 GetARNameList                             */
/*                                                                           */
/*****************************************************************************/

void GetARNameList(value, indent)
ARNameList  *value;            /* OUT; loaded with value for buffer */
char        *indent;

{
   char           buffer[64];  /* prompt text */
   unsigned int   i;           /* working index */
   ARNameType    *tempPtr;     /* working pointer */

   sprintf(buffer, "%s%s", indent, "   Number of names (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->nameList = NULL;
   else
   {
      value->nameList = calloc(value->numItems, sizeof(ARNameType));
      if (value->nameList != NULL)
      {
         tempPtr = value->nameList;
         for (i = 0; i < value->numItems; i++)
         {
            sprintf(buffer, "%s%s", indent, "      Name (): ");
            strcpy(*tempPtr, GetChar(buffer, ""));
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARControlStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARControlStruct(void)

{
   ThreadControlBlock *threadControlBlockPtr;

   threadControlBlockPtr = (ThreadControlBlock *) GetThreadControlBlockPtr();

   DriverPrintPrompt("Control record:\n");
                                 /* prompt for authentication string */
   if (snprintf(threadControlBlockPtr->buffer, sizeof(threadControlBlockPtr->buffer) - 1,
                "   Authentication string (%s) : ",
                threadControlBlockPtr->control.authString) < 0)
      threadControlBlockPtr->buffer[sizeof(threadControlBlockPtr->buffer) - 1] = '\0';
   strcpy(threadControlBlockPtr->control.authString,
          GetChar(threadControlBlockPtr->buffer,
                  threadControlBlockPtr->control.authString));

                                 /* prompt for user name */
   sprintf(threadControlBlockPtr->buffer, "   User name (%s) : ",
           threadControlBlockPtr->control.user);
   strcpy(threadControlBlockPtr->control.user,
          GetChar(threadControlBlockPtr->buffer,
                  threadControlBlockPtr->control.user));

                                 /* always override password */
   threadControlBlockPtr->control.password[0] = '\0'; 
   sprintf(threadControlBlockPtr->buffer, "   Password (%s) : ",
           threadControlBlockPtr->control.password);
   strcpy(threadControlBlockPtr->control.password,
          GetChar(threadControlBlockPtr->buffer,
                  threadControlBlockPtr->control.password));

   {
      /*
       * We store the locale and charSet in separate fields of
       * control.localeInfo, but present them to the user joined
       * together UNIX-style, e.g., "en_US.UTF-8".
       */
      ARLocalizationInfo *const linfo =
         &threadControlBlockPtr->control.localeInfo;
      char b[sizeof(linfo->locale) + sizeof(linfo->charSet) + 2];
      size_t n;

      sprintf(b, (*linfo->charSet ? "%s.%s" : "%s"),
            linfo->locale, linfo->charSet);
      sprintf(threadControlBlockPtr->buffer, "   Locale[.charSet] (%s) : ", b);
      strcpy(linfo->locale, GetChar(threadControlBlockPtr->buffer, b));
      n = strcspn(linfo->locale, ".;");
      if (linfo->locale[n])  /* locale.charSet; separate them */
      {
         char *cp = &linfo->locale[n];
         *cp++ = '\0';
         strcpy(linfo->charSet, cp);
      }
      else
         linfo->charSet[0] = '\0';
   }

   sprintf(threadControlBlockPtr->buffer, "   Time zone (%s) : ",
           threadControlBlockPtr->control.localeInfo.timeZone);
   strcpy(threadControlBlockPtr->control.localeInfo.timeZone,
          GetChar(threadControlBlockPtr->buffer,
                  threadControlBlockPtr->control.localeInfo.timeZone));

   sprintf(threadControlBlockPtr->buffer, "   Server (%s) : ", 
           threadControlBlockPtr->control.server);
   strcpy(threadControlBlockPtr->control.server, 
          GetChar(threadControlBlockPtr->buffer, 
                  threadControlBlockPtr->control.server));
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARFilterStatusStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARFilterStatusStruct(value)
ARFilterStatusStruct  *value;        /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Status Struct:\n");
   value->messageType =
      GetUnsignedInt("   Note, Warning, or Error (0, 1, 2) (2) : ", 2);
   value->messageNum = GetLong("   Message number (0): ", 0L);
   GetMallocChar("   Message text (): ", "", &value->messageText);
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARMessageStruct                             */
/*                                                                           */
/*****************************************************************************/

void GetARMessageStruct(value)
ARMessageStruct *value;        /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Status Struct:\n");
   value->messageType =
      GetUnsignedInt("   Note, Warning, Error, Prompt, Accessible hint or Tooltip (0, 1, 2, 5, 6, 7) (2) : ",2);
   value->messageNum = GetLong("   Message number (10000): ", 10000L);
   GetMallocChar("   Message text (): ", "", &value->messageText);
   value->usePromptingPane = GetARBoolean("   Use Prompting Pane? (T): ", TRUE);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARCoordList                               */
/*                                                                           */
/*****************************************************************************/

void GetARCoordList(value)
ARCoordList **value;

{
   unsigned int    i;
   ARCoordStruct  *tempPtr;

   /* Allocate memory for ARCoordList */
   *value = (ARCoordList *) malloc(sizeof(ARCoordList));
   if (*value != NULL)
   {
      (*value)->numItems = GetUnsignedInt("Number of Items in the List (0): ", 0L);
      if ((*value)->numItems == 0)
         (*value)->coords = NULL;
      else
      {
         (*value)->coords = (ARCoordStruct *) malloc(sizeof(ARCoordStruct) *
                                                     (*value)->numItems);
         if ((*value)->coords != NULL)
         {
            tempPtr = (*value)->coords;
            for (i = 0; i < (*value)->numItems; i++)
            {
               tempPtr->x = GetLong("x (0): ",  0L);
               tempPtr->y = GetLong("y (0): ",  0L);
               tempPtr++;
            }
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARByteList                               */
/*                                                                           */
/*****************************************************************************/

void GetARByteList
(
 ARByteList **value,
 ARBoolean    imageFlag    /* IN;  flag to retrieve image byte list from */
                           /*      a file instead of from tty input      */
)
{
   unsigned int    choice = 2;
   unsigned int    i;
   char            prompt[255];
   unsigned char  *tempPtr;

   /* Allocate memory for ARByteList struct */
   *value = (ARByteList *) malloc(sizeof(ARByteList));
   if (*value != NULL)
   {
      (*value)->type = GetLong("Byte List Type (0): ", 0L);
      if (imageFlag)
      {
         /* determine how user will supply content: via filename or prompted */
         sprintf(prompt, "%s%s%s%s",
            "Choose:\n",
            "   1 - prompt for filename,\n",
            "   2 - prompt for string\n",
            "   default (1): "); 

         choice = GetUnsignedInt(prompt, 1);

         if ((choice < 1) || (choice > 2))
         {
            DriverPrintWarning("   WARNING: choice outside range 1-2; defaulting to prompt for filename(1)\n");
            choice = 1;
         }
      }

      switch (choice)
      {
         case 1:
         {
            long         fileLen;
            ARNameType   fileName;
            FILE        *filePtr = NULL;

            strcpy(fileName, GetChar("Enter filename: ", ""));
            if ((filePtr = fopen(fileName, "rb")) != NULL)
            {
               fseek(filePtr, 0L, 2);
               fileLen = ftell(filePtr);

               (*value)->numItems = (unsigned int) fileLen;
               (*value)->bytes = (unsigned char *) malloc((unsigned)
                                    (sizeof(unsigned char) * (*value)->numItems));

               rewind(filePtr);
               if ((*value)->bytes != NULL)
               {
                  int amtRead = 0;
                  int result = 0;
                  while (amtRead < fileLen)
                  {
                     result = fread(&((*value)->bytes[amtRead]), 1, fileLen, filePtr);
                     if (result <= 0)
                        break;
                     amtRead += result;
                  }
               }
               fclose(filePtr);
            }
            else
            {
               DriverPrintPrompt("Error: Cannot open file \"%s\"\n", fileName);
               (*value)->type = 0;
               (*value)->numItems = 0;
               (*value)->bytes = NULL;
            }
            break;
         }
         case 2:
            (*value)->numItems = GetUnsignedInt("Number of Items in the List (0): ", 0L);
            if ((*value)->numItems == 0)
               (*value)->bytes = NULL;
            else
            {
               (*value)->bytes = (unsigned char *) malloc((unsigned)
                                  (sizeof(unsigned char) * (*value)->numItems));
               if ((*value)->bytes != NULL)
               {
                  tempPtr = (*value)->bytes;
                  for (i = 0; i < (*value)->numItems; i++)
                  {
                     tempPtr = (unsigned char *) GetChar("Byte (0): ",  "");
                     tempPtr++;
                  }
               }
            }
            break;
         default:
            (*value)->type = 0;
            (*value)->numItems = 0;
            (*value)->bytes = NULL;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                              GetARImageData                               */
/*                                                                           */
/*****************************************************************************/

void GetARImageData
(
 ARImageDataStruct **value
)
{
   unsigned int    i;
   unsigned int    choice = 2;
   unsigned char  *tempPtr;
   char            prompt[255];

   /* Allocate memory for ARByteList struct */
   *value = (ARImageDataStruct *) malloc(sizeof(ARImageDataStruct));
   if (*value != NULL)
   {
      /* determine how user will supply content: via filename or prompted */
      sprintf(prompt, "%s%s%s%s",
         "Choose:\n",
         "   1 - prompt for filename,\n",
         "   2 - prompt for string\n",
         "   default (1): "); 

      choice = GetUnsignedInt(prompt, 1);

      if ((choice < 1) || (choice > 2))
      {
         DriverPrintWarning("   WARNING: choice outside range 1-2; defaulting to prompt for filename(1)\n");
         choice = 1;
      }

      switch (choice)
      {
         case 1:
         {
            FILE  *filePtr = NULL;
            long   fileLen;
            ARNameType fileName = "\0";

            strcpy(fileName, GetChar("Enter filename: ", ""));
            if ((fileName[0] != '\0') &&
                (filePtr = fopen(fileName, "rb")) != NULL)
            {
               fseek(filePtr, 0L, 2);
               fileLen = ftell(filePtr);

               (*value)->numItems = (unsigned int) fileLen;
               (*value)->bytes = (unsigned char *) malloc((unsigned)
                                    (sizeof(unsigned char) * (*value)->numItems));

               rewind(filePtr);
               if ((*value)->bytes != NULL)
               {
                  int amtRead = 0;
                  int result = 0;
                  while (amtRead < fileLen)
                  {
                     result = fread(&((*value)->bytes[amtRead]), 1, fileLen, filePtr);
                     if (result <= 0)
                        break;
                     amtRead += result;
                  }
               }
               fclose(filePtr);
            }
            else
            {
               DriverPrintPrompt("Error: Cannot open file \"%s\"\n", fileName);
               (*value)->numItems = 0;
               (*value)->bytes = NULL;
            }
            break;
         }
         case 2:
            (*value)->numItems = GetUnsignedInt("Number of Items in the List (0): ", 0L);
            if ((*value)->numItems == 0)
               (*value)->bytes = NULL;
            else
            {
               (*value)->bytes = (unsigned char *) malloc((unsigned)
                                       (sizeof(unsigned char) * (*value)->numItems));
               if ((*value)->bytes != NULL)
               {
                  tempPtr = (*value)->bytes;
                  for (i = 0; i < (*value)->numItems; i++)
                  {
                     tempPtr = (unsigned char *) GetChar("Byte (0): ",  "");
                     tempPtr++;
                  }
               }
            }
            break;
         default:
            (*value)->numItems = 0;
            (*value)->bytes = NULL;
      }
   }
}

/*****************************************************************************/
/*                                                                           */
/*                            GetARLocStructForGet                           */
/*                                                                           */
/*****************************************************************************/

void GetARLocStructForGet(value)
ARLocStruct  *value;
{
   GetMallocChar("Enter filename: ", "", &value->u.filename);
   /* indicate we are using filename for transfer */
   value->locType = AR_LOC_FILENAME;
}


/*****************************************************************************/
/*                                                                           */
/*                                GetARAttach                                */
/*                                                                           */
/*****************************************************************************/

void GetARAttach(value)
ARAttachStruct **value;
{
   unsigned int   choice;
   FILE          *fd;
   char           prompt[255];
   struct stat    statBuf;
  
   /* Allocate memory for ARAttach struct */
   *value = (ARAttachStruct *) calloc(1, sizeof(ARAttachStruct));
   if (*value != NULL)
   {
      /* get name */
      GetMallocChar("Attachment Name: ", "", &(*value)->name);

      /* determine how user will supply content: via fname or prompted */
      sprintf(prompt, "%s%s%s%s",
         "Choose:\n",
         "   1 - prompt for filename,\n",
         "   2 - prompt for string\n",
         "   default (1): ");

      choice = GetUnsignedInt(prompt, 1);

      if ((choice < 1) || (choice > 2))
      {
         DriverPrintWarning("   WARNING: choice outside range 1-2; defaulting to prompt for filename(1)\n");
         choice = 1;
      }

      switch (choice)
      {
         case 1:   /* load file */
            for (;;)
            {
               GetMallocChar("Enter filename: ", "", &(*value)->loc.u.filename);
               (*value)->loc.locType = AR_LOC_FILENAME;

               if ((fd = fopen((*value)->loc.u.filename, "rb")) != NULL)
               {
                  fclose(fd);
                  stat((*value)->loc.u.filename, &statBuf);
                  (*value)->origSize = (unsigned long) statBuf.st_size;
                     
                     /* API layer will override the compression value with */
                     /* the real value, if any.                            */
                  (*value)->compSize = (*value)->origSize;
                  break;
               }
               else
               {
                  DriverPrintError("  ERROR: failed to open file: %s\n",
                                   (*value)->loc.u.filename);
                  if ((*value)->loc.u.filename)
                  {
                     free((*value)->loc.u.filename);
                  }  
               }
            }
            break;
         case 2: /* prompt for string */
            /* indicate we're using buffer to transfer string */
            (*value)->loc.locType = AR_LOC_BUFFER;

            GetMallocChar("Enter string: ", "", (char **) &(*value)->loc.u.buf.buffer);

            (*value)->origSize =
               (long) strlen((char *)(*value)->loc.u.buf.buffer) + 1;
               /* API layer will override the compression value with */
               /* the real value, if any.                            */
            (*value)->compSize = (*value)->origSize;

            (*value)->loc.u.buf.bufSize = (*value)->origSize;
            break;
         default:
            (*value)->origSize = 0L;
            (*value)->compSize = 0L;
            (*value)->loc.locType = AR_LOC_NULL;
            (*value)->loc.u.buf.bufSize = 0L;
            (*value)->loc.u.buf.buffer = NULL;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARCurrency                               */
/*                                                                           */
/*****************************************************************************/

void GetARCurrency(value)
ARCurrencyStruct **value;
{
   char  *currencyCode;

   /* Allocate memory for ARCurrency struct */
   *value = (ARCurrencyStruct *) calloc(1, sizeof(ARCurrencyStruct));
   if (*value != NULL)
   {
      /* get currency value */
      GetMallocChar("Currency Value: ", "", &(*value)->value);
      /* get currency code */
      GetMallocChar("Currency Code : ", "", &currencyCode);
      strncpy((*value)->currencyCode, currencyCode, AR_CURRENCY_CODE_LEN);
      (*value)->currencyCode[AR_CURRENCY_CODE_LEN] = '\0';
      /* get conversion date */
      (*value)->conversionDate = GetARTimestamp("Conversion Date (0): ",
                                                (ARTimestamp) 0);
   }
}


/*****************************************************************************/
/*                                                                           */
/*                                 GetValue                                  */
/*                                                                           */
/*****************************************************************************/

void GetValue
(
 ARValueStruct  *value,   /* IN; contains data type for value */
 ARBoolean    imageFlag,  /* IN;  flag to retrieve image byte list from */
                          /*      a file instead of from tty input      */
 char           *indent
)
{
   char buffer[128];/* prompt text */
   sprintf(buffer, "%s%s", indent, "Number of query sources (0): ");

   switch (value->dataType)
   {
      case AR_DATA_TYPE_NULL    :
         /* no value needed as is NULL */
         break;
      case AR_DATA_TYPE_KEYWORD :
         sprintf(buffer, "%s%s", indent, "Keyword Code (0): ");
         value->u.keyNum = GetUnsignedInt(buffer, 0);
         break;
      case AR_DATA_TYPE_INTEGER :
         sprintf(buffer, "%s%s", indent, "Integer Value (0): ");
         value->u.intVal = GetLong(buffer, 0L);
         break;
      case AR_DATA_TYPE_REAL    :
         sprintf(buffer, "%s%s", indent, "Real Value (0): ");
         value->u.realVal = GetReal(buffer, (double) 0);
         break;
      case AR_DATA_TYPE_CHAR    :
         sprintf(buffer, "%s%s", indent, "Char Value (): ");
         GetMallocChar(buffer, "", &value->u.charVal);
         break;
      case AR_DATA_TYPE_DIARY   :
         sprintf(buffer, "%s%s", indent, "Diary Value (): ");
         GetMallocChar(buffer, "", &value->u.diaryVal);
         break;
      case AR_DATA_TYPE_ENUM    :
         sprintf(buffer, "%s%s", indent, "Enumerated Value (0): ");
         value->u.enumVal = GetLong(buffer, 0L);
         break;
      case AR_DATA_TYPE_TIME    :
         sprintf(buffer, "%s%s", indent, "Time Value (0): ");
         value->u.timeVal = GetARTimestamp(buffer, (ARTimestamp) 0);
         break;
      case AR_DATA_TYPE_TIME_OF_DAY    :
         sprintf(buffer, "%s%s", indent, "Time Value (0): ");
         value->u.timeOfDayVal = GetARTimestamp(buffer, (ARTime) 0);
         break;
      case AR_DATA_TYPE_BITMASK :
         sprintf(buffer, "%s%s", indent, "Bitmask Value (0): ");
         value->u.maskVal = GetLong(buffer, 0L);
         break;
      case AR_DATA_TYPE_BYTES   :
         GetARByteList(&value->u.byteListVal, imageFlag);
         break;
      case AR_DATA_TYPE_JOIN    :
      case AR_DATA_TYPE_TRIM    :
      case AR_DATA_TYPE_CONTROL :
      case AR_DATA_TYPE_PAGE    :
      case AR_DATA_TYPE_PAGE_HOLDER :
      case AR_DATA_TYPE_ATTACH_POOL :
         break;
      case AR_DATA_TYPE_ULONG   :
         sprintf(buffer, "%s%s", indent, "Ulong Value (0): ");
         value->u.ulongVal = GetLong(buffer, 0L);
         break;
      case AR_DATA_TYPE_COORDS  :
         GetARCoordList(&value->u.coordListVal);
         break;
      case AR_DATA_TYPE_ATTACH  :
         GetARAttach(&value->u.attachVal);
         break;
      case AR_DATA_TYPE_DECIMAL :
         sprintf(buffer, "%s%s", indent, "Decimal Value (): ");
         GetMallocChar(buffer, "", &value->u.decimalVal);
         break;
      case AR_DATA_TYPE_CURRENCY :
         GetARCurrency(&value->u.currencyVal);
         break;
      case AR_DATA_TYPE_VIEW    :
         sprintf(buffer, "%s%s", indent, "View Value (): ");
         GetMallocChar(buffer, "", &value->u.charVal);
         break;
      case AR_DATA_TYPE_DISPLAY :
         sprintf(buffer, "%s%s", indent, "Display Value (): ");
         GetMallocChar(buffer, "", &value->u.charVal);
         break;
      case AR_DATA_TYPE_DATE:
         sprintf(buffer, "%s%s", indent, "Date Value () [YYYY-MM-DD]: ");
         value->u.dateVal = GetDate(buffer, 0);
         break;
      default                   :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARValueStruct                            */
/*                                                                           */
/*****************************************************************************/

void GetARValueStruct
(
 ARValueStruct  *value,   /* OUT; loaded with value for buffer */
 ARBoolean    imageFlag   /* IN;  flag to retrieve image byte list from */
                          /*      a file instead of from tty input      */
)
{
   value->dataType = GetUnsignedInt(
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/attach/\n"
"         currency/date/timeofday/join/trim/control/Table/Column/ulong/\n"
"         coords/view/display (0-14, 30-34, 40-43) (0): ", 0);
   GetValue(value, imageFlag, "");
}


/*****************************************************************************/
/*                                                                           */
/*                                GetARValueList                             */
/*                                                                           */
/*****************************************************************************/

void GetARValueList(value)
ARValueList  *value;         /* OUT; loaded with value for buffer */

{
   unsigned int    i;          /* working index */
   ARValueStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of values (0): ", 0);
   if (value->numItems == 0)
      value->valueList = NULL;
   else
   {
      value->valueList =
         (ARValueStruct *) malloc(sizeof(ARValueStruct) * value->numItems);
      if (value->valueList != NULL)
      {
         tempPtr = value->valueList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARValueStruct(tempPtr, FALSE);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetAREntryListFieldStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetAREntryListFieldStruct(value)
AREntryListFieldStruct  *value;   /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Entry list field entry\n");
   value->fieldId = GetLong("      Field id (0): ", 0L);
   value->columnWidth = GetUnsignedInt("      Column width (20): ", 20);
   strcpy(value->separator, GetChar("      Separator (  ): ", "  "));
}


/*****************************************************************************/
/*                                                                           */
/*                           GetAREntryListFieldList                         */
/*                                                                           */
/*****************************************************************************/

void GetAREntryListFieldList(value)
AREntryListFieldList  *value;  /* OUT; loaded with value for buffer */

{
   unsigned int             i;       /* working index */
   AREntryListFieldStruct  *tempPtr; /* working pointer */

   value->numItems = GetUnsignedInt("Number of query list fields (0): ", 0);
   if (value->numItems == 0)
      value->fieldsList = NULL;
   else
   {
      value->fieldsList =
         (AREntryListFieldStruct *) malloc(sizeof(AREntryListFieldStruct) *
                                           value->numItems);
      if (value->fieldsList != NULL)
      {
         tempPtr = value->fieldsList;
         for (i = 0; i < value->numItems; i++)
         {
            GetAREntryListFieldStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARFieldValueStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARFieldValueStruct(value)
ARFieldValueStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->fieldId = GetLong("Field id: ", 0L);
   GetARValueStruct(&value->value, FALSE);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARFieldValueList                          */
/*                                                                           */
/*****************************************************************************/

void GetARFieldValueList(value)
ARFieldValueList  *value;      /* OUT; loaded with value for buffer */

{
   unsigned int         i;        /* working index */
   ARFieldValueStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of field/value pairs (0): ", 0);
   if (value->numItems == 0)
      value->fieldValueList = NULL;
   else
   {
      value->fieldValueList =
         (ARFieldValueStruct *) malloc(sizeof(ARFieldValueStruct) *
                                       value->numItems);
      if (value->fieldValueList != NULL)
      {
         tempPtr = value->fieldValueList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARFieldValueStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARFieldValueOrArithStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARFieldValueOrArithStruct(value)
ARFieldValueOrArithStruct  *value; /* OUT; loaded with value for buffer */

{
   value->tag = GetUnsignedInt(
"Fld,Value,Arith,Stat-Hist,Val-Set,Currency,Fld-Tran,Fld-DB,L-Var,\nQuery,Currency-Tran,Currency-DB,Currency-Current,Fld-Current (1-6,50-56,99): ",
                       0);
   switch (value->tag)
   {
      case AR_FIELD        :
      case AR_FIELD_TRAN   :
      case AR_FIELD_DB     :
      case AR_FIELD_CURRENT:
         value->u.fieldId = GetLong("Field id (0): ", 0L);
         break;
      case AR_VALUE        :
         GetARValueStruct(&value->u.value, FALSE);
         break;
      case AR_ARITHMETIC   :
         value->u.arithOp = (ARArithOpStruct *) malloc(sizeof(ARArithOpStruct));
         if (value->u.arithOp != NULL)
            GetARArithOpStruct((ARArithOpStruct *) value->u.arithOp);
         break;
      case AR_STAT_HISTORY :
         value->u.statHistory.enumVal =
            (unsigned long) GetLong("Enumerated value index (0): ", 0L);
         value->u.statHistory.userOrTime =
            GetUnsignedInt("User or Time field (1 or 2) (2): ", 2);
         break;
      case AR_VALUE_SET    :
         GetARValueList(&value->u.valueSet);
         break;
      case AR_CURRENCY_FLD        :
      case AR_CURRENCY_FLD_TRAN   :
      case AR_CURRENCY_FLD_DB     :
      case AR_CURRENCY_FLD_CURRENT:
         value->u.currencyField = (ARCurrencyPartStruct *)
                                             malloc(sizeof(ARCurrencyPartStruct));
         if (value->u.currencyField != NULL)
         {
            value->u.currencyField->fieldId = GetLong("Currency Field id (0): ", 0L);
            value->u.currencyField->partTag =
               GetUnsignedInt("Currency Part - Field,Value,Type,Date,Functional (0-4) (0): ", 0);
            if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            {
               strncpy(value->u.currencyField->currencyCode,
                       GetChar("Functional currency code: ", ""),
                       AR_CURRENCY_CODE_LEN);
               value->u.currencyField->currencyCode[AR_CURRENCY_CODE_LEN] = '\0';
            }
            else
               value->u.currencyField->currencyCode[0] = '\0';
         }
         break;
      case AR_LOCAL_VARIABLE :
         value->u.variable =
            GetUnsignedInt("Local Variable number (0 to 10) (0): ", 0);
         break;
      case AR_QUERY        :
         value->u.queryValue = (ARQueryValueStruct *)
                                             malloc(sizeof(ARQueryValueStruct));
         if (value->u.queryValue != NULL)
         {
            strcpy(value->u.queryValue->schema, GetChar("Schema: ", ""));
            strcpy(value->u.queryValue->server, GetChar("Server: ", ""));
            value->u.queryValue->qualifier = (ARQualifierStruct *)
                                                   malloc(sizeof(ARQualifierStruct));
            GetARQualifierStruct(value->u.queryValue->qualifier);
            value->u.queryValue->valueField = GetLong("Field id (0): ", 0L);
            value->u.queryValue->multiMatchCode =
               GetUnsignedInt("Multi-Match code (error, first, set) (1, 2, 3) (1): ", 1);
         }
         break;
      default              :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARArithOpStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARArithOpStruct(value)
ARArithOpStruct  *value;       /* OUT; loaded with value for buffer */

{
   value->operation =
      GetUnsignedInt("Arithmetic op code -- +, -, *, /, %, unary - (1 - 6): ", 0);
   if (value->operation != AR_ARITH_OP_NEGATE)
   {
      DriverPrintPrompt("Left operand - ");
      GetARFieldValueOrArithStruct(&value->operandLeft);
   }
   DriverPrintPrompt("Right operand - ");
   GetARFieldValueOrArithStruct(&value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARRelOpStruct                            */
/*                                                                           */
/*****************************************************************************/

void GetARRelOpStruct(value)
ARRelOpStruct  *value;         /* OUT; loaded with value for buffer */

{
   value->operation =
      GetUnsignedInt("Relational op code - EQ, GT, GE, LT, LE, NE, LIKE, IN (1 - 8): ", 0);
   DriverPrintPrompt("Left operand - ");
   GetARFieldValueOrArithStruct(&value->operandLeft);
   DriverPrintPrompt("Right operand - ");
   GetARFieldValueOrArithStruct(&value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARQualifierStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARQualifierStruct(value)
ARQualifierStruct  *value;     /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Qualifier Struct:\n");

   /* initialize the structure since it does not get completely */
   /* filled in all cases below                                 */
   memset((char *) value, '\0', sizeof(ARQualifierStruct));

   value->operation =
      GetUnsignedInt("None, And, Or, Not, Relop or From Field (0, 1, 2, 3, 4, 5) (0): ", 0);
   switch (value->operation)
   {
      case AR_COND_OP_NONE   :
         /* nothing to load if no qualifier */
         break;
      case AR_COND_OP_AND    :
      case AR_COND_OP_OR     :
         value->u.andor.operandLeft =
            (struct ARQualifierStruct *) malloc(sizeof(ARQualifierStruct));
         if (value->u.andor.operandLeft)
            GetARQualifierStruct(value->u.andor.operandLeft);
         if (value->operation == AR_COND_OP_AND)
            DriverPrintPrompt("AND\n");
         else
            DriverPrintPrompt("OR\n");
         value->u.andor.operandRight =
            (struct ARQualifierStruct *) malloc(sizeof(ARQualifierStruct));
         if (value->u.andor.operandRight)
            GetARQualifierStruct(value->u.andor.operandRight);
         break;
      case AR_COND_OP_NOT    :
         value->u.not =
            (struct ARQualifierStruct *) malloc(sizeof(ARQualifierStruct));
         if (value->u.not != NULL)
            GetARQualifierStruct(value->u.not);
         break;
      case AR_COND_OP_REL_OP :
         value->u.relOp = (ARRelOpStruct *) malloc(sizeof(ARRelOpStruct));
         if (value->u.relOp != NULL)
            GetARRelOpStruct(value->u.relOp);
         break;
      case AR_COND_OP_FROM_FIELD:
         value->u.fieldId = GetLong("Field id (1): ", 1L);
         break;
      default                :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARQualifierList                             */
/*                                                                           */
/*****************************************************************************/

void GetARQualifierList(value, indent)
ARQualifierList  *value;            /* OUT; loaded with value for buffer */
char             *indent;

{
   char                buffer[64];  /* prompt text */
   unsigned int        i;           /* working index */
   ARQualifierStruct  *tempPtr;     /* working pointer */

   sprintf(buffer, "%s%s", indent, "   Number of qualifier structs (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->qualifierList = NULL;
   else
   {
      value->qualifierList = calloc(value->numItems, sizeof(ARQualifierStruct));
      if (value->qualifierList != NULL)
      {
         tempPtr = value->qualifierList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARQualifierStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARSortStruct                              */
/*                                                                           */
/*****************************************************************************/

void GetARSortStruct(value)
ARSortStruct  *value;          /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Sort list entry\n");
   value->fieldId = GetLong("      Internal id of field to sort on (0): ", 0L);
   value->sortOrder =
      GetUnsignedInt("      Ascending or Descending (1 or 2) (1): ", 1);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARSortList                               */
/*                                                                           */
/*****************************************************************************/

void GetARSortList(value)
ARSortList *value;            /* OUT; loaded with value for buffer */

{
   unsigned int   i;           /* working index */
   ARSortStruct  *tempPtr;     /* working pointer */

   value->numItems = GetUnsignedInt("Number of sort fields (0): ", 0);
   if (value->numItems == 0)
      value->sortList = NULL;
   else
   {
      value->sortList =
         (ARSortStruct *) malloc(sizeof(ARSortStruct) * value->numItems);
      if (value->sortList != NULL)
      {
         tempPtr = value->sortList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARSortStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARIndexStruct                              */
/*                                                                           */
/*****************************************************************************/

void GetARIndexStruct(value)
ARIndexStruct  *value;         /* OUT; loaded with value for buffer */

{
   unsigned int   i;          /* working index */

   DriverPrintPrompt("   Index list entry\n");
   value->numFields = GetUnsignedInt("      Number of fields in index (1): ", 1);
   for (i = 0; i < value->numFields; i++)
   {
      value->fieldIds[i] =
                    GetLong("      Internal id of field to index on (0): ", 0L);
   }
   value->unique = GetARBoolean("      Unique index (F): ", FALSE);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARIndexList                               */
/*                                                                           */
/*****************************************************************************/

void GetARIndexList(value)
ARIndexList  *value;           /* OUT; loaded with value for buffer */

{
   unsigned int    i;           /* working index */
   ARIndexStruct  *tempPtr;     /* working pointer */

   value->numItems = GetUnsignedInt("Number of indexes (0): ", 0);
   if (value->numItems == 0)
      value->indexList = NULL;
   else
   {
      value->indexList =
         (ARIndexStruct *) malloc(sizeof(ARIndexStruct) * value->numItems);
      if (value->indexList != NULL)
      {
         tempPtr = value->indexList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARIndexStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARArchiveInfoStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARArchiveInfoStruct(value)
ARArchiveInfoStruct  *value;           /* OUT; loaded with value for buffer */

{
   int   i, day, date, type;
   int   secsFromNow;

   type = GetUnsignedInt("Archive Type (0-None, 1-Form, 2-Delete, 3-Form&Delete, Add 32 for 'No Attachments, Add 64 for 'No Diary fields' ): ", 1);
   value->archiveType = 0;
   if (type == 0)
   {
      memset(value, 0, sizeof(ARArchiveInfoStruct));
      return;
   }
   if (type & AR_ARCHIVE_FORM)
   {
      value->archiveType = AR_ARCHIVE_FORM;
      strcpy(value->u.formName, GetChar("Archive Form Name : ", ""));
   }
   if (type & AR_ARCHIVE_FILE_ARX)
   {
      value->archiveType = AR_ARCHIVE_FILE_ARX;
      value->u.dirPath = strdup(GetChar("Archive Dir Path : ", ""));
   }
   if (type & AR_ARCHIVE_FILE_XML)
   {
      value->archiveType = AR_ARCHIVE_FILE_XML;
      value->u.dirPath = strdup(GetChar("Archive Dir Path : ", ""));
   }
   if (type & AR_ARCHIVE_DELETE)
      value->archiveType |= AR_ARCHIVE_DELETE;
   /* if we are setting for Delete only, then 'no attachments' or
      'no diary fields' cannot be set */
   if (value->archiveType == AR_ARCHIVE_DELETE)
      ;
   else
   {
      if (type & AR_ARCHIVE_NO_ATTACHMENTS)
         value->archiveType |= AR_ARCHIVE_NO_ATTACHMENTS;
      if (type & AR_ARCHIVE_NO_DIARY)
         value->archiveType |= AR_ARCHIVE_NO_DIARY;
   }
   value->enable = GetUnsignedInt("Enable Archive (0): ", 0);
   GetARQualifierStruct(&value->query);
   value->archiveTime.monthday = 0;
   value->archiveTime.weekday = 0;
   value->archiveTime.hourmask = 0;
   value->archiveTime.minute = 0;
   if (GetARBoolean("Do you want to archive now? (F): ", FALSE))
   {
      time_t      now;
      struct tm  *nowTm;
      secsFromNow = GetInt("Enter secs from now: (>= 0) (0): ", 0);
      now = time(NULL);
      if (now != -1)
      {
         now = now + secsFromNow;
         nowTm = localtime(&now);
         value->archiveTime.monthday |= AR_SETDAY(nowTm->tm_mday);
         value->archiveTime.weekday |= AR_SETDAY(nowTm->tm_wday);
         value->archiveTime.hourmask |= AR_SETHOUR(nowTm->tm_hour);
         value->archiveTime.minute = nowTm->tm_min;
      }
   }
   else
   {
      day = GetInt("how many days to set in day/month format? (> 0) (1): ",1);
      i = 0;
      while (i < day)
      {
         date = GetInt("date of the day, 0 to 30 for month/day (0): ", 0);
         value->archiveTime.monthday |= AR_SETDAY(date);
         i++;
      }
      day = GetInt("how many days to set in day/week format? (> 0) (1): ", 1);
      i = 0;
      while (i < day)
      {
         date = GetInt("date of the day, 0 to 6 for sunday to saturday (0): ", 0);
         value->archiveTime.weekday |= AR_SETDAY(date);
         i++;
      }
      day = GetInt("how many hour marks to set in a day? (> 0) (1): ", 1);
      i = 0;
      while (i < day)
      {
         date = GetInt("hour of the day, 0 to 23 for 24 hours of a day (0): ", 0);
         value->archiveTime.hourmask |= AR_SETHOUR(date);
         i++;
      }
      day = GetInt("set minute mark of the hour ? (0-59) (0): ", 0);
      value->archiveTime.minute = day;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARAuditInfoStruct                             */
/*                                                                           */
/*****************************************************************************/

void GetARAuditInfoStruct(value)
ARAuditInfoStruct  *value;           /* OUT; loaded with value for buffer */

{
   int   style;

   style = GetUnsignedInt("Audit Style (0-None, 1-Copy, 2-Log): ", 1);
   value->style = 0;
   if (style == 0)
   {
      memset(value, 0, sizeof(ARAuditInfoStruct));
      return;
   }
   if ((style == AR_AUDIT_COPY) || (style == AR_AUDIT_LOG))
   {
      value->style = style;
      strcpy(value->formName, GetChar("Audit Form Name : ", ""));
   }
   value->enable = GetUnsignedInt("Enable Audit (0): ", 0);
   GetARQualifierStruct(&value->query);
   value->auditMask = GetUnsignedInt("Audit Only Changed Fields (0-Default, 1-Yes, 2-No) (0): ", 1);
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARCompoundSchema                            */
/*                                                                           */
/*****************************************************************************/

void GetARCompoundSchema(value)
ARCompoundSchema  *value;         /* OUT; loaded with value for buffer */

{
   value->schemaType = GetUnsignedInt("Schema Type (1): ", 1);
   if (value->schemaType > AR_SCHEMA_VENDOR || 
       value->schemaType < AR_SCHEMA_REGULAR)
      value->schemaType = AR_SCHEMA_REGULAR;
   switch (value->schemaType) 
   {
      case AR_SCHEMA_REGULAR :
      case AR_SCHEMA_DIALOG :
         break;
      case AR_SCHEMA_JOIN :
         strcpy(value->u.join.memberA, 
                GetChar("   Join member A name : ", ""));
         strcpy(value->u.join.memberB, 
                GetChar("   Join member B name : ", ""));
         GetARQualifierStruct(&value->u.join.joinQual);
         value->u.join.option = GetUnsignedInt("   Join option(0): ", 0);
         break;
      case AR_SCHEMA_VIEW :
         strcpy(value->u.view.tableName,
                GetChar("   View table name : ", ""));
         strcpy(value->u.view.keyField,
                GetChar("   Key Field : ", ""));
         break;
      case AR_SCHEMA_VENDOR :
         strcpy(value->u.vendor.vendorName,
                GetChar("   Vendor name : ", ""));
         strcpy(value->u.vendor.tableName,
                GetChar("   Vendor table name : ", ""));
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARFieldMapping                             */
/*                                                                           */
/*****************************************************************************/

void GetARFieldMapping(value)
ARFieldMappingStruct  *value;         /* OUT; loaded with value for buffer */

{
   value->fieldType = GetUnsignedInt("Field Mapping Type (1): ", 1);
   if (value->fieldType > AR_FIELD_VENDOR || 
       value->fieldType < AR_FIELD_NONE)
      value->fieldType = AR_FIELD_REGULAR;
   switch (value->fieldType) 
   {
      case AR_FIELD_REGULAR :
      case AR_FIELD_NONE:  
         break;
      case AR_FIELD_JOIN :
         value->u.join.schemaIndex = GetUnsignedInt("   schema index (0): ", 0);
         if (value->u.join.schemaIndex > AR_FIELD_MAPPING_SECONDARY ||
             value->u.join.schemaIndex < AR_FIELD_MAPPING_PRIMARY)
            value->u.join.schemaIndex = AR_FIELD_MAPPING_PRIMARY;
         value->u.join.realId = GetLong("   real Id : ", 0);
         break;
      case AR_FIELD_VIEW :
         strcpy(value->u.view.fieldName, 
                GetChar("   field name : ", ""));
         break;
      case AR_FIELD_VENDOR :
         strcpy(value->u.vendor.fieldName, 
                GetChar("   field name : ", ""));
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARPropList                               */
/*                                                                           */
/*****************************************************************************/

void GetARPropList(
ARPropList  *value,        /* OUT; loaded with value for buffer */
ARBoolean    imageFlag     /* IN;  flag to retrieve image byte list from */
                           /*      a file instead of from tty input      */
)
{
   unsigned int   i;
   ARPropStruct  *tempPtr;

   value->numItems = GetUnsignedInt("Number of Properties (0): ", 0);
   if (value->numItems == 0)
      value->props = NULL;
   else
   {
      value->props =
         (ARPropStruct *) malloc((unsigned) (sizeof(ARPropStruct) *
                                             value->numItems));
      if (value->props != NULL)
      {
         tempPtr = value->props;
         for (i = 0; i < value->numItems; i++)
         {
            tempPtr->prop = GetLong("Prop Tag (0): ", 0L);
            GetARValueStruct(&tempPtr->value, imageFlag);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARPropListPtrList                            */
/*                                                                           */
/*****************************************************************************/

void GetARPropListPtrList(
ARPropListList  *value,   /* OUT; loaded with value for buffer */
ARBoolean    imageFlag    /* IN;  flag to retrieve image byte list from */
                          /*      a file instead of from tty input      */
)

{
   unsigned int   i;          /* working index */

   value->numItems = GetUnsignedInt("   Number of Field property instance lists (0): ", 0);
   if (value->numItems == 0)
      value->propsList = NULL;
   else
   {
      value->propsList =
         (ARPropList *) calloc(value->numItems, sizeof(ARPropList));
      if (value->propsList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            GetARPropList(&value->propsList[i], imageFlag);
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARDisplayInstanceList                          */
/*                                                                           */
/*****************************************************************************/

void GetARDisplayInstanceList(
ARDisplayInstanceList  *value,     /* OUT; loaded with value for buffer */
ARBoolean               imageFlag  /* IN;  flag to retrieve image byte list from */
                                   /*      a file instead of from tty input      */
)
{
   unsigned int              i;          /* working index */
   ARDisplayInstanceStruct  *tempPtr;    /* working pointer */

   /* Get the common properties */
   DriverPrintPrompt("Getting the Common Properties for this field\n");
   GetARPropList(&value->commonProps, imageFlag);
   value->numItems = GetUnsignedInt("Number of display instances (0): ", 0);
   if (value->numItems == 0)
      value->dInstanceList = NULL;
   else
   {
      value->dInstanceList =
         (ARDisplayInstanceStruct *) malloc(sizeof(ARDisplayInstanceStruct) *
                                            value->numItems);
      if (value->dInstanceList != NULL)
      {
         tempPtr = value->dInstanceList;
         for (i = 0; i < value->numItems; i++)
         {
            tempPtr->vui = GetLong("vui ID (0): ", 0L);
            GetARPropList(&tempPtr->props, imageFlag);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARAssignFieldStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARAssignFieldStruct(value)
ARAssignFieldStruct  *value;   /* OUT; loaded with value for buffer */

{
   strcpy(value->server, GetChar("Server name (): ", ""));
   strcpy(value->schema, GetChar("Schema name (): ", ""));
   value->tag =
      GetUnsignedInt("Field, status history, currency field (1,4,6) (1): ", 1);
   if (value->tag == AR_FIELD)
      value->u.fieldId = GetLong("Field id (0): ", 0L);
   else if (value->tag == AR_STAT_HISTORY)
   {
      value->u.statHistory.enumVal =
         (unsigned long) GetLong("Enumerated value index (0): ", 0L);
      value->u.statHistory.userOrTime =
         GetUnsignedInt("User or Time field (1 or 2) (2): ", 2);
   }
   else if (value->tag == AR_CURRENCY_FLD)
   {
      value->u.currencyField =
         (ARCurrencyPartStruct *) malloc(sizeof(ARCurrencyPartStruct));
      if (value->u.currencyField != NULL)
      {
         value->u.currencyField->fieldId = GetLong("Currency Field id (0): ", 0L);
         value->u.currencyField->partTag =
            GetUnsignedInt("Currency Part - Field,Value,Type,Date,Functional (0-4) (0): ", 0);
         if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
         {
            strncpy(value->u.currencyField->currencyCode,
                    GetChar("Functional currency code: ", ""),
                    AR_CURRENCY_CODE_LEN);
            value->u.currencyField->currencyCode[AR_CURRENCY_CODE_LEN] = '\0';
         }
         else
            value->u.currencyField->currencyCode[0] = '\0';
      }
   }
   /*else  no action as not recognized */
   GetARQualifierStruct(&value->qualifier);
   value->noMatchOption = GetUnsignedInt(
      "No match action -- error, null, no action, submit (1-4) (2): ", 2);
   value->multiMatchOption = GetUnsignedInt(
      "Multi match action -- error, null, first, picklist, mod all, no action, submit (1-7) (4): ",
      4);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARDDEStruct                              */
/*                                                                           */
/*****************************************************************************/

void GetARDDEStruct(value)
ARDDEStruct  *value;      /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Active Link DDE Struct:\n");
   GetMallocChar("      Service name (): ", "", &value->serviceName);
   GetMallocChar("      Topic (): ", "", &value->topic);
   value->action =
      GetUnsignedInt("      Action (execute, poke, request) (1-3) (1): ", 1);
   GetMallocChar("      Path To Program (): ", "", &value->pathToProgram);
   if (value->action != AR_DDE_REQUEST)
      GetMallocChar("      Command (): ", "", &value->command);
   else
      value->command = NULL;
   if (GetARBoolean("      Set an item string? (F): ", FALSE))
      GetMallocChar("      Item text (): ", "", &value->item);
   else
      value->item = NULL;
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARAutomationStruct                           */
/*                                                                           */
/*****************************************************************************/
 
void GetARAutomationStruct(value)
ARAutomationStruct  *value;      /* OUT; loaded with value for buffer */
 
{
   DriverPrintPrompt("   Active Link Automation Struct:\n");
   GetMallocChar("      Auto Server name (): ", "", &value->autoServerName);
   value->isVisible = GetARBoolean("      Showing Automation Server? (T): ", TRUE);
   GetMallocChar("      Class  Id (): ", "", &value->clsId);
   GetMallocChar("      Automation Action (): ", "", &value->action);
 
   GetARCOMMethodList(&value->methodList);
}
 
 
/*****************************************************************************/
/*                                                                           */
/*                            GetARCOMValueStruct                            */
/*                                                                           */
/*****************************************************************************/
 
void GetARCOMValueStruct(value)
ARCOMValueStruct  *value;      /* OUT; loaded with value for buffer */
 
{
   value->transId = GetLong("      Transient Id (1): ", 1L);
   GetMallocChar("      Value IId (used for method only)(): ", "", 
                 &value->valueIId);
   value->valueType =
      GetUnsignedInt("      COM [Method(0,1)/Parm(0-2)] Type (0): ", 0);
   switch (value->valueType)
   {
      case AR_COM_PARM_FIELDID: /* AR_COM_METHOD_FIELDID has same type */
         value->u.fieldId = GetLong("      FieldId (1): ", 1L);
         break;
      case AR_COM_PARM_VALUE:
         GetARValueStruct(&value->u.value, FALSE);
         break;
      default:
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARCOMMethodParmStruct                          */
/*                                                                           */
/*****************************************************************************/
 
void GetARCOMMethodParmStruct(value, order)
ARCOMMethodParmStruct  *value;   /* OUT; loaded with value for buffer */
unsigned int            order;   /* IN; order of the parameter in the list */
 
{
   DriverPrintPrompt("      Parameter Number %u:\n", order);
   GetMallocChar("      Parameter Name (): ", "", &value->parmName);
   value->parmType =
      GetUnsignedInt("      COM Parameter Type (VT_EMPTY/VT_NULL/VT_I2/VT_I4/VT_R4/VT_R8/VT_CY/VT_DATE/VT_BSTR/VT_DISPATCH/VT_ERROR/VT_BOOL/VT_VARIANT/VT_UNKNOWN/VT_WBSTR) (0-14) (VT_I1/VT_UI1/VT_UI2/VT_UI4/VT_I8/VT_UI8/VT_INT/VT_UINT/VT_VOID/VT_HRESULT/VT_PTR) (16-26) (VT_LPSTR/VT_LPWSTR) (30-31) (VT_BLOB_OBJECT -- 70) (0): ", 0);
   GetARCOMValueStruct(&value->parmValue);
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARCOMMethodParmList                           */
/*                                                                           */
/*****************************************************************************/
 
void GetARCOMMethodParmList(value)
ARCOMMethodParmList  *value;         /* OUT; loaded with value for buffer */
 
{
   unsigned int            i;
   ARCOMMethodParmStruct  *tempPtr;
 
   value->numItems = GetUnsignedInt("      Number of Parameters (0): ", 0);
   if (value->numItems == 0)
      value->parameterList = NULL;
   else
   {
      value->parameterList = (ARCOMMethodParmStruct *)
            calloc((unsigned) value->numItems, sizeof(ARCOMMethodParmStruct));
      if (value->parameterList != NULL)
      {
         tempPtr = value->parameterList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARCOMMethodParmStruct(tempPtr, i + 1);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARCOMMethodStruct                           */
/*                                                                           */
/*****************************************************************************/
 
void GetARCOMMethodStruct(value, order)
ARCOMMethodStruct  *value;   /* OUT; loaded with value for buffer */
unsigned int        order;   /* IN; order of the parameter in the list */
 
{
   DriverPrintPrompt("      Method Number %u:\n", order);
   GetMallocChar("      Method Name (): ", "", &value->methodName);
   GetMallocChar("      Method Interface Id (): ", "", &value->methodIId);
   value->methodType =
      GetUnsignedInt("      COM Method Type (VT_EMPTY/VT_NULL/VT_I2/VT_I4/VT_R4/VT_R8/VT_CY/VT_DATE/VT_BSTR/VT_DISPATCH/VT_ERROR/VT_BOOL/VT_VARIANT/VT_UNKNOWN/VT_WBSTR) (0-14) (VT_I1/VT_UI1/VT_UI2/VT_UI4/VT_I8/VT_UI8/VT_INT/VT_UINT/VT_VOID/VT_HRESULT/VT_PTR) (16-26) (VT_LPSTR/VT_LPWSTR) (30-31) (VT_BLOB_OBJECT -- 70) (0): ", 0);
   GetARCOMValueStruct(&value->methodValue);
   GetARCOMMethodParmList(&value->parameterList);
}
 
 
/*****************************************************************************/
/*                                                                           */
/*                             GetARCOMMethodList                            */
/*                                                                           */
/*****************************************************************************/
 
void GetARCOMMethodList(value)
ARCOMMethodList  *value;         /* OUT; loaded with value for buffer */
 
{
   unsigned int        i;
   ARCOMMethodStruct  *tempPtr;
 
   value->numItems = GetUnsignedInt("      Number of Methods (1): ", 1);
   if (value->numItems == 0)
      value->methodList = NULL;
   else
   {
      value->methodList = (ARCOMMethodStruct *) 
                calloc((unsigned) value->numItems, sizeof(ARCOMMethodStruct));
      if (value->methodList != NULL)
      {
         tempPtr = value->methodList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARCOMMethodStruct(tempPtr, i + 1);
            tempPtr++;
         }
      }
   }
}
 

/*****************************************************************************/
/*                                                                           */
/*                            GetAROpenDlgStruct                             */
/*                                                                           */
/*****************************************************************************/
 
void GetAROpenDlgStruct(value)
AROpenDlgStruct  *value;      /* OUT; loaded with value for buffer */
 
{
   DriverPrintPrompt("   Active Link Open Window Struct:\n");
   strcpy(value->serverName, GetChar("      Server Name: ", ""));
   strcpy(value->schemaName, GetChar("      Schema Name: ", ""));
   strcpy(value->vuiLabel, GetChar("      Vui Label: ", ""));
   value->closeBox = GetARBoolean("      Close Box? (T): ", TRUE);
   GetARFieldAssignList(&value->inputValueFieldPairs);
   GetARFieldAssignList(&value->outputValueFieldPairs);
   value->windowMode = GetInt("      Window Mode: (0 - 20) (0):  ", 0);
   GetMallocChar("      TargetLocation (): ", "", &value->targetLocation);
   GetARQualifierStruct(&value->query);
   value->noMatchContinue = GetARBoolean("      No Match Continue? (F): ", FALSE);
   value->suppressEmptyLst = GetARBoolean("      Suppress Empty List? (F): ", FALSE);
   GetARMessageStruct(&value->msg);
   value->pollinginterval = (unsigned long) GetLong("      Polling Interval: (0): ", 0L);
   GetMallocChar("   Report String (): ", "", &value->reportString);
   GetARSortList(&value->sortOrderList);
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARCloseWndStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARCloseWndStruct(value)
ARCloseWndStruct  *value;      /* OUT; loaded with value for buffer */
 
{
   DriverPrintPrompt("   Active Link Close Window Struct:\n");
   value->closeAll = GetARBoolean("      Close All? (T): ", TRUE);
}  


/*****************************************************************************/
/*                                                                           */
/*                             GetARCallGuideStruct                          */
/*                                                                           */
/*****************************************************************************/
  
void GetARCallGuideStruct(value)
ARCallGuideStruct  *value;
{
   DriverPrintPrompt("   Active Link Call Guide Struct:\n");
   strcpy(value->serverName, GetChar("      Server Name: " , ""));
   strcpy(value->guideName, GetChar("      Guide Name: ", ""));
   value->guideMode = GetInt("      Guide Mode: (0): ", 0);
   value->guideTableId = GetLong("      Guide Table id (0): ", 0L);
   GetARFieldAssignList(&value->inputValueFieldPairs);
   GetARFieldAssignList(&value->outputValueFieldPairs);
   strcpy(value->sampleServer, GetChar("      Sample Server Name: " , ""));
   strcpy(value->sampleGuide, GetChar("      Sample Guide Name: ", ""));
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARExitGuideStruct                          */
/*                                                                           */
/*****************************************************************************/
  
void GetARExitGuideStruct(value)
ARExitGuideStruct  *value;
{
   DriverPrintPrompt("   Active Link Exit Guide Struct:\n");
   value->closeAll = GetARBoolean("      Close All? (T): ", TRUE);  
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARGotoGuideLabel                           */
/*                                                                           */
/*****************************************************************************/
  
void GetARGotoGuideLabel(value)
ARGotoGuideLabelStruct  *value;
{
   DriverPrintPrompt("   Active Link Goto Guide Label Struct:\n");
   GetMallocChar("      Label (): ", "", &value->label);  
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARWaitStruct                             */
/*                                                                           */
/*****************************************************************************/
  
void GetARWaitStruct(value)
ARWaitStruct  *value;
{
   DriverPrintPrompt("   Active Link Wait Struct:\n");
   GetMallocChar("      Continue Button Title (): ", "Continue",
                 &value->continueButtonTitle);
} 
 
 
/*****************************************************************************/
/*                                                                           */
/*                             GetARAssignSQLStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARAssignSQLStruct(value)
ARAssignSQLStruct  *value;     /* OUT; loaded with value for buffer */

{
   strcpy(value->server, GetChar("Server name (): ", ""));
   GetMallocChar("SQL command (): ", "", &value->sqlCommand);
   value->valueIndex = GetUnsignedInt("Index of returned value to select (1): ", 1);
   value->noMatchOption =
      GetUnsignedInt("No match action -- error, null (1, 2) (2): ", 2);
   value->multiMatchOption = GetUnsignedInt(
      "Multi match action -- error, null, first, picklist (1, 2, 3, 4) (4): ", 4);
}


/*****************************************************************************/
/*                                                                           */
/*                        GetARAssignFilterApiStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARAssignFilterApiStruct(
ARAssignFilterApiStruct  *value   /* OUT; loaded with value for buffer */
)

{
   unsigned int   i;

   DriverPrintPrompt("   Assign Filter API Struct:\n");
   strcpy(value->serviceName, GetChar("      Service Name: ", ""));
   DriverPrintPrompt("      Input Value List:\n");
   value->numItems = GetUnsignedInt("      Number of Assignments (0): ", 0);
   if (value->numItems == 0)
      value->inputValues = NULL;
   else
   {
      value->inputValues = (ARAssignStruct *) calloc(value->numItems,
                                                     sizeof(ARAssignStruct));
      if (value->inputValues != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            GetARAssignStruct(&value->inputValues[i]);
         }
      }
   }
   value->valueIndex = GetLong("      Index of the value field (1): ", 1);
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARAssignStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARAssignStruct(value)
ARAssignStruct  *value;        /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Assign type:\n");
   value->assignType = GetUnsignedInt(
   "   Value, Field, Process, Arith, Function, DDE, SQL, Filter API (1 - 8) (0): ", 0);

   switch(value->assignType)
   {
      case AR_ASSIGN_TYPE_VALUE   :
         GetARValueStruct(&value->u.value, FALSE);
         break;
      case AR_ASSIGN_TYPE_FIELD   :
         value->u.field = (ARAssignFieldStruct *)
                                            malloc(sizeof(ARAssignFieldStruct));
         if (value->u.field != NULL)
            GetARAssignFieldStruct(value->u.field);
         break;
      case AR_ASSIGN_TYPE_PROCESS :
         GetMallocChar("Process to run (): ", "", &value->u.process);
         break;
      case AR_ASSIGN_TYPE_ARITH   :
         value->u.arithOp = (ARArithOpAssignStruct *)
                                          malloc(sizeof(ARArithOpAssignStruct));
         if (value->u.arithOp != NULL)
            GetARArithOpAssignStruct(value->u.arithOp);
         break;
      case AR_ASSIGN_TYPE_FUNCTION:
         value->u.function = (struct ARFunctionAssignStruct *)
                                         malloc(sizeof(ARFunctionAssignStruct));
         if (value->u.function != NULL)
            GetARFunctionAssignStruct(value->u.function);
         break;
      case AR_ASSIGN_TYPE_DDE     :
         value->u.dde = (ARDDEStruct *) malloc(sizeof(ARDDEStruct));
         if (value->u.dde != NULL)
            GetARDDEStruct(value->u.dde);
         break;
      case AR_ASSIGN_TYPE_SQL     :
         value->u.sql = (ARAssignSQLStruct *) malloc(sizeof(ARAssignSQLStruct));
         if (value->u.sql != NULL)
            GetARAssignSQLStruct(value->u.sql);
         break;
      case AR_ASSIGN_TYPE_FILTER_API:
         value->u.filterApi =
            (ARAssignFilterApiStruct *) malloc(sizeof(ARAssignFilterApiStruct));
         if (value->u.filterApi != NULL)
            GetARAssignFilterApiStruct(value->u.filterApi);
         break;
      default                     :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARArithOpAssignStruct                        */
/*                                                                           */
/*****************************************************************************/

void GetARArithOpAssignStruct(value)
ARArithOpAssignStruct  *value; /* OUT; loaded with value for buffer */

{
   value->operation =
      GetUnsignedInt("Arithmetic op code -- +, -, *, /, %, unary - (1 - 6): ", 0);
   if (value->operation != AR_ARITH_OP_NEGATE)
   {
      DriverPrintPrompt("Left operand - ");
      GetARAssignStruct(&value->operandLeft);
   }
   DriverPrintPrompt("Right operand - ");
   GetARAssignStruct(&value->operandRight);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFunctionAssignStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARFunctionAssignStruct(value)
ARFunctionAssignStruct  *value; /* OUT; loaded with value for buffer */

{
   unsigned int  i;               /* working index */

   value->functionCode = GetUnsignedInt("Function code -- (1 - 50): ", 0);
   value->numItems = GetUnsignedInt("Number of parameters (0): ", 0);
   if (value->numItems != 0)
   {
      value->parameterList =
         (ARAssignStruct *) malloc((unsigned) (sizeof(ARAssignStruct) *
                                               value->numItems));
      if (value->parameterList == NULL)
         return;

      for (i = 0; i < value->numItems; i++)
      {
         DriverPrintPrompt("Parameter[%d] - ", i);
         GetARAssignStruct(&value->parameterList[i]);
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARFieldAssignStruct                        */
/*                                                                           */
/*****************************************************************************/

void GetARFieldAssignStruct(value)
ARFieldAssignStruct *value;   /* OUT; loaded with value for buffer */

{
   value->fieldId = GetLong("Field id: ", 0L);
   GetARAssignStruct(&value->assignment);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARFieldAssignList                         */
/*                                                                           */
/*****************************************************************************/

void GetARFieldAssignList(value)
ARFieldAssignList  *value;     /* OUT; loaded with value for buffer */

{
   unsigned int          i;        /* working index */
   ARFieldAssignStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of field/assign pairs (0): ", 0);
   if (value->numItems == 0)
      value->fieldAssignList = NULL;
   else
   {
      value->fieldAssignList =
         (ARFieldAssignStruct *) malloc(sizeof(ARFieldAssignStruct) *
                                        value->numItems);
      if (value->fieldAssignList != NULL)
      {
         tempPtr = value->fieldAssignList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARFieldAssignStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARSetFieldsAction                            */
/*                                                                           */
/*****************************************************************************/

void GetARSetFieldsAction(value)
ARSetFieldsActionStruct  *value;     /* OUT; loaded with value for buffer */
{
   GetARFieldAssignList(&value->fieldList);
   strcpy(value->sampleServer, GetChar("Server name (): ", ""));
   strcpy(value->sampleSchema, GetChar("Schema name (): ", ""));
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARPushFieldsAssignList                       */
/*                                                                           */
/*****************************************************************************/

void GetARPushFieldsAssignList(value)
ARPushFieldsList  *value;     /* OUT; loaded with value for buffer */

{
   unsigned int         i;        /* working index */
   ARPushFieldsStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of field/assign pairs (0): ", 0);
   if (value->numItems == 0)
      value->pushFieldsList = NULL;
   else
   {
      value->pushFieldsList =
         (ARPushFieldsStruct *) malloc(sizeof(ARPushFieldsStruct) *
                                       value->numItems);
      if (value->pushFieldsList != NULL)
      {
         tempPtr = value->pushFieldsList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARAssignFieldStruct(&tempPtr->field);
            GetARAssignStruct(&tempPtr->assign);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARPushFieldsAction                           */
/*                                                                           */
/*****************************************************************************/

void GetARPushFieldsAction(value)
ARPushFieldsActionStruct  *value;     /* OUT; loaded with value for buffer */

{
   GetARPushFieldsAssignList(&value->pushFieldsList);
   strcpy(value->sampleServer, GetChar("Server name (): ", ""));
   strcpy(value->sampleSchema, GetChar("Schema name (): ", ""));
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFilterActionNotify                         */
/*                                                                           */
/*****************************************************************************/

void GetARFilterActionNotify(value)
ARFilterActionNotify  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Filter Action Notify:\n");
   GetMallocChar("   User name: ", "", &value->user);
   GetMallocChar("   Notify text (): ", "", &value->notifyText);
   value->notifyPriority = GetUnsignedInt("   Notify priority (1 to 10) (1): ", 1);
   GetMallocChar("   Notify subject (): ", "", &value->subjectText);
   value->notifyBehavior = GetUnsignedInt("   Notify behavior (0): ", 0);
   value->notifyPermission = GetUnsignedInt("   Notify permission (0): ", 0);
   value->notifyMechanism = GetUnsignedInt(
      "   Alert, Email, Default, other, or XRef (1, 2, 3, 4-98, 99) (1): ", 1);
   if (value->notifyMechanism == AR_NOTIFY_VIA_XREF)
      value->notifyMechanismXRef = GetLong("   Cross-reference field id: ", 0L);
   value->notifyAdvanced = malloc(sizeof(ARFilterActionNotifyAdvanced));
   if (value->notifyAdvanced)
   {
      GetMallocChar("   From user: ", "", &value->notifyAdvanced->from);
      GetMallocChar("   Reply To: ", "", &value->notifyAdvanced->replyTo);
      GetMallocChar("   Cc user: ", "", &value->notifyAdvanced->cc);
      GetMallocChar("   Bcc user: ", "", &value->notifyAdvanced->bcc);
      GetMallocChar("   Organization: ", "", &value->notifyAdvanced->organization);
      GetMallocChar("   Mailbox name: ", "", &value->notifyAdvanced->mailboxName);
      GetMallocChar("   Header template: ", "", &value->notifyAdvanced->headerTemplate);
      GetMallocChar("   Footer template: ", "", &value->notifyAdvanced->footerTemplate);
      GetMallocChar("   Content template: ", "", &value->notifyAdvanced->contentTemplate);
   }
   value->fieldIdListType =
        GetUnsignedInt("   None, All, specific list, or changed (0, 1, 2, 3) (0): ", 0);
   if (value->fieldIdListType > 3)
      value->fieldIdListType = 0;
   if (value->fieldIdListType == AR_FILTER_FIELD_IDS_LIST)
      GetARInternalIdList(&value->fieldIdList);
   else
      memset(&value->fieldIdList, 0, sizeof(ARInternalIdList));
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARSQLStruct                               */
/*                                                                           */
/*****************************************************************************/

void GetARSQLStruct(value)
ARSQLStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Action Direct SQL:\n");
   strcpy(value->server, GetChar("      Server (): ", ""));
   GetMallocChar("      SQL command (): ", "", &value->command);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARGotoActionStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARGotoActionStruct(value)
ARGotoActionStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Goto Action Struct:\n");
   value->tag = GetUnsignedInt("      Field, Value, Forward, Backup (1-4): ", 1);
   if (value->tag == AR_GOTO_FIELD_XREF)
      value->fieldIdOrValue = GetLong("      FieldId: ", 0L);
   if (value->tag == AR_GOTO_ABSOLUTE_ORDER)
      value->fieldIdOrValue = GetLong("      Execution Order: ", 0L);
   if (value->tag == AR_GOTO_OFFSET_FORWARD)
      value->fieldIdOrValue = GetLong("      Execution Order Advance By: ", 1L);
   if (value->tag == AR_GOTO_OFFSET_BACKWARD)
      value->fieldIdOrValue = GetLong("      Execution Order Backup By: ", 1L);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARServiceActionStruct                        */
/*                                                                           */
/*****************************************************************************/

void GetARServiceActionStruct(value)
ARSvcActionStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Service Action Struct:\n");
   
   memset(value->serverName, 0, AR_MAX_SERVER_SIZE + 1);
   strncpy(value->serverName, GetChar("      ServerName : ", ""), AR_MAX_SERVER_SIZE);

   memset(value->serviceSchema, 0, AR_MAX_NAME_SIZE + 1);
   strncpy(value->serviceSchema, GetChar("      Service Schema : ", ""), AR_MAX_NAME_SIZE);

   value->requestIdMap = GetLong("      Request Id (1) : ", 1L);

   DriverPrintPrompt("   Input Field Map:\n");
   GetARFieldAssignList(&value->inputFieldMapping);
   DriverPrintPrompt("   Output Field Map:\n");
   GetARFieldAssignList(&value->outputFieldMapping);

   memset(value->sampleServer, 0, AR_MAX_SERVER_SIZE + 1);
   memset(value->sampleSchema, 0, AR_MAX_NAME_SIZE + 1);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFilterActionStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARFilterActionStruct(value)
ARFilterActionStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Action Struct:\n");
   value->action= GetUnsignedInt(
   "   Notify, Message, Log, Fields, Process, Push, SQL, Goto Action,\n\
   Call Guide, Exit Guide, Goto Guide Label, Service (1-12): ", 0);
   switch (value->action)
   {
      case AR_FILTER_ACTION_NOTIFY  :
         GetARFilterActionNotify(&value->u.notify);
         break;
      case AR_FILTER_ACTION_MESSAGE :
         GetARFilterStatusStruct(&value->u.message);
         break;
      case AR_FILTER_ACTION_LOG     :
         GetMallocChar("   Log file name (): ", "", &value->u.logFile);
         break;
      case AR_FILTER_ACTION_FIELDS  :
         GetARSetFieldsAction(&value->u.setFields);
         break;
      case AR_FILTER_ACTION_PROCESS :
         GetMallocChar("   Process (): ", "", &value->u.process);
         break;
      case AR_FILTER_ACTION_FIELDP  :
         GetARPushFieldsAction(&value->u.pushFields);
         break;
      case AR_FILTER_ACTION_SQL     :
         GetARSQLStruct(&value->u.sqlCommand);
         break;
      case AR_FILTER_ACTION_GOTOACTION:
         GetARGotoActionStruct(&value->u.gotoAction);
         break;
      case AR_FILTER_ACTION_CALLGUIDE:
         GetARCallGuideStruct(&value->u.callGuide);
         break;
      case AR_FILTER_ACTION_EXITGUIDE:
         GetARExitGuideStruct(&value->u.exitGuide);
         break;
      case AR_FILTER_ACTION_GOTOGUIDELABEL:
         GetARGotoGuideLabel(&value->u.gotoGuide);
         break;
      case AR_FILTER_ACTION_SERVICE:
         GetARServiceActionStruct(&value->u.serviceAction);
         break;
      default                       :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFilterActionList                           */
/*                                                                           */
/*****************************************************************************/

void GetARFilterActionList(
ARBoolean            actionFlag, /* IN; TRUE - action; FALSE - else */
ARFilterActionList  *value       /* OUT; loaded with value for buffer */
)

{
   unsigned int           i;        /* working index */
   ARFilterActionStruct  *tempPtr;  /* working pointer */

   if (actionFlag)
      value->numItems = GetUnsignedInt("Number of if actions (1): ", 1);
   else
      value->numItems = GetUnsignedInt("Number of else actions (0): ", 0);
   if (value->numItems == 0)
      value->actionList = NULL;
   else
   {
      value->actionList =
         (ARFilterActionStruct *) malloc(sizeof(ARFilterActionStruct) *
                                         value->numItems);
      if (value->actionList != NULL)
      {
         tempPtr = value->actionList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARFilterActionStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARActiveLinkMacroStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARActiveLinkMacroStruct(value)
ARActiveLinkMacroStruct  *value;  /* OUT; loaded with value for buffer */

{
   unsigned int   i;           /* working index */

   DriverPrintPrompt("   Active Link Macro Struct:\n");
   strcpy(value->macroName, GetChar("      Macro name: ", ""));
   GetMallocChar("      Macro text(): ", "", &value->macroText);
   value->macroParms.numItems =
      GetUnsignedInt("      Number of macro parms (0): ", 0);
   if (value->macroParms.numItems == 0)
      value->macroParms.parms = NULL;
   else
   {
      value->macroParms.parms =
         (ARMacroParmStruct *) malloc(sizeof(ARMacroParmStruct) *
                                      value->macroParms.numItems);
      if (value->macroParms.parms != NULL)
      {
         for (i = 0; i < value->macroParms.numItems; i++)
         {
            strcpy(value->macroParms.parms[i].name,
                   GetChar("         Parm Name (): ", ""));
            GetMallocChar("         Parm Value (): ", "",
                          &value->macroParms.parms[i].value);
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARFieldCharacteristics                        */
/*                                                                           */
/*****************************************************************************/

void GetARFieldCharacteristics(value)
ARFieldCharacteristics  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Active Link Set Field Characteristics:\n");
   value->option = GetUnsignedInt(
      "      Option (bitmask: 1-reference) (0): ", 0);
   value->fieldId = GetLong("      Field Id (0): ", 0L);
   if (GetARBoolean("      Change menu? (F): ", FALSE))
      GetMallocChar("      Char menu (): ", "", &value->charMenu);
   else
      value->charMenu = NULL;
   GetARPropList(&value->props, FALSE);
   if (GetARBoolean("      Change focus to this field? (F): ", FALSE))
      value->focus = AR_FOCUS_SET_TO_FIELD;
   else
      value->focus = AR_FOCUS_UNCHANGED;
   value->accessOption = GetUnsignedInt(
      "      Access option (unchanged, read-only, read/write, disable) (0-3) (0): ",
      0);
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARActiveLinkActionStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARActiveLinkActionStruct(value)
ARActiveLinkActionStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("Active Link Action Struct:\n");
   value->action= GetUnsignedInt(
   "   Macro, Fields, Process, Message, Field Characteristics, DDE, Push, SQL,\n\
   Automation, Open Dialog, Commit Changes, Close Window, Call Guide,\n\
   Exit Guide, Goto Guide, Wait, Goto Action, Service (1-18): ", 0);
   switch (value->action)
   {
      case AR_ACTIVE_LINK_ACTION_MACRO   :
         GetARActiveLinkMacroStruct(&value->u.macro);
         break;
      case AR_ACTIVE_LINK_ACTION_FIELDS  :
         GetARSetFieldsAction(&value->u.setFields);
         break;
      case AR_ACTIVE_LINK_ACTION_PROCESS :
         GetMallocChar("   Command string (): ", "", &value->u.process);
         break;
      case AR_ACTIVE_LINK_ACTION_MESSAGE :
         GetARMessageStruct(&value->u.message);
         break;
      case AR_ACTIVE_LINK_ACTION_SET_CHAR :
         GetARFieldCharacteristics(&value->u.characteristics);
         break;
      case AR_ACTIVE_LINK_ACTION_DDE      :
         GetARDDEStruct(&value->u.dde);
         break;
      case AR_ACTIVE_LINK_ACTION_FIELDP  :
         GetARPushFieldsAction(&value->u.pushFields);
         break;
      case AR_ACTIVE_LINK_ACTION_SQL     :
         GetARSQLStruct(&value->u.sqlCommand);
         break;
      case AR_ACTIVE_LINK_ACTION_AUTO    :
         GetARAutomationStruct(&value->u.automation);
         break;
      case AR_ACTIVE_LINK_ACTION_OPENDLG   :
         GetAROpenDlgStruct(&value->u.openDlg);
         break;
      case AR_ACTIVE_LINK_ACTION_COMMITC   :
         break;
      case AR_ACTIVE_LINK_ACTION_CLOSEWND :
         GetARCloseWndStruct(&value->u.closeWnd);
         break;
      case AR_ACTIVE_LINK_ACTION_CALLGUIDE  :
         GetARCallGuideStruct(&value->u.callGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_EXITGUIDE  :
         GetARExitGuideStruct(&value->u.exitGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_GOTOGUIDELABEL :
         GetARGotoGuideLabel(&value->u.gotoGuide);
         break;
      case AR_ACTIVE_LINK_ACTION_WAIT:
         GetARWaitStruct(&value->u.waitAction);
         break;
      case AR_ACTIVE_LINK_ACTION_GOTOACTION:
         GetARGotoActionStruct(&value->u.gotoAction);
         break;
      case AR_ACTIVE_LINK_ACTION_SERVICE:
         GetARServiceActionStruct(&value->u.service);
         break;
      default:
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARActiveLinkActionList                        */
/*                                                                           */
/*****************************************************************************/

void GetARActiveLinkActionList(
ARBoolean                actionFlag,  /* IN; TRUE - action; FALSE - else */
ARActiveLinkActionList  *value        /* OUT; loaded with value for buffer */
)

{
   unsigned int               i;        /* working index */
   ARActiveLinkActionStruct  *tempPtr;  /* working pointer */

   if (actionFlag)
      value->numItems = GetUnsignedInt("Number of actions for the active link (1): ", 1);
   else
      value->numItems = GetUnsignedInt("Number of elses for the active link (0): ", 0);
   if (value->numItems == 0)
      value->actionList = NULL;
   else
   {
      value->actionList =
         (ARActiveLinkActionStruct *) malloc(sizeof(ARActiveLinkActionStruct) *
                                             value->numItems);
      if (value->actionList)
      {
         tempPtr = value->actionList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARActiveLinkActionStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARPermissionStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARPermissionStruct(
ARBoolean            fieldFlag,  /* IN; TRUE - field; FALSE - schema */
ARPermissionStruct  *value       /* OUT; loaded with value for buffer */
)

{
   DriverPrintPrompt("   Permission Struct:\n");
   value->groupId = GetInt("      Group id (3): ", 0);
   if (fieldFlag)
      value->permissions = GetUnsignedInt(
         "      None, View, or Change (0, 1, 2): ", 0);
   else
      value->permissions = GetUnsignedInt(
         "      None, Visible, or Hidden (0, 1, 2): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARPermissionList                             */
/*                                                                           */
/*****************************************************************************/

void GetARPermissionList(
ARBoolean          fieldFlag,  /* IN; TRUE - field; FALSE - schema */
ARPermissionList  *value       /* OUT; loaded with value for buffer */
)

{
   unsigned int         i;       /* working index */
   ARPermissionStruct  *tempPtr; /* working pointer */

   DriverPrintPrompt("Permission List:\n");
   value->numItems = GetUnsignedInt("   Number of permission defns (0): ", 0);
   if (value->numItems == 0)
      value->permissionList = NULL;
   else
   {
      value->permissionList =
         (ARPermissionStruct *) malloc(sizeof(ARPermissionStruct) *
                                       value->numItems);
      if (value->permissionList != NULL)
      {
         tempPtr = value->permissionList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARPermissionStruct(fieldFlag, tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARIntegerLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARIntegerLimitsStruct(value)
ARIntegerLimitsStruct  *value; /* OUT; loaded with value for buffer */

{
   value->rangeLow = GetLong("   Integer range low (0): ", 0L);
   value->rangeHigh = GetLong("   Integer range high (5000): ", 5000L);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARDateLimitsStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARDateLimitsStruct(value)
ARDateLimitsStruct  *value; /* OUT; loaded with value for buffer */

{
   /* December 31, 9999 A.D. = 5373484 julian days */

   value->minDate = GetDate("   Minimum Date (-4713-01-01): ", 0);
   value->maxDate = GetDate("   Maximum Date (9999-12-31): ", 5373484);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARRealLimitsStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARRealLimitsStruct(value)
ARRealLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->rangeLow = GetReal("   Real range low (0): ", (double) 0);
   value->rangeHigh = GetReal("   Real range high (5000): ", (double) 5000);
   value->precision = GetInt("   Precision (-1 is no precision) (-1): ", -1);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARCharLimitsStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARCharLimitsStruct(value)
ARCharLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->lengthUnits = GetUnsignedInt(
                       "   Length Units - Byte or Character (0 or 1) (0): ", 0);
   value->maxLength = GetUnsignedInt("   Maximum length (128): ", 128);
   value->menuStyle = GetUnsignedInt(
                       "   Menu style - Append or Overwrite (1 or 2) (1): ", 1);
   value->qbeMatchOperation = GetUnsignedInt(
     "   Match operation - Anywhere, Leading match, or Equal (1 - 3) (1): ", 1);
   strcpy(value->charMenu, GetChar("   Character menu (): ", ""));
   GetMallocChar("   Pattern (): ", "", &value->pattern);
   value->fullTextOptions = GetUnsignedInt("   FullText Options (0): ", 0);
   value->storageOptionForCLOB = GetUnsignedInt(
                       "   CLOB Storage Option - Default, In-Row or Out-of-Row (0, 1 or 2) (0): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARDiaryLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARDiaryLimitsStruct(value)
ARDiaryLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->fullTextOptions = GetUnsignedInt("   FullText Options (0): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                              GetAREnumItemList                            */
/*                                                                           */
/*****************************************************************************/

void GetAREnumItemList(value, indent)
AREnumItemList  *value;        /* OUT; loaded with value for buffer */
char            *indent;

{
   char               buffer[64];  /* prompt text */
   unsigned int       i;           /* working index */
   AREnumItemStruct  *tempPtr;     /* working pointer */

   sprintf(buffer, "%s%s", indent, "   Number of name/value pairs (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->enumItemList = NULL;
   else
   {
      value->enumItemList = calloc(value->numItems, sizeof(AREnumItemStruct));
      if (value->enumItemList != NULL)
      {
         tempPtr = value->enumItemList;
         for (i = 0; i < value->numItems; i++)
         {
            sprintf(buffer, "%s%s", indent, "   Name (): ");
            strcpy(tempPtr->itemName, GetChar(buffer, ""));
            sprintf(buffer, "%s%s", indent, "      Value (): ");
            tempPtr->itemNumber = GetLong(buffer, 0);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetAREnumQueryStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetAREnumQueryStruct(value, indent)
AREnumQueryStruct  *value;     /* OUT; loaded with value for buffer */
char               *indent;

{
   char   buffer[64];  /* prompt text */

   sprintf(buffer, "%s%s", indent, "   Schema name: ");
   strcpy(value->schema, GetChar(buffer, ""));
   sprintf(buffer, "%s%s", indent, "   Server name: ");
   strcpy(value->server, GetChar(buffer, ""));
   GetARQualifierStruct(&value->qualifier);
   sprintf(buffer, "%s%s", indent, "   Name field Id: ");
   value->nameField = GetLong(buffer, 0);
   sprintf(buffer, "%s%s", indent, "   Number field Id: ");
   value->numberField = GetLong(buffer, 0);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetAREnumLimitsStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetAREnumLimitsStruct(value)
AREnumLimitsStruct  *value;     /* OUT; loaded with value for buffer */

{
   value->listStyle = GetUnsignedInt(
             "   Enum limit type - Regular, Custom, or Query (1 - 3) (1): ", 1);

   switch (value->listStyle)
   {
      case AR_ENUM_STYLE_REGULAR :
         GetARNameList(&value->u.regularList, "");
         break;
      case AR_ENUM_STYLE_CUSTOM  :
         GetAREnumItemList(&value->u.customList, "");
         break;
      case AR_ENUM_STYLE_QUERY   :
         GetAREnumQueryStruct(&value->u.queryList, "");
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARAttachLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARAttachLimitsStruct(value)
ARAttachLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->maxSize = (unsigned long) GetLong("   maxSize (0): ", 0);
   value->attachType = AR_ATTACH_FIELD_TYPE_EMBED;
   value->fullTextOptions = GetUnsignedInt("   FullText Options (0): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARTableLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARTableLimitsStruct(value)
ARTableLimitsStruct *value;    /* OUT; loaded with value for buffer */

{
   strcpy(value->schema, GetChar("   schema name : ", ""));
   strcpy(value->server, GetChar("   server name : ", ""));
   GetARQualifierStruct(&value->qualifier);
   value->maxRetrieve = GetUnsignedInt("  Max rows to retrieve (10): ", 10);
   value->numColumns = 0;
   strcpy(value->sampleSchema, GetChar("   sample schema name : ", ""));
   strcpy(value->sampleServer, GetChar("   sample server name : ", ""));
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARColumnLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARColumnLimitsStruct(value)
ARColumnLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   value->parent = GetLong("   Parent Table field Id : ", 0);
   value->dataField = GetLong("   Data field Id : ", 0);
   value->dataSource = GetUnsignedInt("   Data Source : ", 0);
   value->colLength = GetUnsignedInt("   Length to display (10) : ", 10);
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARDecimalLimitsStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARDecimalLimitsStruct(value)
ARDecimalLimitsStruct  *value;    /* OUT; loaded with value for buffer */

{
   GetMallocChar("   Decimal range low (0.0): ", "0.0", &value->rangeLow);
   GetMallocChar("   Decimal range high (5000.0): ", "5000.0",
                 &value->rangeHigh);
   value->precision = GetInt("   Precision (0): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                       GetARFuncCurrencyLimitsStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARCurrencyDetailStruct(value)
ARCurrencyDetailStruct  *value;    /* OUT; loaded with value for buffer */
{
   char  *currencyCode;

   memset(value, 0, sizeof(*value));
   GetMallocChar("   Currency Code (USD): ", "USD", &currencyCode);
   strncpy(value->currencyCode, currencyCode, 3);
   value->precision = GetInt("   Precision (0): ", 0);
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARCurrencyLimitsStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARCurrencyLimitsStruct(value)
ARCurrencyLimitsStruct  *value;    /* OUT; loaded with value for buffer */
{
   unsigned int   i;
   unsigned int   numAllows;
   unsigned int   numFuncs;

   memset(value, 0, sizeof(*value));
   GetMallocChar("   Currency range low (0.0): ", "0.0", &value->rangeLow);
   GetMallocChar("   Currency range high (5000.0): ", "5000.0", &value->rangeHigh);
   value->precision = GetInt("   Precision (0): ", 0);
   numFuncs = GetUnsignedInt("   Number of Functional Currencies (1): ", 1);
   if (numFuncs > 0)
   {
      value->functionalCurrencies.numItems = numFuncs;
      value->functionalCurrencies.currencyDetailList =
         calloc(numFuncs, sizeof(ARCurrencyDetailStruct));
      if (value->functionalCurrencies.currencyDetailList != NULL)
      {
         for (i = 0; i < numFuncs; i++)
         {
            GetARCurrencyDetailStruct(&value->functionalCurrencies.currencyDetailList[i]);
         }
      }
   }
   numAllows = GetUnsignedInt("   Number of Allowable Currencies (1): ", 1);
   if (numAllows > 0)
   {
      value->allowableCurrencies.numItems = numAllows;
      value->allowableCurrencies.currencyDetailList =
         calloc(numAllows, sizeof(ARCurrencyDetailStruct));
      if (value->allowableCurrencies.currencyDetailList != NULL)
      {
         for (i = 0; i < numAllows; i++)
         {
            GetARCurrencyDetailStruct(&value->allowableCurrencies.currencyDetailList[i]);
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARViewLimits                              */
/*                                                                           */
/*****************************************************************************/

void GetARViewLimits(value)
ARViewLimits  *value;    /* OUT; loaded with value for buffer */

{
   value->maxLength = GetUnsignedInt("   Maximum length (128): ", 128);
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARDisplayLimits                            */
/*                                                                           */
/*****************************************************************************/

void GetARDisplayLimits(value)
ARDisplayLimits  *value;    /* OUT; loaded with value for buffer */

{
   value->lengthUnits = GetUnsignedInt(
                       "   Length Units - Byte or Character (0 or 1) (0): ", 0);
   value->maxLength = GetUnsignedInt("   Maximum length (128): ", 128);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFieldLimitStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARFieldLimitStruct(dataType, value)
int                  dataType; /* IN; datatype to get limits for */
ARFieldLimitStruct  *value;    /* OUT; loaded with value for buffer */

{
   if (GetARBoolean("Set to NO LIMITS? (F): ", FALSE))
      value->dataType = AR_FIELD_LIMIT_NONE;
   else
   {
      value->dataType = dataType;
      switch (dataType)
      {
         case AR_DATA_TYPE_INTEGER :
            GetARIntegerLimitsStruct(&value->u.intLimits);
            break;
         case AR_DATA_TYPE_REAL    :
            GetARRealLimitsStruct(&value->u.realLimits);
            break;
         case AR_DATA_TYPE_CHAR    :
            GetARCharLimitsStruct(&value->u.charLimits);
            break;
         case AR_DATA_TYPE_DIARY   :
            GetARDiaryLimitsStruct(&value->u.diaryLimits);
            break;
         case AR_DATA_TYPE_TIME    :
            break;
         case AR_DATA_TYPE_TIME_OF_DAY    :
            break;
         case AR_DATA_TYPE_ENUM    :
            GetAREnumLimitsStruct(&value->u.enumLimits);
            break;
         case AR_DATA_TYPE_BITMASK :
            GetAREnumLimitsStruct(&value->u.maskLimits);
            break;
         case AR_DATA_TYPE_ATTACH :
            GetARAttachLimitsStruct(&value->u.attachLimits);
            break;
         case AR_DATA_TYPE_DECIMAL:
            GetARDecimalLimitsStruct(&value->u.decimalLimits);
            break;
         case AR_DATA_TYPE_CURRENCY:
            GetARCurrencyLimitsStruct(&value->u.currencyLimits);
            break;
         case AR_DATA_TYPE_TABLE :
            GetARTableLimitsStruct(&value->u.tableLimits);
            break;
         case AR_DATA_TYPE_COLUMN :
            GetARColumnLimitsStruct(&value->u.columnLimits);
            break;
         case AR_DATA_TYPE_VIEW :
            GetARViewLimits(&value->u.viewLimits);
            break;
         case AR_DATA_TYPE_DISPLAY :
            GetARDisplayLimits(&value->u.displayLimits);
            break;
         case AR_DATA_TYPE_DATE :
            GetARDateLimitsStruct(&value->u.dateLimits);
            break;
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARCharMenuItemStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuItemStruct(value)
ARCharMenuItemStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Char Menu Item Struct:\n");
   strcpy(value->menuLabel, GetChar("      Menu Label (): ", ""));
   value->menuType =
      GetUnsignedInt("      Menu type  Value or Menu (1 or 2) (1): ", 1);
   switch (value->menuType)
   {
      case AR_MENU_TYPE_VALUE :
         GetMallocChar("      Char menu text (): ", "", &value->u.menuValue);
         break;
      case AR_MENU_TYPE_MENU  :
         value->u.childMenu =
            (ARCharMenuStruct *) malloc(sizeof(ARCharMenuStruct));
         if (value->u.childMenu != NULL)
            GetARCharMenuStruct(value->u.childMenu);
         break;
      default                 :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetListOfMenuLabelIds                             */
/*                                                                           */
/*****************************************************************************/

void GetListOfMenuLabelIds(idString, defaultId, labelIds)
char          *idString;      /* IN; string to extract id(s) from */
ARInternalId   defaultId;     /* IN; default ID value */
ARInternalId  *labelIds;      /* OUT; loaded with 1 to 5 IDs */

{
   int   i;         /* working index */

   labelIds[0] = defaultId;
   labelIds[1] = 0;
   labelIds[2] = 0;
   labelIds[3] = 0;
   labelIds[4] = 0;

   if (idString == NULL)
      return;

   i = 0;
   while ((*idString != '\0') && (i < 5))
   {                              /* get next ID from string */
      while ((*idString != '\0') && (!isdigit((unsigned char) *idString)))
         idString++;

      labelIds[i] = atol(idString);
      i++;

      while ((*idString != '\0') && (isdigit((unsigned char) *idString)))
         idString++;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARCharMenuQueryStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuQueryStruct(value)
ARCharMenuQueryStruct  *value; /* OUT; loaded with value for buffer */

{
   char  *tempPtr;   /* working pointer */

   DriverPrintPrompt("   Char Menu Query Struct:\n");
   strcpy(value->schema, GetChar("      Schema (): ", ""));
   strcpy(value->server, GetChar("      Server (): ", ""));
   GetARQualifierStruct(&value->qualifier);
   tempPtr = GetChar("      Id of the label field (0): ", "0");
   GetListOfMenuLabelIds(tempPtr, 0, value->labelField);
   value->valueField = GetLong("      Id of the value field (0): ", 0);
   value->sortOnLabel = GetARBoolean("      Sort the label field (T): ", TRUE);
   strcpy(value->sampleSchema, GetChar("      Sample Schema (): ", ""));
   strcpy(value->sampleServer, GetChar("      Sample Server (): ", ""));
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARCharMenuSQLStruct                           */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuSQLStruct(value)
ARCharMenuSQLStruct *value; /* OUT; loaded with value for buffer */

{
   char  *tempPtr;   /* working pointer */

   DriverPrintPrompt("   Char Menu SQL Struct:\n");
   strcpy(value->server, GetChar("      Server (): ", ""));
   GetMallocChar("      SQL command (): ", "", &value->sqlCommand);
   tempPtr = GetChar("      Index of the label field (1): ", "1");
   GetListOfMenuLabelIds(tempPtr, 1, (ARInternalId *) value->labelIndex);
   value->valueIndex = GetLong("      Index of the value field (1): ", 1);
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARCharMenuList                             */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuList(value)
ARCharMenuList  *value;        /* OUT; loaded with value for buffer */

{
   unsigned int           i;        /* working index */
   ARCharMenuItemStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of char menu items (0): ", 0);
   if (value->numItems == 0)
      value->charMenuList = NULL;
   else
   {
      value->charMenuList =
         (ARCharMenuItemStruct *) malloc(sizeof(ARCharMenuItemStruct) *
                                         value->numItems);
      if (value->charMenuList != NULL)
      {
         tempPtr = value->charMenuList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARCharMenuItemStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARCharMenuSSStruct                            */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuSSStruct(value)
ARCharMenuSSStruct  *value;       /* OUT; loaded with value for buffer */
{
   DriverPrintPrompt("   Char Menu Server-Side Struct:\n");
   strcpy(value->menuName, GetChar("      Menu Name (): ", ""));
   DriverPrintPrompt("      Keyword List:\n");
   GetARFieldValueList(&value->keywordList);
   DriverPrintPrompt("      Parameter List:\n");
   GetARFieldValueList(&value->parameterList);
   DriverPrintPrompt("      Extern List:\n");
   GetARQualifierList(&value->externList, "         ");
   GetMallocChar("      Server (): ", "", &value->server);
   GetMallocChar("      Schema (): ", "", &value->schema);
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARCharMenuDDStruct                            */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuDDStruct(value)
ARCharMenuDDStruct  *value;       /* OUT; loaded with value for buffer */
{
   DriverPrintPrompt("   Char Menu Data-Dictionary Struct:\n");
   strcpy(value->server, GetChar("      Server name (): ", ""));
   value->nameType = GetUnsignedInt("      Name type (0 - 2) (0): ", 0);
   value->valueFormat = GetUnsignedInt("      Value format (1 - 11) (2): ", 2);
   value->structType = GetUnsignedInt("      Struct type (1 - 2) (1): ", 1);
   switch (value->structType)
   {
      case AR_CHAR_MENU_DD_NONE:
         break;
      case AR_CHAR_MENU_DD_FORM:
         DriverPrintPrompt("      Data-Dictionary Form Struct:\n");
         value->u.formDefn.schemaType = GetUnsignedInt(
            "        Type of schema (all,regular,join,view,uplink,downlink,dialog,vendor,allow_mfsearch) (0-6,8-9) (0): ", 0);
         value->u.formDefn.includeHidden =
            GetARBoolean("        Include hidden schemas (T): ", TRUE);
         break;
      case AR_CHAR_MENU_DD_FIELD:
         DriverPrintPrompt("        Data-Dictionary Field Struct:\n");
         value->u.fieldDefn.fieldType = GetUnsignedInt(
 "        Field Type - data, trim, control, page, pholder, table,\n                     column, attach, attachpool\n                     (1, 2, 4, 8, 16, 32, 64, 128, 256) (129): ", 129);
         strcpy(value->u.fieldDefn.schema,
                GetChar("        Schema name (): ", ""));
         break;
      default:
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARCharMenuStruct                            */
/*                                                                           */
/*****************************************************************************/

void GetARCharMenuStruct(value)
ARCharMenuStruct  *value;      /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Char Menu Struct:\n");
   value->menuType =
       GetUnsignedInt("      Menu type: None, List, Query, File, SQL, Server-Side, Data-Dict (0 - 6) (1): ", 1);
   switch (value->menuType)
   {
      case AR_CHAR_MENU_NONE  :
         break;
      case AR_CHAR_MENU_LIST  :
         GetARCharMenuList(&value->u.menuList);
         break;
      case AR_CHAR_MENU_QUERY :
         GetARCharMenuQueryStruct(&value->u.menuQuery);
         break;
      case AR_CHAR_MENU_FILE  :
         value->u.menuFile.fileLocation =
              GetUnsignedInt("      File location  Server or Client (1 or 2) (1): ", 1);
         GetMallocChar("      Filename (): ", "", &value->u.menuFile.filename);
         break;
      case AR_CHAR_MENU_SQL   :
         GetARCharMenuSQLStruct(&value->u.menuSQL);
         break;
      case AR_CHAR_MENU_SS   :
         GetARCharMenuSSStruct(&value->u.menuSS);
         break;
      case AR_CHAR_MENU_DATA_DICTIONARY:
         GetARCharMenuDDStruct(&value->u.menuDD);
         break;
      default                 :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARStructItemStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARStructItemStruct(value, exportOption)
ARStructItemStruct  *value;           /* OUT; loaded with value for buffer */
int                 *exportOption;
{
   int localOptions = 0;
   int exportMask = 0;
   int workflowOption = 0;
   
   value->type = GetUnsignedInt(
   "   Schema(full,defn,view,mail),Filter,ActLink,<7-obsolete>,CharMenu,\n\
   Escalation,Dist Map,Schema(min view),Container,Distributed Pool,\n\
   VUI,Field,Application,Image,VUI(locale based) (1-8,9-13,14-18): ", 0);

   value->selectedElements.numItems = 0;
   value->selectedElements.nameList = NULL;

   if (value->type == AR_STRUCT_ITEM_FIELD)
      strcpy(value->name, GetChar("   Schema name(on which fields to import): ", ""));
   else
      strcpy(value->name, GetChar("   Structure name (): ", ""));

   if (value->type == AR_STRUCT_ITEM_VUI ||
       value->type == AR_STRUCT_ITEM_SCHEMA_MAIL)
   {
      DriverPrintPrompt("   VUIs to select (supply VUI's DB name):\n");
      GetARNameList(&value->selectedElements, "   ");
   }
   else if (value->type == AR_STRUCT_ITEM_FIELD)
   {
      DriverPrintPrompt("   Fields to select (supply field name):\n");
      GetARNameList(&value->selectedElements, "   ");
   }
   else if (value->type == AR_STRUCT_ITEM_IMAGE)
   {
      DriverPrintPrompt("   Image to select (supply image name):\n");
      GetARNameList(&value->selectedElements, "   ");
   }
   else if (value->type == AR_STRUCT_ITEM_LOCALE_VUI)
   {
      DriverPrintPrompt("   VUIs to select (supply VUI's locale):\n");
      GetARNameList(&value->selectedElements, "   ");
   }
   else if (value->type == AR_STRUCT_ITEM_APP)
   {                       
      if (GetARBoolean("   Integration Workflow Only T/F? (F): ", FALSE))
      {
         workflowOption = GetUnsignedInt("   Applications to include: (0-Selected 1-All ) (0): ", 0);
         switch (workflowOption)
         {
            case 0 :
               DriverPrintPrompt("   Applications:\n");
               GetARNameList(&value->selectedElements, "   ");
               break;         
            case 1 :
               break;
            default:
               ;
         }
         exportMask |= AR_EXPORT_APP_INTEGRATION_WORKFLOW;
      }
      else
      {
         workflowOption =  GetARBoolean("   Include Shared Workflow T/F? (F): ", FALSE);
         if (workflowOption)
             exportMask |= AR_EXPORT_SHARED_WORKFLOW;                    
      }
      
      if (!(exportMask & AR_EXPORT_APP_INTEGRATION_WORKFLOW))
      {
         if (GetARBoolean("   Include only Locale VUI and data T/F? (F): ", FALSE))
         {       
            exportMask |= AR_EXPORT_LOCALE_ONLY;
            localOptions = GetUnsignedInt("   Locales to include: (0-All 1-Selected) (1): ", 1);
            switch (localOptions)
            {
               case 0 :
                  break;
               case 1 :
                  exportMask |= AR_EXPORT_SELECTED_LOCALES;
                  DriverPrintPrompt("   Locales to select (supply Application locales):\n");
                  GetARNameList(&value->selectedElements, "   ");
               default:
                  ;
            }
         }
         else
         {
            localOptions = GetUnsignedInt("   Locales to include: (0-All 1-Default only 2-Selected) (0): ", 0);
            switch (localOptions)
            {
               case 1 :
                  exportMask |= AR_EXPORT_DEFAULT_LOCALE;
               case 0 :
                  break;
               case 2 :
                  exportMask |= AR_EXPORT_SELECTED_LOCALES;
                  DriverPrintPrompt("   Locales to select (supply Application locales):\n");
                  GetARNameList(&value->selectedElements, "   ");
               default:
                  ;
            }
         }
      }
   }

   if (exportOption)
      *exportOption = exportMask;
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARStructItemList                            */
/*                                                                           */
/*****************************************************************************/

void GetARStructItemList(value, exportOption)
ARStructItemList  *value;        /* OUT; loaded with value for buffer */
int               *exportOption; /* OUT; optional export bit-mask, NULL OK */
{
   unsigned int         exportFormat; /* export format */
   unsigned int         i;            /* working index */
   ARStructItemStruct  *tempPtr;      /* working pointer */

   DriverPrintPrompt("Struct Item List:\n");

   exportFormat = 0;

   while (exportFormat != AR_EXPORT_FORMAT_AR_DEF &&
          exportFormat != AR_EXPORT_FORMAT_XML)
      exportFormat = GetUnsignedInt("   Format: AR_DEF, XML (1,2) (1): ", 1);

   gExportFormat = exportFormat;
   value->numItems = GetUnsignedInt("   Number of structure items (0): ", 0);
   if (value->numItems == 0)
      value->structItemList = NULL;
   else
   {
      value->structItemList =
         (ARStructItemStruct *) malloc(sizeof(ARStructItemStruct) *
                                       value->numItems);
      if (value->structItemList != NULL)
      {
         tempPtr = value->structItemList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARStructItemStruct(tempPtr, exportOption);
            tempPtr++;
         }
         if (exportFormat == AR_EXPORT_FORMAT_XML)
         {
            for (i = 0; i < value->numItems; i++)
               value->structItemList[i].type |= AR_STRUCT_XML_OFFSET;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARServerInfoRequestList                        */
/*                                                                           */
/*****************************************************************************/

void GetARServerInfoRequestList(maxInfoID, value)
int                       maxInfoID; /* IN; max item identifier for prompt */
ARServerInfoRequestList  *value;     /* OUT; loaded with value for buffer */

{
   char           buffer[80];  /* place to put the prompt */
   unsigned int   i;           /* working index */
   unsigned int  *tempPtr;     /* working pointer */

   value->numItems = GetUnsignedInt("   Number of server info operations (0): ", 0);
   if (value->numItems == 0)
      value->requestList = NULL;
   else
   {
      value->requestList =
         (unsigned int *) malloc(sizeof(unsigned int) * value->numItems);
      if (value->requestList != NULL)
      {
         tempPtr = value->requestList;
         for (i = 0; i < value->numItems; i++)
         {
            sprintf(buffer, "   Operation (1-%d) (1): ", maxInfoID);
            *tempPtr = GetUnsignedInt(buffer, 1);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARServerInfoStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARServerInfoStruct(value)
ARServerInfoStruct  *value;     /* OUT; loaded with value for buffer */

{
   char   buffer[80];          /* place to put the prompt */

   DriverPrintPrompt("Server info list entry\n");
   sprintf(buffer, "Operation (1-%d) (1): ", AR_MAX_SERVER_INFO_USED);
   value->operation = GetUnsignedInt(buffer, 1);
   GetARValueStruct(&value->value, FALSE);
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARServerInfoList                           */
/*                                                                           */
/*****************************************************************************/

void GetARServerInfoList(value)
ARServerInfoList  *value;    /* OUT; loaded with value for buffer */

{
   unsigned int         i;        /* working index */
   ARServerInfoStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of server info operations (0): ", 0);
   if (value->numItems == 0)
      value->serverInfoList = NULL;
   else
   {
      value->serverInfoList =
         (ARServerInfoStruct *) malloc(sizeof(ARServerInfoStruct) *
                                       value->numItems);
      if (value->serverInfoList != NULL)
      {
         tempPtr = value->serverInfoList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARServerInfoStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARReferenceList                           */
/*                                                                           */
/*****************************************************************************/

void GetARReferenceList(
ARReferenceList  *value      /* OUT; loaded with value for buffer */
)
{
   unsigned int        i;        /* working index */
   ARReferenceStruct  *tempPtr;  /* working pointer */

   value->numItems = GetUnsignedInt("   Number of references (0): ", 0);
   if (value->numItems == 0)
      value->referenceList = NULL;
   else
   {
      value->referenceList =
         (ARReferenceStruct *) malloc(sizeof(ARReferenceStruct) *
                                      value->numItems);
      if (value->referenceList != NULL)
      {
         tempPtr = value->referenceList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARReferenceStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARReferenceStruct                          */
/*                                                                           */
/*****************************************************************************/

void GetARReferenceStruct(
ARReferenceStruct  *value      /* OUT; loaded with value for buffer */
)
{
   GetMallocChar("   Reference label (): ", "", &value->label);
   GetMallocChar("   Reference description (): ", "", &value->description);
   DriverPrintPrompt("   Reference type none,schema,filter,escl,actlink,\n");
   value->type = GetUnsignedInt("      container,charmenu, image (0,2-8) (0): ", 0);

   value->reference.dataType =
      GetUnsignedInt("   References data type ARS,external (0,1) (0): ", 0);

   if (value->reference.dataType == ARREF_DATA_ARSREF)
      strcpy(value->reference.u.name, GetChar("   ARS reference name (): ", ""));
   else if (value->reference.dataType == ARREF_DATA_EXTREF)
      GetARExtReferenceStruct(&value->reference.u.extRef);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARExtReferenceStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARExtReferenceStruct(
ARExtReferenceStruct  *value      /* OUT; loaded with value for buffer */
)
{
   DriverPrintPrompt("   External reference access group ids: \n");
   GetARInternalIdList(&value->permittedGroups);
   DriverPrintPrompt("   External reference value: ");
   GetARValueStruct(&value->value, FALSE);
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARReferenceTypeList                         */
/*                                                                           */
/*****************************************************************************/

void GetARReferenceTypeList(
ARReferenceTypeList  *value,      /* OUT; loaded with value for buffer */
char                 *indent
)
{
   char           buffer[256];
   unsigned int   i;          /* working index */
   int           *tempPtr;    /* working pointer */

   sprintf(buffer, "%s%s", indent, "Number of reference types (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->refType = NULL;
   else
   {
      value->refType = (int *) malloc(sizeof(unsigned int) * value->numItems);
      if (value->refType != NULL)
      {
         tempPtr = value->refType;
         for (i = 0; i < value->numItems; i++)
         {
            sprintf(buffer, "%s%s", indent, "   Reference type none,schema,filter,escl,actlink,container (0,2-6) (0): ");
            *tempPtr = GetInt(buffer, 0);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARContainerOwnerObjList                        */
/*                                                                           */
/*****************************************************************************/

void GetARContainerOwnerObjList(
ARContainerOwnerObjList  *value,      /* OUT; loaded with value for buffer */
char                     *indent
)
{
   char           buffer[256];
   unsigned int   i;          /* working index */

   sprintf(buffer, "%s%s", indent, "Number of container owner objects (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->ownerObjList = NULL;
   else
   {
      value->ownerObjList = malloc(sizeof(ARContainerOwnerObj) * value->numItems);
      if (value->ownerObjList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            GetARContainerOwnerObj(&value->ownerObjList[i], indent);
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARContainerOwnerObj                         */
/*                                                                           */
/*****************************************************************************/

void GetARContainerOwnerObj(
ARContainerOwnerObj  *value,      /* OUT; loaded with value for buffer */
char                 *indent
)
{
   char   buffer[256];

   sprintf(buffer, "%s%s", indent,
           "   Container owner type (unowned, all, owned) (0-2) (0): ");
   value->type = GetUnsignedInt(buffer, 0);
   if ((value->type == ARCONOWNER_NONE) || (value->type == ARCONOWNER_ALL))
      value->ownerName[0] = '\0';
   else
   {
      sprintf(buffer, "%s%s", indent, "   Container owner name (): ");
      strcpy(value->ownerName, GetChar(buffer, ""));
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARContainerTypeList                         */
/*                                                                           */
/*****************************************************************************/

void GetARContainerTypeList(
ARContainerTypeList  *value,     /* OUT; loaded with value for buffer */
char                 *indent
)
{
   char           buffer[256];
   unsigned int   i;          /* working index */
   int           *tempPtr;    /* working pointer */

   sprintf(buffer, "%s%s", indent, "Number of container types (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->type = NULL;
   else
   {
      value->type = malloc(sizeof(unsigned int) * value->numItems);
      if (value->type != NULL)
      {
         tempPtr = value->type;
         for (i = 0; i < value->numItems; i++)
         {
            sprintf(buffer, "%s%s", indent, "   Container type (0): ");
            *tempPtr = GetInt(buffer, 0);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARSignalList                              */
/*                                                                           */
/*****************************************************************************/

void GetARSignalList(
ARSignalList  *value      /* OUT; loaded with value for buffer */
)
{
   unsigned int     i;          /* working index */
   char            *sigArgPtr;  
   ARSignalStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of Signals (0): ", 0);
   if (value->numItems == 0)
      value->signalList = NULL;
   else
   {
      value->signalList= (ARSignalStruct *) malloc(sizeof(ARSignalStruct) * 
                                                   value->numItems);
      if (value->signalList != NULL)
      {
         tempPtr = value->signalList;
         for (i = 0; i < value->numItems; i++)
         {
            tempPtr->signalType = GetInt(
"   Signal (config,group,license,alert,DSO,user,application,\n"
"           archive,escalation,recache,computedgroup,hierarchicalgroup) (1-12): ", 0);
            sigArgPtr = GetChar("   Signal Argument (): ", "");
            tempPtr->sigArgument = strdup(sigArgPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARLocalizedRequestStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARLocalizedRequestStruct(
ARLocalizedRequestStruct  *value      /* OUT; loaded with value for buffer */
)      
{
   DriverPrintPrompt("   Localized Message Struct:\n");
   value->messageType = GetUnsignedInt("\
      Message type   System, Active Link, Filter, Active Link Help,\n\
                     Form Help, Field Help, Container Desc., List Menu Defn,\n\
                     External Report, Container Label, Application Help, \n\
                     Application About, Application Help Index\n\
                     (0 - 13) (0): ", 0);

   strcpy(value->name, GetChar("   name (): ", ""));

   switch (value->messageType)
   {
      case AR_LOCAL_TEXT_EXTERN_REPORT:
      case AR_LOCAL_TEXT_FIELD_HELP:
         value->u.fieldId = GetInt("  fieldID (0): ", 0);
         break;

      case AR_LOCAL_TEXT_ACT_LINK_MESSAGE:
      case AR_LOCAL_TEXT_FILTER_MESSAGE:
         value->u.workflow.ifElse = GetUnsignedInt("  Message Number (0): ", 0);
         break;

      default:
         break;
   } /* end switch */
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARLocalizedRequestList                         */
/*                                                                           */
/*****************************************************************************/

void GetARLocalizedRequestList(value)
ARLocalizedRequestList  *value;    /* OUT; loaded with desired messages */
{
   unsigned int               i;       /* working index */
   ARLocalizedRequestStruct  *tempPtr; /* working pointer */

   value->numItems =
      GetUnsignedInt("   Number of localized value operations (0): ", 0);
   if (value->numItems == 0)
      value->localizedRequestList = NULL;
   else
   {
      value->localizedRequestList =
         (ARLocalizedRequestStruct *) malloc(sizeof(ARLocalizedRequestStruct) *
                                             value->numItems);
      if (value->localizedRequestList != NULL)
      {
         tempPtr = value->localizedRequestList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARLocalizedRequestStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARStatusHistoryStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARStatusHistoryStruct(
ARStatusHistoryStruct  *value        /* OUT; loaded with value for buffer */
)      
{
   DriverPrintPrompt("   Status History Struct:\n");

   strncpy(value->user, GetChar("       User (): ", ""),
           AR_MAX_ACCESS_NAME_SIZE);                                   
   value->user[AR_MAX_ACCESS_NAME_SIZE] = '\0';
   
   value->timeVal = GetLong("       Time value (0): ", 0L);
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARStatusHistoryList                          */
/*                                                                           */
/*****************************************************************************/

void GetARStatusHistoryList(
ARStatusHistoryList  *value          /* OUT; loaded with value for buffer */
)
{
   ARStatusHistoryStruct  *histPtr;    /* working pointer */
   unsigned int            i;          /* working index */

   value->numItems =
      GetUnsignedInt("   Number of status history struct (0): ", 0);
   if (value->numItems == 0)
      value->statHistList = NULL;
   else
   {
      value->statHistList = (ARStatusHistoryStruct *)
                           malloc(sizeof(ARStatusHistoryStruct) * value->numItems);
      if (value->statHistList != NULL)
      {
         histPtr = value->statHistList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARStatusHistoryStruct(histPtr);
            histPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARDiaryStruct                              */
/*                                                                           */
/*****************************************************************************/

void GetARDiaryStruct(
ARDiaryStruct  *value          /* OUT; loaded with value for buffer */
)      
{
   DriverPrintPrompt("   ARDiaryStruct Struct:\n");

   strncpy(value->user, GetChar("       User (): ", ""), AR_MAX_ACCESS_NAME_SIZE);
   value->user[AR_MAX_ACCESS_NAME_SIZE] = '\0';
   
   value->timeVal = GetLong("       Time value (0): ", 0L);
   GetMallocChar("       Text (): ", "", &value->value);
   if (value->value == NULL)
      memset(value, 0, sizeof(ARDiaryStruct));
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARDiaryList                              */
/*                                                                           */
/*****************************************************************************/

void GetARDiaryList(
ARDiaryList  *value               /* OUT; loaded with value for buffer */
)
{
   ARDiaryStruct  *diaryPtr;   /* working pointer */
   unsigned int    i;          /* working index */

   value->numItems = GetUnsignedInt("   Number of diary struct (0): ", 0);
   if (value->numItems == 0)
      value->diaryList = NULL;
   else
   {
      value->diaryList = (ARDiaryStruct *)
                           malloc(sizeof(ARDiaryStruct) * value->numItems);
      if (value->diaryList != NULL)
      {
         diaryPtr = value->diaryList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARDiaryStruct(diaryPtr);
            diaryPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARWorkflowLockStruct                        */
/*                                                                           */
/*****************************************************************************/

void GetARWorkflowLockStruct(
ARWorkflowLockStruct  *value      /* OUT; loaded with value for buffer */
)
{
   value->lockType = GetInt("      Lock Type: (0): ", 0);
   if ((value->lockType & 1) || (value->lockType & 2))
      strcpy(value->lockKey, GetChar("   Lock Key (): ", ""));
   else
      value->lockKey[0] = '\0';
}


/*****************************************************************************/
/*                                                                           */
/*                               GetARDataType                               */
/*                                                                           */
/*****************************************************************************/

unsigned int GetARDataType()
{
   return GetUnsignedInt(
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/\n"
"         Attach/Currency/Date/TimeOfDay/Join/Trim/Control/Table/Column/\n"
"         ULong/Coords/View/Display (0-14, 30-34, 40-43) (2): ", AR_DATA_TYPE_INTEGER);
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARNamePtrList                              */
/*                                                                           */
/*****************************************************************************/

void GetARNamePtrList(value, indent)
ARNamePtrList  *value;            /* OUT; loaded with value for structure */
char           *indent;

{
   char           buffer[100]; /* prompt text */
   unsigned int   i;           /* working index */

   sprintf(buffer, "%s%s", indent, "   Number of names (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->namePtrList = NULL;
   else
   {
      value->namePtrList = (char **) calloc(value->numItems, sizeof(ARNameType *));
      if (value->namePtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have name for item? (T): ", TRUE))
            {
               GetMallocChar("      Name (): ", "", &value->namePtrList[i]);
            }
            else
               value->namePtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              GetARValuePtrList                            */
/*                                                                           */
/*****************************************************************************/

void GetARValuePtrList(value)
ARValuePtrList  *value;      /* OUT; loaded with value for structure */

{
   unsigned int    i;          /* working index */
   ARValueStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of values (0): ", 0);
   if (value->numItems == 0)
      value->valuePtrList = NULL;
   else
   {
      value->valuePtrList =
         (ARValueStruct **) calloc(value->numItems, sizeof(ARValueStruct *));
      if (value->valuePtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have default value for item? (T): ", TRUE))
            {
               tempPtr = (ARValueStruct *) calloc(1, sizeof(ARValueStruct));
               if (tempPtr != NULL)
               {
                  GetARValueStruct(tempPtr, FALSE);
                  value->valuePtrList[i] = tempPtr;
               } 
            }
            else
               value->valuePtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                             GetARBooleanList                              */
/*                                                                           */
/*****************************************************************************/

void GetARBooleanList(value)
ARBooleanList  *value;      /* OUT; loaded with value for structure */

{
   unsigned int   i;          /* working index */
   ARBoolean     *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of boolean flags (0): ", 0);
   if (value->numItems == 0)
      value->booleanList = NULL;
   else
   {
      value->booleanList =
         (ARBoolean *) calloc(value->numItems, sizeof(ARBoolean));
      if (value->booleanList != NULL)
      {
         tempPtr = value->booleanList;
         for (i = 0; i < value->numItems; i++)
         {
            *tempPtr = GetARBoolean("   Boolean (F): ", FALSE);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARFieldMappingList                           */
/*                                                                           */
/*****************************************************************************/

void GetARFieldMappingList(value)
ARFieldMappingList  *value;      /* OUT; loaded with value for structure */

{
   unsigned int           i;          /* working index */
   ARFieldMappingStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of field mappings (0): ", 0);
   if (value->numItems == 0)
      value->mappingList = NULL;
   else
   {
      value->mappingList =
         (ARFieldMappingStruct *) calloc(value->numItems, sizeof(ARFieldMappingStruct));
      if (value->mappingList != NULL)
      {
         tempPtr = value->mappingList;
         for (i = 0; i < value->numItems; i++)
         {
            GetARFieldMapping(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARFieldMappingPtrList                          */
/*                                                                           */
/*****************************************************************************/

void GetARFieldMappingPtrList(
ARFieldMappingPtrList  *value        /* OUT; loaded with value for structure */
)

{
   unsigned int           i;          /* working index */
   ARFieldMappingStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of mapping structures (0): ", 0);
   if (value->numItems == 0)
      value->mappingPtrList = NULL;
   else
   {
      value->mappingPtrList =
         (ARFieldMappingStruct **) calloc(value->numItems, sizeof(ARFieldMappingStruct *));
      if (value->mappingPtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have mapping for item? (T): ", TRUE))
            {
               tempPtr = (ARFieldMappingStruct *) calloc(1, sizeof(ARFieldMappingStruct));
               if (tempPtr != NULL)
               {
                  GetARFieldMapping(tempPtr);
                  value->mappingPtrList[i] = tempPtr;
               } 
            }
            else
               value->mappingPtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                           GetARUnsignedIntList                            */
/*                                                                           */
/*****************************************************************************/

void GetARUnsignedIntList(value)
ARUnsignedIntList  *value;      /* OUT; loaded with value for structure */

{
   unsigned int   i;          /* working index */

   value->numItems = GetUnsignedInt("   Number of unsigned ints (0): ", 0);
   if (value->numItems == 0)
      value->intList = NULL;
   else
   {
      value->intList =
         (unsigned int *) calloc(value->numItems, sizeof(unsigned int));
      if (value->intList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            value->intList[i] = GetUnsignedInt("Unsigned int (0): ", 0);
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARUnsignedIntPtrList                          */
/*                                                                           */
/*****************************************************************************/

void GetARUnsignedIntPtrList(value)
ARUnsignedIntPtrList  *value;      /* OUT; loaded with value for structure */

{
   unsigned int   i;          /* working index */
   unsigned int  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of unsigned ints (0): ", 0);
   if (value->numItems == 0)
      value->intPtrList = NULL;
   else
   {
      value->intPtrList =
         (unsigned int **) calloc(value->numItems, sizeof(unsigned int *));
      if (value->intPtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have unsigned int for item? (T): ", TRUE))
            {
               tempPtr = (unsigned int *) malloc(sizeof(unsigned int));
               if (tempPtr != NULL)
               {
                  *tempPtr = GetUnsignedInt("         Unsigned int (0): ", 0);
                  value->intPtrList[i] = tempPtr;
               } 
            }
            else
               value->intPtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        GetARPermissionListPtrList                         */
/*                                                                           */
/*****************************************************************************/

void GetARPermissionListPtrList(
ARBoolean                 fieldFlag,  /* IN; TRUE - field; FALSE - schema */
ARPermissionListPtrList  *value       /* OUT; loaded with value for structure */
)

{
   unsigned int       i;          /* working index */
   ARPermissionList  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of permission lists (0): ", 0);
   if (value->numItems == 0)
      value->permissionListPtrList = NULL;
   else
   {
      value->permissionListPtrList =
         (ARPermissionList **) calloc(value->numItems, sizeof(ARPermissionList *));
      if (value->permissionListPtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have permission list for item? (T): ", TRUE))
            {
               tempPtr = (ARPermissionList *) calloc(1, sizeof(ARPermissionList));
               if (tempPtr != NULL)
               {
                  GetARPermissionList(fieldFlag, tempPtr);
                  value->permissionListPtrList[i] = tempPtr;
               } 
            }
            else
               value->permissionListPtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARFieldLimitPtrList                           */
/*                                                                           */
/*****************************************************************************/

void GetARFieldLimitPtrList(
ARUnsignedIntList    *dataTypeList,   /* IN; datatype to get limits for */ /* NULL OK */
ARFieldLimitPtrList  *value           /* OUT; loaded with value for structure */
)

{
   unsigned int         dataType;   /* working variable */
   unsigned int         i;          /* working index */
   ARFieldLimitStruct  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of limit structures (0): ", 0);
   if (value->numItems == 0)
      value->fieldLimitPtrList = NULL;
   else
   {
      value->fieldLimitPtrList =
         (ARFieldLimitStruct **) calloc(value->numItems, sizeof(ARFieldLimitStruct *));
      if (value->fieldLimitPtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have field limit for item? (T): ", TRUE))
            {
               tempPtr = (ARFieldLimitStruct *) calloc(1, sizeof(ARFieldLimitStruct));
               if (tempPtr != NULL)
               {
                  if (dataTypeList == NULL)
                     dataType = GetARDataType();
                  else
                     dataType = dataTypeList->intList[i];
                  GetARFieldLimitStruct((int) dataType, tempPtr);
                  value->fieldLimitPtrList[i] = tempPtr;
               } 
            }
            else
               value->fieldLimitPtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARDisplayInstanceListPtrList                      */
/*                                                                           */
/*****************************************************************************/

void GetARDisplayInstanceListPtrList(
ARDisplayInstanceListPtrList  *value    /* OUT; loaded with value for structure */
)

{
   unsigned int            i;          /* working index */
   ARDisplayInstanceList  *tempPtr;    /* working pointer */

   value->numItems = GetUnsignedInt("   Number of display instance lists (0): ", 0);
   if (value->numItems == 0)
      value->dInstanceListPtrList = NULL;
   else
   {
      value->dInstanceListPtrList =
         (ARDisplayInstanceList **) calloc(value->numItems, sizeof(ARDisplayInstanceList *));
      if (value->dInstanceListPtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have display instance list for item? (T): ", TRUE))
            {
               tempPtr = (ARDisplayInstanceList *) calloc(1, sizeof(ARDisplayInstanceList));
               if (tempPtr != NULL)
               {
                  GetARDisplayInstanceList(tempPtr, FALSE);
                  value->dInstanceListPtrList[i] = tempPtr;
               } 
            }
            else
               value->dInstanceListPtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetARTextStringList                            */
/*                                                                           */
/*****************************************************************************/

void GetARTextStringList(
ARTextStringList   *value           /* OUT; loaded with value for structure */
)

{
   unsigned int   i;          /* working index */

   value->numItems = GetUnsignedInt("   Number of text strings (0): ", 0);
   if (value->numItems == 0)
      value->stringList = NULL;
   else
   {
      value->stringList = (char **) calloc(value->numItems, sizeof(char *));
      if (value->stringList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have text string for item? (T): ", TRUE))
            {
               GetMallocChar("      Text (): ", "", &value->stringList[i]);
            }
            else
               value->stringList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARAccessNamePtrList                           */
/*                                                                           */
/*****************************************************************************/

void GetARAccessNamePtrList(value, indent)
ARAccessNamePtrList  *value;         /* OUT; loaded with value for structure */
char                 *indent;

{
   char           buffer[100]; /* prompt text */
   unsigned int   i;           /* working index */

   sprintf(buffer, "%s%s", indent, "   Number of access names (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->namePtrList = NULL;
   else
   {
      value->namePtrList = (char **) calloc(value->numItems, sizeof(ARAccessNameType *));
      if (value->namePtrList != NULL)
      {
         for (i = 0; i < value->numItems; i++)
         {
            if (GetARBoolean("      Have access name for item? (T): ", TRUE))
            {
               GetMallocChar("      Access Name (): ", "", &value->namePtrList[i]);
            }
            else
               value->namePtrList[i] = NULL;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                              WFDFillBreakPt                               */
/*                                                                           */
/*****************************************************************************/

void WFDFillBreakPt(
ARWfdRmtBreakpoint  *bp,
int                  local
)
{
   if (! local)
      bp->id = GetUnsignedInt("  BP location -\n  ID ? (0): ", 0);
   GetMallocChar("  Filter: ", "", (char **) &bp->filter);
   GetMallocChar("  Schema (*): ", "*", (char **) &bp->schema);
   bp->stage = WFD_BEFORE_QUAL + GetUnsignedInt("  Stage PreQual/Phase1/Phase2/Phase3/Escl/CMDB (0-5) (1): ", 1);
   if ((bp->stage < WFD_BEFORE_QUAL) || (bp->stage > WFD_BEFORE_CMDB))
   {
      free(bp->filter);
      free(bp->schema);
      printf("Invalid stage %d\n", bp->stage);
      return;
   }
   bp->elsePath = GetARBoolean("  Else path? (F): ", FALSE);
   bp->actioNo = GetUnsignedInt("  Action number (0): ", 0);
   bp->disable = GetARBoolean("  Disabled (F): ", FALSE);
   bp->passcount = GetUnsignedInt("  Passcount ? (0): ", 0);
   /* Since the qualifier isn't processed on the server yet,   */
   /* Don't ask for it                                         */
   /*****
   if ((! local) && (GetARBoolean("Add BP qualifier? (F): ", FALSE)))
   {
      if (GetARBoolean("Add BP qualifier? (F): ", FALSE))
         GetARQualifierStruct(bp->bpQualifier);
   }
   else
   ******/
   {
      bp->bpQualifier = NULL;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                       GetARMultiSchemaQueryFromList                       */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaQueryFromList(value, recursiveFlag, indent)
ARMultiSchemaFuncQueryFromList  *value;         /* OUT; loaded with value for buffer */
ARBoolean                        recursiveFlag; /* IN; is this source for a recursive query */ 
char                            *indent;

{
   char                               buffer[128];  /* prompt text */
   unsigned int                       i;            /* working index */
   ARMultiSchemaFuncQueryFromStruct  *tempPtr;      /* working pointer */

   sprintf(buffer, "%s%s", indent, "Number of query sources (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);
   if (value->numItems == 0)
      value->listPtr = NULL;
   else
   {
      value->listPtr =
         (ARMultiSchemaFuncQueryFromStruct *) malloc(sizeof(ARMultiSchemaFuncQueryFromStruct) *
                                                     value->numItems);
      if (value->listPtr != NULL)
      {
         tempPtr = value->listPtr;
         for (i = 0; i < value->numItems; i++)
         {
            GetARMultiSchemaQueryFromStruct(tempPtr, recursiveFlag, 
                                            (i == 0) ? TRUE : FALSE,
                                            indent);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARMultiSchemaQueryFromStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaQueryFromStruct(value, recursiveFlag, joinFlag, indent)
ARMultiSchemaFuncQueryFromStruct  *value;         /* OUT; loaded with value for buffer */
ARBoolean                          recursiveFlag; /* is this source for a recursive query */
ARBoolean                          joinFlag;      /* prompt for a join info */
char                              *indent;

{      
   char   buffer[AR_MAX_NAME_SIZE + 50];  /* prompt text */
   char   qualifierIndent[64];            /* prompt text */
   
   DriverPrintPrompt("%s%s\n", indent, "   Query source:");
   if (!recursiveFlag)
   {
      sprintf(buffer, "%s%s", indent, 
              "      Type: Schema name, Nested, Recursive (0 - 2) (0): ");
      value->type = GetUnsignedInt(buffer, 0);
   }
   else
      value->type = AR_MULTI_SCHEMA_SCHEMA_NAME;

   switch (value->type)
   {
      case AR_MULTI_SCHEMA_SCHEMA_NAME:
         sprintf(buffer, "%s%s", indent, "      Schema: ");
         strcpy(value->u.schemaName, GetChar(buffer, ""));
         sprintf(buffer, "%s%s(%s):", indent, "      Schema alias ", value->u.schemaName);
         strncpy(value->queryFromAlias, GetChar(buffer, value->u.schemaName), AR_MAX_NAME_SIZE);
         value->queryFromAlias[AR_MAX_NAME_SIZE] = '\0';
         break;
      case AR_MULTI_SCHEMA_NESTED_QUERY:
         sprintf(buffer, "%s%s", indent, "      Nested query alias: ");
         strncpy(value->queryFromAlias, GetChar(buffer, ""), AR_MAX_NAME_SIZE);
         value->queryFromAlias[AR_MAX_NAME_SIZE] = '\0';

         value->u.nestedQuery = (ARMultiSchemaNestedFuncQueryStruct *) 
            malloc(sizeof(ARMultiSchemaNestedFuncQueryStruct));
         if (value->u.nestedQuery != NULL)
            GetARMultiSchemaNestedQueryStruct(value->u.nestedQuery);
         break;
      case AR_MULTI_SCHEMA_RECURSIVE_QUERY:
         sprintf(buffer, "%s%s", indent, "      Recursive query alias: ");
         strncpy(value->queryFromAlias, GetChar(buffer, ""), AR_MAX_NAME_SIZE);
         value->queryFromAlias[AR_MAX_NAME_SIZE] = '\0';

         value->u.recursiveQuery = (ARMultiSchemaRecursiveFuncQueryStruct *) 
            malloc(sizeof(ARMultiSchemaRecursiveFuncQueryStruct));
         if (value->u.recursiveQuery != NULL)
            GetARMultiSchemaRecursiveQueryStruct(value->u.recursiveQuery);
         break;                  
   }

   value->joinQual = NULL;
   value->joinType = 0;

   if (!joinFlag)
   {
      sprintf(buffer, "%s%s", indent, "      Join type: Inner, Left, Right (0 - 2) (0): ");
      value->joinType = GetUnsignedInt(buffer, 0);

      value->joinQual = (ARMultiSchemaQualifierStruct *)
                           malloc(sizeof(ARMultiSchemaQualifierStruct));
      if (value->joinQual != NULL)
      {
         sprintf(qualifierIndent, "%s%s", indent, "      ");
         GetARMultiSchemaQualifierStruct(value->joinQual, "Join qualifier struct",
                                         FALSE, qualifierIndent);
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARMultiSchemaNestedQueryStruct                    */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaNestedQueryStruct(value)
ARMultiSchemaNestedFuncQueryStruct  *value;  /* OUT; loaded with value for buffer */

{   
   GetARMultiSchemaQueryFromList(&value->queryFromList, FALSE, "      ");
   GetARMultiSchemaFieldIdList(&value->getListFuncs, "      ");

   value->qualifier = (ARMultiSchemaQualifierStruct *)
                            malloc(sizeof(ARMultiSchemaQualifierStruct));
   if (value->qualifier != NULL)
      GetARMultiSchemaQualifierStruct(value->qualifier, "Qualifier struct",
                                      FALSE, "      ");
}


/*****************************************************************************/
/*                                                                           */
/*                   GetARMultiSchemaRecursiveQueryStruct                    */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaRecursiveQueryStruct(value)
ARMultiSchemaRecursiveFuncQueryStruct  *value;  /* OUT; loaded with value for buffer */

{
   char   buffer[AR_MAX_NAME_SIZE + 35];  /* prompt text */   

   GetARMultiSchemaQueryFromList(&value->queryFromList, TRUE, "      ");

   if (value->queryFromList.numItems > 0 && value->queryFromList.listPtr != NULL)
   {
      sprintf(buffer, "      Recursive schema alias (%s): ",          
              value->queryFromList.listPtr[0].queryFromAlias);

      strncpy(value->recursiveSchemaAlias, 
              GetChar(buffer, value->queryFromList.listPtr[0].queryFromAlias),
              AR_MAX_NAME_SIZE);
   }
   else
      strncpy(value->recursiveSchemaAlias, 
              GetChar("      Recursive schema alias: ", ""), AR_MAX_NAME_SIZE);

   value->recursiveSchemaAlias[AR_MAX_NAME_SIZE] = '\0';

   GetARMultiSchemaFieldIdList(&value->getListFuncs, "      ");

   value->startQual = (ARMultiSchemaQualifierStruct *)
                        malloc(sizeof(ARMultiSchemaQualifierStruct));
   if (value->startQual != NULL)
      GetARMultiSchemaQualifierStruct(value->startQual, "Start qualifier struct",
                                      FALSE, "      ");

   value->recursionQual = (ARMultiSchemaQualifierStruct *)
                            malloc(sizeof(ARMultiSchemaQualifierStruct));
   if (value->recursionQual != NULL)
      GetARMultiSchemaQualifierStruct(value->recursionQual, "Recursion qualifier struct",
                                      TRUE, "      ");

   value->levelsToRetrieve = GetLong("      Levels to retrieve (0): ", 0L);
}


/*****************************************************************************/
/*                                                                           */
/*                       GetARMultiSchemaFieldIdStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFieldIdStruct(value, source, indent)
ARMultiSchemaFieldIdStruct  *value;   /* OUT; loaded with value for buffer */
char                        *source;  /* IN: field source to appear as default */
char                        *indent;
{
   char   buffer[AR_MAX_NAME_SIZE + 50];  /* prompt text */
   
   DriverPrintPrompt("%s%s\n", indent, "   Query source field:");

   sprintf(buffer, "%s%s", indent, "      Field id (0): ");
   value->fieldId = GetLong(buffer, 0L);

   sprintf(buffer, "%s%s(%s): ", indent, "      Field source alias ", source);
   strncpy(value->queryFromAlias, GetChar(buffer, source), AR_MAX_NAME_SIZE);
   value->queryFromAlias[AR_MAX_NAME_SIZE] = '\0';
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARMultiSchemaFieldFuncStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFieldFuncStruct(value, source, indent)
ARMultiSchemaFieldFuncStruct  *value;   /* OUT; loaded with value for buffer */
char                          *source;  /* IN: field source to appear as default */
char                          *indent;
{
   char   buffer[AR_MAX_NAME_SIZE + 50];  /* prompt text */
   
   DriverPrintPrompt("%s%s\n", indent, "   Query source field:");

   sprintf(buffer, "%s%s", indent, "      Field id (0): ");
   value->fieldId = GetLong(buffer, 0L);

   sprintf(buffer, "%s%s", indent, "      Func id (0): ");
   value->funcId = GetLong(buffer, 0L);

   sprintf(buffer, "%s%s(%s): ", indent, "      Field source alias ", source);
   strncpy(value->queryFromAlias, GetChar(buffer, source), AR_MAX_NAME_SIZE);
   value->queryFromAlias[AR_MAX_NAME_SIZE] = '\0';
}


/*****************************************************************************/
/*                                                                           */
/*                        GetARMultiSchemaFieldIdList                        */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFieldIdList(value, indent)
ARMultiSchemaFieldFuncList  *value;  /* OUT; loaded with value for buffer */
char                        *indent;
{
   char                           buffer[64];  /* prompt text */
   unsigned int                   i;           /* working index */
   ARMultiSchemaFieldFuncStruct  *tempPtr;     /* working pointer */

   sprintf(buffer, "%s%s", indent, "Number of query source fields (0): ");
   value->numItems = GetUnsignedInt(buffer, 0);

   if (value->numItems == 0)
      value->listPtr = NULL;
   else
   {
      value->listPtr =
         (ARMultiSchemaFieldFuncStruct *) malloc(sizeof(ARMultiSchemaFieldFuncStruct) *
                                                 value->numItems);
      if (value->listPtr != NULL)
      {
         tempPtr = value->listPtr;
         for (i = 0; i < value->numItems; i++)
         {
            GetARMultiSchemaFieldFuncStruct(tempPtr, 
                                            (i == 0) ? "" : value->listPtr[i -1].queryFromAlias,
                                            indent);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                          GetARValueStructIndent                           */
/*                                                                           */
/*****************************************************************************/

void GetARValueStructIndent(
ARValueStruct  *value,      /* OUT; loaded with value for buffer */
ARBoolean       imageFlag,  /* IN;  flag to retrieve image byte list from */
                            /*      a file instead of from tty input      */
char           *indent
)
{
   char   buffer[255]; /* prompt text */

   sprintf(buffer, "%s%s", indent,
"Datatype Null/Key/Int/Real/Char/Diary/Enum/Time/Bitmask/Byte/Decimal/attach/"
"currency/date/timeofday/join/trim/control/Table/Column/ulong/"
"coords/view/display (0-14, 30-34, 40-43) (0): ");

   value->dataType = GetUnsignedInt(buffer, 0);
   GetValue(value, imageFlag, indent);
}


/*****************************************************************************/
/*                                                                           */
/*                    GetARMultiSchemaFuncQualifierStruct                    */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFuncQualifierStruct(value, label, recursionQualFlag, indent)
ARMultiSchemaFuncQualifierStruct  *value;              /* OUT; loaded with value for buffer */
char                              *label;              /* IN; label to display in prompt */
ARBoolean                          recursionQualFlag;  /* IN; is this a recursion qualifier */
char                              *indent;  
{
   char   buffer[128];  /* prompt text */   
   
   DriverPrintPrompt("%s%s:\n", indent, label);

   /* initialize the structure since it does not get completely */
   /* filled in all cases below                                 */
   memset((char *) value, '\0', sizeof(ARMultiSchemaFuncQualifierStruct));

   sprintf(buffer, "%s%s", indent, 
           "None, And, Or, Not, Relop or From Field (0 - 5) (0): ");
   value->operation = GetUnsignedInt(buffer, 0);
   switch (value->operation)
   {
      case AR_COND_OP_NONE   :
         /* nothing to load if no qualifier */         
         break;
      case AR_COND_OP_AND    :
      case AR_COND_OP_OR     :
         value->u.andor.operandLeft =
            (struct ARMultiSchemaFuncQualifierStruct *) malloc(sizeof(ARMultiSchemaFuncQualifierStruct));
         if (value->u.andor.operandLeft)
            GetARMultiSchemaFuncQualifierStruct(value->u.andor.operandLeft, 
                                                "Qualifier struct", recursionQualFlag,
                                                indent);
         DriverPrintPrompt("%s%s", indent,
                           (value->operation == AR_COND_OP_AND) ? "AND\n" : "OR\n");
         value->u.andor.operandRight =
            (struct ARMultiSchemaFuncQualifierStruct *) malloc(sizeof(ARMultiSchemaFuncQualifierStruct));
         if (value->u.andor.operandRight)
            GetARMultiSchemaFuncQualifierStruct(value->u.andor.operandRight, 
                                                "Qualifier struct", recursionQualFlag,
                                                indent);
         break;
      case AR_COND_OP_NOT    :
         value->u.notQual =
            (struct ARMultiSchemaFuncQualifierStruct *) malloc(sizeof(ARMultiSchemaFuncQualifierStruct));
         if (value->u.notQual != NULL)
            GetARMultiSchemaFuncQualifierStruct(value->u.notQual, "Qualifier struct",
                                                recursionQualFlag, indent);
         break;
      case AR_COND_OP_REL_OP :
         value->u.relOp = (ARMultiSchemaFuncRelOpStruct *) malloc(sizeof(ARMultiSchemaFuncRelOpStruct));
         if (value->u.relOp != NULL)
            GetARMultiSchemaFuncRelOpStruct(value->u.relOp, recursionQualFlag, indent);
         break;
      case AR_COND_OP_FROM_FIELD:
            GetARMultiSchemaFieldFuncStruct(&value->u.fieldFunc, "", indent);
         break;
      default                :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARMultiSchemaQualifierStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaQualifierStruct(value, label, recursionQualFlag, indent)
ARMultiSchemaQualifierStruct  *value;              /* OUT; loaded with value for buffer */
char                          *label;              /* IN; label to display in prompt */
ARBoolean                      recursionQualFlag;  /* IN; is this a recursion qualifier */
char                          *indent;  
{
   char   buffer[128];  /* prompt text */   
   
   DriverPrintPrompt("%s%s:\n", indent, label);

   /* initialize the structure since it does not get completely */
   /* filled in all cases below                                 */
   memset((char *) value, '\0', sizeof(ARMultiSchemaQualifierStruct));

   sprintf(buffer, "%s%s", indent, 
           "None, And, Or, Not, Relop or From Field (0 - 5) (0): ");
   value->operation = GetUnsignedInt(buffer, 0);
   switch (value->operation)
   {
      case AR_COND_OP_NONE   :
         /* nothing to load if no qualifier */         
         break;
      case AR_COND_OP_AND    :
      case AR_COND_OP_OR     :
         value->u.andor.operandLeft =
            (struct ARMultiSchemaQualifierStruct *) malloc(sizeof(ARMultiSchemaQualifierStruct));
         if (value->u.andor.operandLeft)
            GetARMultiSchemaQualifierStruct(value->u.andor.operandLeft, 
                                            "Qualifier struct", recursionQualFlag,
                                            indent);
         DriverPrintPrompt("%s%s", indent,
                           (value->operation == AR_COND_OP_AND) ? "AND\n" : "OR\n");
         value->u.andor.operandRight =
            (struct ARMultiSchemaQualifierStruct *) malloc(sizeof(ARMultiSchemaQualifierStruct));
         if (value->u.andor.operandRight)
            GetARMultiSchemaQualifierStruct(value->u.andor.operandRight, 
                                            "Qualifier struct", recursionQualFlag,
                                            indent);
         break;
      case AR_COND_OP_NOT    :
         value->u.notQual =
            (struct ARMultiSchemaQualifierStruct *) malloc(sizeof(ARMultiSchemaQualifierStruct));
         if (value->u.notQual != NULL)
            GetARMultiSchemaQualifierStruct(value->u.notQual, "Qualifier struct",
                                            recursionQualFlag, indent);
         break;
      case AR_COND_OP_REL_OP :
         value->u.relOp = (ARMultiSchemaRelOpStruct *) malloc(sizeof(ARMultiSchemaRelOpStruct));
         if (value->u.relOp != NULL)
            GetARMultiSchemaRelOpStruct(value->u.relOp, recursionQualFlag, indent);
         break;
      case AR_COND_OP_FROM_FIELD:
            GetARMultiSchemaFieldIdStruct(&value->u.fieldId, "", indent);
         break;
      default                :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                        GetARMultiSchemaRelOpStruct                        */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaRelOpStruct(value, recursionQualFlag, indent)
ARMultiSchemaRelOpStruct  *value;              /* OUT; loaded with value for buffer */
ARBoolean                  recursionQualFlag;  /* IN; is this a recursion qualifier */
char                      *indent;

{
   char   buffer[128];  /* prompt text */

   sprintf(buffer, "%s%s", indent, 
           "Relational op code - EQ, GT, GE, LT, LE, NE, LIKE, IN (1 - 8): ");
   value->operation = GetUnsignedInt(buffer, 0);

   DriverPrintPrompt("%s%s", indent, "Left operand - ");
   GetARMultiSchemaFieldValueOrArithStruct(&value->operandLeft, 
                                           recursionQualFlag, indent);

   DriverPrintPrompt("%s%s", indent, "Right operand - ");
   GetARMultiSchemaFieldValueOrArithStruct(&value->operandRight, 
                                           recursionQualFlag, indent);
}


/*****************************************************************************/
/*                                                                           */
/*                      GetARMultiSchemaFuncRelOpStruct                      */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFuncRelOpStruct(value, recursionQualFlag, indent)
ARMultiSchemaFuncRelOpStruct  *value;              /* OUT; loaded with value for buffer */
ARBoolean                      recursionQualFlag;  /* IN; is this a recursion qualifier */
char                          *indent;

{
   char   buffer[128];  /* prompt text */

   sprintf(buffer, "%s%s", indent, 
           "Relational op code - EQ, GT, GE, LT, LE, NE, LIKE, IN (1 - 8): ");
   value->operation = GetUnsignedInt(buffer, 0);

   DriverPrintPrompt("%s%s", indent, "Left operand - ");
   GetARMultiSchemaFieldFuncValueOrArithStruct(&value->operandLeft, 
                                               recursionQualFlag, indent);

   DriverPrintPrompt("%s%s", indent, "Right operand - ");
   GetARMultiSchemaFieldFuncValueOrArithStruct(&value->operandRight, 
                                               recursionQualFlag, indent);
}


/*****************************************************************************/
/*                                                                           */
/*                    GetARMultiSchemaFieldValueOrArithStruct                */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFieldValueOrArithStruct(value, recursionQualFlag, indent)
ARMultiSchemaFieldValueOrArithStruct  *value;              /* OUT; value for buffer */
ARBoolean                              recursionQualFlag;  /* IN; is this a recursion qualifier */
char                                  *indent;

{
   char   buffer[255];  /* prompt text */

   value->tag = GetUnsignedInt(
"Fld,Value,Arith,Stat-Hist,Val-Set,Currency,Val-Set Query,Fld-Tran,Fld-DB,Currency-Tran,Currency-DB,Currency-Current,Fld-Current (1-7,50-56,99): ",
                               0);

   switch (value->tag)
   {
      case AR_FIELD        :
      case AR_FIELD_TRAN   :
      case AR_FIELD_DB     :
      case AR_FIELD_CURRENT:
         GetARMultiSchemaFieldIdStruct(&value->u.fieldId, 
                                       (recursionQualFlag == TRUE) ? AR_REC_QRY_PARENT_ALIAS : "",
                                       indent);
         break;
      case AR_VALUE        :
         GetARValueStructIndent(&value->u.value, FALSE, indent);
         break;
      case AR_ARITHMETIC   :
         value->u.arithOp = (ARMultiSchemaArithOpStruct *) malloc(sizeof(ARMultiSchemaArithOpStruct));
         if (value->u.arithOp != NULL)
            GetARMultiSchemaArithOpStruct((ARMultiSchemaArithOpStruct *) value->u.arithOp, recursionQualFlag, 
                                          indent);
         break;
      case AR_STAT_HISTORY :
         sprintf(buffer, "%s%s", indent, "Source alias: ");
         strcpy(value->u.statHistory.queryFromAlias, GetChar(buffer, ""));         

         sprintf(buffer, "%s%s", indent, "Enumerated value index (0): ");
         value->u.statHistory.enumVal = (unsigned long) GetLong(buffer, 0L);

         sprintf(buffer, "%s%s", indent, "User or Time field (1 or 2) (2): ");
         value->u.statHistory.userOrTime = GetUnsignedInt(buffer, 2);
         break;
      case AR_VALUE_SET    :
         GetARValueList(&value->u.valueSet);
         break;
      case AR_CURRENCY_FLD        :
      case AR_CURRENCY_FLD_TRAN   :
      case AR_CURRENCY_FLD_DB     :
      case AR_CURRENCY_FLD_CURRENT:
         value->u.currencyField = (ARMultiSchemaFuncCurrencyPartStruct *)
                                             malloc(sizeof(ARMultiSchemaFuncCurrencyPartStruct));
         if (value->u.currencyField != NULL)
         {
            GetARMultiSchemaFieldFuncStruct(&value->u.currencyField->fieldFunc, 
                                            (recursionQualFlag == TRUE) ? "parent" : "",
                                            indent);
            value->u.currencyField->partTag =
               GetUnsignedInt("Currency Part - Field,Value,Type,Date,Functional (0-4) (0): ", 0);
            if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            {
               strncpy(value->u.currencyField->currencyCode,
                       GetChar("Functional currency code: ", ""),
                       AR_CURRENCY_CODE_LEN);
               value->u.currencyField->currencyCode[AR_CURRENCY_CODE_LEN] = '\0';
            }
            else
               value->u.currencyField->currencyCode[0] = '\0';
         }
         break;
      case AR_VALUE_SET_QUERY        :
         value->u.valueSetQuery = (ARMultiSchemaValueSetFuncQueryStruct *)
                                             malloc(sizeof(ARMultiSchemaValueSetFuncQueryStruct));
         if (value->u.valueSetQuery != NULL)
         {
            GetARMultiSchemaQueryFromList(&value->u.valueSetQuery->queryFromList, FALSE, indent);
            GetARMultiSchemaFieldIdStruct(&value->u.valueSetQuery->fieldId, "", indent);
            value->u.valueSetQuery->qualifier = (ARMultiSchemaQualifierStruct *)
                                                   malloc(sizeof(ARMultiSchemaQualifierStruct));
            if (value->u.valueSetQuery->qualifier != NULL)
               GetARMultiSchemaQualifierStruct(value->u.valueSetQuery->qualifier, 
                                               "Qualifier struct", FALSE, indent);
         }
         break;
      default              :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                  GetARMultiSchemaFieldFuncValueOrArithStruct              */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFieldFuncValueOrArithStruct(value, recursionQualFlag, indent)
ARMultiSchemaFieldFuncValueOrArithStruct  *value;              /* OUT; value for buffer */
ARBoolean                                  recursionQualFlag;  /* IN; is this a recursion qualifier */
char                                      *indent;

{
   char   buffer[255];  /* prompt text */

   value->tag = GetUnsignedInt(
"Fld,Value,Arith,Stat-Hist,Val-Set,Currency,Val-Set Query,Fld-Tran,Fld-DB,Currency-Tran,Currency-DB,Currency-Current,Fld-Current (1-7,50-56,99): ",
                               0);

   switch (value->tag)
   {
      case AR_FIELD        :
      case AR_FIELD_TRAN   :
      case AR_FIELD_DB     :
      case AR_FIELD_CURRENT:
         GetARMultiSchemaFieldFuncStruct(&value->u.fieldFunc, 
                                         (recursionQualFlag == TRUE) ? AR_REC_QRY_PARENT_ALIAS : "",
                                         indent);
         break;
      case AR_VALUE        :
         GetARValueStructIndent(&value->u.value, FALSE, indent);
         break;
      case AR_ARITHMETIC   :
         value->u.arithOp = (ARMultiSchemaFuncArithOpStruct *) malloc(sizeof(ARMultiSchemaFuncArithOpStruct));
         if (value->u.arithOp != NULL)
            GetARMultiSchemaFuncArithOpStruct((ARMultiSchemaFuncArithOpStruct *) value->u.arithOp, recursionQualFlag, 
                                              indent);
         break;
      case AR_STAT_HISTORY :
         sprintf(buffer, "%s%s", indent, "Source alias: ");
         strcpy(value->u.statHistory.queryFromAlias, GetChar(buffer, ""));         

         sprintf(buffer, "%s%s", indent, "Enumerated value index (0): ");
         value->u.statHistory.enumVal = (unsigned long) GetLong(buffer, 0L);

         sprintf(buffer, "%s%s", indent, "User or Time field (1 or 2) (2): ");
         value->u.statHistory.userOrTime = GetUnsignedInt(buffer, 2);
         break;
      case AR_VALUE_SET    :
         GetARValueList(&value->u.valueSet);
         break;
      case AR_CURRENCY_FLD        :
      case AR_CURRENCY_FLD_TRAN   :
      case AR_CURRENCY_FLD_DB     :
      case AR_CURRENCY_FLD_CURRENT:
         value->u.currencyField = (ARMultiSchemaFuncCurrencyPartStruct *)
                                             malloc(sizeof(ARMultiSchemaFuncCurrencyPartStruct));
         if (value->u.currencyField != NULL)
         {
            GetARMultiSchemaFieldFuncStruct(&value->u.currencyField->fieldFunc, 
                                            (recursionQualFlag == TRUE) ? "parent" : "",
                                            indent);
            value->u.currencyField->partTag =
               GetUnsignedInt("Currency Part - Field,Value,Type,Date,Functional (0-4) (0): ", 0);
            if (value->u.currencyField->partTag == AR_CURRENCY_PART_FUNCTIONAL)
            {
               strncpy(value->u.currencyField->currencyCode,
                       GetChar("Functional currency code: ", ""),
                       AR_CURRENCY_CODE_LEN);
               value->u.currencyField->currencyCode[AR_CURRENCY_CODE_LEN] = '\0';
            }
            else
               value->u.currencyField->currencyCode[0] = '\0';
         }
         break;
      case AR_VALUE_SET_QUERY        :
         value->u.valueSetQuery = (ARMultiSchemaValueSetFuncQueryStruct *)
                                             malloc(sizeof(ARMultiSchemaValueSetFuncQueryStruct));
         if (value->u.valueSetQuery != NULL)
         {
            GetARMultiSchemaQueryFromList(&value->u.valueSetQuery->queryFromList, FALSE, indent);
            GetARMultiSchemaFieldIdStruct(&value->u.valueSetQuery->fieldId, "", indent);
            value->u.valueSetQuery->qualifier = (ARMultiSchemaQualifierStruct *)
                                                   malloc(sizeof(ARMultiSchemaQualifierStruct));
            if (value->u.valueSetQuery->qualifier != NULL)
               GetARMultiSchemaQualifierStruct(value->u.valueSetQuery->qualifier, 
                                               "Qualifier struct", FALSE, indent);
         }
         break;
      default              :
         break;
   }
}


/*****************************************************************************/
/*                                                                           */
/*                       GetARMultiSchemaArithOpStruct                       */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaArithOpStruct(value, recursionQualFlag, indent)
ARMultiSchemaArithOpStruct  *value;              /* OUT; loaded with value for buffer */
ARBoolean                    recursionQualFlag;  /* IN; is this a recursion qualifier */
char                        *indent;

{
   char   buffer[128];  /* prompt text */

   sprintf(buffer, "%s%s", indent, 
           "Arithmetic op code -- +, -, *, /, %, unary - (1 - 6): ");

   value->operation = GetUnsignedInt(buffer, 0);

   if (value->operation != AR_ARITH_OP_NEGATE)
   {
      DriverPrintPrompt("%s%s", indent, "Left operand - ");
      GetARMultiSchemaFieldValueOrArithStruct(&value->operandLeft, recursionQualFlag,
                                              indent);
   }

   DriverPrintPrompt("%s%s", indent, "Right operand - ");

   GetARMultiSchemaFieldValueOrArithStruct(&value->operandRight, recursionQualFlag,
                                           indent);
}


/*****************************************************************************/
/*                                                                           */
/*                     GetARMultiSchemaFuncArithOpStruct                     */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaFuncArithOpStruct(value, recursionQualFlag, indent)
ARMultiSchemaFuncArithOpStruct  *value;              /* OUT; loaded with value for buffer */
ARBoolean                        recursionQualFlag;  /* IN; is this a recursion qualifier */
char                            *indent;

{
   char   buffer[128];  /* prompt text */

   sprintf(buffer, "%s%s", indent, 
           "Arithmetic op code -- +, -, *, /, %, unary - (1 - 6): ");

   value->operation = GetUnsignedInt(buffer, 0);

   if (value->operation != AR_ARITH_OP_NEGATE)
   {
      DriverPrintPrompt("%s%s", indent, "Left operand - ");
      GetARMultiSchemaFieldFuncValueOrArithStruct(&value->operandLeft, 
                                                  recursionQualFlag, indent);
   }

   DriverPrintPrompt("%s%s", indent, "Right operand - ");

   GetARMultiSchemaFieldFuncValueOrArithStruct(&value->operandRight, 
                                               recursionQualFlag, indent);
}


/*****************************************************************************/
/*                                                                           */
/*                        GetARMultiSchemaSortStruct                         */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaSortStruct(value)
ARMultiSchemaSortStruct  *value;  /* OUT; loaded with value for buffer */

{
   DriverPrintPrompt("   Sort list entry\n");
   GetARMultiSchemaFieldIdStruct(&value->fieldId, "", "");   
   value->sortOrder =
      GetUnsignedInt("      Ascending or Descending (1 or 2) (1): ", 1);
}


/*****************************************************************************/
/*                                                                           */
/*                         GetARMultiSchemaSortList                          */
/*                                                                           */
/*****************************************************************************/

void GetARMultiSchemaSortList(value)
ARMultiSchemaSortList  *value;  /* OUT; loaded with value for buffer */

{
   int                       i;         /* working index */
   ARMultiSchemaSortStruct  *tempPtr;   /* working pointer */

   value->numItems = GetUnsignedInt("Number of sort fields (0): ", 0);
   if (value->numItems == 0)
      value->listPtr = NULL;
   else
   {
      value->listPtr = (ARMultiSchemaSortStruct *) 
         malloc(sizeof(ARMultiSchemaSortStruct) * value->numItems);
      if (value->listPtr != NULL)
      {
         tempPtr = value->listPtr;
         for (i = 0; i < value->numItems; i++)
         {
            GetARMultiSchemaSortStruct(tempPtr);
            tempPtr++;
         }
      }
   }
}


/*****************************************************************************/
/*                                                                           */
/*                            GetTaskAndOMLLabel                             */
/*                                                                           */
/*****************************************************************************/

void GetTaskAndOMLLabel(objectModificationLogLabel)
char **objectModificationLogLabel;  /* IN/OUT; object modification log label name text */

{
   GetMallocChar("Object Modification Log Label (): ", "", objectModificationLogLabel);
}
