package com.bmc.arsys.demo.javadriver;

import com.bmc.arsys.api.*;

class RowIterator implements IARRowIterator
{
   public RowIterator()
   {
   }

   public void iteratorCallback(Entry entry)
   {
      System.out.println("currentEntryId = " + entry.getEntryId());

      for (Value val : entry.values())
      {
         System.out.println(val.toString());
      }
   }

}