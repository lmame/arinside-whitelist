# Nmake macros for building Windows 32-Bit apps

CPU=i386

!include <ntwin32.mak>

#------------------------------------------------------------------------------
# model for compiling .C and .PC files
#
.SUFFIXES:
.SUFFIXES: .exe .obj .c

.c.obj:
    $(cc) $(cflags) $(cvarsdll) $(cdebug) -I. -I..\include -D_CONSOLE $*.c

#------------------------------------------------------------------------------


objects = api.obj get.obj main.obj util.obj print.obj
libs    = ..\lib\arapi7604_build002.lib

all: driver.exe

# Build the executable

driver.exe: $(objects)
    $(link) $(linkdebug) $(conlflags) -out:$@ $(objects) $(conlibsdll) $(libs) wsock32.lib user32.lib
