package com.bmc.arsys.demo.javadriver;

class WfdCommands
{
   public static final int UNKNOWN_COMMAND                    = -2;
   public static final int COMMAND_EXIT                       = -1;
   public static final int COMMAND_LOGIN                      =  0;
   public static final int COMMAND_INITIALIZATION             =  1;
   public static final int COMMAND_TERMINATION                =  2;
   public static final int COMMAND_SET_SERVER_PORT            =  3;
   public static final int COMMAND_WFD_STEP                   =  4;
   public static final int COMMAND_WFD_GO_TO_BP               =  5;
   public static final int COMMAND_WFD_GET_CURRENT_LOCATION   =  6;
   public static final int COMMAND_WFD_GET_FIELDVALUES        =  7;
   public static final int COMMAND_WFD_SET_FIELDVALUES        =  8;
   public static final int COMMAND_WFD_GET_DEBUG_MODE         =  9;
   public static final int COMMAND_WFD_SET_DEBUG_MODE         =  10;
   public static final int COMMAND_WFD_GET_FILTER_QUAL        =  11;
   public static final int COMMAND_WFD_SET_QUAL_RESULT        =  12;
   public static final int COMMAND_WFD_SET_BREAK_PT           =  13;
   public static final int COMMAND_WFD_CLR_BREAK_PT           =  14;
   public static final int COMMAND_WFD_LIST_BREAKPTS          =  15;
   public static final int COMMAND_WFD_CLR_BREAK_LST          =  16;
   public static final int COMMAND_WFD_TERMINATE_API          =  17;
   public static final int COMMAND_WFD_DISPLAY_STK_FRM        =  18;
   public static final int COMMAND_WFD_FINISH_API             =  19;
   public static final int COMMAND_WFD_LIST_STACK             =  20;
   public static final int COMMAND_WFD_RESTART                =  21;
   public static final int COMMAND_WFD_GET_KEYWORD            =  22;
   public static final int COMMAND_WFD_GET_USER_CONTEXT       =  23;
   public static final int COMMAND_WFD_RMT_SET_BKPT           =  24;
   public static final int COMMAND_WFD_RMT_CLR_BKPT           =  25;
   public static final int COMMAND_WFD_RMT_LIST_BKPT          =  26;
   public static final int COMMAND_WFD_RMT_CLR_BP_LIST        =  27;
   public static final int COMMAND_WFD_RUN_TO_RBP             =  28;
   public static final int COMMAND_WFD_SETALL_MODE            =  29;
   public static final int COMMAND_WFD_CLEAR_MODE             =  30;
   public static final int COMMAND_WFD_INFO                   =  31;

   
   static public  int getCommandCode(String command)
   {
      if(command.equals("log"))    return   COMMAND_LOGIN;
      if(command.equals("init"))   return   COMMAND_INITIALIZATION;
      if(command.equals("term"))   return   COMMAND_TERMINATION;
      if(command.equals("ssp"))    return   COMMAND_SET_SERVER_PORT;
      if(command.equals("s"))      return   COMMAND_WFD_STEP;
      if(command.equals("g"))      return   COMMAND_WFD_GO_TO_BP;
      if(command.equals("r"))      return   COMMAND_WFD_RUN_TO_RBP;
      if(command.equals("cl"))     return   COMMAND_WFD_GET_CURRENT_LOCATION;
      if(command.equals("gf"))     return   COMMAND_WFD_GET_FIELDVALUES;
      if(command.equals("sf"))     return   COMMAND_WFD_SET_FIELDVALUES;
      if(command.equals("gm"))     return   COMMAND_WFD_GET_DEBUG_MODE;
      if(command.equals("sm"))     return   COMMAND_WFD_SET_DEBUG_MODE;
      if(command.equals("gq"))     return   COMMAND_WFD_GET_FILTER_QUAL;
      if(command.equals("sq"))     return   COMMAND_WFD_SET_QUAL_RESULT;
      if(command.equals("bp"))     return   COMMAND_WFD_SET_BREAK_PT;
      if(command.equals("bc"))     return   COMMAND_WFD_CLR_BREAK_PT;
      if(command.equals("bl"))     return   COMMAND_WFD_LIST_BREAKPTS;
      if(command.equals("bx"))     return   COMMAND_WFD_CLR_BREAK_LST;
      if(command.equals("ta"))     return   COMMAND_WFD_TERMINATE_API;
      if(command.equals("st"))     return   COMMAND_WFD_DISPLAY_STK_FRM;
      if(command.equals("fi"))     return   COMMAND_WFD_FINISH_API;
      if(command.equals("li"))     return   COMMAND_WFD_LIST_STACK;
      if(command.equals("rst"))    return   COMMAND_WFD_RESTART;
      if(command.equals("kw"))     return   COMMAND_WFD_GET_KEYWORD;
      if(command.equals("uc"))     return   COMMAND_WFD_GET_USER_CONTEXT;
      if(command.equals("rbp"))    return   COMMAND_WFD_RMT_SET_BKPT;
      if(command.equals("rbc"))    return   COMMAND_WFD_RMT_CLR_BKPT;
      if(command.equals("rbl"))    return   COMMAND_WFD_RMT_LIST_BKPT;
      if(command.equals("rbx"))    return   COMMAND_WFD_RMT_CLR_BP_LIST;
      if(command.equals("sma"))    return   COMMAND_WFD_SETALL_MODE;
      if(command.equals("smc"))    return   COMMAND_WFD_CLEAR_MODE;
      if(command.equals("i"))      return   COMMAND_WFD_INFO;

      return UNKNOWN_COMMAND;
   }

}